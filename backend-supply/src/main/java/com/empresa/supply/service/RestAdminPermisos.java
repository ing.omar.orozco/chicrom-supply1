package com.empresa.supply.service;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.empresa.supply.mappers.administrador.MappersAdministradorPermisos;
import com.empresa.supply.modelo.administrador.Permisos;


@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
@RequestMapping(path = "/servicesRest/supply/administradorPermisos")
public class RestAdminPermisos {
	private MappersAdministradorPermisos mappersAdministradorPermisos;
	public RestAdminPermisos(MappersAdministradorPermisos mappersAdministradorPermisos) {
		super();
		this.mappersAdministradorPermisos = mappersAdministradorPermisos;
	}

	@RequestMapping(method = RequestMethod.POST, path = "/crearPermisos", consumes = "application/json", produces = "application/json")
	public @ResponseBody Permisos crearPermisos(@RequestBody Permisos permisos) throws Exception {
		int secuencia = mappersAdministradorPermisos.buscaConsecutivo("MODPE");
		mappersAdministradorPermisos.actualizaConsecutivo("MODPE",secuencia);
		this.mappersAdministradorPermisos.crearPermisos(secuencia,permisos.getUrl(),1);
		return mappersAdministradorPermisos.buscarPermiso(secuencia);
	}
	
	@GetMapping(path = "/buscarPermisos",  produces = "application/json")
	public List<Permisos> buscarPermisos() throws Exception {
		return this.mappersAdministradorPermisos.buscarPermisos();
	}
	
	@GetMapping(path = "/inactivarPermisos/{id}",  produces = "application/json")
	public void inactivarPermisos(@PathVariable int id) throws Exception {
		this.mappersAdministradorPermisos.inactivarPermiso(id);
	}
	
	@GetMapping(path = "/activarPermisos/{id}",  produces = "application/json")
	public Permisos activarPerfil(@PathVariable int id) throws Exception {
		this.mappersAdministradorPermisos.activarPermiso(id);
		return mappersAdministradorPermisos.buscarPermiso(id);
	}
	
	@GetMapping(path = "/editarPermisos/{id}/{nombre}",  produces = "application/json")
	public Permisos editarPerfil(@PathVariable int id,@PathVariable String nombre) throws Exception {
		this.mappersAdministradorPermisos.editarPermiso(id,nombre);
		return mappersAdministradorPermisos.buscarPermiso(id);
	}
}
