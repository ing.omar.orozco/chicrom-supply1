package com.empresa.supply.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.empresa.supply.mappers.bitacora.MappersBitacora;
import com.empresa.supply.modelo.bitacora.Bitacora;
import com.empresa.supply.modelo.bitacora.RegistroCausales;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
@RequestMapping(path = "/servicesRest/supply/bitacora")
public class RestBitacora {
	private MappersBitacora mappersBitacora;

	public RestBitacora(MappersBitacora mappersBitacora) {
		super();
		this.mappersBitacora = mappersBitacora;
	}
	
	@GetMapping(path = "/bitacora/{idPlanta}",  produces = "application/json")
	public List<Bitacora> bitacora(@PathVariable int idPlanta) throws Exception {
		List<Bitacora> lista = this.mappersBitacora.buscarBitacora(idPlanta);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date fecha = null;
		long minutosRestantes = 0;
		for(Bitacora b:lista) {
			b.setColor("");
			b.setTieneCausales("N");
			if(!b.getInfoInicio().isEmpty()) {
				fecha = simpleDateFormat.parse(b.getInfoInicio());
				long minutos=diferenciaMinutos(new Date(),fecha);
				if(minutos>b.getHoras()) {
					b.setColor("R");
					minutosRestantes = b.getHoras() - minutos ; 
					List<RegistroCausales> lc = mappersBitacora.buscarCausalesDeTiempoPorTicketEstado(b.getIdTicket(),b.getIdEstadoTicket());
					if(lc.isEmpty()) {
						b.setTieneCausales("N");
					}else {
						b.setTieneCausales("S");
					}
				}else {
					if(minutos<=b.getHoras()){
						if(minutos>(b.getHoras() - 10)) {
							b.setColor("A");
							minutosRestantes = b.getHoras() - minutos;
						}else {
							b.setColor("V");
							minutosRestantes = b.getHoras() - minutos;
						}
					}
				}
			}
			b.setMinutosRestante(String.valueOf(minutosRestantes));
		}//fin for
		return lista;
	}
	
	private long diferenciaMinutos(Date fechaMayor, Date fechaMenor) {
		long diferenciaEn_ms = fechaMayor.getTime() - fechaMenor.getTime();
		//long dias = diferenciaEn_ms / (1000 * 60 * 60 * 24);
		long minutos = diferenciaEn_ms / (1000 * 60);
		return (int) minutos;
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/insertarCausalesDeTiempo", consumes = "application/json", produces = "application/json")
	public @ResponseBody List<RegistroCausales> insertarCausalesDeTiempo(@RequestBody List<RegistroCausales> registroCausales) throws Exception {
		int idTicket = 0,idEstadoTicket = 0;
		for(RegistroCausales r:registroCausales) {
			idTicket = r.getIdTicket();idEstadoTicket = r.getIdEstadoTicket();
			mappersBitacora.insertarCausalesDeTiempo(r.getIdTicket(),r.getIdEstadoTicket(),r.getIdCausalTiempo(),r.getRetardo());
		}
		return this.mappersBitacora.buscarCausalesDeTiempoPorTicketEstado(idTicket,idEstadoTicket);
	}
	
	@GetMapping(path = "/buscarCausalesDeTiempoPor/{idTicket}/{idEstadoTicket}/{idCausalTiempo}",  produces = "application/json")
	public RegistroCausales buscarTipoTransporte(@PathVariable int idTicket,@PathVariable int idEstadoTicket,@PathVariable int idCausalTiempo) throws Exception {
		return this.mappersBitacora.buscaRegistroCausal(idTicket,idEstadoTicket,idCausalTiempo);
	}
	
	@GetMapping(path = "/buscarCausalesDeTiempoPorTicketEstado/{idTicket}/{idEstadoTicket}",  produces = "application/json")
	public List<RegistroCausales> buscarCausalesDeTiempoPorTicketEstado(@PathVariable int idTicket,@PathVariable int idEstadoTicket) throws Exception {
		return this.mappersBitacora.buscarCausalesDeTiempoPorTicketEstado(idTicket,idEstadoTicket);
	}
	
	@GetMapping(path = "/buscarTodaCausalesDeTiempo",  produces = "application/json")
	public List<RegistroCausales> buscarTodaCausalesDeTiempo() throws Exception {
		return this.mappersBitacora.buscarTodaCausalesDeTiempo();
	}
	
	
	@GetMapping(path = "/editarTiempoCausalesDeTiempo/{idTicket}/{idEstadoTicket}/{idCausalTiempo}/{tiempoMinutos}",  produces = "application/json")
	public RegistroCausales editarTiempoCausalesDeTiempo(@PathVariable int idTicket,@PathVariable int idEstadoTicket,@PathVariable int idCausalTiempo,@PathVariable int tiempoMinutos) throws Exception {
		 this.mappersBitacora.editarTiempoCausalesDeTiempo(idTicket,idEstadoTicket,idCausalTiempo,tiempoMinutos);
		 return this.mappersBitacora.buscaRegistroCausal(idTicket,idEstadoTicket,idCausalTiempo);
	}
	
	@RequestMapping(path = "/editarTiempoCausalesDeTiempo",  produces = "application/json")
	public List<RegistroCausales> editarCausalesDeTiempo(@RequestBody List<RegistroCausales> registroCausales) throws Exception {
		int idTicket = 0,idEstadoTicket = 0;
		for(RegistroCausales c:registroCausales) {
			idTicket = c.getIdTicket();idEstadoTicket = c.getIdEstadoTicket();
			this.mappersBitacora.editarTiempoCausalesDeTiempo(c.getIdTicket(),c.getIdEstadoTicket(),c.getIdCausalTiempo(),c.getRetardo());
		}
		 
		 return this.mappersBitacora.buscaRegistroCausal2(idTicket,idEstadoTicket);
	}

}
