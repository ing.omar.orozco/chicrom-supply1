package com.empresa.supply.service;

import java.util.Date;
import java.util.List;

//import org.hibernate.validator.internal.util.logging.Log_.logger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.empresa.supply.mappers.remisiones.MappersRemisiones;
import com.empresa.supply.modelo.remisiones.Remisiones;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
@RequestMapping(path = "/servicesRest/supply/remisiones")
public class RestRemisiones {
	private MappersRemisiones mappersRemisiones;
	private static final Logger logger = LoggerFactory.getLogger(RestGraficos.class);

	public RestRemisiones(MappersRemisiones mappersRemisiones) {
		super();
		this.mappersRemisiones = mappersRemisiones;
		// TODO Auto-generated constructor stub
	}
	
	@GetMapping(path = "/buscarRemisionPorId/{id}",  produces = "application/json")
	public int buscarRemisionPorId(@PathVariable int id) throws Exception {
		return this.mappersRemisiones.buscarRemisionPorId(id);
	}
	
	@GetMapping(path = "/buscarRemisionPorIdTicket/{idTicket}",  produces = "application/json")
	public List<Remisiones> buscarRemisionPorIdTicket(@PathVariable int idTicket) throws Exception {
		return this.mappersRemisiones.buscarRemisionPorIdTicket(idTicket);
	}
	
	@GetMapping(path = "/buscarRemisionPorIdRemision/{idRemision}",  produces = "application/json")
	public Remisiones buscarRemisionPorIdRemision(@PathVariable int idRemision) throws Exception {
		return this.mappersRemisiones.buscarRemisionPorIdRemision(idRemision);
	}
	
	
	@GetMapping(path = "/eliminarRemisionPorId/{id}",produces = "application/json")
	public void eliminarRemisionPorId(@PathVariable int id) throws Exception {
		this.mappersRemisiones.eliminarRemisionPorId(id);
	}
	
	@RequestMapping(method = RequestMethod.POST ,path = "/editarRemisionPorId", consumes = "application/json", produces = "application/json")
	public @ResponseBody Remisiones editarRemisionPorId(@RequestBody Remisiones remisiones) throws Exception {
		this.mappersRemisiones.editarRemisionPorId(remisiones.getNumeroRemision(),remisiones.getFoto(),remisiones.getEstado(),remisiones.getId());
		return this.mappersRemisiones.buscarRemisionPorIdRemision(remisiones.getId());
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/crearRemisiones", consumes = "application/json", produces = "application/json")
	public @ResponseBody Remisiones crearRemisiones(@RequestBody Remisiones remisiones) throws Exception {
		String isPrincipal = "P";
		logger.info("paso por aca");
		int existeRemision = this.mappersRemisiones.buscarRemisionPorId(remisiones.getIdTicket());
		if(existeRemision>0) {
			isPrincipal = "S";
		}
		logger.info("paso por aca2");
		int idRemision = this.mappersRemisiones.buscaConsecutivo("MODR");
		logger.info("paso por aca4");
		this.mappersRemisiones.actualizaConsecutivo("MODR",idRemision);
		logger.info("paso por aca3");
		mappersRemisiones.crearRemisiones(idRemision,remisiones.getNumeroRemision(),remisiones.getIdTicket(),remisiones.getFoto(),isPrincipal,remisiones.getEstado(),new Date());
		logger.info("paso por aca5");
		Remisiones r = this.mappersRemisiones.buscarRemisionPorIdRemision(idRemision);
		return r;
	}

}
