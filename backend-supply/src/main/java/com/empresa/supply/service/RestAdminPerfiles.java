package com.empresa.supply.service;

import java.util.List;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.empresa.supply.mappers.administrador.MappersAdministrador;
import com.empresa.supply.modelo.administrador.Perfiles;


@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
@RequestMapping(path = "/servicesRest/supply/administrador")
public class RestAdminPerfiles {
	private MappersAdministrador mappersAdministrador;
	public RestAdminPerfiles(MappersAdministrador mappersAdministrador) {
		super();
		this.mappersAdministrador = mappersAdministrador;
	}

	@RequestMapping(method = RequestMethod.POST, path = "/crearPerfiles", consumes = "application/json", produces = "application/json")
	public @ResponseBody Perfiles crearPerfiles(@RequestBody Perfiles perfiles) throws Exception {
		int secuencia = mappersAdministrador.buscaConsecutivo("MODP");
		mappersAdministrador.actualizaConsecutivo("MODP",secuencia);
		this.mappersAdministrador.crearPerfiles(secuencia,perfiles.getNombre(),1);
		return mappersAdministrador.buscarPerfil(secuencia);
	}
	
	@GetMapping(path = "/buscarPerfiles",  produces = "application/json")
	public List<Perfiles> buscarPerfiles() throws Exception {
		return this.mappersAdministrador.buscarPerfiles();
	}
	
	@GetMapping(path = "/buscarPerfilesActivos",  produces = "application/json")
	public List<Perfiles> buscarPerfilesActivos() throws Exception {
		return this.mappersAdministrador.buscarPerfilesActivos();
	}
	
	@GetMapping(path = "/inactivarPerfil/{id}",  produces = "application/json")
	public void inactivarPerfil(@PathVariable int id) throws Exception {
		this.mappersAdministrador.inactivarPerfil(id);
	}
	
	@GetMapping(path = "/activarPerfil/{id}",  produces = "application/json")
	public Perfiles activarPerfil(@PathVariable int id) throws Exception {
		this.mappersAdministrador.activarPerfil(id);
		return mappersAdministrador.buscarPerfil(id);
	}
	
	@GetMapping(path = "/editarPerfil/{id}/{nombre}",  produces = "application/json")
	public Perfiles editarPerfil(@PathVariable int id,@PathVariable String nombre) throws Exception {
		this.mappersAdministrador.editarPerfil(id,nombre);
		return mappersAdministrador.buscarPerfil(id);
	}
}
