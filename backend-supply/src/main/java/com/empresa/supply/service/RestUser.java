package com.empresa.supply.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.empresa.supply.mappers.VouUserMappers;
import com.empresa.supply.modelo.Auxiliares;
import com.empresa.supply.modelo.Dependencias;
import com.empresa.supply.modelo.Operaciones;
import com.empresa.supply.modelo.SubDependencia;
import com.empresa.supply.modelo.TicketFase2;
import com.empresa.supply.modelo.TicketInterfaz;
import com.empresa.supply.modelo.Tickets;
import com.empresa.supply.modelo.TipoTransporte;
import com.empresa.supply.modelo.Transportista;
import com.empresa.supply.modelo.UsuarioAutenticado;
import com.empresa.supply.modelo.VOUser;


@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
@RequestMapping(path = "/servicesREST/JR")
public class RestUser {
	
	private VouUserMappers vouUserMappers;
	

	public RestUser(VouUserMappers vouUserMappers) {
		super();
		this.vouUserMappers = vouUserMappers;
	}
	
	@GetMapping(path = "/findAll",  produces = "application/json")
	public List<VOUser> findAll() throws Exception {
		return this.vouUserMappers.findAll();
	}
	
	@GetMapping(path = "/findId/{id}",  produces = "application/json")
	public VOUser findId(@PathVariable int id) throws Exception {
		return this.vouUserMappers.findId(id);
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/validateUser", consumes = "application/json", produces = "application/json")
	public @ResponseBody List<UsuarioAutenticado> validateUser(@RequestBody VOUser user) throws Exception {
		List<UsuarioAutenticado> lista= vouUserMappers.autenticacionUsuario(user.getUsuario(),user.getClave());
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
		if(!lista.isEmpty()) {
			int id =0;
			int login = 0;
			for(UsuarioAutenticado t:lista) {
				id = t.getIdUsuario();
				login = t.getLogin();
				if (login==0) {
					vouUserMappers.initLogin(id);
				}
			}
			if (login==0) {
//				vouUserMappers.initLogin(id);
				return lista;
			}else {
				vouUserMappers.EndLogin(id);
				return null;
			}
		}
		return null;
	}
	
	@GetMapping(path = "/closelogin/{id}",  produces = "application/json")
	public @ResponseBody void CloseLogin(@PathVariable int id) throws Exception {
		this.vouUserMappers.closeLogin(id);
	}
	
	
	@RequestMapping(method = RequestMethod.POST, path = "/crearTickets", consumes = "application/json", produces = "application/json")
	public @ResponseBody Tickets crearTickets(@RequestBody Tickets ticket) throws Exception {
			int secuencia = vouUserMappers.buscaConsecutivo("MODT");
			vouUserMappers.actualizaConsecutivo("MODT",secuencia);
			int idPlanta = vouUserMappers.buscaIdPlantaUsuario(ticket.getIdUsuario());
			vouUserMappers.crearTicket(secuencia,ticket.getPlaca(),ticket.getIdOperacion(),ticket.getIdDependencia(),ticket.getIdDependenciaDestino(),
									   ticket.getIdTransportista(),ticket.getIdAuxiliares(),ticket.getIdTipoTransporte(),new Date(),ticket.getSubDependencia(),ticket.getMuelle(),idPlanta);
			return this.vouUserMappers.findIdTickets(secuencia);
	}
	
	@GetMapping(path = "/findIdTickets/{id}",  produces = "application/json")
	public Tickets findIdTickets(@PathVariable int id) throws Exception {
		return this.vouUserMappers.findIdTickets(id);
	}
	
	@GetMapping(path = "/findIdTicketsAll/{idPlanta}",  produces = "application/json")
	public List<Tickets> findIdTicketsAll(@PathVariable int idPlanta) throws Exception {
		List<Tickets> lista = this.vouUserMappers.findIdTicketsAll(idPlanta);
		for(Tickets t:lista) {
			t.setCerrarTicket("N");
			if(t.getTieneSello().equals("S")) {
				if(this.vouUserMappers.estadosTicketCompletados(t.getId())==5) {
					t.setCerrarTicket("S");
				}
			}
		}
		return lista;
	}
	
	
	@RequestMapping(method = RequestMethod.POST, path = "/editIdTickets", consumes = "application/json", produces = "application/json")
	public @ResponseBody Tickets editIdTickets(@RequestBody Tickets ticket) throws Exception {
		vouUserMappers.editIdTickets(ticket.getId(),ticket.getPlaca(),ticket.getIdOperacion(),ticket.getIdDependencia(),ticket.getIdDependenciaDestino(),ticket.getIdTransportista(),ticket.getIdAuxiliares(),ticket.getIdTipoTransporte(),ticket.getSubDependencia(),ticket.getMuelle());
		return  this.vouUserMappers.findIdTickets(ticket.getId());
	}
	
	@GetMapping(path = "/cerrarTickets/{id}",  produces = "application/json")
	public void cerrarTickets(@PathVariable int id) throws Exception {
			vouUserMappers.cerrarTicket(id);
	}
	
	@GetMapping(path = "/eliminaIdTickets/{id}",  produces = "application/json")
	public void eliminaIdTickets(@PathVariable int id) throws Exception {
			vouUserMappers.eliminaIdTickets(id);
	}
	
	@GetMapping(path = "/buscarOperaciones/{idPlanta}",  produces = "application/json")
	public List<Operaciones> buscarOperaciones(@PathVariable int idPlanta) throws Exception {
		return this.vouUserMappers.buscarOperaciones(idPlanta);
	}
	
	@GetMapping(path = "/buscarDependencia",  produces = "application/json")
	public List<Dependencias> buscarDependencia() throws Exception {
		return this.vouUserMappers.buscarDependencias();
	}
	
	@GetMapping(path = "/buscarTrasnportista",  produces = "application/json")
	public List<Transportista> buscarTransportista() throws Exception {
		return this.vouUserMappers.buscarTransportista();
	}
	
	@GetMapping(path = "/buscarAuxiliares/{idPlanta}",  produces = "application/json")
	public List<Auxiliares> buscarAuxiliares(@PathVariable int idPlanta) throws Exception {
		return this.vouUserMappers.buscarAuxiliares(idPlanta);
	}
	
	@GetMapping(path = "/buscarAuxiliaresPorTicket/{idTicket}",  produces = "application/json")
	public List<Auxiliares> buscarAuxiliaresPorTicket(@PathVariable int idTicket) throws Exception {
		return this.vouUserMappers.buscarAuxiliaresPorTicket(idTicket);
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/editarAuxiliar", consumes = "application/json", produces = "application/json")
	public @ResponseBody void editarAuxiliar(@RequestBody TicketFase2 ticket) throws Exception {
		for(Auxiliares idAuxiliar:ticket.getListaAuxiliares()) {
			vouUserMappers.actualizaAuxiliar(ticket.getEstadoAuxiliar(), ticket.getIdTicket(), idAuxiliar.getId());
		}
				
	}
	
	@GetMapping(path = "/buscarTipoTransporte",  produces = "application/json")
	public List<TipoTransporte> buscarTipoTransporte() throws Exception {
		return this.vouUserMappers.buscarTipoTransporte();
	}
	
	@GetMapping(path = "/buscaTicketsFase2PorId/{id}",  produces = "application/json")
	public int buscaTicketsFase2PorId(@PathVariable int id) throws Exception {
		return this.vouUserMappers.buscaTicketsFase2PorId(id);
	}
	
	@GetMapping(path = "/buscaTicketsFase2EstadoActivo/{id}",  produces = "application/json")
	public String buscaTicketsFase2EstadoActivo(@PathVariable int id) throws Exception {
		return this.vouUserMappers.buscaTicketsFase2EstadoActivo(id);
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/crearTicketsFase2", consumes = "application/json", produces = "application/json")
	public @ResponseBody void crearTicketFase2(@RequestBody TicketFase2 ticket) throws Exception {
		boolean actualizaRemisiones = false;
		if(ticket.getEstadoTicket()==3) {
			if(vouUserMappers.tipoOperacion(ticket.getIdTicket()).equals("D")){
				if(vouUserMappers.tieneSellos(ticket.getIdTicket()).equals("N")) {
					vouUserMappers.editRemisionesTickets(ticket.getIdTicket());
					
				}
			}
			
		}
		if(ticket.getEstadoTicket()==4) {
			if(vouUserMappers.tipoOperacion(ticket.getIdTicket()).equals("D")){
				if(vouUserMappers.tieneSellos(ticket.getIdTicket()).equals("N")) {
					vouUserMappers.editRemisionesTickets(ticket.getIdTicket());
					actualizaRemisiones = true;
				}
			}
			
		}
		if(actualizaRemisiones==false) {
			vouUserMappers.crearTicketFase2(new Date(),ticket.getEstadoTicket(),ticket.getNota(),ticket.getIdTicket(),ticket.getFoto());
		}
		if(ticket.getEstadoTicket()==3) {
			// SE DEBE INSERTAR LOS AUXILIARES POR TICKET
			for(Auxiliares idAuxiliar:ticket.getListaAuxiliares()) {
				int secuencia = vouUserMappers.buscaConsecutivo("AUXT");
				vouUserMappers.actualizaConsecutivo("AUXT",secuencia);
				vouUserMappers.insertaAuxiliaresTickets(secuencia, idAuxiliar.getId(), ticket.getIdTicket());
			}
			
		}
		if(ticket.getEstadoTicket()==5) {
			vouUserMappers.editRemisionesTickets(ticket.getIdTicket());
		}
		
	}
	
	@GetMapping(path = "/buscaTicketsFase2/{id}",  produces = "application/json")
	public List<TicketFase2> buscaTicketsFase2(@PathVariable int id) throws Exception {
		return this.vouUserMappers.buscaTicketsFase2(id);
	}
	
	@GetMapping(path = "/buscaTicketsFase2Interfaz/{id}",  produces = "application/json")
	public List<TicketInterfaz> buscaTicketsFase2Interfaz(@PathVariable int id) throws Exception {
		List<TicketInterfaz> lista =  this.vouUserMappers.buscaTicketsFase2Interfaz(id);
		List<TicketFase2> lista2 = vouUserMappers.buscaTicketsFase2All(id);
		List<Auxiliares> listaAuxiliares = vouUserMappers.buscaAuxiliaresPorTicket(id);
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		int y=0;
		if(lista2.isEmpty()) {
			lista.get(0).setEstado("A");
		}else {
			for(TicketFase2 i:lista2) {
				lista.get(y).setEstado(i.getEstado());
				lista.get(y).setEstadoTicket(i.getEstadoTicket());
				lista.get(y).setFoto(i.getFoto());
				lista.get(y).setFechaEstado(format.format(i.getFechaEstado()));
				lista.get(y).setNota(i.getNota());
				lista.get(y).setListaAuxiliares(listaAuxiliares);
				y++;				
			}
			if(y<=4) {
				lista.get(y).setEstado("A");
			}
			
		}
		
		return lista;
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/editarTicketsFase2", consumes = "application/json", produces = "application/json")
	public @ResponseBody List<TicketFase2> editarTicketsFase2(@RequestBody TicketFase2 ticket) throws Exception {
		vouUserMappers.editarTicketsFase2(ticket.getNota(),ticket.getIdTicket(),ticket.getFoto(),ticket.getEstadoTicket());
		vouUserMappers.borraAuxiliaresTickets(ticket.getIdTicket());
		for(Auxiliares idAuxiliar:ticket.getListaAuxiliares()) {
			int secuencia = vouUserMappers.buscaConsecutivo("AUXT");
			vouUserMappers.actualizaConsecutivo("AUXT",secuencia);
			vouUserMappers.insertaAuxiliaresTickets(secuencia, idAuxiliar.getId(), ticket.getIdTicket());
		}
		List<TicketFase2> lista2 = vouUserMappers.buscaTicketsFase2All(ticket.getIdTicket());
		List<Auxiliares> listaAuxiliares = vouUserMappers.buscaAuxiliaresPorTicket(ticket.getIdTicket());
		if(lista2!=null) {
			lista2.get(0).setListaAuxiliares(listaAuxiliares);
		}
		return lista2;
	}
	
	@GetMapping(path = "/buscarSubdependencias",  produces = "application/json")
	public List<SubDependencia> buscarSubdependencias() throws Exception {
		return this.vouUserMappers.buscaSubdependencias();
	}
}