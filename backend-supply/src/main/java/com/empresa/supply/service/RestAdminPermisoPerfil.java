package com.empresa.supply.service;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.empresa.supply.mappers.administrador.MappersAdministradorPermisoPerfil;
import com.empresa.supply.modelo.administrador.PermisoPerfil;


@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
@RequestMapping(path = "/servicesRest/supply/administradorPermisoPerfil")
public class RestAdminPermisoPerfil {
	private MappersAdministradorPermisoPerfil mappersAdministradorPermisoPerfil;
	public RestAdminPermisoPerfil(MappersAdministradorPermisoPerfil mappersAdministradorPermisoPerfil) {
		super();
		this.mappersAdministradorPermisoPerfil = mappersAdministradorPermisoPerfil;
	}

	@RequestMapping(method = RequestMethod.POST, path = "/crearPermisoPerfil", consumes = "application/json", produces = "application/json")
	public @ResponseBody List<PermisoPerfil> crearPermisoPerfil(@RequestBody List<PermisoPerfil> permisoPerfil) throws Exception {
		int secuencia = 0;
		for(PermisoPerfil p:permisoPerfil) {
			secuencia = mappersAdministradorPermisoPerfil.buscaConsecutivo("MODPP");
			mappersAdministradorPermisoPerfil.actualizaConsecutivo("MODPP",secuencia);
			if(mappersAdministradorPermisoPerfil.existePermisoPerfil(p.getIdPermiso(), p.getIdPerfil())>0) {
				this.mappersAdministradorPermisoPerfil.activarPermisoPerfil(p.getIdPermiso(),p.getIdPerfil());
			}else {
				this.mappersAdministradorPermisoPerfil.crearPermisoPerfil(secuencia,p.getIdPermiso(),p.getIdPerfil(),1);
			}
			
		}
		return this.mappersAdministradorPermisoPerfil.buscarPermisoPerfiles();
	}
	
	@GetMapping(path = "/buscarPermisoPerfiles",  produces = "application/json")
	public List<PermisoPerfil> buscarPermisoPerfiles() throws Exception {
		List<PermisoPerfil> listaPerfiles = this.mappersAdministradorPermisoPerfil.buscarPermisoPerfiles();
		if(listaPerfiles!=null) {
			for(PermisoPerfil p:listaPerfiles) {
				p.setListaPermisos(this.mappersAdministradorPermisoPerfil.buscarPermiso(p.getIdPerfil()));
			}
		}
		
		return listaPerfiles;
	}
	
	@GetMapping(path = "/inactivarPermisoPerfil/{idPermiso}/{idPerfil}",  produces = "application/json")
	public List<PermisoPerfil> inactivarPermisoPerfil(@PathVariable int idPermiso,@PathVariable int idPerfil) throws Exception {
		this.mappersAdministradorPermisoPerfil.inactivarPermisoPerfil(idPermiso,idPerfil);
		List<PermisoPerfil> listaPerfiles = mappersAdministradorPermisoPerfil.buscarPermisoPerfiles();
		if(listaPerfiles!=null) {
			for(PermisoPerfil p:listaPerfiles) {
				p.setListaPermisos(this.mappersAdministradorPermisoPerfil.buscarPermiso(p.getIdPerfil()));
			}
		}
		
		return listaPerfiles;
	}
	
	@GetMapping(path = "/activarPermisoPerfil/{idPermiso}/{idPerfil}",  produces = "application/json")
	public List<PermisoPerfil> activarPerfil(@PathVariable int idPermiso,@PathVariable int idPerfil) throws Exception {
		this.mappersAdministradorPermisoPerfil.activarPermisoPerfil(idPermiso,idPerfil);
		List<PermisoPerfil> listaPerfiles = mappersAdministradorPermisoPerfil.buscarPermisoPerfiles();
		if(listaPerfiles!=null) {
			for(PermisoPerfil p:listaPerfiles) {
				p.setListaPermisos(this.mappersAdministradorPermisoPerfil.buscarPermiso(p.getIdPerfil()));
			}
		}
		
		return listaPerfiles;
	}
	
	@GetMapping(path = "/editarPermisoPerfil/{id}/{idPermiso}/{idPerfil}",  produces = "application/json")
	public PermisoPerfil editarPerfil(@PathVariable int id,@PathVariable int idPermiso,@PathVariable int idPerfil) throws Exception {
		this.mappersAdministradorPermisoPerfil.editarPermisoPerfil(id,idPermiso,idPerfil);
		return mappersAdministradorPermisoPerfil.buscarPermisoPerfil(idPermiso,idPerfil);
	}
}
