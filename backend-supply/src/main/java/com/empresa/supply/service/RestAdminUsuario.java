package com.empresa.supply.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.empresa.supply.mappers.admin.usuario.VouAdminUserMappers;
import com.empresa.supply.modelo.admin.usuario.AdminUsuario;
import com.empresa.supply.modelo.admin.usuario.CargoUsuario;
import com.empresa.supply.modelo.admin.usuario.CiudadUsuario;
import com.empresa.supply.modelo.admin.usuario.ContratoUsuario;
import com.empresa.supply.modelo.admin.usuario.Plantas;
import com.empresa.supply.modelo.admin.usuario.TipoDocumento;
import com.empresa.supply.modelo.admin.usuario.UsuarioPerfil;
import com.empresa.supply.modelo.administrador.Perfiles;


@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
@RequestMapping(path = "/servicesREST/adminUsuario")
public class RestAdminUsuario {
	
	private VouAdminUserMappers vouAdminUserMappers;
	

	public RestAdminUsuario(VouAdminUserMappers vouAdminUserMappers) {
		super();
		this.vouAdminUserMappers = vouAdminUserMappers;
	}
	
	
	@GetMapping(path = "/eliminaUsuario/{id}",  produces = "application/json")
	public void eliminaUsuario(@PathVariable int id) throws Exception {
		this.vouAdminUserMappers.eliminaUsuario(id);
	}
	
	@GetMapping(path = "/activaUsuario/{id}",  produces = "application/json")
	public AdminUsuario activaUsuario(@PathVariable int id) throws Exception {
		this.vouAdminUserMappers.activaUsuario(id);
		return this.vouAdminUserMappers.buscaUsuarioPorId(id);
	}
	
	@GetMapping(path = "/inactivaUsuario/{id}",  produces = "application/json")
	public AdminUsuario inactivaUsuario(@PathVariable int id) throws Exception {
		this.vouAdminUserMappers.activaUsuario(id);
		return this.vouAdminUserMappers.buscaUsuarioPorId(id);
	}
	
	@GetMapping(path = "/buscarPlantas",  produces = "application/json")
	public List<Plantas> buscarPlantas() throws Exception {
		return this.vouAdminUserMappers.buscarPlantas();
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/usuarioPerfil", consumes = "application/json", produces = "application/json")
	public @ResponseBody UsuarioPerfil usuarioPerfil(@RequestBody UsuarioPerfil userPerfil) throws Exception {
		int secuencia = 0;
		for(Perfiles p:userPerfil.getListaPerfiles()) {
			secuencia = vouAdminUserMappers.buscaConsecutivo("MODUP");
			vouAdminUserMappers.actualizaConsecutivo("MODUP",secuencia);
			if(vouAdminUserMappers.existeUsuarioPerfil(userPerfil.getIdUsuario(), p.getId())>0) {
				vouAdminUserMappers.activarUsuarioPerfil(userPerfil.getIdUsuario(), p.getId());
			}else {
				vouAdminUserMappers.usuarioPerfil(secuencia,userPerfil.getIdUsuario(),p.getId(),1);
			}
			
		}
		AdminUsuario au= vouAdminUserMappers.buscaUsuarioPorId(userPerfil.getIdUsuario());
		UsuarioPerfil up = new UsuarioPerfil();
		up.setIdUsuario(au.getId());
		up.setNombreUsuario(au.getPrimerNombre()+" "+au.getSegundoNombre()+" "+au.getPrimerApellido()+" "+au.getSegundoApellido());
		up.setListaPerfiles(vouAdminUserMappers.buscarPerfiles());
		return up;
	}
	
	@GetMapping(path = "/obtenerTipoDocumentos",  produces = "application/json")
	public List<TipoDocumento> obtenerTipoDocumentos() throws Exception {
		return vouAdminUserMappers.buscaTipoDocumento();
	}
	
	@GetMapping(path = "/obtenerTodosLosUsuarioPerfil",  produces = "application/json")
	public List<UsuarioPerfil> obtenerTodosLosUsuarioPerfil() throws Exception {
		List<UsuarioPerfil> lista;
		lista = vouAdminUserMappers.buscaLosUsuariosConPerfiles();
		UsuarioPerfil up;
		List<UsuarioPerfil> listaUsuarioPerfil = new ArrayList<>();
		if(lista!=null) {
			for(UsuarioPerfil u:lista) {
				up = new UsuarioPerfil();
				AdminUsuario au= vouAdminUserMappers.buscaUsuarioPorId(u.getIdUsuario());
				up.setIdUsuario(au.getId());
				up.setNombreUsuario(au.getPrimerNombre()+" "+au.getSegundoNombre()+" "+au.getPrimerApellido()+" "+au.getSegundoApellido());
				up.setListaPerfiles(vouAdminUserMappers.buscarPerfilesPorId(u.getIdUsuario()));
				listaUsuarioPerfil.add(up);
			}
			
		}
		return listaUsuarioPerfil;
	}
	
	
	@GetMapping(path = "/activarUsuarioPerfil/{idUsuario}/{idPerfil}",  produces = "application/json")
	public void activarUsuarioPerfil(@PathVariable int idUsuario,@PathVariable int idPerfil) throws Exception {
		this.vouAdminUserMappers.activarUsuarioPerfil(idUsuario,idPerfil);
	}
	
	@GetMapping(path = "/inactivarUsuarioPerfil/{idUsuario}/{idPerfil}",  produces = "application/json")
	public void inactivarUsuarioPerfil(@PathVariable int idUsuario,@PathVariable int idPerfil) throws Exception {
		this.vouAdminUserMappers.inactivarUsuarioPerfil(idUsuario,idPerfil);
	}
	
	
	@RequestMapping(method = RequestMethod.POST, path = "/crearUsuario", consumes = "application/json", produces = "application/json")
	public @ResponseBody AdminUsuario crearUsuario(@RequestBody AdminUsuario user) throws Exception {
		int secuencia = vouAdminUserMappers.buscaConsecutivo("MODU");
		vouAdminUserMappers.actualizaConsecutivo("MODU",secuencia);
		vouAdminUserMappers.crearUsuario(secuencia,user.getTipoDocumento(),user.getDocumento(), user.getPrimerApellido(), user.getSegundoApellido(), user.getPrimerNombre(), user.getSegundoNombre(), user.getIdContratoUsuario(), user.getIdCargoOcupacion(), user.getIdCiudadUsuario(), user.getUsuario(), user.getClave(), user.getEstado(), user.getIdPlanta());
		return this.vouAdminUserMappers.buscaUsuarioPorId(user.getId());
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/editarUsuario", consumes = "application/json", produces = "application/json")
	public @ResponseBody AdminUsuario editarUsuario(@RequestBody AdminUsuario user) throws Exception {
		vouAdminUserMappers.editarUsuario(user.getId(),user.getTipoDocumento(),user.getDocumento(), user.getPrimerApellido(), user.getSegundoApellido(), user.getPrimerNombre(), user.getSegundoNombre(), user.getIdContratoUsuario(), user.getIdCargoOcupacion(), user.getIdCiudadUsuario(), user.getUsuario(), user.getClave(), user.getEstado(), user.getIdPlanta());
		return this.vouAdminUserMappers.buscaUsuarioPorId(user.getId());
	}
	
	@GetMapping(path = "/buscarTodosLosUsuarios",  produces = "application/json")
	public List<AdminUsuario> buscarTodosLosUsuarios() throws Exception {
		return this.vouAdminUserMappers.buscarTodosLosUsuarios();
	}
	
	@GetMapping(path = "/buscarTodosLosUsuariosActivos",  produces = "application/json")
	public List<AdminUsuario> buscarTodosLosUsuariosActivos() throws Exception {
		return this.vouAdminUserMappers.buscarTodosLosUsuariosActivos();
	}
	
	@GetMapping(path = "/buscarContratoUsuarios",  produces = "application/json")
	public List<ContratoUsuario> buscarContratoUsuarios() throws Exception {
		return this.vouAdminUserMappers.buscarContratoUsuarios();
	}
	
	@GetMapping(path = "/buscarCargoUsuarios",  produces = "application/json")
	public List<CargoUsuario> buscarCargoUsuarios() throws Exception {
		return this.vouAdminUserMappers.buscarCargoUsuarios();
	}
	
	@GetMapping(path = "/buscarCiudadUsuarios",  produces = "application/json")
	public List<CiudadUsuario> buscarCiudadUsuarios() throws Exception {
		return this.vouAdminUserMappers.buscarCiudadUsuarios();
	}
	
	
	/*
	@GetMapping(path = "/findId/{id}",  produces = "application/json")
	public VOUser findId(@PathVariable int id) throws Exception {
		return this.vouUserMappers.findId(id);
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/validateUser", consumes = "application/json", produces = "application/json")
	public @ResponseBody List<UsuarioAutenticado> validateUser(@RequestBody VOUser user) throws Exception {
		System.out.println("user.getUsuario()-> "+user.getUsuario());
		System.out.println("user.getClave()-> "+user.getClave());
		List<UsuarioAutenticado> lista= vouUserMappers.autenticacionUsuario(user.getUsuario(),user.getClave());
		if(!lista.isEmpty()) {
			return lista;
		}
		return null;
	}
	
	
	@RequestMapping(method = RequestMethod.POST, path = "/crearTickets", consumes = "application/json", produces = "application/json")
	public @ResponseBody Tickets crearTickets(@RequestBody Tickets ticket) throws Exception {
			int secuencia = vouUserMappers.buscaConsecutivo("MODT");
			vouUserMappers.actualizaConsecutivo("MODT",secuencia);
			int idPlanta = vouUserMappers.buscaIdPlantaUsuario(ticket.getIdUsuario());
			vouUserMappers.crearTicket(secuencia,ticket.getPlaca(),ticket.getIdOperacion(),ticket.getIdDependencia(),ticket.getIdDependenciaDestino(),
									   ticket.getIdTransportista(),ticket.getIdAuxiliares(),ticket.getIdTipoTransporte(),new Date(),ticket.getSubDependencia(),ticket.getMuelle(),idPlanta);
			return this.vouUserMappers.findIdTickets(secuencia);
	}
	
	@GetMapping(path = "/findIdTickets/{id}",  produces = "application/json")
	public Tickets findIdTickets(@PathVariable int id) throws Exception {
		return this.vouUserMappers.findIdTickets(id);
	}
	
	@GetMapping(path = "/findIdTicketsAll/{idPlanta}",  produces = "application/json")
	public List<Tickets> findIdTicketsAll(@PathVariable int idPlanta) throws Exception {
		List<Tickets> lista = this.vouUserMappers.findIdTicketsAll(idPlanta);
		for(Tickets t:lista) {
			t.setCerrarTicket("N");
			if(t.getTieneSello().equals("S")) {
				if(this.vouUserMappers.estadosTicketCompletados(t.getId())==5) {
					t.setCerrarTicket("S");
				}
			}
		}
		return lista;
	}
	
	
	@RequestMapping(method = RequestMethod.POST, path = "/editIdTickets", consumes = "application/json", produces = "application/json")
	public @ResponseBody Tickets editIdTickets(@RequestBody Tickets ticket) throws Exception {
		vouUserMappers.editIdTickets(ticket.getId(),ticket.getPlaca(),ticket.getIdOperacion(),ticket.getIdDependencia(),ticket.getIdDependenciaDestino(),ticket.getIdTransportista(),ticket.getIdAuxiliares(),ticket.getIdTipoTransporte(),ticket.getSubDependencia(),ticket.getMuelle());
		return  this.vouUserMappers.findIdTickets(ticket.getId());
	}
	
	@GetMapping(path = "/cerrarTickets/{id}",  produces = "application/json")
	public void cerrarTickets(@PathVariable int id) throws Exception {
			vouUserMappers.cerrarTicket(id);
	}
	
	@GetMapping(path = "/eliminaIdTickets/{id}",  produces = "application/json")
	public void eliminaIdTickets(@PathVariable int id) throws Exception {
			vouUserMappers.eliminaIdTickets(id);
	}
	
	@GetMapping(path = "/buscarOperaciones/{idPlanta}",  produces = "application/json")
	public List<Operaciones> buscarOperaciones(@PathVariable int idPlanta) throws Exception {
		return this.vouUserMappers.buscarOperaciones(idPlanta);
	}
	
	@GetMapping(path = "/buscarDependencia",  produces = "application/json")
	public List<Dependencias> buscarDependencia() throws Exception {
		return this.vouUserMappers.buscarDependencias();
	}
	
	@GetMapping(path = "/buscarTrasnportista",  produces = "application/json")
	public List<Transportista> buscarTransportista() throws Exception {
		return this.vouUserMappers.buscarTransportista();
	}
	
	@GetMapping(path = "/buscarAuxiliares/{idPlanta}",  produces = "application/json")
	public List<Auxiliares> buscarAuxiliares(@PathVariable int idPlanta) throws Exception {
		return this.vouUserMappers.buscarAuxiliares(idPlanta);
	}
	
	@GetMapping(path = "/buscarAuxiliaresPorTicket/{idTicket}",  produces = "application/json")
	public List<Auxiliares> buscarAuxiliaresPorTicket(@PathVariable int idTicket) throws Exception {
		return this.vouUserMappers.buscarAuxiliaresPorTicket(idTicket);
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/editarAuxiliar", consumes = "application/json", produces = "application/json")
	public @ResponseBody void editarAuxiliar(@RequestBody TicketFase2 ticket) throws Exception {
		for(Auxiliares idAuxiliar:ticket.getListaAuxiliares()) {
			vouUserMappers.actualizaAuxiliar(ticket.getEstadoAuxiliar(), ticket.getIdTicket(), idAuxiliar.getId());
		}
				
	}
	
	@GetMapping(path = "/buscarTipoTransporte",  produces = "application/json")
	public List<TipoTransporte> buscarTipoTransporte() throws Exception {
		return this.vouUserMappers.buscarTipoTransporte();
	}
	
	@GetMapping(path = "/buscaTicketsFase2PorId/{id}",  produces = "application/json")
	public int buscaTicketsFase2PorId(@PathVariable int id) throws Exception {
		return this.vouUserMappers.buscaTicketsFase2PorId(id);
	}
	
	@GetMapping(path = "/buscaTicketsFase2EstadoActivo/{id}",  produces = "application/json")
	public String buscaTicketsFase2EstadoActivo(@PathVariable int id) throws Exception {
		return this.vouUserMappers.buscaTicketsFase2EstadoActivo(id);
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/crearTicketsFase2", consumes = "application/json", produces = "application/json")
	public @ResponseBody void crearTicketFase2(@RequestBody TicketFase2 ticket) throws Exception {
		boolean actualizaRemisiones = false;
		if(ticket.getEstadoTicket()==3) {
			if(vouUserMappers.tipoOperacion(ticket.getIdTicket()).equals("D")){
				if(vouUserMappers.tieneSellos(ticket.getIdTicket()).equals("N")) {
					vouUserMappers.editRemisionesTickets(ticket.getIdTicket());
					
				}
			}
			
		}
		if(ticket.getEstadoTicket()==4) {
			if(vouUserMappers.tipoOperacion(ticket.getIdTicket()).equals("D")){
				if(vouUserMappers.tieneSellos(ticket.getIdTicket()).equals("N")) {
					vouUserMappers.editRemisionesTickets(ticket.getIdTicket());
					actualizaRemisiones = true;
				}
			}
			
		}
		if(actualizaRemisiones==false) {
			vouUserMappers.crearTicketFase2(new Date(),ticket.getEstadoTicket(),ticket.getNota(),ticket.getIdTicket(),ticket.getFoto());
		}
		if(ticket.getEstadoTicket()==3) {
			// SE DEBE INSERTAR LOS AUXILIARES POR TICKET
			for(Auxiliares idAuxiliar:ticket.getListaAuxiliares()) {
				int secuencia = vouUserMappers.buscaConsecutivo("AUXT");
				vouUserMappers.actualizaConsecutivo("AUXT",secuencia);
				vouUserMappers.insertaAuxiliaresTickets(secuencia, idAuxiliar.getId(), ticket.getIdTicket());
			}
			
		}
		if(ticket.getEstadoTicket()==5) {
			vouUserMappers.editRemisionesTickets(ticket.getIdTicket());
		}
		
	}
	
	@GetMapping(path = "/buscaTicketsFase2/{id}",  produces = "application/json")
	public List<TicketFase2> buscaTicketsFase2(@PathVariable int id) throws Exception {
		return this.vouUserMappers.buscaTicketsFase2(id);
	}
	
	@GetMapping(path = "/buscaTicketsFase2Interfaz/{id}",  produces = "application/json")
	public List<TicketInterfaz> buscaTicketsFase2Interfaz(@PathVariable int id) throws Exception {
		List<TicketInterfaz> lista =  this.vouUserMappers.buscaTicketsFase2Interfaz(id);
		List<TicketFase2> lista2 = vouUserMappers.buscaTicketsFase2All(id);
		List<Auxiliares> listaAuxiliares = vouUserMappers.buscaAuxiliaresPorTicket(id);
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		int y=0;
		if(lista2.isEmpty()) {
			lista.get(0).setEstado("A");
		}else {
			for(TicketFase2 i:lista2) {
				lista.get(y).setEstado(i.getEstado());
				lista.get(y).setEstadoTicket(i.getEstadoTicket());
				lista.get(y).setFoto(i.getFoto());
				lista.get(y).setFechaEstado(format.format(i.getFechaEstado()));
				lista.get(y).setNota(i.getNota());
				lista.get(y).setListaAuxiliares(listaAuxiliares);
				y++;				
			}
			if(y<=4) {
				lista.get(y).setEstado("A");
			}
			
		}
		
		return lista;
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/editarTicketsFase2", consumes = "application/json", produces = "application/json")
	public @ResponseBody List<TicketFase2> editarTicketsFase2(@RequestBody TicketFase2 ticket) throws Exception {
		vouUserMappers.editarTicketsFase2(ticket.getNota(),ticket.getIdTicket(),ticket.getFoto(),ticket.getEstadoTicket());
		vouUserMappers.borraAuxiliaresTickets(ticket.getIdTicket());
		for(Auxiliares idAuxiliar:ticket.getListaAuxiliares()) {
			int secuencia = vouUserMappers.buscaConsecutivo("AUXT");
			vouUserMappers.actualizaConsecutivo("AUXT",secuencia);
			vouUserMappers.insertaAuxiliaresTickets(secuencia, idAuxiliar.getId(), ticket.getIdTicket());
		}
		List<TicketFase2> lista2 = vouUserMappers.buscaTicketsFase2All(ticket.getIdTicket());
		List<Auxiliares> listaAuxiliares = vouUserMappers.buscaAuxiliaresPorTicket(ticket.getIdTicket());
		if(lista2!=null) {
			lista2.get(0).setListaAuxiliares(listaAuxiliares);
		}
		return lista2;
	}
	
	@GetMapping(path = "/buscarSubdependencias",  produces = "application/json")
	public List<SubDependencia> buscarSubdependencias() throws Exception {
		return this.vouUserMappers.buscaSubdependencias();
	}*/
}