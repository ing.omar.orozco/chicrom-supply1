package com.empresa.supply.service;

import java.util.Date;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.empresa.supply.mappers.sellos.MappersSellos;
import com.empresa.supply.modelo.sellos.Sellos;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
@RequestMapping(path = "/servicesRest/supply/sellos")
public class RestSellos {
	private MappersSellos mappersSellos;
	
	
	
	public RestSellos(MappersSellos mappersSellos) {
		super();
		this.mappersSellos = mappersSellos;
	}

	@RequestMapping(method = RequestMethod.POST, path = "/crearSellos", consumes = "application/json", produces = "application/json")
	public @ResponseBody List<Sellos> crearSellos(@RequestBody Sellos sellos) throws Exception {
		if(this.mappersSellos.existeRemisionPorId(sellos.getIdRemision())==0) {
			this.mappersSellos.crearSellos(sellos.getIdRemision(), sellos.getIdTicket(),sellos.getIdRemision(),sellos.getEstado(),new Date());
		}
		this.mappersSellos.crearDetalleSellos(sellos.getId(),sellos.getNombre(),sellos.getOperacion(),sellos.getSelloNumero(),sellos.getFoto());
		return this.mappersSellos.buscaListadoDeSellos(sellos.getId());
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/crearListaDeSellos", consumes = "application/json", produces = "application/json")
	public @ResponseBody List<Sellos> crearListaDeSellos(@RequestBody List<Sellos> sellos) throws Exception {
		int idRemision = 0;
		for(Sellos selloss: sellos) {
			idRemision = selloss.getIdRemision();
			
			if(this.mappersSellos.existeRemisionPorId(selloss.getIdRemision())==0) {
				this.mappersSellos.crearSellos(selloss.getIdRemision(), selloss.getIdTicket(),selloss.getIdRemision(),selloss.getEstado(),new Date());
			}
			this.mappersSellos.crearDetalleSellos(selloss.getIdRemision(),selloss.getNombre(),selloss.getOperacion(),selloss.getSelloNumero(),selloss.getFoto());
		}
		return this.mappersSellos.buscaListadoDeSellos(idRemision);
	}
	
	@GetMapping(path = "/eliminarSelloPorId/{id}",  produces = "application/json")
	public void eliminarSelloPorId(@PathVariable int id) throws Exception {
		 this.mappersSellos.eliminarSelloPorId(id);
	}
	
	@RequestMapping(method = RequestMethod.POST ,path = "/editarSelloPorId", consumes = "application/json", produces = "application/json")
	public @ResponseBody Sellos editarSello(@RequestBody Sellos sellos) throws Exception {
		this.mappersSellos.editarSello(sellos.getId(),sellos.getSelloNumero(),sellos.getNombre(),sellos.getOperacion(),sellos.getFoto());
		return this.mappersSellos.buscarSelloPorNumero(sellos.getId(), sellos.getSelloNumero());
	}
	
	@GetMapping(path = "/buscarSelloPorId/{id}",  produces = "application/json")
	public List<Sellos> buscarSelloPorId(@PathVariable int id) throws Exception {
		return this.mappersSellos.buscaListadoDeSellos(id);
	}

}
