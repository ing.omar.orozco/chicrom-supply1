package com.empresa.supply.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.empresa.supply.mappers.consulta.MappersConsulta;
import com.empresa.supply.modelo.consulta.CausalesTiempo;
import com.empresa.supply.modelo.consulta.CedulasAuxiliares;
import com.empresa.supply.modelo.consulta.ConsultaTicket;
import com.empresa.supply.modelo.consulta.SegmentoAnalisiTiempoPerdido;
import com.empresa.supply.modelo.consulta.SegmentoVolumen;
import com.empresa.supply.modelo.consulta.SegmetoTiempoAtencion;
import com.empresa.supply.modelo.consulta.TicketFase2;
import com.empresa.supply.modelo.consulta.VolumenTiempoOperacion;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
@RequestMapping(path = "/servicesRest/supply/consulta")
public class RestConsulta {
	private MappersConsulta mappersConsulta;

	public RestConsulta(MappersConsulta mappersConsulta) {
		super();
		this.mappersConsulta = mappersConsulta;
	}
	
	@GetMapping(path = "/segmentoVolumen/{fechaInicio}/{fechaFinal}",  produces = "application/json")
	public List<SegmentoVolumen> segmentoVolumen(@PathVariable String fechaInicio,@PathVariable String fechaFinal) throws Exception {
		List<SegmentoVolumen> lista = mappersConsulta.segmentoVolumen(fechaInicio,fechaFinal);
		List<TicketFase2> listaFase2;
		for(SegmentoVolumen x:lista) {
			listaFase2 = mappersConsulta.buscarTicketFase2(x.getId());
			if(listaFase2!=null) {
				if(!listaFase2.isEmpty()) {
					x.setTiempoOperacion(diferenciaMinutos(ParseFecha(listaFase2.get(3).getFechaEstado()),ParseFecha(listaFase2.get(2).getFechaEstado())));
				}
				
			}
			x.setProductividad(x.getNumeroPlaca()/(x.getTiempoOperacion()*24));
		}
		return lista;
	}
	
	@GetMapping(path = "/cerrados/{idPlanta}/{fechaInicial}/{fechaFinal}",  produces = "application/json")
	public List<ConsultaTicket> bitacora(@PathVariable int idPlanta,@PathVariable String fechaInicial,@PathVariable String fechaFinal) throws Exception {
		List<ConsultaTicket> lista = mappersConsulta.buscarTicketCerrado(idPlanta,fechaInicial,fechaFinal);
		String observaciones = null;
		int tiempoTotalCausalesTiempo = 0;
		double tiempoAtraso2Horas  =0;
		List<TicketFase2> listaFase2;
		List<CausalesTiempo> causales;
		List<CedulasAuxiliares> listaCedulas;
		for(ConsultaTicket x:lista) {
			tiempoTotalCausalesTiempo = 0;
			tiempoAtraso2Horas  =0;
			observaciones = "";
			listaFase2 = mappersConsulta.buscarTicketFase2(x.getId());
			causales = mappersConsulta.causalesConTiempos(x.getId());
			for(CausalesTiempo y:causales) {
				observaciones += ("Nombre Causal: "+y.getNombreCausal()+" Tiempo(Minutos): "+y.getTiempoCausal()+"; ");
				tiempoTotalCausalesTiempo += Integer.parseInt(y.getTiempoCausal());
				if(Integer.parseInt(y.getTiempoCausal())>120) {
					tiempoAtraso2Horas+=Integer.parseInt(y.getTiempoCausal());
				}
			}
			String fecha1 = null,fecha2 = null,fecha3 = null,fecha4 = null,fecha5 = null;
			for(TicketFase2 z:listaFase2) {
				switch (z.getEstadoTicket()) {
					case 1:
						fecha1 = z.getFechaEstado();
						x.setHoraArriboPlanta(z.getFechaEstado());
						break;
					case 2:
						fecha2 = z.getFechaEstado();
						x.setHoraIngresoPlanta(z.getFechaEstado());
						break;
					case 3:
						fecha3 = z.getFechaEstado();
						x.setHoraInicioOperacion(z.getFechaEstado());
						break;
					case 4:
						fecha4 = z.getFechaEstado();
						x.setHoraFinOperacion(z.getFechaEstado());
						break;
					case 5:
						fecha5 = z.getFechaEstado();
						x.setHoraVerificacion(z.getFechaEstado());
						break;
				}
			}//fin for
			x.setTiempoEsperaIngresoPlanta(diferenciaMinutos(ParseFecha(fecha2),ParseFecha(fecha1)));
			x.setTiempoEsperaInicioOperacion(diferenciaMinutos(ParseFecha(fecha3),ParseFecha(fecha2)));
			x.setTiempoOperacion(diferenciaMinutos(ParseFecha(fecha4),ParseFecha(fecha3)));
			x.setTiempoEsperaVerfificacion(diferenciaMinutos(ParseFecha(fecha5),ParseFecha(fecha4)));
			x.setTiepoTotalPlanta(x.getTiempoEsperaInicioOperacion()+x.getTiempoOperacion());
			x.setTiempoTotalAtrasoOperacion(tiempoAtraso2Horas);
			x.setTiempoAtencionSinAfectacion(x.getTiepoTotalPlanta()-x.getTiempoTotalAtrasoOperacion());
			x.setObservaciones(observaciones);
			listaCedulas = mappersConsulta.cedulaAuxiliares(x.getId());
			x.setTarifa(mappersConsulta.buscaTarifa(x.getIdOperacion(), x.getIdPlanta()));
			int indice = 1;
			for(CedulasAuxiliares v:listaCedulas) {
				switch (indice) {
				case 1:
					x.setCedula1(v.getCedulaAuxiliares());
					break;
				case 2:
					x.setCedula2(v.getCedulaAuxiliares());
					break;
				case 3:
					x.setCedula3(v.getCedulaAuxiliares());
					break;
				case 4:
					x.setCedula4(v.getCedulaAuxiliares());
					break;
				case 5:
					x.setCedula5(v.getCedulaAuxiliares());
					break;
				case 6:
					x.setCedula6(v.getCedulaAuxiliares());
					break;
				case 7:
					x.setCedula7(v.getCedulaAuxiliares());
					break;
				case 8:
					x.setCedula8(v.getCedulaAuxiliares());
					break;
				case 9:
					x.setCedula9(v.getCedulaAuxiliares());
					break;
				case 10:
					x.setCedula10(v.getCedulaAuxiliares());
					break;
				
				}
				indice++;
			}
			
		}//fin for
		return lista;
	}
	
	@GetMapping(path = "/segmentoComportamientoTiempoAtencion/{mes}",  produces = "application/json")
	public List<SegmetoTiempoAtencion> segmentoComportamientoTiempoAtencion(@PathVariable int mes) throws Exception {
		List<TicketFase2> listaFase2;
		int tiempoTotalCausalesTiempo = 0;
		double tiempoAtraso2Horas  =0;
		List<CausalesTiempo> causales;
		List<SegmetoTiempoAtencion> lista =  mappersConsulta.segmetoComportamientoTiempoAtencion(mes);
		for(SegmetoTiempoAtencion x:lista) {
			listaFase2 = mappersConsulta.buscarTicketFase2(x.getId());
			causales = mappersConsulta.causalesConTiempos(x.getId());
			tiempoTotalCausalesTiempo = 0;
			tiempoAtraso2Horas  =0;
			for(CausalesTiempo y:causales) {
				//observaciones += ("Nombre Causal: "+y.getNombreCausal()+" Tiempo(Minutos): "+y.getTiempoCausal()+"; ");
				tiempoTotalCausalesTiempo += Integer.parseInt(y.getTiempoCausal());
				if(Integer.parseInt(y.getTiempoCausal())>120) {
					tiempoAtraso2Horas+=Integer.parseInt(y.getTiempoCausal());
				}
			}
			String fecha1 = null,fecha2 = null,fecha3 = null,fecha4 = null,fecha5 = null;
			for(TicketFase2 z:listaFase2) {
				switch (z.getEstadoTicket()) {
					case 1:
						fecha1 = z.getFechaEstado();
						x.setHoraArriboPlanta(z.getFechaEstado());
						break;
					case 2:
						fecha2 = z.getFechaEstado();
						x.setHoraIngresoPlanta(z.getFechaEstado());
						break;
					case 3:
						fecha3 = z.getFechaEstado();
						x.setHoraInicioOperacion(z.getFechaEstado());
					break;
					case 4:
						fecha4 = z.getFechaEstado();
						x.setHoraFinOperacion(z.getFechaEstado());
					break;
					case 5:
						fecha5 = z.getFechaEstado();
						x.setHoraVerificacion(z.getFechaEstado());
					break;
				}
			}//fin for
			x.setTiempoEsperaInicioOperacion(diferenciaMinutos(ParseFecha(fecha3),ParseFecha(fecha2)));
			x.setTiempoOperacion(diferenciaMinutos(ParseFecha(fecha4),ParseFecha(fecha3)));
			x.setTiepoTotalPlanta(x.getTiempoEsperaInicioOperacion()+x.getTiempoOperacion());
			x.setTiempoTotalAtrasoOperacion(tiempoAtraso2Horas);
			x.setTiempoAtencionSinAfectacion(x.getTiepoTotalPlanta()-x.getTiempoTotalAtrasoOperacion());
		}
		return lista;
	}
	
	@GetMapping(path = "/segmentoAnalisisTiempoPerdido/{mes}/{dia}",  produces = "application/json")
	public List<SegmentoAnalisiTiempoPerdido> segmentoAnalisisTiempoPerdido(@PathVariable int mes,@PathVariable int dia) throws Exception {
		List<SegmentoAnalisiTiempoPerdido>  lista =  mappersConsulta.segmentoAnalisisTiempoPerdido(mes, dia);
		List<TicketFase2> listaFase2;
		double tiempoTotalCausalesTiempo = 0;
		double tiempoAtraso2Horas  =0;
		double tiempoAtraso2HorasInicioOperacion = 0;
		double cuentaCausales = 0;
		List<CausalesTiempo> causales;
		String nombreCausal = "";
		for(SegmentoAnalisiTiempoPerdido x:lista) {
			listaFase2 = mappersConsulta.buscarTicketFase2(x.getId());
			causales = mappersConsulta.causalesConTiempos(x.getId());
			tiempoTotalCausalesTiempo = 0;
			tiempoAtraso2Horas  =0;
			tiempoAtraso2HorasInicioOperacion = 0;
			cuentaCausales = 0;
			for(CausalesTiempo y:causales) {
				cuentaCausales+=1;
				nombreCausal+=y.getNombreCausal()+" ; ";
				tiempoTotalCausalesTiempo += Integer.parseInt(y.getTiempoCausal());
				if(Integer.parseInt(y.getTiempoCausal())>120) {
					tiempoAtraso2Horas+=Integer.parseInt(y.getTiempoCausal());
				}
				if(y.getIdEstadoTicket()==3) {
					tiempoAtraso2HorasInicioOperacion+=Integer.parseInt(y.getTiempoCausal());
				}
				
			}
			String fecha1 = null,fecha2 = null,fecha3 = null,fecha4 = null,fecha5 = null;
			for(TicketFase2 z:listaFase2) {
				switch (z.getEstadoTicket()) {
					case 1:
						fecha1 = z.getFechaEstado();
						x.setHoraArriboPlanta(z.getFechaEstado());
					break;
					case 2:
						fecha2 = z.getFechaEstado();
						x.setHoraIngresoPlanta(z.getFechaEstado());
						break;
					case 3:
						fecha3 = z.getFechaEstado();
						x.setHoraInicioOperacion(z.getFechaEstado());
						break;
					case 4:
						fecha4 = z.getFechaEstado();
						x.setHoraFinOperacion(z.getFechaEstado());
						break;
					case 5:
						fecha5 = z.getFechaEstado();
						x.setHoraVerificacion(z.getFechaEstado());
						break;
				}
			}
			x.setTiempoTotalAtrasoOperacion(tiempoAtraso2Horas);
			x.setTiempoTotalAtrasoInicioOperacion(tiempoAtraso2HorasInicioOperacion);
			x.setTiempoTotalPerdido(tiempoTotalCausalesTiempo);
			if(cuentaCausales!=0) {
				double porcentaje = (cuentaCausales/(tiempoTotalCausalesTiempo/60))*100;
				x.setPorcentajeHorasPerdidas(porcentaje);
			}else {
				x.setPorcentajeHorasPerdidas(0);
			}
			
			x.setNumeroPlacas(lista.size());
			x.setCausalTiempoPerdido(nombreCausal);
			if(causales.size()!=0) {
				x.setMediana(calcularMediana(causales));
			}else {
				x.setMediana(0);
			}
			
		}
		return lista;
	}
	
	private double calcularMediana(List<CausalesTiempo> causales) {
		List<CausalesTiempo> listaOrdenada = ordenarLista(causales);
		double calcularMediana;
		if(listaOrdenada.size() % 2 == 0) {
			double sumaMedios = Double.parseDouble(listaOrdenada.get(listaOrdenada.size()/2).getTiempoCausal()) + Double.parseDouble(listaOrdenada.get(listaOrdenada.size()/2 - 1).getTiempoCausal());
			calcularMediana = sumaMedios/2;
		}else {
			calcularMediana = Double.parseDouble(listaOrdenada.get(listaOrdenada.size()/2).getTiempoCausal());
		}
		return calcularMediana;
	}
	
	
	private List<CausalesTiempo> ordenarLista(List<CausalesTiempo> causales) {
		Collections.sort(causales, new Comparator<CausalesTiempo>() {

			@Override
			public int compare(CausalesTiempo o1, CausalesTiempo o2) {
				return new Integer(o1.getTiempoCausal()).compareTo(new Integer(o2.getTiempoCausal()));
			}
			
		});
		return causales;
	}
	
	
	private double diferenciaMinutos(Date fechaMayor, Date fechaMenor) {
		double diferenciaEn_ms = fechaMayor.getTime() - fechaMenor.getTime();
		//long dias = diferenciaEn_ms / (1000 * 60 * 60 * 24);
		double minutos = diferenciaEn_ms / (1000 * 60);
		return (double)minutos;
	}
	
	public static Date ParseFecha(String fecha)
    {
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date fechaDate = null;
        try {
            fechaDate = formato.parse(fecha);
        } 
        catch (ParseException ex) 
        {
            System.out.println("Error de parsing: "+ex);
        }
        return fechaDate;
    }
	
	
	@RequestMapping(method = RequestMethod.POST, path = "/volumenTiempoOperacion", consumes = "application/json", produces = "application/json")
	public @ResponseBody List<VolumenTiempoOperacion> validateUser(@RequestBody VolumenTiempoOperacion volumenTiempoOperacion) throws Exception {
		List<VolumenTiempoOperacion> lista = null;
		if(volumenTiempoOperacion.getTipoBusqueda().equals("R")) {
			lista= mappersConsulta.reporteVolumenTiempoOperacion(volumenTiempoOperacion.getFechaInicial(), volumenTiempoOperacion.getFechaFinal(), volumenTiempoOperacion.getTipoOperacion());
		}else {
			lista= mappersConsulta.reporteVolumenTiempoOperacionHoy(volumenTiempoOperacion.getTipoOperacion());
		}
		return lista;
	}

}
