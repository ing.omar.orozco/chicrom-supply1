package com.empresa.supply.service;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.empresa.supply.mappers.causal.tiempo.MappersCausalTiempo;
import com.empresa.supply.modelo.bitacora.RegistroCausales;
import com.empresa.supply.modelo.causal.tiempos.CausalTiempo;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
@RequestMapping(path = "/servicesRest/supply/causalTiempos")
public class RestCausalTiempos {
	private MappersCausalTiempo mappersCausalTiempo;

	public RestCausalTiempos(MappersCausalTiempo mappersCausalTiempo) {
		this.mappersCausalTiempo = mappersCausalTiempo;
	}
	
	@GetMapping(path = "/buscaCausalTiempo/{idTicket}/{idEstadoTicket}",  produces = "application/json")
	public List<CausalTiempo> buscaCausalTiempo(@PathVariable int idTicket,@PathVariable int idEstadoTicket) throws Exception {
		List<CausalTiempo> lista = this.mappersCausalTiempo.buscaCausalesTiempo();
		buscaTiempoDeCausales(lista,idTicket,idEstadoTicket);
		return lista;
	}
	
	private List<CausalTiempo> buscaTiempoDeCausales(List<CausalTiempo> causalTiempo,int idTicket,int idEstadoTicket){
		List<CausalTiempo> lista = null;
		if(causalTiempo!=null) {
			lista = this.mappersCausalTiempo.buscaTiempoDeCausales(idTicket, idEstadoTicket);
		}
		if(lista==null) {
			return causalTiempo;
		}else {
			for(CausalTiempo x:lista) {
				for(CausalTiempo y:causalTiempo) {
					if(x.getCausalDeTiempo()==y.getId()) {
						y.setMinutosRetardo(x.getMinutosRetardo());
						y.setIdTicket(x.getIdTicket());
						y.setIdEstadoTicket(x.getIdEstadoTicket());
					}
				}
			}
		}
		return causalTiempo;
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/editarTiempoCausales", consumes = "application/json", produces = "application/json")
	public @ResponseBody List<CausalTiempo> editarTiempoCausales(@RequestBody List<CausalTiempo> causalTiempo) throws Exception {
		int idTicket = 0,idEstadoTicket = 0;
		for(CausalTiempo x:causalTiempo) {
			idTicket = x.getIdTicket();idEstadoTicket = x.getIdEstadoTicket();
			if(this.mappersCausalTiempo.existeCausal(x.getIdTicket(), x.getIdEstadoTicket(),  x.getId())>0) {
				this.mappersCausalTiempo.editarTiempoCausales(x.getIdTicket(), x.getIdEstadoTicket(), x.getMinutosRetardo(), x.getId());
			}else {
				this.mappersCausalTiempo.insertarCausalesDeTiempo(x.getIdTicket(),x.getIdEstadoTicket(),x.getId(),x.getMinutosRetardo());
			}
			
		}
		List<CausalTiempo> lista = this.mappersCausalTiempo.buscaCausalesTiempo();
		buscaTiempoDeCausales(lista,idTicket,idEstadoTicket);
		return lista;
		
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/insertarCausalesDeTiempo", consumes = "application/json", produces = "application/json")
	public @ResponseBody List<CausalTiempo> insertarCausalesDeTiempo(@RequestBody List<RegistroCausales> registroCausales) throws Exception {
		int idTicket = 0,idEstadoTicket = 0;
		for(RegistroCausales r:registroCausales) {
			idTicket = r.getIdTicket();idEstadoTicket = r.getIdEstadoTicket();
			this.mappersCausalTiempo.insertarCausalesDeTiempo(r.getIdTicket(),r.getIdEstadoTicket(),r.getIdCausalTiempo(),r.getRetardo());
		}
		List<CausalTiempo> lista = this.mappersCausalTiempo.buscaCausalesTiempo();
		buscaTiempoDeCausales(lista,idTicket,idEstadoTicket);
		return lista;
	}
	

}
