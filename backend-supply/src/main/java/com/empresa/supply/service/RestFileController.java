package com.empresa.supply.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.empresa.supply.modelo.file.FileService;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
@RequestMapping(path = "/servicesRest/supply/file")
public class RestFileController {
	private FileService fileService;
	 
	@Autowired
	public void FileController(FileService fileService) {
		this.fileService = fileService;
	}
 
	//@PostMapping(value = "/subirFoto")
	@RequestMapping(method = RequestMethod.POST,headers=("content-type=multipart/*"), path = "/subirFoto")
	@ResponseStatus(HttpStatus.OK)
	public void handleFileUpload(@RequestParam("file") MultipartFile file) throws IOException {
		fileService.storeFile(file);
	}
}
