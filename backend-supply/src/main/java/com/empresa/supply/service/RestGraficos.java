package com.empresa.supply.service;

import java.text.DateFormat;
import java.text.ParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.empresa.supply.mappers.consulta.MappersConsulta;
import com.empresa.supply.mappers.graficos.MappersGraficos;
import com.empresa.supply.modelo.consulta.CausalesTiempo;
import com.empresa.supply.modelo.consulta.CedulasAuxiliares;
import com.empresa.supply.modelo.consulta.ConsultaTicket;
import com.empresa.supply.modelo.consulta.SegmentoAnalisiTiempoPerdido;
import com.empresa.supply.modelo.consulta.SegmentoVolumen;
import com.empresa.supply.modelo.consulta.SegmetoTiempoAtencion;
import com.empresa.supply.modelo.consulta.TicketFase2;
import com.empresa.supply.modelo.consulta.VolumenTiempoOperacion;
import com.empresa.supply.modelo.graficos.Grafica13;
import com.empresa.supply.modelo.graficos.Grafico15;
import com.empresa.supply.modelo.graficos.Grafico2;
import com.empresa.supply.modelo.graficos.Grafico3;
import com.empresa.supply.modelo.graficos.Graficos;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
@RequestMapping(path = "/servicesRest/supply/graficos")
public class RestGraficos {
	private MappersGraficos mappersGraficos;
	private MappersConsulta mappersConsulta;
	private static final Logger logger = LoggerFactory.getLogger(RestGraficos.class);

	public RestGraficos(MappersConsulta mappersConsulta,MappersGraficos mappersGraficos) {
		super();
		this.mappersConsulta = mappersConsulta;
		this.mappersGraficos = mappersGraficos;
	}
	
	@GetMapping(path = "/grafico1/{idPlanta}/{fechaInicial}/{fechaFinal}",  produces = "application/json")
	public List<Graficos> grafico1(@PathVariable int idPlanta,@PathVariable String fechaInicial,@PathVariable String fechaFinal) throws Exception {
		List<Graficos> lista = mappersGraficos.grafico1(idPlanta,fechaInicial,fechaFinal);
		return lista;
	}
	
	@GetMapping(path = "/grafico2/{idPlanta}/{fechaInicial}/{fechaFinal}",  produces = "application/json")
	public List<Grafico2> grafico2(@PathVariable int idPlanta,@PathVariable String fechaInicial,@PathVariable String fechaFinal) throws Exception {
		List<Grafico2> lista = mappersGraficos.grafico2(idPlanta,fechaInicial,fechaFinal);
		return lista;
	}
	
	@GetMapping(path = "/grafico3/{idPlanta}/{fechaInicial}/{fechaFinal}",  produces = "application/json")
	public List<Grafico3> grafico3(@PathVariable int idPlanta,@PathVariable String fechaInicial,@PathVariable String fechaFinal) throws Exception {
		List<Grafico3> lista = mappersGraficos.grafico3(idPlanta,fechaInicial,fechaFinal);
		String observaciones = null;
		int tiempoTotalCausalesTiempo = 0;
		double tiempoAtraso2Horas  =0;
		List<TicketFase2> listaFase2;
		List<CausalesTiempo> causales;
		String jornada =""; 
		String hora_inicial = "00:00:00";
		String hora_final = "06:00:00";
		List<CedulasAuxiliares> listaCedulas;
		DateFormat dateFormat = new SimpleDateFormat ("HH:mm:ss");
		DateFormat dateFormat2 = new SimpleDateFormat ("yyyy/MM/dd");
		Calendar calendar = Calendar.getInstance();
		Date date1, date2, dateNueva, datereal;
//		logger.info("La hora esta en el rango");
		for(Grafico3 x:lista) {
			
//			date1 = dateFormat.parse(hora_inicial);
//			date2 = dateFormat.parse(hora_final);
//			dateNueva = dateFormat.parse(x.getHoraCreacion2());
//			logger.info(x.getId().toString());
//			logger.info("INFO - Level Log Message - : " + dateNueva.toString());
//			if ((date1.compareTo(dateNueva) <= 0) && (date2.compareTo(dateNueva) >= 0)){
//				 
//				logger.info("La hora esta en el rango");
//				logger.info("Fecha string original: " + x.getFechaCreacion());
//
//				datereal = new SimpleDateFormat("dd/MM/yyyy").parse(x.getFechaCreacion3()); 
//				logger.info("Fecha original:" + datereal.toString());
//				calendar.setTime(datereal);
//				calendar.add(Calendar.DAY_OF_YEAR, -1); 
//				datereal = calendar.getTime();
//				logger.info("Fecha final:" + datereal.toString());
//				logger.info("Fecha final string:" + dateFormat2.format(datereal));
//				x.setFechaCreacion2(dateFormat2.format(datereal));
//			}else {
//				logger.info("La hora esta fuera del rango");
//			}
			tiempoTotalCausalesTiempo = 0;
			tiempoAtraso2Horas  =0;
			observaciones = "";
			listaFase2 = mappersConsulta.buscarTicketFase2(x.getId());
			causales = mappersConsulta.causalesConTiempos(x.getId());
			for(CausalesTiempo y:causales) {
				observaciones += ("Nombre Causal: "+y.getNombreCausal()+" Tiempo(Minutos): "+y.getTiempoCausal()+"; ");
				tiempoTotalCausalesTiempo += Integer.parseInt(y.getTiempoCausal());
				if(Integer.parseInt(y.getTiempoCausal())>120) {
					tiempoAtraso2Horas+=Integer.parseInt(y.getTiempoCausal());
				}
			}
			String fecha1 = null,fecha2 = null,fecha3 = null,fecha4 = null,fecha5 = null;
			for(TicketFase2 z:listaFase2) {
				switch (z.getEstadoTicket()) {
					case 1:
						fecha1 = z.getFechaEstado();
						x.setHoraArriboPlanta(z.getFechaEstado());
						break;
					case 2:
						fecha2 = z.getFechaEstado();
						x.setHoraIngresoPlanta(z.getFechaEstado());
						break;
					case 3:
						fecha3 = z.getFechaEstado();
						x.setHoraInicioOperacion(z.getFechaEstado());
						break;
					case 4:
						fecha4 = z.getFechaEstado();
						x.setHoraFinOperacion(z.getFechaEstado());
						break;
					case 5:
						fecha5 = z.getFechaEstado();
						x.setHoraVerificacion(z.getFechaEstado());
						break;
				}
			}//fin for
			x.setTiempoEsperaIngresoPlanta(diferenciaMinutos(ParseFecha(fecha2),ParseFecha(fecha1)));
			x.setTiempoEsperaInicioOperacion(diferenciaMinutos(ParseFecha(fecha3),ParseFecha(fecha2)));
			x.setTiempoOperacion(diferenciaMinutos(ParseFecha(fecha4),ParseFecha(fecha3)));
			x.setTiempoEsperaVerfificacion(diferenciaMinutos(ParseFecha(fecha5),ParseFecha(fecha4)));
			x.setTiepoTotalPlanta(x.getTiempoEsperaInicioOperacion()+x.getTiempoOperacion());
			x.setTiempoTotalAtrasoOperacion(tiempoAtraso2Horas);
			x.setTiempoAtencionSinAfectacion(x.getTiepoTotalPlanta()-x.getTiempoTotalAtrasoOperacion());
			x.setObservaciones(observaciones);
			listaCedulas = mappersConsulta.cedulaAuxiliares(x.getId());
			x.setTarifa(mappersConsulta.buscaTarifa(x.getIdOperacion(), x.getIdPlanta()));
			int indice = 1;
			for(CedulasAuxiliares v:listaCedulas) {
				switch (indice) {
				case 1:
					x.setCedula1(v.getCedulaAuxiliares());
					break;
				case 2:
					x.setCedula2(v.getCedulaAuxiliares());
					break;
				case 3:
					x.setCedula3(v.getCedulaAuxiliares());
					break;
				case 4:
					x.setCedula4(v.getCedulaAuxiliares());
					break;
				case 5:
					x.setCedula5(v.getCedulaAuxiliares());
					break;
				case 6:
					x.setCedula6(v.getCedulaAuxiliares());
					break;
				case 7:
					x.setCedula7(v.getCedulaAuxiliares());
					break;
				case 8:
					x.setCedula8(v.getCedulaAuxiliares());
					break;
				case 9:
					x.setCedula9(v.getCedulaAuxiliares());
					break;
				case 10:
					x.setCedula10(v.getCedulaAuxiliares());
					break;
				
				}
				indice++;
			}
			
		}//fin for
		return lista;
	}
	
	@GetMapping(path = "/grafico4/{idPlanta}/{fechaInicial}/{fechaFinal}",  produces = "application/json")
	public List<Grafico3> grafico4(@PathVariable int idPlanta,@PathVariable String fechaInicial,@PathVariable String fechaFinal) throws Exception {
		List<Grafico3> lista = mappersGraficos.grafico4(idPlanta,fechaInicial,fechaFinal);
		String observaciones = null;
		int tiempoTotalCausalesTiempo = 0;
		double tiempoAtraso2Horas  =0;
		List<TicketFase2> listaFase2;
		List<CausalesTiempo> causales;
		String jornada =""; 
		String hora_inicial = "00:00:00";
		String hora_final = "06:00:00";
		List<CedulasAuxiliares> listaCedulas;
		DateFormat dateFormat = new SimpleDateFormat ("HH:mm:ss");
		DateFormat dateFormat2 = new SimpleDateFormat ("yyyy/MM/dd");
		Calendar calendar = Calendar.getInstance();
		Date date1, date2, dateNueva, datereal;
		for(Grafico3 x:lista) {
			tiempoTotalCausalesTiempo = 0;
			tiempoAtraso2Horas  =0;
			observaciones = "";
			listaFase2 = mappersConsulta.buscarTicketFase2(x.getId());
			causales = mappersConsulta.causalesConTiempos(x.getId());
			for(CausalesTiempo y:causales) {
				observaciones += ("Nombre Causal: "+y.getNombreCausal()+" Tiempo(Minutos): "+y.getTiempoCausal()+"; ");
				tiempoTotalCausalesTiempo += Integer.parseInt(y.getTiempoCausal());
				if(Integer.parseInt(y.getTiempoCausal())>120) {
					tiempoAtraso2Horas+=Integer.parseInt(y.getTiempoCausal());
				}
			}
			String fecha1 = null,fecha2 = null,fecha3 = null,fecha4 = null,fecha5 = null;
			for(TicketFase2 z:listaFase2) {
				switch (z.getEstadoTicket()) {
					case 1:
						fecha1 = z.getFechaEstado();
						x.setHoraArriboPlanta(z.getFechaEstado());
						break;
					case 2:
						fecha2 = z.getFechaEstado();
						x.setHoraIngresoPlanta(z.getFechaEstado());
						break;
					case 3:
						fecha3 = z.getFechaEstado();
						x.setHoraInicioOperacion(z.getFechaEstado());
						break;
					case 4:
						fecha4 = z.getFechaEstado();
						x.setHoraFinOperacion(z.getFechaEstado());
						break;
					case 5:
						fecha5 = z.getFechaEstado();
						x.setHoraVerificacion(z.getFechaEstado());
						break;
				}
			}//fin for
			x.setTiempoEsperaIngresoPlanta(diferenciaMinutos(ParseFecha(fecha2),ParseFecha(fecha1)));
			x.setTiempoEsperaInicioOperacion(diferenciaMinutos(ParseFecha(fecha3),ParseFecha(fecha2)));
			x.setTiempoOperacion(diferenciaMinutos(ParseFecha(fecha4),ParseFecha(fecha3)));
			x.setTiempoEsperaVerfificacion(diferenciaMinutos(ParseFecha(fecha5),ParseFecha(fecha4)));
			x.setTiepoTotalPlanta(x.getTiempoEsperaInicioOperacion()+x.getTiempoOperacion());
			x.setTiempoTotalAtrasoOperacion(tiempoAtraso2Horas);
			x.setTiempoAtencionSinAfectacion(x.getTiepoTotalPlanta()-x.getTiempoTotalAtrasoOperacion());
			x.setObservaciones(observaciones);
			listaCedulas = mappersConsulta.cedulaAuxiliares(x.getId());
			x.setTarifa(mappersConsulta.buscaTarifa(x.getIdOperacion(), x.getIdPlanta()));
			int indice = 1;
			for(CedulasAuxiliares v:listaCedulas) {
				switch (indice) {
				case 1:
					x.setCedula1(v.getCedulaAuxiliares());
					break;
				case 2:
					x.setCedula2(v.getCedulaAuxiliares());
					break;
				case 3:
					x.setCedula3(v.getCedulaAuxiliares());
					break;
				case 4:
					x.setCedula4(v.getCedulaAuxiliares());
					break;
				case 5:
					x.setCedula5(v.getCedulaAuxiliares());
					break;
				case 6:
					x.setCedula6(v.getCedulaAuxiliares());
					break;
				case 7:
					x.setCedula7(v.getCedulaAuxiliares());
					break;
				case 8:
					x.setCedula8(v.getCedulaAuxiliares());
					break;
				case 9:
					x.setCedula9(v.getCedulaAuxiliares());
					break;
				case 10:
					x.setCedula10(v.getCedulaAuxiliares());
					break;
				
				}
				indice++;
			}
			
		}//fin for
		return lista;
	}
	
	@GetMapping(path = "/grafico5_c/{idPlanta}/{fechaInicial}/{fechaFinal}",  produces = "application/json")
	public List<Grafico3> grafico5_c(@PathVariable int idPlanta,@PathVariable String fechaInicial,@PathVariable String fechaFinal) throws Exception {
		List<Grafico3> lista = mappersGraficos.grafico5_c(idPlanta,fechaInicial,fechaFinal);
		String observaciones = null;
		int tiempoTotalCausalesTiempo = 0;
		double tiempoAtraso2Horas  =0;
		List<TicketFase2> listaFase2;
		List<CausalesTiempo> causales;
		List<CedulasAuxiliares> listaCedulas;
		for(Grafico3 x:lista) {
			tiempoTotalCausalesTiempo = 0;
			tiempoAtraso2Horas  =0;
			observaciones = "";
			listaFase2 = mappersConsulta.buscarTicketFase2(x.getId());
			causales = mappersConsulta.causalesConTiempos(x.getId());
			for(CausalesTiempo y:causales) {
				observaciones += ("Nombre Causal: "+y.getNombreCausal()+" Tiempo(Minutos): "+y.getTiempoCausal()+"; ");
				tiempoTotalCausalesTiempo += Integer.parseInt(y.getTiempoCausal());
				if(Integer.parseInt(y.getTiempoCausal())>120) {
					tiempoAtraso2Horas+=Integer.parseInt(y.getTiempoCausal());
				}
			}
			String fecha1 = null,fecha2 = null,fecha3 = null,fecha4 = null,fecha5 = null;
			for(TicketFase2 z:listaFase2) {
				switch (z.getEstadoTicket()) {
					case 1:
						fecha1 = z.getFechaEstado();
						x.setHoraArriboPlanta(z.getFechaEstado());
						break;
					case 2:
						fecha2 = z.getFechaEstado();
						x.setHoraIngresoPlanta(z.getFechaEstado());
						break;
					case 3:
						fecha3 = z.getFechaEstado();
						x.setHoraInicioOperacion(z.getFechaEstado());
						break;
					case 4:
						fecha4 = z.getFechaEstado();
						x.setHoraFinOperacion(z.getFechaEstado());
						break;
					case 5:
						fecha5 = z.getFechaEstado();
						x.setHoraVerificacion(z.getFechaEstado());
						break;
				}
			}//fin for
			x.setTiempoEsperaIngresoPlanta(diferenciaMinutos(ParseFecha(fecha2),ParseFecha(fecha1)));
			x.setTiempoEsperaInicioOperacion(diferenciaMinutos(ParseFecha(fecha3),ParseFecha(fecha2)));
			x.setTiempoOperacion(diferenciaMinutos(ParseFecha(fecha4),ParseFecha(fecha3)));
			x.setTiempoEsperaVerfificacion(diferenciaMinutos(ParseFecha(fecha5),ParseFecha(fecha4)));
			x.setTiepoTotalPlanta(x.getTiempoEsperaInicioOperacion()+x.getTiempoOperacion());
			x.setTiempoTotalAtrasoOperacion(tiempoAtraso2Horas);
			x.setTiempoAtencionSinAfectacion(x.getTiepoTotalPlanta()-x.getTiempoTotalAtrasoOperacion());
			x.setObservaciones(observaciones);
			listaCedulas = mappersConsulta.cedulaAuxiliares(x.getId());
			x.setTarifa(mappersConsulta.buscaTarifa(x.getIdOperacion(), x.getIdPlanta()));
			int indice = 1;
			for(CedulasAuxiliares v:listaCedulas) {
				switch (indice) {
				case 1:
					x.setCedula1(v.getCedulaAuxiliares());
					break;
				case 2:
					x.setCedula2(v.getCedulaAuxiliares());
					break;
				case 3:
					x.setCedula3(v.getCedulaAuxiliares());
					break;
				case 4:
					x.setCedula4(v.getCedulaAuxiliares());
					break;
				case 5:
					x.setCedula5(v.getCedulaAuxiliares());
					break;
				case 6:
					x.setCedula6(v.getCedulaAuxiliares());
					break;
				case 7:
					x.setCedula7(v.getCedulaAuxiliares());
					break;
				case 8:
					x.setCedula8(v.getCedulaAuxiliares());
					break;
				case 9:
					x.setCedula9(v.getCedulaAuxiliares());
					break;
				case 10:
					x.setCedula10(v.getCedulaAuxiliares());
					break;
				
				}
				indice++;
			}
			
		}//fin for
		return lista;
	}
	
	@GetMapping(path = "/grafico5_d/{idPlanta}/{fechaInicial}/{fechaFinal}",  produces = "application/json")
	public List<Grafico3> grafico5_d(@PathVariable int idPlanta,@PathVariable String fechaInicial,@PathVariable String fechaFinal) throws Exception {
		List<Grafico3> lista = mappersGraficos.grafico5_d(idPlanta,fechaInicial,fechaFinal);
		String observaciones = null;
		int tiempoTotalCausalesTiempo = 0;
		double tiempoAtraso2Horas  =0;
		List<TicketFase2> listaFase2;
		List<CausalesTiempo> causales;
		List<CedulasAuxiliares> listaCedulas;
		for(Grafico3 x:lista) {
			tiempoTotalCausalesTiempo = 0;
			tiempoAtraso2Horas  =0;
			observaciones = "";
			listaFase2 = mappersConsulta.buscarTicketFase2(x.getId());
			causales = mappersConsulta.causalesConTiempos(x.getId());
			for(CausalesTiempo y:causales) {
				observaciones += ("Nombre Causal: "+y.getNombreCausal()+" Tiempo(Minutos): "+y.getTiempoCausal()+"; ");
				tiempoTotalCausalesTiempo += Integer.parseInt(y.getTiempoCausal());
				if(Integer.parseInt(y.getTiempoCausal())>120) {
					tiempoAtraso2Horas+=Integer.parseInt(y.getTiempoCausal());
				}
			}
			String fecha1 = null,fecha2 = null,fecha3 = null,fecha4 = null,fecha5 = null;
			for(TicketFase2 z:listaFase2) {
				switch (z.getEstadoTicket()) {
					case 1:
						fecha1 = z.getFechaEstado();
						x.setHoraArriboPlanta(z.getFechaEstado());
						break;
					case 2:
						fecha2 = z.getFechaEstado();
						x.setHoraIngresoPlanta(z.getFechaEstado());
						break;
					case 3:
						fecha3 = z.getFechaEstado();
						x.setHoraInicioOperacion(z.getFechaEstado());
						break;
					case 4:
						fecha4 = z.getFechaEstado();
						x.setHoraFinOperacion(z.getFechaEstado());
						break;
					case 5:
						fecha5 = z.getFechaEstado();
						x.setHoraVerificacion(z.getFechaEstado());
						break;
				}
			}//fin for
			x.setTiempoEsperaIngresoPlanta(diferenciaMinutos(ParseFecha(fecha2),ParseFecha(fecha1)));
			x.setTiempoEsperaInicioOperacion(diferenciaMinutos(ParseFecha(fecha3),ParseFecha(fecha2)));
			x.setTiempoOperacion(diferenciaMinutos(ParseFecha(fecha4),ParseFecha(fecha3)));
			x.setTiempoEsperaVerfificacion(diferenciaMinutos(ParseFecha(fecha5),ParseFecha(fecha4)));
			x.setTiepoTotalPlanta(x.getTiempoEsperaInicioOperacion()+x.getTiempoOperacion());
			x.setTiempoTotalAtrasoOperacion(tiempoAtraso2Horas);
			x.setTiempoAtencionSinAfectacion(x.getTiepoTotalPlanta()-x.getTiempoTotalAtrasoOperacion());
			x.setObservaciones(observaciones);
			listaCedulas = mappersConsulta.cedulaAuxiliares(x.getId());
			x.setTarifa(mappersConsulta.buscaTarifa(x.getIdOperacion(), x.getIdPlanta()));
			int indice = 1;
			for(CedulasAuxiliares v:listaCedulas) {
				switch (indice) {
				case 1:
					x.setCedula1(v.getCedulaAuxiliares());
					break;
				case 2:
					x.setCedula2(v.getCedulaAuxiliares());
					break;
				case 3:
					x.setCedula3(v.getCedulaAuxiliares());
					break;
				case 4:
					x.setCedula4(v.getCedulaAuxiliares());
					break;
				case 5:
					x.setCedula5(v.getCedulaAuxiliares());
					break;
				case 6:
					x.setCedula6(v.getCedulaAuxiliares());
					break;
				case 7:
					x.setCedula7(v.getCedulaAuxiliares());
					break;
				case 8:
					x.setCedula8(v.getCedulaAuxiliares());
					break;
				case 9:
					x.setCedula9(v.getCedulaAuxiliares());
					break;
				case 10:
					x.setCedula10(v.getCedulaAuxiliares());
					break;
				
				}
				indice++;
			}
			
		}//fin for
		return lista;
	}
	
	@GetMapping(path = "/grafico6/{idPlanta}/{fechaInicial}/{fechaFinal}",  produces = "application/json")
	public List<Grafico3> grafico6(@PathVariable int idPlanta,@PathVariable String fechaInicial,@PathVariable String fechaFinal) throws Exception {
		List<Grafico3> lista = mappersGraficos.grafico6(idPlanta,fechaInicial,fechaFinal);
		String observaciones = null;
		int tiempoTotalCausalesTiempo = 0;
		double tiempoAtraso2Horas  =0;
		String jornada =""; 
		List<TicketFase2> listaFase2;
		List<CausalesTiempo> causales;
		List<CedulasAuxiliares> listaCedulas;
		for(Grafico3 x:lista) {
			tiempoTotalCausalesTiempo = 0;
			tiempoAtraso2Horas  =0;
			observaciones = "";
			listaFase2 = mappersConsulta.buscarTicketFase2(x.getId());
			causales = mappersConsulta.causalesConTiempos(x.getId());
			for(CausalesTiempo y:causales) {
				observaciones += ("Nombre Causal: "+y.getNombreCausal()+" Tiempo(Minutos): "+y.getTiempoCausal()+"; ");
				tiempoTotalCausalesTiempo += Integer.parseInt(y.getTiempoCausal());
				if(Integer.parseInt(y.getTiempoCausal())>120) {
					tiempoAtraso2Horas+=Integer.parseInt(y.getTiempoCausal());
				}
			}
			String fecha1 = null,fecha2 = null,fecha3 = null,fecha4 = null,fecha5 = null;
			for(TicketFase2 z:listaFase2) {
				switch (z.getEstadoTicket()) {
					case 1:
						fecha1 = z.getFechaEstado();
						x.setHoraArriboPlanta(z.getFechaEstado());
						break;
					case 2:
						fecha2 = z.getFechaEstado();
						x.setHoraIngresoPlanta(z.getFechaEstado());
						break;
					case 3:
						fecha3 = z.getFechaEstado();
						x.setHoraInicioOperacion(z.getFechaEstado());
						break;
					case 4:
						fecha4 = z.getFechaEstado();
						x.setHoraFinOperacion(z.getFechaEstado());
						break;
					case 5:
						fecha5 = z.getFechaEstado();
						x.setHoraVerificacion(z.getFechaEstado());
						break;
				}
			}//fin for
			x.setTiempoEsperaIngresoPlanta(diferenciaMinutos(ParseFecha(fecha2),ParseFecha(fecha1)));
			x.setTiempoEsperaInicioOperacion(diferenciaMinutos(ParseFecha(fecha3),ParseFecha(fecha2)));
			x.setTiempoOperacion(diferenciaMinutos(ParseFecha(fecha4),ParseFecha(fecha3)));
			x.setTiempoEsperaVerfificacion(diferenciaMinutos(ParseFecha(fecha5),ParseFecha(fecha4)));
			x.setTiepoTotalPlanta(x.getTiempoEsperaInicioOperacion()+x.getTiempoOperacion());
			x.setTiempoTotalAtrasoOperacion(tiempoAtraso2Horas);
			x.setTiempoAtencionSinAfectacion(x.getTiepoTotalPlanta()-x.getTiempoTotalAtrasoOperacion());
			x.setObservaciones(observaciones);
			listaCedulas = mappersConsulta.cedulaAuxiliares(x.getId());
			x.setTarifa(mappersConsulta.buscaTarifa(x.getIdOperacion(), x.getIdPlanta()));
			int indice = 1;
			for(CedulasAuxiliares v:listaCedulas) {
				switch (indice) {
				case 1:
					x.setCedula1(v.getCedulaAuxiliares());
					break;
				case 2:
					x.setCedula2(v.getCedulaAuxiliares());
					break;
				case 3:
					x.setCedula3(v.getCedulaAuxiliares());
					break;
				case 4:
					x.setCedula4(v.getCedulaAuxiliares());
					break;
				case 5:
					x.setCedula5(v.getCedulaAuxiliares());
					break;
				case 6:
					x.setCedula6(v.getCedulaAuxiliares());
					break;
				case 7:
					x.setCedula7(v.getCedulaAuxiliares());
					break;
				case 8:
					x.setCedula8(v.getCedulaAuxiliares());
					break;
				case 9:
					x.setCedula9(v.getCedulaAuxiliares());
					break;
				case 10:
					x.setCedula10(v.getCedulaAuxiliares());
					break;
				
				}
				indice++;
			}
			
		}//fin for
		return lista;
	}
	
	@GetMapping(path = "/grafico13/{idPlanta}/{fechaInicial}/{fechaFinal}",  produces = "application/json")
	public List<Grafica13> grafico13(@PathVariable int idPlanta,@PathVariable String fechaInicial,@PathVariable String fechaFinal) throws Exception {
		List<Grafica13> lista = mappersGraficos.grafico13(idPlanta,fechaInicial,fechaFinal);
		//fin for
		return lista;
	}
	
	@GetMapping(path = "/grafico13_inicio/{idPlanta}/{fechaInicial}/{fechaFinal}",  produces = "application/json")
	public List<Grafica13> grafico13_inicio(@PathVariable int idPlanta,@PathVariable String fechaInicial,@PathVariable String fechaFinal) throws Exception {
		List<Grafica13> lista = mappersGraficos.grafico13_inicio(idPlanta,fechaInicial,fechaFinal);
		//fin for
		return lista;
	}
	
	@GetMapping(path = "/grafico13_operacion/{idPlanta}/{fechaInicial}/{fechaFinal}",  produces = "application/json")
	public List<Grafica13> grafico13_operacion(@PathVariable int idPlanta,@PathVariable String fechaInicial,@PathVariable String fechaFinal) throws Exception {
		List<Grafica13> lista = mappersGraficos.grafico13_operacion(idPlanta,fechaInicial,fechaFinal);
		//fin for
		return lista;
	}
	
	@GetMapping(path = "/grafico14_inicio/{idPlanta}/{fechaInicial}/{fechaFinal}",  produces = "application/json")
	public List<Grafica13> grafico14_inicio(@PathVariable int idPlanta,@PathVariable String fechaInicial,@PathVariable String fechaFinal) throws Exception {
		List<Grafica13> lista = mappersGraficos.grafico14_inicio(idPlanta,fechaInicial,fechaFinal);
		//fin for
		return lista;
	}
	
	@GetMapping(path = "/grafico14_operacion/{idPlanta}/{fechaInicial}/{fechaFinal}",  produces = "application/json")
	public List<Grafica13> grafico14_operacion(@PathVariable int idPlanta,@PathVariable String fechaInicial,@PathVariable String fechaFinal) throws Exception {
		List<Grafica13> lista = mappersGraficos.grafico14_operacion(idPlanta,fechaInicial,fechaFinal);
		//fin for
		return lista;
	}
	
	@GetMapping(path = "/grafico14/{idPlanta}/{fechaInicial}/{fechaFinal}",  produces = "application/json")
	public List<Grafica13> grafico14(@PathVariable int idPlanta,@PathVariable String fechaInicial,@PathVariable String fechaFinal) throws Exception {
		List<Grafica13> lista = mappersGraficos.grafico14(idPlanta,fechaInicial,fechaFinal);
		//fin for
		return lista;
	}
	
	@GetMapping(path = "/grafico15/{idPlanta}/{fechaInicial}/{fechaFinal}",  produces = "application/json")
	public List<Grafico15> grafico15(@PathVariable int idPlanta,@PathVariable String fechaInicial,@PathVariable String fechaFinal) throws Exception {
		List<Grafico15> lista = mappersGraficos.grafico15(idPlanta,fechaInicial,fechaFinal);
		//fin for
		return lista;
	}
	
	@GetMapping(path = "/cerrados/{idPlanta}/{fechaInicial}/{fechaFinal}",  produces = "application/json")
	public List<ConsultaTicket> bitacora(@PathVariable int idPlanta,@PathVariable String fechaInicial,@PathVariable String fechaFinal) throws Exception {
		List<ConsultaTicket> lista = mappersConsulta.buscarTicketCerrado(idPlanta,fechaInicial,fechaFinal);
		String observaciones = null;
		int tiempoTotalCausalesTiempo = 0;
		double tiempoAtraso2Horas  =0;
		List<TicketFase2> listaFase2;
		List<CausalesTiempo> causales;
		List<CedulasAuxiliares> listaCedulas;
		for(ConsultaTicket x:lista) {
			tiempoTotalCausalesTiempo = 0;
			tiempoAtraso2Horas  =0;
			observaciones = "";
			listaFase2 = mappersConsulta.buscarTicketFase2(x.getId());
			causales = mappersConsulta.causalesConTiempos(x.getId());
			for(CausalesTiempo y:causales) {
				observaciones += ("Nombre Causal: "+y.getNombreCausal()+" Tiempo(Minutos): "+y.getTiempoCausal()+"; ");
				tiempoTotalCausalesTiempo += Integer.parseInt(y.getTiempoCausal());
				if(Integer.parseInt(y.getTiempoCausal())>120) {
					tiempoAtraso2Horas+=Integer.parseInt(y.getTiempoCausal());
				}
			}
			String fecha1 = null,fecha2 = null,fecha3 = null,fecha4 = null,fecha5 = null;
			for(TicketFase2 z:listaFase2) {
				switch (z.getEstadoTicket()) {
					case 1:
						fecha1 = z.getFechaEstado();
						x.setHoraArriboPlanta(z.getFechaEstado());
						break;
					case 2:
						fecha2 = z.getFechaEstado();
						x.setHoraIngresoPlanta(z.getFechaEstado());
						break;
					case 3:
						fecha3 = z.getFechaEstado();
						x.setHoraInicioOperacion(z.getFechaEstado());
						break;
					case 4:
						fecha4 = z.getFechaEstado();
						x.setHoraFinOperacion(z.getFechaEstado());
						break;
					case 5:
						fecha5 = z.getFechaEstado();
						x.setHoraVerificacion(z.getFechaEstado());
						break;
				}
			}//fin for
			x.setTiempoEsperaIngresoPlanta(diferenciaMinutos(ParseFecha(fecha2),ParseFecha(fecha1)));
			x.setTiempoEsperaInicioOperacion(diferenciaMinutos(ParseFecha(fecha3),ParseFecha(fecha2)));
			x.setTiempoOperacion(diferenciaMinutos(ParseFecha(fecha4),ParseFecha(fecha3)));
			x.setTiempoEsperaVerfificacion(diferenciaMinutos(ParseFecha(fecha5),ParseFecha(fecha4)));
			x.setTiepoTotalPlanta(x.getTiempoEsperaInicioOperacion()+x.getTiempoOperacion());
			x.setTiempoTotalAtrasoOperacion(tiempoAtraso2Horas);
			x.setTiempoAtencionSinAfectacion(x.getTiepoTotalPlanta()-x.getTiempoTotalAtrasoOperacion());
			x.setObservaciones(observaciones);
			listaCedulas = mappersConsulta.cedulaAuxiliares(x.getId());
			x.setTarifa(mappersConsulta.buscaTarifa(x.getIdOperacion(), x.getIdPlanta()));
			int indice = 1;
			for(CedulasAuxiliares v:listaCedulas) {
				switch (indice) {
				case 1:
					x.setCedula1(v.getCedulaAuxiliares());
					break;
				case 2:
					x.setCedula2(v.getCedulaAuxiliares());
					break;
				case 3:
					x.setCedula3(v.getCedulaAuxiliares());
					break;
				case 4:
					x.setCedula4(v.getCedulaAuxiliares());
					break;
				case 5:
					x.setCedula5(v.getCedulaAuxiliares());
					break;
				case 6:
					x.setCedula6(v.getCedulaAuxiliares());
					break;
				case 7:
					x.setCedula7(v.getCedulaAuxiliares());
					break;
				case 8:
					x.setCedula8(v.getCedulaAuxiliares());
					break;
				case 9:
					x.setCedula9(v.getCedulaAuxiliares());
					break;
				case 10:
					x.setCedula10(v.getCedulaAuxiliares());
					break;
				
				}
				indice++;
			}
			
		}//fin for
		return lista;
	}
	
	private double diferenciaMinutos(Date fechaMayor, Date fechaMenor) {
		double diferenciaEn_ms = fechaMayor.getTime() - fechaMenor.getTime();
		//long dias = diferenciaEn_ms / (1000 * 60 * 60 * 24);
		double minutos = diferenciaEn_ms / (1000 * 60);
		return (double)minutos;
	}
	
	public static Date ParseFecha(String fecha)
    {
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date fechaDate = null;
        try {
            fechaDate = formato.parse(fecha);
        } 
        catch (ParseException ex) 
        {
            System.out.println("Error de parsing: "+ex);
        }
        return fechaDate;
    }

}
