package com.empresa.supply.modelo.admin.usuario;

import java.util.List;

import com.empresa.supply.modelo.administrador.Perfiles;

public class UsuarioPerfil {
	private int idUsuario;
	private String nombreUsuario;
	List<Perfiles> listaPerfiles;
	public UsuarioPerfil() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	public List<Perfiles> getListaPerfiles() {
		return listaPerfiles;
	}
	public void setListaPerfiles(List<Perfiles> listaPerfiles) {
		this.listaPerfiles = listaPerfiles;
	}

}
