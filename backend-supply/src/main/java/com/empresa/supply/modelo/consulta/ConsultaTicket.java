package com.empresa.supply.modelo.consulta;

public class ConsultaTicket {
	private int id;
	private int idPlanta;
	private String nombrePlanta;
	private int muelle;
	private String fechaCreacion;
	private String placa;
	private int idTipoTransporte;
	private int idTransportista;
	private String nombreTransportista;
	private String tipoTransporte;
	private int idOperacion;
	private String nombreOperacion;
	private int numeroRemision;
	//private List<TicketFase2> listaTicketFase2;
	//private List<CausalesTiempo> listaCausalesTiempo;
	private String observaciones;
	private double tiempoEsperaIngresoPlanta;
	private double tiempoEsperaInicioOperacion;
	private double tiempoOperacion;
	private double tiempoEsperaVerfificacion;
	private double tiepoTotalPlanta; 
	private double tiempoTotalAtrasoOperacion;
	private double tiempoAtencionSinAfectacion;
	//private List<CedulasAuxiliares> cedulasAuxiliares;
	private long tarifa;
	private String horaArriboPlanta;
	private String horaIngresoPlanta;
	private String horaInicioOperacion;
	private String horaFinOperacion;
	private String horaVerificacion;
	private long cedula1;
	private long cedula2;
	private long cedula3;
	private long cedula4;
	private long cedula5;
	private long cedula6;
	private long cedula7;
	private long cedula8;
	private long cedula9;
	private long cedula10;
	
	public ConsultaTicket() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public String getHoraArriboPlanta() {
		return horaArriboPlanta;
	}


	public void setHoraArriboPlanta(String horaArriboPlanta) {
		this.horaArriboPlanta = horaArriboPlanta;
	}


	public String getHoraIngresoPlanta() {
		return horaIngresoPlanta;
	}


	public void setHoraIngresoPlanta(String horaIngresoPlanta) {
		this.horaIngresoPlanta = horaIngresoPlanta;
	}


	public String getHoraInicioOperacion() {
		return horaInicioOperacion;
	}


	public void setHoraInicioOperacion(String horaInicioOperacion) {
		this.horaInicioOperacion = horaInicioOperacion;
	}


	public String getHoraFinOperacion() {
		return horaFinOperacion;
	}


	public void setHoraFinOperacion(String horaFinOperacion) {
		this.horaFinOperacion = horaFinOperacion;
	}


	public String getHoraVerificacion() {
		return horaVerificacion;
	}


	public void setHoraVerificacion(String horaVerificacion) {
		this.horaVerificacion = horaVerificacion;
	}


	public long getCedula1() {
		return cedula1;
	}


	public void setCedula1(long cedula1) {
		this.cedula1 = cedula1;
	}


	public long getCedula2() {
		return cedula2;
	}


	public void setCedula2(long cedula2) {
		this.cedula2 = cedula2;
	}


	public long getCedula3() {
		return cedula3;
	}


	public void setCedula3(long cedula3) {
		this.cedula3 = cedula3;
	}


	public long getCedula4() {
		return cedula4;
	}


	public void setCedula4(long cedula4) {
		this.cedula4 = cedula4;
	}


	public long getCedula5() {
		return cedula5;
	}


	public void setCedula5(long cedula5) {
		this.cedula5 = cedula5;
	}


	public long getCedula6() {
		return cedula6;
	}


	public void setCedula6(long cedula6) {
		this.cedula6 = cedula6;
	}


	public long getCedula7() {
		return cedula7;
	}


	public void setCedula7(long cedula7) {
		this.cedula7 = cedula7;
	}


	public long getCedula8() {
		return cedula8;
	}


	public void setCedula8(long cedula8) {
		this.cedula8 = cedula8;
	}


	public long getCedula9() {
		return cedula9;
	}


	public void setCedula9(long cedula9) {
		this.cedula9 = cedula9;
	}


	public long getCedula10() {
		return cedula10;
	}


	public void setCedula10(long cedula10) {
		this.cedula10 = cedula10;
	}


	public long getTarifa() {
		return tarifa;
	}

	public void setTarifa(long tarifa) {
		this.tarifa = tarifa;
	}

	
	public double getTiempoAtencionSinAfectacion() {
		return tiempoAtencionSinAfectacion;
	}



	public void setTiempoAtencionSinAfectacion(double tiempoAtencionSinAfectacion) {
		this.tiempoAtencionSinAfectacion = tiempoAtencionSinAfectacion;
	}



	public double getTiempoTotalAtrasoOperacion() {
		return tiempoTotalAtrasoOperacion;
	}



	public void setTiempoTotalAtrasoOperacion(double tiempoTotalAtrasoOperacion) {
		this.tiempoTotalAtrasoOperacion = tiempoTotalAtrasoOperacion;
	}



	public double getTiepoTotalPlanta() {
		return tiepoTotalPlanta;
	}

	public void setTiepoTotalPlanta(double tiepoTotalPlanta) {
		this.tiepoTotalPlanta = tiepoTotalPlanta;
	}

	public double getTiempoEsperaVerfificacion() {
		return tiempoEsperaVerfificacion;
	}

	public void setTiempoEsperaVerfificacion(double tiempoEsperaVerfificacion) {
		this.tiempoEsperaVerfificacion = tiempoEsperaVerfificacion;
	}

	public double getTiempoOperacion() {
		return tiempoOperacion;
	}

	public void setTiempoOperacion(double tiempoOperacion) {
		this.tiempoOperacion = tiempoOperacion;
	}

	public double getTiempoEsperaInicioOperacion() {
		return tiempoEsperaInicioOperacion;
	}

	public void setTiempoEsperaInicioOperacion(double tiempoEsperaInicioOperacion) {
		this.tiempoEsperaInicioOperacion = tiempoEsperaInicioOperacion;
	}

	public double getTiempoEsperaIngresoPlanta() {
		return tiempoEsperaIngresoPlanta;
	}

	public void setTiempoEsperaIngresoPlanta(double tiempoEsperaIngresoPlanta) {
		this.tiempoEsperaIngresoPlanta = tiempoEsperaIngresoPlanta;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdPlanta() {
		return idPlanta;
	}

	public void setIdPlanta(int idPlanta) {
		this.idPlanta = idPlanta;
	}

	public String getNombrePlanta() {
		return nombrePlanta;
	}

	public void setNombrePlanta(String nombrePlanta) {
		this.nombrePlanta = nombrePlanta;
	}

	public int getMuelle() {
		return muelle;
	}

	public void setMuelle(int muelle) {
		this.muelle = muelle;
	}

	public String getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public int getIdTipoTransporte() {
		return idTipoTransporte;
	}

	public void setIdTipoTransporte(int idTipoTransporte) {
		this.idTipoTransporte = idTipoTransporte;
	}

	public int getIdTransportista() {
		return idTransportista;
	}

	public void setIdTransportista(int idTransportista) {
		this.idTransportista = idTransportista;
	}

	public String getNombreTransportista() {
		return nombreTransportista;
	}

	public void setNombreTransportista(String nombreTransportista) {
		this.nombreTransportista = nombreTransportista;
	}

	public String getTipoTransporte() {
		return tipoTransporte;
	}

	public void setTipoTransporte(String tipoTransporte) {
		this.tipoTransporte = tipoTransporte;
	}

	public int getIdOperacion() {
		return idOperacion;
	}

	public void setIdOperacion(int idOperacion) {
		this.idOperacion = idOperacion;
	}

	public String getNombreOperacion() {
		return nombreOperacion;
	}

	public void setNombreOperacion(String nombreOperacion) {
		this.nombreOperacion = nombreOperacion;
	}

	public int getNumeroRemision() {
		return numeroRemision;
	}

	public void setNumeroRemision(int numeroRemision) {
		this.numeroRemision = numeroRemision;
	}

}
