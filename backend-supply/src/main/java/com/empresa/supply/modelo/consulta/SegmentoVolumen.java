package com.empresa.supply.modelo.consulta;

public class SegmentoVolumen {
	private int id;
	private String fechaCreacion;
	private String subdependencia;
	private String tipoTransporte;
	private int muelle;
	private int numeroPlaca;
	private double tiempoOperacion; 
	private double productividad;
	public SegmentoVolumen() {
		super();
		// TODO Auto-generated constructor stub
	}
	public double getProductividad() {
		return productividad;
	}
	public void setProductividad(double productividad) {
		this.productividad = productividad;
	}
	public double getTiempoOperacion() {
		return tiempoOperacion;
	}
	public void setTiempoOperacion(double tiempoOperacion) {
		this.tiempoOperacion = tiempoOperacion;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public String getSubdependencia() {
		return subdependencia;
	}
	public void setSubdependencia(String subdependencia) {
		this.subdependencia = subdependencia;
	}
	public String getTipoTransporte() {
		return tipoTransporte;
	}
	public void setTipoTransporte(String tipoTransporte) {
		this.tipoTransporte = tipoTransporte;
	}
	public int getMuelle() {
		return muelle;
	}
	public void setMuelle(int muelle) {
		this.muelle = muelle;
	}
	public int getNumeroPlaca() {
		return numeroPlaca;
	}
	public void setNumeroPlaca(int numeroPlaca) {
		this.numeroPlaca = numeroPlaca;
	}

}
