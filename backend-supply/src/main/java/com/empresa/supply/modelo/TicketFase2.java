package com.empresa.supply.modelo;

import java.util.Date;
import java.util.List;

public class TicketFase2 {
		private Date fechaEstado;
		private int estadoTicket;
		private String nota;
		private int idTicket;
		private String estado;
		private String foto;
		private List<Auxiliares> listaAuxiliares;
		private int estadoAuxiliar;
		public TicketFase2() {
			super();
			// TODO Auto-generated constructor stub
		}
		public int getEstadoAuxiliar() {
			return estadoAuxiliar;
		}
		public void setEstadoAuxiliar(int estadoAuxiliar) {
			this.estadoAuxiliar = estadoAuxiliar;
		}
		public List<Auxiliares> getListaAuxiliares() {
			return listaAuxiliares;
		}
		public void setListaAuxiliares(List<Auxiliares> listaAuxiliares) {
			this.listaAuxiliares = listaAuxiliares;
		}
		public String getFoto() {
			return foto;
		}
		public void setFoto(String foto) {
			this.foto = foto;
		}
		public Date getFechaEstado() {
			return fechaEstado;
		}
		public void setFechaEstado(Date fechaEstado) {
			this.fechaEstado = fechaEstado;
		}
		public int getEstadoTicket() {
			return estadoTicket;
		}
		public void setEstadoTicket(int estadoTicket) {
			this.estadoTicket = estadoTicket;
		}
		public String getNota() {
			return nota;
		}
		public void setNota(String nota) {
			this.nota = nota;
		}
		public int getIdTicket() {
			return idTicket;
		}
		public void setIdTicket(int idTicket) {
			this.idTicket = idTicket;
		}
		public String getEstado() {
			return estado;
		}
		public void setEstado(String estado) {
			this.estado = estado;
		}
}
