package com.empresa.supply.modelo.consulta;

public class VolumenTiempoOperacion {
	private int muelle;
	private int cantidad;
	private long tiempoTotalOperacion;
	private String fechaInicial;
	private String fechaFinal;
	private String tipoOperacion;
	private String tipoBusqueda;
	public VolumenTiempoOperacion() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getTipoBusqueda() {
		return tipoBusqueda;
	}
	public void setTipoBusqueda(String tipoBusqueda) {
		this.tipoBusqueda = tipoBusqueda;
	}
	public String getFechaInicial() {
		return fechaInicial;
	}
	public void setFechaInicial(String fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
	public String getFechaFinal() {
		return fechaFinal;
	}
	public void setFechaFinal(String fechaFinal) {
		this.fechaFinal = fechaFinal;
	}
	
	public String getTipoOperacion() {
		return tipoOperacion;
	}
	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
	public int getMuelle() {
		return muelle;
	}
	public void setMuelle(int muelle) {
		this.muelle = muelle;
	}
	public long getTiempoTotalOperacion() {
		return tiempoTotalOperacion;
	}
	public void setTiempoTotalOperacion(long tiempoTotalOperacion) {
		this.tiempoTotalOperacion = tiempoTotalOperacion;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

}
