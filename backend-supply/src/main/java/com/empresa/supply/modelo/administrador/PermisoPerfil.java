package com.empresa.supply.modelo.administrador;

import java.util.List;

public class PermisoPerfil {
	private int id;
	private int idPermiso;
	private int idPerfil;
	//private int estado;
	private String nombrePermiso;
	private String nombrePerfil;
	private List<Permisos> listaPermisos;
	public PermisoPerfil() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public String getNombrePermiso() {
		return nombrePermiso;
	}

	public void setNombrePermiso(String nombrePermiso) {
		this.nombrePermiso = nombrePermiso;
	}

	public int getIdPermiso() {
		return idPermiso;
	}

	public void setIdPermiso(int idPermiso) {
		this.idPermiso = idPermiso;
	}

	public int getIdPerfil() {
		return idPerfil;
	}

	public void setIdPerfil(int idPerfil) {
		this.idPerfil = idPerfil;
	}

	public List<Permisos> getListaPermisos() {
		return listaPermisos;
	}

	public void setListaPermisos(List<Permisos> listaPermisos) {
		this.listaPermisos = listaPermisos;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getNombrePerfil() {
		return nombrePerfil;
	}

	public void setNombrePerfil(String nombrePerfil) {
		this.nombrePerfil = nombrePerfil;
	}



	
	
}
