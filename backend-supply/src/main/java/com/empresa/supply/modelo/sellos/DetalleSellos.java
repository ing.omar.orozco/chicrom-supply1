package com.empresa.supply.modelo.sellos;

public class DetalleSellos {
	private int idSellos;
	private String nombre;
	private int operacion;
	private int selloNumero;
	private String fechaCreacion;
	
	public DetalleSellos() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getIdSellos() {
		return idSellos;
	}

	public void setIdSellos(int idSellos) {
		this.idSellos = idSellos;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getOperacion() {
		return operacion;
	}

	public void setOperacion(int operacion) {
		this.operacion = operacion;
	}

	public int getSelloNumero() {
		return selloNumero;
	}

	public void setSelloNumero(int selloNumero) {
		this.selloNumero = selloNumero;
	}

	public String getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

}
