package com.empresa.supply.modelo.admin.usuario;

public class AdminUsuario {
	private int id;
	private String tipoDocumento;
	private String nombretipoDocumento;
	private int documento;
	private String primerApellido;
	private String segundoApellido;
	private String primerNombre;
	private String segundoNombre;
	private int idContratoUsuario;
	private int idCargoOcupacion;
	private int idCiudadUsuario;
	private String usuario;
	private String clave;
	private int estado;
	private int idPlanta;
	private String nombreContrato;
	private String nombreCargo;
	private String nombreCiudad;
	private String nombrePlanta;
	
	public AdminUsuario() {
		super();
		// TODO Auto-generated constructor stub
	}


	public String getNombretipoDocumento() {
		return nombretipoDocumento;
	}


	public void setNombretipoDocumento(String nombretipoDocumento) {
		this.nombretipoDocumento = nombretipoDocumento;
	}


	public String getNombrePlanta() {
		return nombrePlanta;
	}


	public void setNombrePlanta(String nombrePlanta) {
		this.nombrePlanta = nombrePlanta;
	}


	public String getNombreCiudad() {
		return nombreCiudad;
	}


	public void setNombreCiudad(String nombreCiudad) {
		this.nombreCiudad = nombreCiudad;
	}


	public String getNombreCargo() {
		return nombreCargo;
	}


	public void setNombreCargo(String nombreCargo) {
		this.nombreCargo = nombreCargo;
	}


	public String getNombreContrato() {
		return nombreContrato;
	}


	public void setNombreContrato(String nombreContrato) {
		this.nombreContrato = nombreContrato;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getTipoDocumento() {
		return tipoDocumento;
	}


	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}


	public int getDocumento() {
		return documento;
	}


	public void setDocumento(int documento) {
		this.documento = documento;
	}


	public String getPrimerApellido() {
		return primerApellido;
	}


	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}


	public String getSegundoApellido() {
		return segundoApellido;
	}


	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}


	public String getPrimerNombre() {
		return primerNombre;
	}


	public void setPrimerNombre(String primerNombre) {
		this.primerNombre = primerNombre;
	}


	public String getSegundoNombre() {
		return segundoNombre;
	}


	public void setSegundoNombre(String segundoNombre) {
		this.segundoNombre = segundoNombre;
	}


	public int getIdContratoUsuario() {
		return idContratoUsuario;
	}


	public void setIdContratoUsuario(int idContratoUsuario) {
		this.idContratoUsuario = idContratoUsuario;
	}


	public int getIdCargoOcupacion() {
		return idCargoOcupacion;
	}


	public void setIdCargoOcupacion(int idCargoOcupacion) {
		this.idCargoOcupacion = idCargoOcupacion;
	}


	public int getIdCiudadUsuario() {
		return idCiudadUsuario;
	}


	public void setIdCiudadUsuario(int idCiudadUsuario) {
		this.idCiudadUsuario = idCiudadUsuario;
	}


	public String getUsuario() {
		return usuario;
	}


	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}


	public String getClave() {
		return clave;
	}


	public void setClave(String clave) {
		this.clave = clave;
	}


	public int getEstado() {
		return estado;
	}


	public void setEstado(int estado) {
		this.estado = estado;
	}


	public int getIdPlanta() {
		return idPlanta;
	}


	public void setIdPlanta(int idPlanta) {
		this.idPlanta = idPlanta;
	}

}
