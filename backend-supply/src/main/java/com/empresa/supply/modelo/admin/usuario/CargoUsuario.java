package com.empresa.supply.modelo.admin.usuario;

public class CargoUsuario {
	private int id;
	private String nombreCargo;
	public CargoUsuario() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombreCargo() {
		return nombreCargo;
	}
	public void setNombreCargo(String nombreCargo) {
		this.nombreCargo = nombreCargo;
	}

}
