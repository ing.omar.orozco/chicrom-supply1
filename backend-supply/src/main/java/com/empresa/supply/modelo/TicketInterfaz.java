package com.empresa.supply.modelo;

import java.util.List;

public class TicketInterfaz {
	private int id;
	private String nombre;
	private String fechaEstado;
	private int estadoTicket;
	private String nota;
	private int idTicket;
	private String estado;
	private String foto;
	private List<Auxiliares> listaAuxiliares;
	private String tipoOperacion;
	private String tieneSello;
	public TicketInterfaz() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getTieneSello() {
		return tieneSello;
	}
	public void setTieneSello(String tieneSello) {
		this.tieneSello = tieneSello;
	}
	public String getTipoOperacion() {
		return tipoOperacion;
	}
	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
	public List<Auxiliares> getListaAuxiliares() {
		return listaAuxiliares;
	}
	public void setListaAuxiliares(List<Auxiliares> listaAuxiliares) {
		this.listaAuxiliares = listaAuxiliares;
	}
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getFechaEstado() {
		return fechaEstado;
	}
	public void setFechaEstado(String fechaEstado) {
		this.fechaEstado = fechaEstado;
	}
	public int getEstadoTicket() {
		return estadoTicket;
	}
	public void setEstadoTicket(int estadoTicket) {
		this.estadoTicket = estadoTicket;
	}
	public String getNota() {
		return nota;
	}
	public void setNota(String nota) {
		this.nota = nota;
	}
	public int getIdTicket() {
		return idTicket;
	}
	public void setIdTicket(int idTicket) {
		this.idTicket = idTicket;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}

}
