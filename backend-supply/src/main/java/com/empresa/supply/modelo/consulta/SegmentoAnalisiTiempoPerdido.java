package com.empresa.supply.modelo.consulta;

public class SegmentoAnalisiTiempoPerdido {
	private int id;
	private int subDependencia;
	private String nombreSubdependencia;
	private String fechaCreacion;
	private int muelle;
	private String horaArriboPlanta;
	private String horaIngresoPlanta;
	private String horaInicioOperacion;
	private String horaFinOperacion;
	private String horaVerificacion;
	private double tiempoTotalAtrasoOperacion;
	private double tiempoTotalAtrasoInicioOperacion;
	private double tiempoTotalPerdido;
	private double porcentajeHorasPerdidas;
	private double mediana;
	private String causalTiempoPerdido;
	private int numeroPlacas;
	public SegmentoAnalisiTiempoPerdido() {
		super();
		// TODO Auto-generated constructor stub
	}
	public double getMediana() {
		return mediana;
	}
	public void setMediana(double mediana) {
		this.mediana = mediana;
	}
	public int getNumeroPlacas() {
		return numeroPlacas;
	}
	public void setNumeroPlacas(int numeroPlacas) {
		this.numeroPlacas = numeroPlacas;
	}
	public double getPorcentajeHorasPerdidas() {
		return porcentajeHorasPerdidas;
	}
	public void setPorcentajeHorasPerdidas(double porcentajeHorasPerdidas) {
		this.porcentajeHorasPerdidas = porcentajeHorasPerdidas;
	}
	public double getTiempoTotalPerdido() {
		return tiempoTotalPerdido;
	}
	public void setTiempoTotalPerdido(double tiempoTotalPerdido) {
		this.tiempoTotalPerdido = tiempoTotalPerdido;
	}
	public double getTiempoTotalAtrasoInicioOperacion() {
		return tiempoTotalAtrasoInicioOperacion;
	}
	public void setTiempoTotalAtrasoInicioOperacion(double tiempoTotalAtrasoInicioOperacion) {
		this.tiempoTotalAtrasoInicioOperacion = tiempoTotalAtrasoInicioOperacion;
	}
	public String getCausalTiempoPerdido() {
		return causalTiempoPerdido;
	}
	public void setCausalTiempoPerdido(String causalTiempoPerdido) {
		this.causalTiempoPerdido = causalTiempoPerdido;
	}
	public double getTiempoTotalAtrasoOperacion() {
		return tiempoTotalAtrasoOperacion;
	}
	public void setTiempoTotalAtrasoOperacion(double tiempoTotalAtrasoOperacion) {
		this.tiempoTotalAtrasoOperacion = tiempoTotalAtrasoOperacion;
	}
	public String getHoraArriboPlanta() {
		return horaArriboPlanta;
	}
	public void setHoraArriboPlanta(String horaArriboPlanta) {
		this.horaArriboPlanta = horaArriboPlanta;
	}
	public String getHoraIngresoPlanta() {
		return horaIngresoPlanta;
	}
	public void setHoraIngresoPlanta(String horaIngresoPlanta) {
		this.horaIngresoPlanta = horaIngresoPlanta;
	}
	public String getHoraInicioOperacion() {
		return horaInicioOperacion;
	}
	public void setHoraInicioOperacion(String horaInicioOperacion) {
		this.horaInicioOperacion = horaInicioOperacion;
	}
	public String getHoraFinOperacion() {
		return horaFinOperacion;
	}
	public void setHoraFinOperacion(String horaFinOperacion) {
		this.horaFinOperacion = horaFinOperacion;
	}
	public String getHoraVerificacion() {
		return horaVerificacion;
	}
	public void setHoraVerificacion(String horaVerificacion) {
		this.horaVerificacion = horaVerificacion;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getSubDependencia() {
		return subDependencia;
	}
	public void setSubDependencia(int subDependencia) {
		this.subDependencia = subDependencia;
	}
	public String getNombreSubdependencia() {
		return nombreSubdependencia;
	}
	public void setNombreSubdependencia(String nombreSubdependencia) {
		this.nombreSubdependencia = nombreSubdependencia;
	}
	public String getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public int getMuelle() {
		return muelle;
	}
	public void setMuelle(int muelle) {
		this.muelle = muelle;
	}

}
