package com.empresa.supply.modelo.admin.usuario;

public class TipoDocumento {
	private int id;
	private String tipoDocumento;
	public TipoDocumento() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

}
