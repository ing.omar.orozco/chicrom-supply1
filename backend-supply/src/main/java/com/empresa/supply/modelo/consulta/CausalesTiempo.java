package com.empresa.supply.modelo.consulta;

public class CausalesTiempo {
	private String nombreCausal;
	private String tiempoCausal;
	private int idEstadoTicket;
	public CausalesTiempo() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getIdEstadoTicket() {
		return idEstadoTicket;
	}
	public void setIdEstadoTicket(int idEstadoTicket) {
		this.idEstadoTicket = idEstadoTicket;
	}
	public String getNombreCausal() {
		return nombreCausal;
	}
	public void setNombreCausal(String nombreCausal) {
		this.nombreCausal = nombreCausal;
	}
	public String getTiempoCausal() {
		return tiempoCausal;
	}
	public void setTiempoCausal(String tiempoCausal) {
		this.tiempoCausal = tiempoCausal;
	}

}
