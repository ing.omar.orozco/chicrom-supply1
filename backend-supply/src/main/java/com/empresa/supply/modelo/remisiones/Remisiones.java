package com.empresa.supply.modelo.remisiones;


import java.util.Date;


public class Remisiones {
	private int id;
	private long numeroRemision;
	private int idTicket;
	private String foto;
	private String tipo;
	private int estado;
	private Date fechaCreacion;
	public Remisiones() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public long getNumeroRemision() {
		return numeroRemision;
	}
	public void setNumeroRemision(long numeroRemision) {
		this.numeroRemision = numeroRemision;
	}
	public int getIdTicket() {
		return idTicket;
	}
	public void setIdTicket(int idTicket) {
		this.idTicket = idTicket;
	}
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public int getEstado() {
		return estado;
	}
	public void setEstado(int estado) {
		this.estado = estado;
	}

}
