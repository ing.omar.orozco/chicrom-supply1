package com.empresa.supply.modelo.admin.usuario;

public class CiudadUsuario {
	private int id;
	private String nombre;
	public CiudadUsuario() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
