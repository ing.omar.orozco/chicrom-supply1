package com.empresa.supply.modelo.causal.tiempos;

public class CausalTiempo {
	private int id;
	private String causal;
	private int minutosRetardo;
	private int idTicket;
	private int idEstadoTicket;
	private int causalDeTiempo;
	public CausalTiempo() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public int getCausalDeTiempo() {
		return causalDeTiempo;
	}

	public void setCausalDeTiempo(int causalDeTiempo) {
		this.causalDeTiempo = causalDeTiempo;
	}

	public int getIdTicket() {
		return idTicket;
	}
	public void setIdTicket(int idTicket) {
		this.idTicket = idTicket;
	}
	public int getIdEstadoTicket() {
		return idEstadoTicket;
	}
	public void setIdEstadoTicket(int idEstadoTicket) {
		this.idEstadoTicket = idEstadoTicket;
	}
	public int getMinutosRetardo() {
		return minutosRetardo;
	}
	public void setMinutosRetardo(int minutosRetardo) {
		this.minutosRetardo = minutosRetardo;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCausal() {
		return causal;
	}
	public void setCausal(String causal) {
		this.causal = causal;
	}

}
