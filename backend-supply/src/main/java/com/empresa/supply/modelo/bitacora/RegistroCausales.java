package com.empresa.supply.modelo.bitacora;

public class RegistroCausales {
	private int idTicket;
	private int idEstadoTicket;
	private int idCausalTiempo;
	private int retardo;
	private int estado;
	private String nombreCausalTiempo;
	public RegistroCausales() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getNombreCausalTiempo() {
		return nombreCausalTiempo;
	}
	public void setNombreCausalTiempo(String nombreCausalTiempo) {
		this.nombreCausalTiempo = nombreCausalTiempo;
	}
	public int getIdTicket() {
		return idTicket;
	}
	public void setIdTicket(int idTicket) {
		this.idTicket = idTicket;
	}
	public int getIdEstadoTicket() {
		return idEstadoTicket;
	}
	public void setIdEstadoTicket(int idEstadoTicket) {
		this.idEstadoTicket = idEstadoTicket;
	}
	public int getIdCausalTiempo() {
		return idCausalTiempo;
	}
	public void setIdCausalTiempo(int idCausalTiempo) {
		this.idCausalTiempo = idCausalTiempo;
	}
	public int getRetardo() {
		return retardo;
	}
	public void setRetardo(int retardo) {
		this.retardo = retardo;
	}
	public int getEstado() {
		return estado;
	}
	public void setEstado(int estado) {
		this.estado = estado;
	}

}
