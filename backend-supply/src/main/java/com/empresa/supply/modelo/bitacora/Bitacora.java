package com.empresa.supply.modelo.bitacora;

public class Bitacora {
	private String fechaOperacion;
	private String numeroTicket;
	private String placa;
	private String vehiculo;
	private String estado;
	private String infoInicio;
	private String subDependencia;
	private int muelle;
	private int idEstadoTicket;
	private int horas;
	private String color;
	private int idTicket;
	private String minutosRestante;
	private String tieneCausales;
	public Bitacora() {
		super();
		minutosRestante = "";
	}
	
	

	public String getTieneCausales() {
		return tieneCausales;
	}



	public void setTieneCausales(String tieneCausales) {
		this.tieneCausales = tieneCausales;
	}



	public String getMinutosRestante() {
		return minutosRestante;
	}



	public void setMinutosRestante(String minutosRestante) {
		this.minutosRestante = minutosRestante;
	}



	public int getIdTicket() {
		return idTicket;
	}

	public void setIdTicket(int idTicket) {
		this.idTicket = idTicket;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getHoras() {
		return horas;
	}

	public void setHoras(int horas) {
		this.horas = horas;
	}

	public int getIdEstadoTicket() {
		return idEstadoTicket;
	}

	public void setIdEstadoTicket(int idEstadoTicket) {
		this.idEstadoTicket = idEstadoTicket;
	}

	public String getSubDependencia() {
		return subDependencia;
	}

	public void setSubDependencia(String subDependencia) {
		this.subDependencia = subDependencia;
	}

	public int getMuelle() {
		return muelle;
	}

	public void setMuelle(int muelle) {
		this.muelle = muelle;
	}

	public String getFechaOperacion() {
		return fechaOperacion;
	}
	public void setFechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}
	public String getNumeroTicket() {
		return numeroTicket;
	}
	public void setNumeroTicket(String numeroTicket) {
		this.numeroTicket = numeroTicket;
	}
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public String getVehiculo() {
		return vehiculo;
	}
	public void setVehiculo(String vehiculo) {
		this.vehiculo = vehiculo;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getInfoInicio() {
		return infoInicio;
	}

	public void setInfoInicio(String infoInicio) {
		this.infoInicio = infoInicio;
	}
	
}
