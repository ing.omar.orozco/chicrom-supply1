package com.empresa.supply.modelo.sellos;

public class Sellos {
	private int id;
	private int idTicket;
	private int idRemision;
	private int estado;
	private String fechaCreacion;
	private String nombre;
	private int operacion;
	private int selloNumero;
	private String foto;
	
	public Sellos() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getOperacion() {
		return operacion;
	}
	public void setOperacion(int operacion) {
		this.operacion = operacion;
	}
	public int getSelloNumero() {
		return selloNumero;
	}
	public void setSelloNumero(int selloNumero) {
		this.selloNumero = selloNumero;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getIdTicket() {
		return idTicket;
	}
	public void setIdTicket(int idTicket) {
		this.idTicket = idTicket;
	}
	public int getIdRemision() {
		return idRemision;
	}
	public void setIdRemision(int idRemision) {
		this.idRemision = idRemision;
	}
	public int getEstado() {
		return estado;
	}
	public void setEstado(int estado) {
		this.estado = estado;
	}
	public String getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

}
