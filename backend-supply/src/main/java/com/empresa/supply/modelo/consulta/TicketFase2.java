package com.empresa.supply.modelo.consulta;

public class TicketFase2 {
	private int estadoTicket;
	private String fechaEstado;
	public TicketFase2() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getEstadoTicket() {
		return estadoTicket;
	}
	public void setEstadoTicket(int estadoTicket) {
		this.estadoTicket = estadoTicket;
	}
	public String getFechaEstado() {
		return fechaEstado;
	}
	public void setFechaEstado(String fechaEstado) {
		this.fechaEstado = fechaEstado;
	}

}
