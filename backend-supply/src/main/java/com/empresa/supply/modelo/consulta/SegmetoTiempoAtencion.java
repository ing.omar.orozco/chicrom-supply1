package com.empresa.supply.modelo.consulta;

public class SegmetoTiempoAtencion {
	private int id;
	private String fechaCreacion;
	private int subDependencia;
	private String nombreSubdependencia;
	private int muelle;
	private int idOperacion;
	private String operacion;
	private double tiempoEsperaInicioOperacion;
	private double tiempoOperacion;
	private double tiepoTotalPlanta; 
	private double tiempoAtencionSinAfectacion;
	private double tiempoTotalAtrasoOperacion;
	private String horaArriboPlanta;
	private String horaIngresoPlanta;
	private String horaInicioOperacion;
	private String horaFinOperacion;
	private String horaVerificacion;
	public SegmetoTiempoAtencion() {
		super();
		// TODO Auto-generated constructor stub
	}
	public double getTiempoTotalAtrasoOperacion() {
		return tiempoTotalAtrasoOperacion;
	}
	public void setTiempoTotalAtrasoOperacion(double tiempoTotalAtrasoOperacion) {
		this.tiempoTotalAtrasoOperacion = tiempoTotalAtrasoOperacion;
	}
	public String getHoraArriboPlanta() {
		return horaArriboPlanta;
	}
	public void setHoraArriboPlanta(String horaArriboPlanta) {
		this.horaArriboPlanta = horaArriboPlanta;
	}
	public String getHoraIngresoPlanta() {
		return horaIngresoPlanta;
	}
	public void setHoraIngresoPlanta(String horaIngresoPlanta) {
		this.horaIngresoPlanta = horaIngresoPlanta;
	}
	public String getHoraInicioOperacion() {
		return horaInicioOperacion;
	}
	public void setHoraInicioOperacion(String horaInicioOperacion) {
		this.horaInicioOperacion = horaInicioOperacion;
	}
	public String getHoraFinOperacion() {
		return horaFinOperacion;
	}
	public void setHoraFinOperacion(String horaFinOperacion) {
		this.horaFinOperacion = horaFinOperacion;
	}
	public String getHoraVerificacion() {
		return horaVerificacion;
	}
	public void setHoraVerificacion(String horaVerificacion) {
		this.horaVerificacion = horaVerificacion;
	}
	public double getTiempoEsperaInicioOperacion() {
		return tiempoEsperaInicioOperacion;
	}
	public void setTiempoEsperaInicioOperacion(double tiempoEsperaInicioOperacion) {
		this.tiempoEsperaInicioOperacion = tiempoEsperaInicioOperacion;
	}
	public double getTiempoOperacion() {
		return tiempoOperacion;
	}
	public void setTiempoOperacion(double tiempoOperacion) {
		this.tiempoOperacion = tiempoOperacion;
	}
	public double getTiepoTotalPlanta() {
		return tiepoTotalPlanta;
	}
	public void setTiepoTotalPlanta(double tiepoTotalPlanta) {
		this.tiepoTotalPlanta = tiepoTotalPlanta;
	}
	public double getTiempoAtencionSinAfectacion() {
		return tiempoAtencionSinAfectacion;
	}
	public void setTiempoAtencionSinAfectacion(double tiempoAtencionSinAfectacion) {
		this.tiempoAtencionSinAfectacion = tiempoAtencionSinAfectacion;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public int getSubDependencia() {
		return subDependencia;
	}
	public void setSubDependencia(int subDependencia) {
		this.subDependencia = subDependencia;
	}
	public String getNombreSubdependencia() {
		return nombreSubdependencia;
	}
	public void setNombreSubdependencia(String nombreSubdependencia) {
		this.nombreSubdependencia = nombreSubdependencia;
	}
	public int getMuelle() {
		return muelle;
	}
	public void setMuelle(int muelle) {
		this.muelle = muelle;
	}
	public int getIdOperacion() {
		return idOperacion;
	}
	public void setIdOperacion(int idOperacion) {
		this.idOperacion = idOperacion;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

}
