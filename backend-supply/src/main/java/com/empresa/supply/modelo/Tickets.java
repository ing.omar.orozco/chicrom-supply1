package com.empresa.supply.modelo;

public class Tickets {
	private int id;
	private String placa;
	private int idOperacion;
	private int idDependencia;
	private int idDependenciaDestino;
	private int idTransportista;
	private int idAuxiliares;
	private int idTipoTransporte;
	private int idUsuario;
	private int idPlanta;
	
	
	private String operacion;
	private String origen;
	private String destino;
	private String transportista;
	private String auxiliares;
	private String tipoTransporte;
	private String remisiones;
	private int muelle;
	private int subDependencia;
	private String nombreSubdependencia;
	private String nombrePlanta;
	
	private String tipoOperacion;
	private String tieneSello;
	private String cerrarTicket;
	
	
	public Tickets() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public String getCerrarTicket() {
		return cerrarTicket;
	}

	public void setCerrarTicket(String cerrarTicket) {
		this.cerrarTicket = cerrarTicket;
	}

	public String getTipoOperacion() {
		return tipoOperacion;
	}

	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}

	public String getTieneSello() {
		return tieneSello;
	}

	public void setTieneSello(String tieneSello) {
		this.tieneSello = tieneSello;
	}

	public String getNombrePlanta() {
		return nombrePlanta;
	}

	public void setNombrePlanta(String nombrePlanta) {
		this.nombrePlanta = nombrePlanta;
	}

	public int getIdPlanta() {
		return idPlanta;
	}

	public void setIdPlanta(int idPlanta) {
		this.idPlanta = idPlanta;
	}

	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}


	public String getNombreSubdependencia() {
		return nombreSubdependencia;
	}



	public void setNombreSubdependencia(String nombreSubdependencia) {
		this.nombreSubdependencia = nombreSubdependencia;
	}



	public int getMuelle() {
		return muelle;
	}



	public void setMuelle(int muelle) {
		this.muelle = muelle;
	}



	public int getSubDependencia() {
		return subDependencia;
	}



	public void setSubDependencia(int subDependencia) {
		this.subDependencia = subDependencia;
	}



	public String getRemisiones() {
		return remisiones;
	}



	public void setRemisiones(String remisiones) {
		this.remisiones = remisiones;
	}



	public String getOperacion() {
		return operacion;
	}



	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}



	public String getOrigen() {
		return origen;
	}



	public void setOrigen(String origen) {
		this.origen = origen;
	}



	public String getDestino() {
		return destino;
	}



	public void setDestino(String destino) {
		this.destino = destino;
	}



	public String getTransportista() {
		return transportista;
	}



	public void setTransportista(String transportista) {
		this.transportista = transportista;
	}



	public String getAuxiliares() {
		return auxiliares;
	}



	public void setAuxiliares(String auxiliares) {
		this.auxiliares = auxiliares;
	}



	public String getTipoTransporte() {
		return tipoTransporte;
	}



	public void setTipoTransporte(String tipoTransporte) {
		this.tipoTransporte = tipoTransporte;
	}



	public int getIdAuxiliares() {
		return idAuxiliares;
	}



	public void setIdAuxiliares(int idAuxiliares) {
		this.idAuxiliares = idAuxiliares;
	}



	public int getIdTipoTransporte() {
		return idTipoTransporte;
	}

	public void setIdTipoTransporte(int idTipoTransporte) {
		this.idTipoTransporte = idTipoTransporte;
	}

	public int getIdTransportista() {
		return idTransportista;
	}

	public void setIdTransportista(int idTransportista) {
		this.idTransportista = idTransportista;
	}

	public int getIdDependenciaDestino() {
		return idDependenciaDestino;
	}

	public void setIdDependenciaDestino(int idDependenciaDestino) {
		this.idDependenciaDestino = idDependenciaDestino;
	}

	public int getIdDependencia() {
		return idDependencia;
	}

	public void setIdDependencia(int idDependencia) {
		this.idDependencia = idDependencia;
	}

	public int getIdOperacion() {
		return idOperacion;
	}
	public void setIdOperacion(int idOperacion) {
		this.idOperacion = idOperacion;
	}
	/*public String getNota() {
		return nota;
	}
	public void setNota(String nota) {
		this.nota = nota;
	}*/
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	/*public String getDependencia() {
		return dependencia;
	}
	public void setDependencia(String dependencia) {
		this.dependencia = dependencia;
	}*/
	/*public String getTipoOperacion() {
		return tipoOperacion;
	}
	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}*/
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	/*public String getAuxiliares() {
		return auxiliares;
	}
	public void setAuxiliares(String auxiliares) {
		this.auxiliares = auxiliares;
	}*/

}
