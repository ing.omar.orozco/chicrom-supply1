package com.empresa.supply.modelo.graficos;

public class Grafico15 {
	private int viajes;
	private int minutos;
	private String fecha;
	
	public int getViajes() {
		return viajes;
	}
	public void setViajes(int viajes) {
		this.viajes = viajes;
	}
	public int getMinutos() {
		return minutos;
	}
	public void setMinutos(int minutos) {
		this.minutos = minutos;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	
}
