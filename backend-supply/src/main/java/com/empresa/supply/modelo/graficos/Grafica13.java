package com.empresa.supply.modelo.graficos;

public class Grafica13 {
	private int minutos;
	private String causal;
	
	public int getMinutos() {
		return minutos;
	}
	public void setMinutos(int minutos) {
		this.minutos = minutos;
	}
	public String getCausal() {
		return causal;
	}
	public void setCausal(String causal) {
		this.causal = causal;
	}
	
	
}
