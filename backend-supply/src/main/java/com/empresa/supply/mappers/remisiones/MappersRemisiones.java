package com.empresa.supply.mappers.remisiones;

import java.util.Date;
import java.util.List;

import javax.websocket.server.PathParam;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;


import com.empresa.supply.modelo.remisiones.Remisiones;

@Mapper
public interface MappersRemisiones {
	
	@Select("SELECT COUNT(*) FROM remisiones WHERE id_ticket = #{id} and estado = 1")
	int buscarRemisionPorId(@PathParam("id") int id);
	
	@Select("SELECT id,numero_remision numeroRemision,id_ticket idTicket,foto,tipo,estado,fecha_creacion fechaCreacion FROM remisiones WHERE id_ticket = #{idTicket} and estado = 1")
	List<Remisiones> buscarRemisionPorIdTicket(@PathParam("idTicket") int idTicket);
	
	@Select("SELECT id,numero_remision numeroRemision,id_ticket idTicket,foto,tipo,estado,fecha_creacion fechaCreacion FROM remisiones WHERE id = #{idRemision} and estado = 1")
	Remisiones buscarRemisionPorIdRemision(@PathParam("idRemision") int idRemision);
	
	@Select("UPDATE remisiones SET estado = 2 WHERE id = #{idRemision}")
	void eliminarRemisionPorId(@PathParam("idRemision") int idRemision);
	
	@Select("UPDATE remisiones SET numero_remision = #{numeroRemision},foto = #{foto},estado = #{estado} WHERE id = #{id}")
	void editarRemisionPorId(@PathParam("numeroRemision") long numeroRemision,@PathParam("foto") String foto,@PathParam("estado") int estado,@PathParam("id") int id);
	
	@Select("INSERT INTO remisiones(id,numero_remision,id_ticket,foto,tipo,estado,fecha_creacion) VALUES(#{id},#{numeroRemision},#{idTicket},#{foto},#{tipo},#{estado},#{fechaCreacion})")
	Remisiones crearRemisiones(@PathParam("id") int id,@PathParam("numeroRemision") long numeroRemision,@PathParam("idTicket") int idTicket,@PathParam("foto") String foto,@PathParam("tipo") String tipo,@PathParam("estado") int estado,@PathParam("fechaCreacion") Date fechaCreacion);

	@Select("INSERT INTO remisiones(id,numero_remision,id_ticket,tipo,estado,fecha_creacion) VALUES(#{id},#{numeroRemision},#{idTicket},#{tipo},#{estado},#{fechaCreacion})")
	Remisiones crearRemisiones_not_photo(@PathParam("id") int id,@PathParam("numeroRemision") long numeroRemision,@PathParam("idTicket") int idTicket,@PathParam("tipo") String tipo,@PathParam("estado") int estado,@PathParam("fechaCreacion") Date fechaCreacion);
	
	@Select("SELECT secuencia+1 FROM consecutivos WHERE CODIGO = #{codigo} AND estado = 1")
	int buscaConsecutivo(@PathParam("codigo") String codigo);
	
	@Select("UPDATE consecutivos SET secuencia = #{secuencia} WHERE CODIGO = #{codigo} AND estado = 1")
	void actualizaConsecutivo(@PathParam("codigo") String codigo,@PathParam("secuencia") int secuencia);
	

}
