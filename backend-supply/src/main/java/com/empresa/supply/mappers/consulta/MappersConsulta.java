package com.empresa.supply.mappers.consulta;

import java.util.List;

import javax.websocket.server.PathParam;

import org.apache.ibatis.annotations.Select;

import com.empresa.supply.modelo.consulta.CausalesTiempo;
import com.empresa.supply.modelo.consulta.CedulasAuxiliares;
import com.empresa.supply.modelo.consulta.ConsultaTicket;
import com.empresa.supply.modelo.consulta.SegmentoAnalisiTiempoPerdido;
import com.empresa.supply.modelo.consulta.SegmentoVolumen;
import com.empresa.supply.modelo.consulta.SegmetoTiempoAtencion;
import com.empresa.supply.modelo.consulta.TicketFase2;
import com.empresa.supply.modelo.consulta.VolumenTiempoOperacion;


public interface MappersConsulta {
	
	@Select("SELECT  \r\n" + 
			"				t.id id, \r\n" + 
			"				t.id_planta idPlanta, \r\n" + 
			"				pl.nombre nombrePlanta, \r\n" + 
			"				t.muelle muelle,\r\n" + 
			"				t.fecha_creacion fechaCreacion, \r\n" + 
			"				t.placa placa,\r\n" + 
			"				t.id_tipo_transporte idTipoTransporte,\r\n" + 
			"				t.id_transportista idTransportista, \r\n" + 
			"				tr.transportista nombreTransportista, \r\n" + 
			"				tt.tipo_transporte tipoTransporte, \r\n" + 
			"				t.id_operacion idOperacion, \r\n" + 
			"				op.operacion nombreOperacion, \r\n" + 
			"				r.numero_remision numeroRemision \r\n" + 
			"			FROM tickets t\r\n" + 
			"			INNER JOIN remisiones r ON t.id = r.id_ticket \r\n" + 
			"			INNER JOIN tipo_transporte tt ON tt.id = t.id_tipo_transporte\r\n" + 
			"			INNER JOIN operacion op ON op.id = t.id_operacion\r\n" + 
			"			INNER JOIN planta pl ON pl.id = t.id_planta \r\n" + 
			"			INNER JOIN transportista tr ON tr.id = t.id_transportista \r\n" + 
			"			WHERE DATE(t.fecha_creacion) BETWEEN #{fechaInicial} AND #{fechaFinal}\r\n" + 
			"			AND t.id_planta = #{idPlanta}\r\n" + 
			"			AND t.cerrado = 1")
	List<ConsultaTicket> buscarTicketCerrado(@PathParam("idPlanta") int idPlanta,@PathParam("fechaInicial") String fechaInicial,@PathParam("fechaFinal") String fechaFinal);
	
	@Select("SELECT fecha_estado fechaEstado,estado_ticket estadoTicket FROM ticket_fase2 WHERE id_ticket = #{idTicket} ORDER BY estado_ticket")
	List<TicketFase2> buscarTicketFase2(@PathParam("idTicket") int idTicket);
	
	@Select("SELECT  id,\r\n" + 
			"	fecha_creacion fechaCreacion, \r\n" + 
			"	(SELECT IFNULL(nombre,' ') FROM subdependencia WHERE id = sub_dependencia) subdependencia,\r\n" + 
			"	(SELECT tipo_transporte  FROM tipo_transporte WHERE id = id_tipo_transporte) tipoTransporte,\r\n" + 
			"	muelle muelle,\r\n" + 
			"	(SELECT COUNT(id) FROM tickets WHERE fecha_creacion BETWEEN #{fechaInicio} AND #{fechaFinal}) numeroPlaca\r\n" + 
			"FROM tickets\r\n" + 
			"WHERE fecha_creacion BETWEEN #{fechaInicio} AND #{fechaFinal} AND cerrado = 1")
	List<SegmentoVolumen> segmentoVolumen(@PathParam("fechaInicio") String fechaInicio,@PathParam("fechaFinal") String fechaFinal);
	
	@Select("SELECT c.causal nombreCausal,\r\n" + 
			"       r.minutos_retardo tiempoCausal,r.id_estado_ticket idEstadoTicket \r\n" + 
			"FROM registro_causales r\r\n" + 
			"INNER JOIN causal_tiempos c\r\n" + 
			"ON r.id_causal_tiempo = c.id\r\n" + 
			"WHERE id_ticket = #{idTicket}")
	List<CausalesTiempo> causalesConTiempos(@PathParam("idTicket") int idTicket);
	
	@Select("SELECT u.documento cedulaAuxiliares FROM auxiliares_ticket a\r\n" + 
			"INNER JOIN usuarios u\r\n" + 
			"ON a.id_auxiliar = u.id\r\n" + 
			"WHERE id_ticket = #{idTicket}")
	List<CedulasAuxiliares> cedulaAuxiliares(@PathParam("idTicket") int idTicket);
	
	@Select("SELECT tarifa FROM planta_operacion WHERE id_operacion = #{idOperacion} AND id_planta = #{idPlanta}")
	long buscaTarifa(@PathParam("idOperacion") int idOperacion,@PathParam("idPlanta") int idPlanta);
	
	
	@Select("SELECT muelle,COUNT(muelle) cantidad \r\n" + 
			"FROM tickets t\r\n" + 
			"WHERE fecha_creacion >= #{fechaInicial} AND fecha_creacion <= #{fechaFinal}\r\n" + 
			"AND (SELECT tipo FROM operacion WHERE id = id_operacion) = #{tipoOperacion}\r\n" + 
			"GROUP BY muelle\r\n" + 
			"ORDER BY muelle")
	List<VolumenTiempoOperacion> reporteVolumenTiempoOperacion(@PathParam("fechaInicial") String fechaInicial,@PathParam("fechaFinal") String fechaFinal,@PathParam("tipoOperacion") String tipoOperacion);
	
	@Select("SELECT muelle,COUNT(muelle) cantidad \r\n" + 
			"FROM tickets t\r\n" + 
			"WHERE fecha_creacion >= CURDATE()\r\n" + 
			"AND (SELECT tipo FROM operacion WHERE id = id_operacion) = #{tipoOperacion}\r\n" + 
			"GROUP BY muelle\r\n" + 
			"ORDER BY muelle")
	List<VolumenTiempoOperacion> reporteVolumenTiempoOperacionHoy(@PathParam("tipoOperacion") String tipoOperacion);
	
	
	@Select("SELECT id,	fecha_creacion fechaCreacion,\r\n" + 
			"	sub_dependencia subDependencia,\r\n" + 
			"	(SELECT nombre FROM subdependencia WHERE id = sub_dependencia) nombreSubdependencia,\r\n" + 
			"	muelle muelle,\r\n" + 
			"	id_operacion idOperacion,\r\n" + 
			"	(SELECT operacion FROM operacion WHERE id = id_operacion) operacion\r\n" + 
			"FROM tickets \r\n" + 
			"WHERE MONTH(fecha_creacion) BETWEEN #{mes} AND #{mes} AND cerrado = 1 \r\n" + 
			"ORDER BY nombreSubdependencia,muelle,operacion")
	List<SegmetoTiempoAtencion> segmetoComportamientoTiempoAtencion(@PathParam("mes") int mes);
	
	
	@Select("SELECT 	id,\r\n" + 
			"	sub_dependencia subDependencia,\r\n" + 
			"	(SELECT nombre FROM subdependencia WHERE id = sub_dependencia) nombreSubdependencia,\r\n" + 
			"	fecha_creacion fechaCreacion,\r\n" + 
			"	muelle muelle\r\n" + 
			"FROM tickets \r\n" + 
			"WHERE MONTH(fecha_creacion) BETWEEN #{mes} AND #{mes}\r\n" + 
			"AND DAYOFMONTH(fecha_creacion) BETWEEN #{dia} AND #{dia}\r\n" + 
			"AND cerrado = 1")
	List<SegmentoAnalisiTiempoPerdido> segmentoAnalisisTiempoPerdido(@PathParam("mes") int mes,@PathParam("dia") int dia);
	
	
	
	
	
}
