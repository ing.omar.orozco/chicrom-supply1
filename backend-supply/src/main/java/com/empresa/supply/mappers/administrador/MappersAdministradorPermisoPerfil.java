package com.empresa.supply.mappers.administrador;

import java.util.List;

import javax.websocket.server.PathParam;

import org.apache.ibatis.annotations.Select;

import com.empresa.supply.modelo.administrador.PermisoPerfil;
import com.empresa.supply.modelo.administrador.Permisos;


public interface MappersAdministradorPermisoPerfil {
	@Select("INSERT INTO permiso_perfil(id,id_permiso,id_perfil,estado) VALUES(#{id},#{idPermiso},#{idPerfil},#{estado})")
	void crearPermisoPerfil(@PathParam("id") int id,@PathParam("idPermiso") int idPermiso,@PathParam("idPerfil") int idPerfil,@PathParam("estado") int estado);
	
	@Select("SELECT COUNT(id) FROM permiso_perfil WHERE id_permiso = #{idPermiso} AND id_perfil = #{idPerfil}")
	int existePermisoPerfil(@PathParam("idPermiso") int idPermiso,@PathParam("idPerfil") int idPerfil);
	
	
	
	@Select("SELECT secuencia+1 FROM consecutivos WHERE CODIGO = #{codigo} AND estado = 1")
	int buscaConsecutivo(@PathParam("codigo") String codigo);
	
	@Select("UPDATE consecutivos SET secuencia = #{secuencia} WHERE CODIGO = #{codigo} AND estado = 1")
	void actualizaConsecutivo(@PathParam("codigo") String codigo,@PathParam("secuencia") int secuencia);
	
	@Select("SELECT pp.id,pp.id_permiso idPermiso,pp.id_perfil idPerfil,pp.estado , p.url nombrePermiso, per.nombre nombrePerfil\r\n" + 
			"FROM permiso_perfil pp INNER JOIN permisos p\r\n" + 
			"ON pp.id_permiso = p.id\r\n" + 
			"INNER JOIN perfiles per\r\n" + 
			"ON per.id = pp.id_perfil\r\n" + 
			"where pp.id_permiso = #{idPermiso}\r\n" +
			"AND pp.id_perfil = #{idPerfil}")
	PermisoPerfil buscarPermisoPerfil(@PathParam("idPermiso") int idPermiso,@PathParam("idPerfil") int idPerfil);
	
	/*
	@Select("SELECT pp.id,pp.id_permiso idPermiso,pp.id_perfil idPerfil,pp.estado , p.url nombrePermiso, per.nombre nombrePerfil\r\n" + 
			"FROM permiso_perfil pp INNER JOIN permisos p\r\n" + 
			"ON pp.id_permiso = p.id\r\n" + 
			"INNER JOIN perfiles per\r\n" + 
			"ON per.id = pp.id_perfil\r\n")
	List<PermisoPerfil> buscarPermisoPerfiles();*/
	
	@Select("SELECT id idPerfil,nombre nombrePerfil\r\n" + 
			"FROM  perfiles per WHERE estado = 1\r\n" + 
			"ORDER BY  per.nombre,per.estado ")
	List<PermisoPerfil> buscarPermisoPerfiles();
	
	@Select("SELECT id_permiso id,\r\n" + 
			" p.url url,\r\n" + 
			" p.estado estado     \r\n" + 
			"FROM permiso_perfil pp\r\n" + 
			"INNER JOIN permisos p ON p.id = pp.id_permiso\r\n" + 
			"WHERE id_perfil = #{idPerfil} AND pp.estado = 1")
	List<Permisos> buscarPermiso(@PathParam("idPerfil") int idPerfil);
	
	@Select("UPDATE permiso_perfil SET estado = 2 WHERE id_permiso = #{idPermiso} AND id_perfil = #{idPerfil}")
	void inactivarPermisoPerfil(@PathParam("idPermiso") int idPermiso,@PathParam("idPerfil") int idPerfil);
	
	@Select("UPDATE permiso_perfil SET estado = 1 WHERE id_permiso = #{idPermiso} AND id_perfil = #{idPerfil}")
	void activarPermisoPerfil(@PathParam("idPermiso") int idPermiso,@PathParam("idPerfil") int idPerfil);
	
	@Select("UPDATE permiso_perfil SET id_permiso = #{idPermiso}, id_perfil = #{idPerfil} WHERE id = #{id}")
	void editarPermisoPerfil(@PathParam("id") int id,@PathParam("idPermiso") int idPermiso,@PathParam("idPerfil") int idPerfil);
}
