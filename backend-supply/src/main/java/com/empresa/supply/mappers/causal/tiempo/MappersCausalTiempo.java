package com.empresa.supply.mappers.causal.tiempo;

import java.util.List;

import javax.websocket.server.PathParam;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.empresa.supply.modelo.causal.tiempos.CausalTiempo;

@Mapper
public interface MappersCausalTiempo {
	@Select("SELECT id,causal,\r\n" + 
			"       IFNULL(r.minutos_retardo,0) minutosRetardo\r\n" + 
			"FROM causal_tiempos c LEFT JOIN registro_causales r\r\n" + 
			"ON c.id=r.id_ticket\r\n" + 
			"ORDER BY causal")
	List<CausalTiempo> buscaCausalesTiempo();
	
	@Select("SELECT id_ticket idTicket,\r\n" + 
			"       id_estado_ticket idEstadoTicket,\r\n" + 
			"       id_causal_tiempo causalDeTiempo,\r\n" + 
			"       minutos_retardo  minutosRetardo\r\n" + 
			"FROM registro_causales \r\n" + 
			"WHERE id_ticket = #{idTicket} \r\n" + 
			"AND id_estado_ticket = #{estadoTicket}")
	List<CausalTiempo> buscaTiempoDeCausales(@PathParam("idTicket") int idTicket,@PathParam("estadoTicket") int estadoTicket);
	
	@Select("UPDATE registro_causales SET minutos_retardo = #{minutoRetardo} WHERE id_ticket = #{idTicket} AND id_estado_ticket = #{estadoTicket} AND id_causal_tiempo = #{idCausal}")
	void editarTiempoCausales(@PathParam("idTicket") int idTicket,@PathParam("estadoTicket") int estadoTicket,@PathParam("minutoRetardo") int minutoRetardo,@PathParam("idCausal") int idCausal);
	
	@Select("INSERT INTO registro_causales(id_ticket,id_Estado_ticket,id_causal_tiempo,minutos_retardo,estado) VALUES(#{idTicket},#{idEstadoTicket},#{idCausalTiempo},#{retardo},1)")
	void insertarCausalesDeTiempo(@PathParam("idTicket") int idTicket,@PathParam("idEstadoTicket") int idEstadoTicket,
							      @PathParam("idCausalTiempo") int idCausalTiempo,@PathParam("retardo") int retardo);
	
	@Select("SELECT COUNT(*) FROM registro_causales \r\n" + 
			"WHERE id_ticket = #{idTicket} \r\n" + 
			"AND id_estado_ticket = #{idEstadoTicket} \r\n" + 
			"AND id_causal_tiempo = #{idCausalTiempo}")
	int existeCausal(@PathParam("idTicket") int idTicket,@PathParam("idEstadoTicket") int idEstadoTicket,
		      @PathParam("idCausalTiempo") int idCausalTiempo);
	
		
}
