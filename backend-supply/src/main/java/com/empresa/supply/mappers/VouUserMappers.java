package com.empresa.supply.mappers;

import java.util.Date;
import java.util.List;

import javax.websocket.server.PathParam;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.empresa.supply.modelo.Auxiliares;
import com.empresa.supply.modelo.Dependencias;
import com.empresa.supply.modelo.Operaciones;
import com.empresa.supply.modelo.SubDependencia;
import com.empresa.supply.modelo.TicketFase2;
import com.empresa.supply.modelo.TicketInterfaz;
import com.empresa.supply.modelo.Tickets;
import com.empresa.supply.modelo.TipoTransporte;
import com.empresa.supply.modelo.Transportista;
import com.empresa.supply.modelo.UsuarioAutenticado;
import com.empresa.supply.modelo.VOUser;



@Mapper
public interface VouUserMappers {
	
	@Select("select * from usuarios")
	List<VOUser> findAll();

	@Select("select * from usuarios where id = #{id}")
	VOUser findId(@PathParam("id") int id);
	
	@Select("SELECT \r\n" + 
			"	'' clave,u.usuario usuario,per.url url,p.nombre perfil,\r\n" + 
			"	u.primer_nombre primerNombre,u.segundo_nombre segundoNombre,u.primer_apellido primerApellido,\r\n" + 
			"	u.segudo_apellido segundoApellido,cu.nombre_cargo nombreCargo,cou.numero numeroContrato,id_planta idPlanta,u.id idUsuario, u.login login, u.login_date login_date \r\n" + 
			"FROM usuarios u INNER JOIN usuario_perfil up\r\n" + 
			"			ON up.id_usuario = u.id\r\n" + 
			"			AND u.usuario = #{usuario}\r\n" + 
			"			AND u.clave = #{clave}\r\n" + 
			"			AND u.estado = 1\r\n" + 
			"			INNER JOIN permiso_perfil pp\r\n" + 
			"			ON pp.id_perfil = up.id_perfil\r\n" + 
			"			AND u.id = up.id_usuario\r\n" + 
			"			INNER JOIN permisos per \r\n" + 
			"			ON per.id= pp.id_permiso\r\n" + 
			"			INNER JOIN perfiles p\r\n" + 
			"			ON p.id = up.id_perfil\r\n" + 
			"			INNER JOIN cargo_usuario cu\r\n" + 
			"			ON cu.id = u.id_cargo_ocupacion\r\n" + 
			"			INNER JOIN contrato_usuario cou\r\n" + 
			"			ON cou.id = u.id_contrato_usuario\r\n" + 
			"			AND cu.estado = 1\r\n" + 
			"			AND p.estado = 1\r\n" + 
			"			AND pp.estado = 1\r\n" + 
			"			AND up.estado = 1\r\n" + 
			"			AND per.estado = 1")
	List<UsuarioAutenticado> autenticacionUsuario(@PathParam("usuario") String usuario,@PathParam("clave") String clave);
	
	@Select("update usuarios set login=1, login_date = now() where id=#{id};")
	void initLogin(@PathParam("id") int id);
	
	@Select("update usuarios set login=0, login_date = null where id in (SELECT m2.id FROM (SELECT * FROM `usuarios`) as m2 where m2.login_date + INTERVAL 2 MINUTE < NOW())")
	void EndLogin(@PathParam("id") int id);
	
	@Select("update usuarios set login=0, login_date = null where id=#{id};")
	void closeLogin(@PathParam("id") int id);
	
	
	@Insert("INSERT INTO tickets(id,placa,id_operacion,id_dependencia,id_dependencia_destino,id_transportista,id_auxiliar,id_tipo_transporte,estado,fecha_creacion,remisiones,sub_dependencia,muelle,id_planta,cerrado) VALUES (#{id},#{placa},#{idOperaciones},#{idDEpendencia},#{idDEpendenciaDestino},#{idTransportista},#{idAuxiliar},#{idTipoTransporte},1,#{fechaCreacion},0,#{subDependencia},#{muelle},#{idPlanta},0)")
	void crearTicket(@PathParam("id") int id,@PathParam("placa") String placa,
					 @PathParam("idOperaciones") int idOperaciones,
					 @PathParam("idDEpendencia") int idDEpendencia,@PathParam("idDEpendenciaDestino") int idDEpendenciaDestino,
					 @PathParam("idTransportista") int idTransportista,@PathParam("idAuxiliar") int idAuxiliar,@PathParam("idTipoTransporte") int idTipoTransporte,
					 @PathParam("fechaCreacion") Date fechaCreacion,@PathParam("subDependencia") int subDependencia,@PathParam("muelle") int muelle,
					 @PathParam("idPlanta") int idPlanta);
	
	
	@Select("SELECT id_planta FROM usuarios WHERE id = #{idUsuario} AND estado = 1")
	int buscaIdPlantaUsuario(@PathParam("idUsuario") int idUsuario);
	
	@Select("select id,placa,id_operacion idOperacion,id_dependencia idDependencia,id_dependencia_destino idDependenciaDestino,"
			+ "id_transportista idTransportista,id_auxiliar idAuxiliares,id_tipo_transporte idTipoTransporte,remisiones,"
			+ "sub_dependencia subDependencia,muelle,id_planta idPlanta,"
			+ "(SELECT nombre FROM planta WHERE id = id_planta AND estado = 1) nombrePlanta,"
			+ "(SELECT nombre FROM subdependencia WHERE id = sub_dependencia) nombreSubdependencia,"
			+ "(SELECT tipo FROM operacion WHERE id = (SELECT id_operacion FROM tickets WHERE id = #{id})) tipoOperacion\r\n"
			+ "from tickets where id = #{id}")
	Tickets findIdTickets(@PathParam("id") int id);
	
	/*@Select("select id,placa,id_operacion idOperacion,id_dependencia idDependencia,id_dependencia_destino idDependenciaDestino,id_transportista idTransportista,id_auxiliar idAuxiliares,id_tipo_transporte idTipoTransporte from tickets")
	List<Tickets> findIdTicketsAll();*/
	@Select("SELECT t.id id,placa placa,o.operacion operacion,d.dependencia origen,t.remisiones, \r\n" + 
			"	(SELECT nombre FROM subdependencia WHERE id = t.sub_dependencia) nombreSubdependencia,\r\n" + 
			"	muelle, \r\n" + 
			"			(SELECT dependencia FROM dependencia WHERE id = t.id_dependencia_destino) destino,\r\n" + 
			"			tr.transportista transportista, \r\n" + 
			"			(SELECT tipo FROM operacion WHERE id = (SELECT id_operacion FROM tickets WHERE id = t.id)) tipoOperacion,\r\n" + 
			"			(CASE \r\n" + 
			"			   WHEN (SELECT COUNT(id) FROM sellos WHERE id_ticket = t.id) = 0 THEN 'N' \r\n" + 
			"			   ELSE 'S' \r\n" + 
			"			END) tieneSello,\r\n" + 
			"			tt.tipo_transporte tipoTransporte,o.id idOperacion,t.id_planta idPlanta\r\n" + 
			"			FROM tickets t\r\n" + 
			"			INNER JOIN operacion AS o\r\n" + 
			"			ON t.id_operacion = o.id\r\n" + 
			"			INNER JOIN dependencia AS d \r\n" + 
			"			ON t.id_dependencia = d.id \r\n" + 
			"			INNER JOIN transportista tr \r\n" + 
			"			ON t.id_transportista = tr.id\r\n" + 
			"			INNER JOIN tipo_transporte tt \r\n" + 
			"			ON t.id_tipo_transporte = tt.id \r\n" + 
			"			WHERE t.estado = 1 and t.cerrado = 0 \r\n"+
			"			AND t.id_planta = #{idPlanta} \r\n"+
			" 		    ORDER BY t.id DESC")
	List<Tickets> findIdTicketsAll(@PathParam("idPlanta") int idPlanta);
	
	@Select("SELECT COUNT(estado_ticket) FROM ticket_fase2 WHERE id_ticket = #{idTicket}")
	int estadosTicketCompletados(@PathParam("idTicket") int idTicket);
	
	@Select("UPDATE tickets SET cerrado = 1 WHERE id = #{id}")
	void cerrarTicket(@PathParam("id") int id);
	
	@Select("UPDATE tickets SET placa = #{placa},id_operacion = #{idOperacion}, id_dependencia =#{idDependencia},id_dependencia_destino = #{idDependenciaDestino},id_transportista=#{idTransportista},id_auxiliar=#{idAuxiliar},id_tipo_transporte=#{idTipoTransporte},sub_dependencia = #{subDependencia},muelle = #{muelle} WHERE id = #{id}")
	void editIdTickets(@PathParam("id") int id,
					   @PathParam("placa") String placa,@PathParam("idOperacion") int idOperacion,@PathParam("idDependencia") int idDependencia,@PathParam("idDependenciaDestino") int idDependenciaDestino,@PathParam("idTransportista") int idTransportista,@PathParam("idAuxiliar") int idAuxiliar,@PathParam("idTipoTransporte") int idTipoTransporte,@PathParam("subDependencia") int subDependencia,@PathParam("muelle") int muelle);
	
	@Select("UPDATE tickets set remisiones = 1 WHERE id = #{id}")
	void editRemisionesTickets(@PathParam("id") int id);
	
	@Select("SELECT \r\n" + 
			"(\r\n" + 
			" CASE\r\n" + 
			"   WHEN COUNT(id) = 0 THEN 'N' \r\n" + 
			"   ELSE 'S'\r\n" + 
			" END  \r\n" + 
			")tieneSello\r\n" + 
			"FROM sellos WHERE id_ticket = #{id}")
	String tieneSellos(@PathParam("id") int id);
	
	@Select("SELECT tipo tipoOperacion FROM operacion WHERE id = (SELECT id_operacion FROM tickets WHERE id = #{id})")
	String tipoOperacion(@PathParam("id") int id);
	
	@Select("INSERT INTO auxiliares_ticket(id,id_auxiliar,id_ticket,estado) VALUES(#{id},#{idAuxiliar},#{idTicket},1)")
	void insertaAuxiliaresTickets(@PathParam("id") int id,@PathParam("idAuxiliar") int idAuxiliar,@PathParam("idTicket") int idTicket);
	
	@Select("SELECT t.id_auxiliar id,\r\n" + 
			"       CONCAT(u.primer_nombre, ' ', u.primer_apellido,' ',u.segudo_apellido) AS auxiliares \r\n" + 
			"FROM auxiliares_ticket t INNER JOIN usuarios u\r\n" + 
			"ON t.id_auxiliar = u.id\r\n" + 
			"WHERE id_ticket = #{idTicket}")
	List<Auxiliares> buscaAuxiliaresPorTicket(@PathParam("idTicket") int idTicket);
	
	@Select("DELETE FROM auxiliares_ticket WHERE id_ticket = #{idTicket}")
	void borraAuxiliaresTickets(@PathParam("idTicket") int idTicket);
	
	@Select("SELECT COUNT(*) FROM auxiliares_ticket \r\n" + 
			"WHERE id_ticket = #{idTicket} AND id_auxiliar = #{idAuxiliar}")
	int existeAuxiliar(@PathParam("idTicket") int id,@PathParam("idAuxiliar") int idAuxiliar);
	
	@Select("UPDATE auxiliares_ticket SET estado = #{estado} WHERE id_ticket = #{idTicket} AND id_auxiliar = #{idAuxiliar}")
	void actualizaAuxiliar(@PathParam("estado") int estado,@PathParam("idTicket") int idTicket,@PathParam("idAuxiliar") int idAuxiliar);
	
	
	@Select("UPDATE tickets SET estado = 2 WHERE id = #{id}")
	void eliminaIdTickets(@PathParam("id") int id);
	
	@Select("SELECT secuencia+1 FROM consecutivos WHERE CODIGO = #{codigo} AND estado = 1")
	int buscaConsecutivo(@PathParam("codigo") String codigo);
	
	@Select("UPDATE consecutivos SET secuencia = #{secuencia} WHERE CODIGO = #{codigo} AND estado = 1")
	void actualizaConsecutivo(@PathParam("codigo") String codigo,@PathParam("secuencia") int secuencia);
	
	//@Select("select id,operacion nombreOperacion from operacion where estado = 1")
	@Select("SELECT \r\n" + 
			"				p.id_operacion id,o.operacion nombreOperacion\r\n" + 
			"			FROM planta_operacion p INNER JOIN operacion o\r\n" + 
			"			ON p.id_operacion = o.id \r\n" + 
			"			AND p.id_planta = #{idPlanta} \r\n" + 
			"			AND p.estado = 1\r\n" + 
			"			AND o.estado = 1")
	List<Operaciones> buscarOperaciones(@PathParam("idPlanta") int idPlanta);
	
	@Select("select id,dependencia from dependencia where estado = 1")
	List<Dependencias> buscarDependencias();
	
	@Select("select id,transportista from transportista where estado = 1")
	List<Transportista> buscarTransportista();
	
	//@Select("select id,auxiliares from auxiliares where estado = 1")
	@Select("SELECT id, CONCAT(primer_nombre, ' ', primer_apellido,' ',segudo_apellido) AS auxiliares FROM usuarios \r\n" + 
			"WHERE id_cargo_ocupacion = 2 AND ESTADO = 1 AND id_planta = #{idPlanta}")
	List<Auxiliares> buscarAuxiliares(@PathParam("idPlanta") int idPlanta);
	
	@Select("SELECT t.id,CONCAT(u.primer_nombre, ' ', u.primer_apellido,' ',u.segudo_apellido) AS auxiliares \r\n" + 
			"FROM auxiliares_ticket t \r\n" + 
			"INNER JOIN usuarios u\r\n" + 
			"ON u.id = t.id\r\n" + 
			"WHERE t.id_ticket = #{idTicket} AND t.estado = 1")
	List<Auxiliares> buscarAuxiliaresPorTicket(@PathParam("idTicket") int idTicket);
	
	@Select("select id,tipo_transporte tipoTransporte from tipo_transporte where estado = 1")
	List<TipoTransporte> buscarTipoTransporte();
	
	@Select("SELECT COUNT(id_ticket) FROM ticket_fase2 WHERE id_ticket = #{id}")
	int buscaTicketsFase2PorId(@PathParam("id") int id);
	
	@Select("SELECT estado_ticket FROM ticket_fase2 WHERE id_ticket = #{id} and estado = 'A'")
	String buscaTicketsFase2EstadoActivo(@PathParam("id") int id);
	
	@Insert("INSERT INTO ticket_fase2(fecha_estado,estado_ticket,nota,id_ticket,estado,foto) VALUES (#{fechaEstado},#{estadoTicket},#{nota},#{idTicket},'E',#{foto})")
	void crearTicketFase2(@PathParam("fechaEstado") Date fechaEstado,@PathParam("estadoTicket") int estadoTicket,
						@PathParam("nota") String nota,@PathParam("idTicket") int idTicket,@PathParam("foto") String foto);
	
	@Select("UPDATE ticket_fase2 set nota = #{nota},foto = #{foto} WHERE id_ticket = #{idTicket} and estado_ticket = #{estadoTicket}")
	void editarTicketsFase2(@PathParam("nota") String nota,@PathParam("idTicket") int idTicket,@PathParam("foto") String foto,@PathParam("estadoTicket") int estadoTicket);
	
	@Select("UPDATE ticket_fase2 SET estado = 'I' WHERE id_ticket = #{id}")
	void actualizaTicketFase2(@PathParam("id") int id);
	
	@Select("UPDATE ticket_fase2 SET estado = 'A' WHERE id_ticket = #{id} and estado_ticket = #{estadoTicket}")
	void actualizaTicketFase2Activa(@PathParam("id") int id,@PathParam("estadoTicket") int estadoTicket);
	
	@Select("SELECT fecha_estado fechaEstado,estado_ticket estadoTicket,nota,id_ticket idTicket,estado,foto FROM ticket_fase2 WHERE id_ticket = #{id} and estado = 'A'")
	List<TicketFase2> buscaTicketsFase2(@PathParam("id") int id);
	
	@Select("SELECT fecha_estado fechaEstado,estado_ticket estadoTicket,nota,id_ticket idTicket,estado,foto FROM ticket_fase2 WHERE id_ticket = #{id} order by estado_ticket")
	List<TicketFase2> buscaTicketsFase2All(@PathParam("id") int id);
	
	@Select("SELECT e.id id,e.nombre,t.fecha_estado fechaEstado,e.id estadoTicket,IFNULL(t.nota,'') nota,t.id_ticket idTicket,IFNULL(t.estado,'I') estado,\r\n" + 
			"			IFNULL(t.foto,'') foto,\r\n" + 
			"			(SELECT tipo FROM operacion WHERE id = (SELECT id_operacion FROM tickets WHERE id = #{id})) tipoOperacion,\r\n" + 
			"			(CASE\r\n" + 
			"			  WHEN (SELECT COUNT(id) FROM sellos WHERE id_ticket = #{id}) = 0 THEN 'N'\r\n" + 
			"			  ELSE 'S'\r\n" + 
			"			END) tieneSello\r\n" + 
			"			FROM estados_tickets e LEFT JOIN ticket_fase2 t \r\n" + 
			"			ON e.id = t.id_ticket\r\n" + 
			"			AND t.id_ticket = #{id}")
	List<TicketInterfaz> buscaTicketsFase2Interfaz(@PathParam("id") int id);
	
	@Select("SELECT id,nombre FROM subdependencia WHERE estado = 1")
	List<SubDependencia> buscaSubdependencias();
}
