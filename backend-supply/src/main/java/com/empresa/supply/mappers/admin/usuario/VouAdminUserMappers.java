package com.empresa.supply.mappers.admin.usuario;

import java.util.List;

import javax.websocket.server.PathParam;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.empresa.supply.modelo.admin.usuario.AdminUsuario;
import com.empresa.supply.modelo.admin.usuario.CargoUsuario;
import com.empresa.supply.modelo.admin.usuario.CiudadUsuario;
import com.empresa.supply.modelo.admin.usuario.ContratoUsuario;
import com.empresa.supply.modelo.admin.usuario.Plantas;
import com.empresa.supply.modelo.admin.usuario.TipoDocumento;
import com.empresa.supply.modelo.admin.usuario.UsuarioPerfil;
import com.empresa.supply.modelo.administrador.Perfiles;


@Mapper
public interface VouAdminUserMappers {
	
	@Select("UPDATE usuarios\r\n" + 
			"SET estado=2\r\n" + 
			"WHERE id=#{id}")
	void eliminaUsuario(@PathParam("id") int id);
	
	@Select("UPDATE usuarios\r\n" + 
			"SET estado=1\r\n" + 
			"WHERE id=#{id}")
	void activaUsuario(@PathParam("id") int id);
	
	@Select("UPDATE usuarios\r\n" + 
			"SET estado=2\r\n" + 
			"WHERE id=#{id}")
	void inactivaUsuario(@PathParam("id") int id);
	
	@Select("SELECT secuencia+1 FROM consecutivos WHERE CODIGO = #{codigo} AND estado = 1")
	int buscaConsecutivo(@PathParam("codigo") String codigo);
	
	@Select("UPDATE consecutivos SET secuencia = #{secuencia} WHERE CODIGO = #{codigo} AND estado = 1")
	void actualizaConsecutivo(@PathParam("codigo") String codigo,@PathParam("secuencia") int secuencia);
	
	@Select("SELECT id,nombre nombrePlanta FROM planta WHERE estado = 1")
	List<Plantas> buscarPlantas();
	
	@Select("UPDATE usuario_perfil SET estado = 1 WHERE id_usuario = #{idUsuario} AND id_perfil = #{idPerfil}")
	void activarUsuarioPerfil(@PathParam("idUsuario") int idUsuario,@PathParam("idPerfil") int idPerfil);
	
	@Select("UPDATE usuario_perfil SET estado = 2 WHERE id_usuario = #{idUsuario} AND id_perfil = #{idPerfil}")
	void inactivarUsuarioPerfil(@PathParam("idUsuario") int idUsuario,@PathParam("idPerfil") int idPerfil);
	
	@Select("INSERT INTO usuario_perfil(id,id_usuario,id_perfil,estado) VALUES(#{id},#{idUsuario},#{idPerfil},#{estado})")
	List<UsuarioPerfil> usuarioPerfil(@PathParam("id") int id,@PathParam("idUsuario") int idUsuario,@PathParam("idPerfil") int idPerfil,@PathParam("estado") int estado);
	
	@Select("SELECT COUNT(id) FROM usuario_perfil WHERE id_usuario = #{idUsuario} AND id_perfil = #{idPerfil}")
	int existeUsuarioPerfil(@PathParam("idUsuario") int idUsuario,@PathParam("idPerfil") int idPerfil);
	
	@Select("SELECT  \r\n" + 
			"							u.id id, \r\n" + 
			"							u.tipo_documento tipoDocumento,\r\n" + 
			"							(SELECT tipo_documento tipoDocumento  FROM tipo_documento WHERE id = u.tipo_documento) nombretipoDocumento,\r\n" + 
			"							documento documento, u.primer_apellido primerApellido, \r\n" + 
			"							u.segudo_apellido segundoApellido, u.primer_nombre primerNombre, u.segundo_nombre segundoNombre,  \r\n" + 
			"							u.id_contrato_usuario idContratoUsuario,  \r\n" + 
			"							(SELECT numero FROM contrato_usuario WHERE id = u.id_contrato_usuario) nombreContrato,				 \r\n" + 
			"							u.id_cargo_ocupacion idCargoOcupacion,  \r\n" + 
			"							(SELECT nombre_cargo FROM cargo_usuario WHERE id = u.id_cargo_ocupacion) nombreCargo, \r\n" + 
			"							u.id_ciudad_usuario idCiudadUsuario,  \r\n" + 
			"							(SELECT nombre FROM ciudad_usuario WHERE id = u.id_ciudad_usuario) nombreCiudad,				 \r\n" + 
			"							u.usuario usuario, u.clave clave, u.estado estado,  \r\n" + 
			"							u.id_planta idPlanta,\r\n" + 
			"							(SELECT nombre FROM planta WHERE id =  u.id_planta) nombrePlanta \r\n" + 
			"						FROM usuarios u WHERE id = #{id} ORDER BY u.id DESC")
	AdminUsuario buscaUsuarioPorId(@PathParam("id") int id);
	
	@Select("SELECT DISTINCT id_usuario idUsuario FROM usuario_perfil p\r\n" + 
			"						INNER JOIN usuarios u\r\n" + 
			"						ON u.id = p.id_usuario\r\n" + 
			"						WHERE u.estado = 1")
	List<UsuarioPerfil> buscaLosUsuariosConPerfiles();
	
	@Select("SELECT id,tipo_documento tipoDocumento  FROM tipo_documento WHERE estado = 1")
	List<TipoDocumento> buscaTipoDocumento();
	
	@Select("SELECT id,nombre,estado FROM perfiles")
	List<Perfiles> buscarPerfiles();
	
	@Select("SELECT id,nombre,estado FROM perfiles WHERE id IN(\r\n" + 
			"			SELECT id_perfil FROM usuario_perfil \r\n" + 
			"			WHERE id_usuario = #{idUsuario} AND estado = 1)")
	List<Perfiles> buscarPerfilesPorId(@PathParam("idUsuario") int idUsuario);
	
	@Select("SELECT   \r\n" + 
			"				u.id id, \r\n" + 
			"				u.tipo_documento tipoDocumento,\r\n" + 
			"				(SELECT tipo_documento tipoDocumento  FROM tipo_documento WHERE id = u.tipo_documento) nombretipoDocumento,\r\n" + 
			"				documento documento, u.primer_apellido primerApellido,   \r\n" + 
			"				u.segudo_apellido segundoApellido, u.primer_nombre primerNombre, u.segundo_nombre segundoNombre,   \r\n" + 
			"				u.id_contrato_usuario idContratoUsuario, \r\n" + 
			"				(SELECT numero FROM contrato_usuario WHERE id = u.id_contrato_usuario) nombreContrato, \r\n" + 
			"				u.id_cargo_ocupacion idCargoOcupacion,  \r\n" + 
			"				(SELECT nombre_cargo FROM cargo_usuario WHERE id = u.id_cargo_ocupacion) nombreCargo,\r\n" + 
			"				u.id_ciudad_usuario idCiudadUsuario, \r\n" + 
			"				(SELECT nombre FROM ciudad_usuario WHERE id = u.id_ciudad_usuario) nombreCiudad,\r\n" + 
			"				u.usuario usuario, u.clave clave, u.estado estado, \r\n" + 
			"				u.id_planta idPlanta,\r\n" + 
			"				(SELECT nombre FROM planta WHERE id =  u.id_planta) nombrePlanta\r\n" + 
			"			    FROM usuarios u ORDER BY u.id DESC")
	List<AdminUsuario> buscarTodosLosUsuarios();
	
	@Select("SELECT   \r\n" + 
			"				u.id id, \r\n" + 
			"				u.tipo_documento tipoDocumento,\r\n" + 
			"				(SELECT tipo_documento tipoDocumento  FROM tipo_documento WHERE id = u.tipo_documento) nombretipoDocumento,\r\n" + 
			"				documento documento, u.primer_apellido primerApellido,   \r\n" + 
			"				u.segudo_apellido segundoApellido, u.primer_nombre primerNombre, u.segundo_nombre segundoNombre,   \r\n" + 
			"				u.id_contrato_usuario idContratoUsuario, \r\n" + 
			"				(SELECT numero FROM contrato_usuario WHERE id = u.id_contrato_usuario) nombreContrato, \r\n" + 
			"				u.id_cargo_ocupacion idCargoOcupacion,  \r\n" + 
			"				(SELECT nombre_cargo FROM cargo_usuario WHERE id = u.id_cargo_ocupacion) nombreCargo,\r\n" + 
			"				u.id_ciudad_usuario idCiudadUsuario, \r\n" + 
			"				(SELECT nombre FROM ciudad_usuario WHERE id = u.id_ciudad_usuario) nombreCiudad,\r\n" + 
			"				u.usuario usuario, u.clave clave, u.estado estado, \r\n" + 
			"				u.id_planta idPlanta,\r\n" + 
			"				(SELECT nombre FROM planta WHERE id =  u.id_planta) nombrePlanta\r\n" + 
			"			    FROM usuarios u WHERE u.estado = 1 ORDER BY u.id DESC")
	List<AdminUsuario> buscarTodosLosUsuariosActivos();
	
	@Select("SELECT id id,numero numero FROM contrato_usuario WHERE estado = 1 ORDER BY numero")
	List<ContratoUsuario> buscarContratoUsuarios();
	
	@Select("SELECT id id,nombre_cargo nombreCargo FROM cargo_usuario WHERE estado = 1 ORDER BY nombre_cargo")
	List<CargoUsuario> buscarCargoUsuarios();
	
	@Select("SELECT id id,nombre nombre FROM ciudad_usuario WHERE estado = 1 ORDER BY nombre")
	List<CiudadUsuario> buscarCiudadUsuarios();
	
	@Select("insert into usuarios (id,tipo_documento, documento,primer_apellido,segudo_apellido,primer_nombre,segundo_nombre,id_contrato_usuario,id_cargo_ocupacion, id_ciudad_usuario, usuario, clave, estado, id_planta)\r\n" + 
			"values (#{id}, #{tpDocumento},#{documento}, #{pApellido}, #{sApellido},#{pNombre},#{sNombre}, #{idContratoUsuario},#{idCargoOcupacion},#{idCiudadUsuario}, #{usuario}, #{clave}, #{estado}, #{idPlanta})")
	void crearUsuario(@PathParam("id") int id,@PathParam("tpDocumento") String tpDocumento,@PathParam("documento") int documento,@PathParam("pApellido") String pApellido, @PathParam("sApellido") String sApellido, @PathParam("pNombre") String pNombre, @PathParam("sNombre") String sNombre, @PathParam("idContratoUsuario") int idContratoUsuario, @PathParam("idCargoOcupacion") int idCargoOcupacion, @PathParam("idCiudadUsuario") int idCiudadUsuario, @PathParam("usuario") String usuario, @PathParam("clave") String clave, @PathParam("estado") int estado, @PathParam("idPlanta") int idPlanta);
	
	@Select("UPDATE usuarios\r\n" + 
			"SET tipo_documento=#{tpDocumento},documento=#{documento},primer_apellido=#{pApellido},segudo_apellido=#{sApellido},primer_nombre=#{pNombre},segundo_nombre=#{sNombre},id_contrato_usuario=#{idContratoUsuario},id_cargo_ocupacion=#{idCargoOcupacion}, id_ciudad_usuario=#{idCiudadUsuario}, usuario=#{usuario}, clave=#{clave}, estado=#{estado}, id_planta= #{idPlanta}\r\n" + 
			"WHERE id=#{id}")
	void editarUsuario(@PathParam("id") int id,@PathParam("tpDocumento") String tpDocumento,@PathParam("documento") int documento,@PathParam("pApellido") String pApellido, @PathParam("sApellido") String sApellido, @PathParam("pNombre") String pNombre, @PathParam("sNombre") String sNombre, @PathParam("idContratoUsuario") int idContratoUsuario, @PathParam("idCargoOcupacion") int idCargoOcupacion, @PathParam("idCiudadUsuario") int idCiudadUsuario, @PathParam("usuario") String usuario, @PathParam("clave") String clave, @PathParam("estado") int estado, @PathParam("idPlanta") int idPlanta);
	

}
