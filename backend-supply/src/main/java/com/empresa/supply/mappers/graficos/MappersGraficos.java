package com.empresa.supply.mappers.graficos;

import java.util.List;

import javax.websocket.server.PathParam;

import org.apache.ibatis.annotations.Select;

import com.empresa.supply.modelo.consulta.ConsultaTicket;
import com.empresa.supply.modelo.graficos.Grafico2;
import com.empresa.supply.modelo.graficos.Grafico3;
import com.empresa.supply.modelo.graficos.Grafico15;
import com.empresa.supply.modelo.graficos.Grafica13;
import com.empresa.supply.modelo.graficos.Graficos;


public interface MappersGraficos {
	@Select("SELECT  \r\n" + 
			"			month(t.fecha_creacion) as mes,year(t.fecha_creacion) as anio,s.nombre as dependencia,count(t.fecha_creacion) as cantidad \r\n" + 
			"			FROM tickets t, subdependencia s \r\n" + 
			"			WHERE DATE(t.fecha_creacion) BETWEEN #{fechaInicial} AND #{fechaFinal}\r\n" + 
			"			AND t.id_planta = #{idPlanta}\r\n" + 
			"			AND t.cerrado = 1 and t.sub_dependencia = s.id group by anio,mes,dependencia order by anio,mes,dependencia ")
	List<Graficos> grafico1(@PathParam("idPlanta") int idPlanta,@PathParam("fechaInicial") String fechaInicial,@PathParam("fechaFinal") String fechaFinal);
	
	@Select("SELECT  \r\n" + 
			"			o.operacion as operaciones, count(t.id) as cantidad \r\n" + 
			"			FROM tickets t, operacion o \r\n" + 
			"			WHERE DATE(t.fecha_creacion) BETWEEN #{fechaInicial} AND #{fechaFinal}\r\n" + 
			"			AND t.id_planta = #{idPlanta}\r\n" + 
			"			AND t.cerrado = 1 and o.id = t.id_operacion group by operaciones order by cantidad DESC")
	List<Grafico2> grafico2(@PathParam("idPlanta") int idPlanta,@PathParam("fechaInicial") String fechaInicial,@PathParam("fechaFinal") String fechaFinal);
	
	@Select("SELECT DISTINCT \r\n" + 
			"				t.id id, \r\n" + 
			"				t.id_planta idPlanta, \r\n" + 
			"				pl.nombre nombrePlanta, \r\n" + 
			"				t.muelle muelle,\r\n" + 
			"				t.fecha_creacion fechaCreacion, \r\n" + 
			"				month(t.fecha_creacion) as mesCreacion,\r\n" +
			"				year(t.fecha_creacion) as anioCreacion,\r\n" +
			"				IF(DATE_FORMAT(t.fecha_creacion, \"%H\") <= 6,DATE_FORMAT(date_add(t.fecha_creacion, INTERVAL -1 DAY),\"%Y-%m-%d\"),DATE_FORMAT(t.fecha_creacion,\"%Y-%m-%d\")) as fechaCreacion2,  \r\n" + 
			"				DATE_FORMAT(t.fecha_creacion,\"%H:%i:00\") as horaCreacion2, \r\n" +
			"				t.placa placa,\r\n" + 
			"				t.id_tipo_transporte idTipoTransporte,\r\n" + 
			"				t.id_transportista idTransportista, \r\n" + 
			"				tr.transportista nombreTransportista, \r\n" + 
			"				tt.tipo_transporte tipoTransporte, \r\n" + 
			"				t.id_operacion idOperacion, \r\n" + 
			"				op.operacion nombreOperacion, \r\n" + 
			"				op.tipo tipoOperacion, \r\n" + 
			"				r.numero_remision numeroRemision \r\n" + 
			"			FROM tickets t\r\n" + 
			"			INNER JOIN remisiones r ON t.id = r.id_ticket \r\n" + 
			"			INNER JOIN tipo_transporte tt ON tt.id = t.id_tipo_transporte\r\n" + 
			"			INNER JOIN operacion op ON op.id = t.id_operacion\r\n" + 
			"			INNER JOIN planta pl ON pl.id = t.id_planta \r\n" + 
			"			INNER JOIN transportista tr ON tr.id = t.id_transportista \r\n" + 
			"			WHERE DATE(t.fecha_creacion) BETWEEN #{fechaInicial} AND #{fechaFinal}\r\n" + 
			"			AND t.id_planta = #{idPlanta}\r\n" + 
			"			AND t.cerrado = 1 order by fechaCreacion2")
	List<Grafico3> grafico3(@PathParam("idPlanta") int idPlanta,@PathParam("fechaInicial") String fechaInicial,@PathParam("fechaFinal") String fechaFinal);
	
	@Select("SELECT DISTINCT \r\n" + 
			"				t.id id, \r\n" + 
			"				t.id_planta idPlanta, \r\n" + 
			"				pl.nombre nombrePlanta, \r\n" + 
			"				t.muelle muelle,\r\n" + 
			"				t.fecha_creacion fechaCreacion, \r\n" + 
			"				IF(DATE_FORMAT(t.fecha_creacion, \"%H\") <= 6,DATE_FORMAT(date_add(t.fecha_creacion, INTERVAL -1 DAY),\"%Y/%m/%d\"),DATE_FORMAT(t.fecha_creacion,\"%Y/%m/%d\")) as fechaCreacion2,  \r\n" + 
			"				t.placa placa,\r\n" + 
			"				t.id_tipo_transporte idTipoTransporte,\r\n" + 
			"				t.id_transportista idTransportista, \r\n" + 
			"				tr.transportista nombreTransportista, \r\n" + 
			"				tt.tipo_transporte tipoTransporte, \r\n" + 
			"				t.id_operacion idOperacion, \r\n" + 
			"				op.operacion nombreOperacion, \r\n" + 
			"				op.tipo tipoOperacion, \r\n" + 
			"				r.numero_remision numeroRemision \r\n" + 
			"			FROM tickets t\r\n" + 
			"			INNER JOIN remisiones r ON t.id = r.id_ticket \r\n" + 
			"			INNER JOIN tipo_transporte tt ON tt.id = t.id_tipo_transporte\r\n" + 
			"			INNER JOIN operacion op ON op.id = t.id_operacion\r\n" + 
			"			INNER JOIN planta pl ON pl.id = t.id_planta \r\n" + 
			"			INNER JOIN transportista tr ON tr.id = t.id_transportista \r\n" + 
			"			WHERE DATE(t.fecha_creacion) BETWEEN #{fechaInicial} AND #{fechaFinal}\r\n" + 
			"			AND t.id_planta = #{idPlanta}\r\n" + 
			"			AND t.cerrado = 1 AND t.sub_dependencia = 1 order by muelle,fechaCreacion")
	List<Grafico3> grafico4(@PathParam("idPlanta") int idPlanta,@PathParam("fechaInicial") String fechaInicial,@PathParam("fechaFinal") String fechaFinal);
	
	@Select("SELECT DISTINCT \r\n" + 
			"				t.id id, \r\n" + 
			"				t.id_planta idPlanta, \r\n" + 
			"				pl.nombre nombrePlanta, \r\n" + 
			"				t.muelle muelle,\r\n" + 
			"				t.fecha_creacion fechaCreacion, \r\n" + 
			"				IF(DATE_FORMAT(t.fecha_creacion, \"%H\") <= 6,DATE_FORMAT(date_add(t.fecha_creacion, INTERVAL -1 DAY),\"%Y/%m/%d\"),DATE_FORMAT(t.fecha_creacion,\"%Y/%m/%d\")) as fechaCreacion2,  \r\n" + 
			"				t.placa placa,\r\n" + 
			"				t.id_tipo_transporte idTipoTransporte,\r\n" + 
			"				t.id_transportista idTransportista, \r\n" + 
			"				tr.transportista nombreTransportista, \r\n" + 
			"				tt.tipo_transporte tipoTransporte, \r\n" + 
			"				t.id_operacion idOperacion, \r\n" + 
			"				op.operacion nombreOperacion, \r\n" + 
			"				op.tipo tipoOperacion, \r\n" + 
			"				r.numero_remision numeroRemision \r\n" + 
			"			FROM tickets t\r\n" + 
			"			INNER JOIN remisiones r ON t.id = r.id_ticket \r\n" + 
			"			INNER JOIN tipo_transporte tt ON tt.id = t.id_tipo_transporte\r\n" + 
			"			INNER JOIN operacion op ON op.id = t.id_operacion\r\n" + 
			"			INNER JOIN planta pl ON pl.id = t.id_planta \r\n" + 
			"			INNER JOIN transportista tr ON tr.id = t.id_transportista \r\n" + 
			"			WHERE DATE(t.fecha_creacion) BETWEEN #{fechaInicial} AND #{fechaFinal}\r\n" + 
			"			AND t.id_planta = #{idPlanta}\r\n" + 
			"			AND t.cerrado = 1 AND op.tipo = 'C' order by muelle,fechaCreacion")
	List<Grafico3> grafico5_c(@PathParam("idPlanta") int idPlanta,@PathParam("fechaInicial") String fechaInicial,@PathParam("fechaFinal") String fechaFinal);
	
	@Select("SELECT DISTINCT \r\n" + 
			"				t.id id, \r\n" + 
			"				t.id_planta idPlanta, \r\n" + 
			"				pl.nombre nombrePlanta, \r\n" + 
			"				t.muelle muelle,\r\n" + 
			"				t.fecha_creacion fechaCreacion, \r\n" + 
			"				IF(DATE_FORMAT(t.fecha_creacion, \"%H\") <= 6,DATE_FORMAT(date_add(t.fecha_creacion, INTERVAL -1 DAY),\"%Y/%m/%d\"),DATE_FORMAT(t.fecha_creacion,\"%Y/%m/%d\")) as fechaCreacion2,  \r\n" + 
			"				t.placa placa,\r\n" + 
			"				t.id_tipo_transporte idTipoTransporte,\r\n" + 
			"				t.id_transportista idTransportista, \r\n" + 
			"				tr.transportista nombreTransportista, \r\n" + 
			"				tt.tipo_transporte tipoTransporte, \r\n" + 
			"				t.id_operacion idOperacion, \r\n" + 
			"				op.operacion nombreOperacion, \r\n" + 
			"				op.tipo tipoOperacion, \r\n" + 
			"				r.numero_remision numeroRemision \r\n" + 
			"			FROM tickets t\r\n" + 
			"			INNER JOIN remisiones r ON t.id = r.id_ticket \r\n" + 
			"			INNER JOIN tipo_transporte tt ON tt.id = t.id_tipo_transporte\r\n" + 
			"			INNER JOIN operacion op ON op.id = t.id_operacion\r\n" + 
			"			INNER JOIN planta pl ON pl.id = t.id_planta \r\n" + 
			"			INNER JOIN transportista tr ON tr.id = t.id_transportista \r\n" + 
			"			WHERE DATE(t.fecha_creacion) BETWEEN #{fechaInicial} AND #{fechaFinal}\r\n" + 
			"			AND t.id_planta = #{idPlanta}\r\n" + 
			"			AND t.cerrado = 1 AND op.tipo = 'D' order by muelle,fechaCreacion")
	List<Grafico3> grafico5_d(@PathParam("idPlanta") int idPlanta,@PathParam("fechaInicial") String fechaInicial,@PathParam("fechaFinal") String fechaFinal);
	
	@Select("SELECT  \r\n" + 
			"				t.id id, \r\n" + 
			"				t.id_planta idPlanta, \r\n" + 
			"				pl.nombre nombrePlanta, \r\n" + 
			"				t.muelle muelle,\r\n" + 
			"				t.fecha_creacion fechaCreacion, \r\n" + 
			"				IF(DATE_FORMAT(t.fecha_creacion, \"%H\") <= 6,DATE_FORMAT(date_add(t.fecha_creacion, INTERVAL -1 DAY),\"%Y/%m/%d\"),DATE_FORMAT(t.fecha_creacion,\"%Y/%m/%d\")) as fechaCreacion2,  \r\n" +
			"				t.placa placa,\r\n" + 
			"				t.id_tipo_transporte idTipoTransporte,\r\n" + 
			"				t.id_transportista idTransportista, \r\n" + 
			"				tr.transportista nombreTransportista, \r\n" + 
			"				tt.tipo_transporte tipoTransporte, \r\n" + 
			"				t.id_operacion idOperacion, \r\n" + 
			"				op.operacion nombreOperacion, \r\n" + 
			"				op.tipo tipoOperacion, \r\n" + 
			"				r.numero_remision numeroRemision \r\n" + 
			"			FROM tickets t\r\n" + 
			"			INNER JOIN remisiones r ON t.id = r.id_ticket \r\n" + 
			"			INNER JOIN tipo_transporte tt ON tt.id = t.id_tipo_transporte\r\n" + 
			"			INNER JOIN operacion op ON op.id = t.id_operacion\r\n" + 
			"			INNER JOIN planta pl ON pl.id = t.id_planta \r\n" + 
			"			INNER JOIN transportista tr ON tr.id = t.id_transportista \r\n" + 
			"			WHERE DATE(t.fecha_creacion) BETWEEN #{fechaInicial} AND #{fechaFinal}\r\n" + 
			"			AND t.id_planta = #{idPlanta}\r\n" + 
			"			AND t.cerrado = 1 order by muelle,fechaCreacion2")
	List<Grafico3> grafico6(@PathParam("idPlanta") int idPlanta,@PathParam("fechaInicial") String fechaInicial,@PathParam("fechaFinal") String fechaFinal);
	
	@Select("SELECT DISTINCT \r\n" + 
			"				t.id id, \r\n" + 
			"				t.id_planta idPlanta, \r\n" + 
			"				pl.nombre nombrePlanta, \r\n" + 
			"				t.muelle muelle,\r\n" +
			"				month(t.fecha_creacion) as mesCreacion,\r\n" +
			"				year(t.fecha_creacion) as anioCreacion,\r\n" +
			"				t.fecha_creacion fechaCreacion, \r\n" + 
			"				IF(DATE_FORMAT(t.fecha_creacion, \"%H\") <= 6,DATE_FORMAT(date_add(t.fecha_creacion, INTERVAL -1 DAY),\"%Y-%m-%d\"),DATE_FORMAT(t.fecha_creacion,\"%Y-%m-%d\")) as fechaCreacion2,  \r\n" + 
			"				t.placa placa,\r\n" + 
			"				t.id_tipo_transporte idTipoTransporte,\r\n" + 
			"				t.id_transportista idTransportista, \r\n" + 
			"				tr.transportista nombreTransportista, \r\n" + 
			"				tt.tipo_transporte tipoTransporte, \r\n" + 
			"				t.id_operacion idOperacion, \r\n" + 
			"				op.operacion nombreOperacion, \r\n" + 
			"				op.tipo tipoOperacion, \r\n" + 
			"				r.numero_remision numeroRemision \r\n" + 
			"			FROM tickets t\r\n" + 
			"			INNER JOIN remisiones r ON t.id = r.id_ticket \r\n" + 
			"			INNER JOIN tipo_transporte tt ON tt.id = t.id_tipo_transporte\r\n" + 
			"			INNER JOIN operacion op ON op.id = t.id_operacion\r\n" + 
			"			INNER JOIN planta pl ON pl.id = t.id_planta \r\n" + 
			"			INNER JOIN transportista tr ON tr.id = t.id_transportista \r\n" + 
			"			WHERE DATE(t.fecha_creacion) BETWEEN #{fechaInicial} AND #{fechaFinal}\r\n" + 
			"			AND t.id_planta = #{idPlanta}\r\n" + 
			"			AND t.cerrado = 1 order by fechaCreacion2")
	List<Grafico3> grafico8(@PathParam("idPlanta") int idPlanta,@PathParam("fechaInicial") String fechaInicial,@PathParam("fechaFinal") String fechaFinal);
	
	
	@Select("SELECT DISTINCT \r\n" + 
			"				ct.causal as causal, \r\n" + 
			"				sum(rc.minutos_retardo) as minutos \r\n" + 			
			"			FROM tickets t\r\n" + 
			"			INNER JOIN remisiones r ON t.id = r.id_ticket \r\n" + 
			"			INNER JOIN tipo_transporte tt ON tt.id = t.id_tipo_transporte\r\n" + 
			"			INNER JOIN operacion op ON op.id = t.id_operacion\r\n" + 
			"			INNER JOIN planta pl ON pl.id = t.id_planta \r\n" + 
			"			INNER JOIN transportista tr ON tr.id = t.id_transportista \r\n" + 
			"			INNER JOIN registro_causales rc ON rc.id_ticket = t.id \r\n" + 
			"			INNER JOIN causal_tiempos ct ON ct.id = rc.id_causal_tiempo \r\n" + 
			"			WHERE DATE(t.fecha_creacion) BETWEEN #{fechaInicial} AND #{fechaFinal}\r\n" + 
			"			AND t.id_planta = #{idPlanta}\r\n" + 
			"			AND t.cerrado = 1 group by ct.causal order by ct.causal")
	List<Grafica13> grafico13(@PathParam("idPlanta") int idPlanta,@PathParam("fechaInicial") String fechaInicial,@PathParam("fechaFinal") String fechaFinal);
	
	@Select("SELECT DISTINCT \r\n" + 
			"				ct.causal as causal, \r\n" + 
			"				sum(rc.minutos_retardo) as minutos \r\n" + 			
			"			FROM tickets t\r\n" + 
			"			INNER JOIN remisiones r ON t.id = r.id_ticket \r\n" + 
			"			INNER JOIN tipo_transporte tt ON tt.id = t.id_tipo_transporte\r\n" + 
			"			INNER JOIN operacion op ON op.id = t.id_operacion\r\n" + 
			"			INNER JOIN planta pl ON pl.id = t.id_planta \r\n" + 
			"			INNER JOIN transportista tr ON tr.id = t.id_transportista \r\n" + 
			"			INNER JOIN registro_causales rc ON rc.id_ticket = t.id \r\n" + 
			"			INNER JOIN causal_tiempos ct ON ct.id = rc.id_causal_tiempo \r\n" + 
			"			WHERE DATE(t.fecha_creacion) BETWEEN #{fechaInicial} AND #{fechaFinal}\r\n" + 
			"			AND t.id_planta = #{idPlanta}\r\n" + 
			"			AND t.cerrado = 1 AND rc.id_estado_ticket = 3  group by ct.causal order by ct.causal")
	List<Grafica13> grafico13_inicio(@PathParam("idPlanta") int idPlanta,@PathParam("fechaInicial") String fechaInicial,@PathParam("fechaFinal") String fechaFinal);
	
	@Select("SELECT DISTINCT \r\n" + 
			"				ct.causal as causal, \r\n" + 
			"				sum(rc.minutos_retardo) as minutos \r\n" + 			
			"			FROM tickets t\r\n" + 
			"			INNER JOIN remisiones r ON t.id = r.id_ticket \r\n" + 
			"			INNER JOIN tipo_transporte tt ON tt.id = t.id_tipo_transporte\r\n" + 
			"			INNER JOIN operacion op ON op.id = t.id_operacion\r\n" + 
			"			INNER JOIN planta pl ON pl.id = t.id_planta \r\n" + 
			"			INNER JOIN transportista tr ON tr.id = t.id_transportista \r\n" + 
			"			INNER JOIN registro_causales rc ON rc.id_ticket = t.id \r\n" + 
			"			INNER JOIN causal_tiempos ct ON ct.id = rc.id_causal_tiempo \r\n" + 
			"			WHERE DATE(t.fecha_creacion) BETWEEN #{fechaInicial} AND #{fechaFinal}\r\n" + 
			"			AND t.id_planta = #{idPlanta}\r\n" + 
			"			AND t.cerrado = 1 AND rc.id_estado_ticket = 4 group by ct.causal order by ct.causal")
	List<Grafica13> grafico13_operacion(@PathParam("idPlanta") int idPlanta,@PathParam("fechaInicial") String fechaInicial,@PathParam("fechaFinal") String fechaFinal);
	
	@Select("SELECT DISTINCT \r\n" + 
			"				t.muelle as causal,\r\n" +
			"				sum(rc.minutos_retardo) as minutos \r\n" + 			
			"			FROM tickets t\r\n" + 
			"			INNER JOIN remisiones r ON t.id = r.id_ticket \r\n" + 
			"			INNER JOIN tipo_transporte tt ON tt.id = t.id_tipo_transporte\r\n" + 
			"			INNER JOIN operacion op ON op.id = t.id_operacion\r\n" + 
			"			INNER JOIN planta pl ON pl.id = t.id_planta \r\n" + 
			"			INNER JOIN transportista tr ON tr.id = t.id_transportista \r\n" + 
			"			INNER JOIN registro_causales rc ON rc.id_ticket = t.id \r\n" + 
			"			INNER JOIN causal_tiempos ct ON ct.id = rc.id_causal_tiempo \r\n" + 
			"			WHERE DATE(t.fecha_creacion) BETWEEN #{fechaInicial} AND #{fechaFinal}\r\n" + 
			"			AND t.id_planta = #{idPlanta}\r\n" + 
			"			AND t.cerrado = 1 group by t.muelle order by t.muelle")
	List<Grafica13> grafico14(@PathParam("idPlanta") int idPlanta,@PathParam("fechaInicial") String fechaInicial,@PathParam("fechaFinal") String fechaFinal);
	
	@Select("SELECT DISTINCT \r\n" + 
			"				t.muelle as causal,\r\n" +
			"				sum(rc.minutos_retardo) as minutos \r\n" + 					
			"			FROM tickets t\r\n" + 
			"			INNER JOIN remisiones r ON t.id = r.id_ticket \r\n" + 
			"			INNER JOIN tipo_transporte tt ON tt.id = t.id_tipo_transporte\r\n" + 
			"			INNER JOIN operacion op ON op.id = t.id_operacion\r\n" + 
			"			INNER JOIN planta pl ON pl.id = t.id_planta \r\n" + 
			"			INNER JOIN transportista tr ON tr.id = t.id_transportista \r\n" + 
			"			INNER JOIN registro_causales rc ON rc.id_ticket = t.id \r\n" + 
			"			INNER JOIN causal_tiempos ct ON ct.id = rc.id_causal_tiempo \r\n" + 
			"			WHERE DATE(t.fecha_creacion) BETWEEN #{fechaInicial} AND #{fechaFinal}\r\n" + 
			"			AND t.id_planta = #{idPlanta}\r\n" + 
			"			AND t.cerrado = 1 AND rc.id_estado_ticket = 3  group by t.muelle order by t.muelle")
	List<Grafica13> grafico14_inicio(@PathParam("idPlanta") int idPlanta,@PathParam("fechaInicial") String fechaInicial,@PathParam("fechaFinal") String fechaFinal);
	
	@Select("SELECT DISTINCT \r\n" + 
			"				t.muelle as causal,\r\n" +
			"				sum(rc.minutos_retardo) as minutos \r\n" + 		 			
			"			FROM tickets t\r\n" + 
			"			INNER JOIN remisiones r ON t.id = r.id_ticket \r\n" + 
			"			INNER JOIN tipo_transporte tt ON tt.id = t.id_tipo_transporte\r\n" + 
			"			INNER JOIN operacion op ON op.id = t.id_operacion\r\n" + 
			"			INNER JOIN planta pl ON pl.id = t.id_planta \r\n" + 
			"			INNER JOIN transportista tr ON tr.id = t.id_transportista \r\n" + 
			"			INNER JOIN registro_causales rc ON rc.id_ticket = t.id \r\n" + 
			"			INNER JOIN causal_tiempos ct ON ct.id = rc.id_causal_tiempo \r\n" + 
			"			WHERE DATE(t.fecha_creacion) BETWEEN #{fechaInicial} AND #{fechaFinal}\r\n" + 
			"			AND t.id_planta = #{idPlanta}\r\n" + 
			"			AND t.cerrado = 1 AND rc.id_estado_ticket = 4 group by t.muelle order by t.muelle")
	List<Grafica13> grafico14_operacion(@PathParam("idPlanta") int idPlanta,@PathParam("fechaInicial") String fechaInicial,@PathParam("fechaFinal") String fechaFinal);
	
	@Select("SELECT DISTINCT \r\n" + 
			"				IF(DATE_FORMAT(t.fecha_creacion, \"%H\") <= 6,DATE_FORMAT(date_add(t.fecha_creacion, INTERVAL -1 DAY),\"%Y-%m-%d\"),DATE_FORMAT(t.fecha_creacion,\"%Y-%m-%d\")) as fecha,  \r\n" + 
			"				sum(rc.minutos_retardo) as minutos, \r\n" + 	
			"				count(rc.minutos_retardo) as viajes \r\n" + 	
			"			FROM tickets t\r\n" + 
			"			INNER JOIN remisiones r ON t.id = r.id_ticket \r\n" + 
			"			INNER JOIN tipo_transporte tt ON tt.id = t.id_tipo_transporte\r\n" + 
			"			INNER JOIN operacion op ON op.id = t.id_operacion\r\n" + 
			"			INNER JOIN planta pl ON pl.id = t.id_planta \r\n" + 
			"			INNER JOIN transportista tr ON tr.id = t.id_transportista \r\n" + 
			"			INNER JOIN registro_causales rc ON rc.id_ticket = t.id \r\n" + 
			"			INNER JOIN causal_tiempos ct ON ct.id = rc.id_causal_tiempo \r\n" + 
			"			WHERE DATE(t.fecha_creacion) BETWEEN #{fechaInicial} AND #{fechaFinal}\r\n" + 
			"			AND t.id_planta = #{idPlanta}\r\n" + 
			"			AND t.cerrado = 1 group by fecha order by fecha")
	List<Grafico15> grafico15(@PathParam("idPlanta") int idPlanta,@PathParam("fechaInicial") String fechaInicial,@PathParam("fechaFinal") String fechaFinal);
	
	
}
