package com.empresa.supply.mappers.bitacora;

import java.util.List;

import javax.websocket.server.PathParam;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import com.empresa.supply.modelo.bitacora.Bitacora;
import com.empresa.supply.modelo.bitacora.HoraOperacion;
import com.empresa.supply.modelo.bitacora.RegistroCausales;


@Mapper
public interface MappersBitacora {
	@Select("SELECT t.fecha_creacion fechaOperacion,t.id numeroTicket,t.placa,tt.tipo_transporte vehiculo,t.id idTicket, \r\n" + 
			"			       (SELECT COUNT(*) FROM ticket_fase2 WHERE id_ticket = t.id) idEstadoTicket,\r\n" + 
			"			       IFNULL(( \r\n" + 
			"				SELECT valor  \r\n" + 
			"			        FROM horas_operacion  \r\n" + 
			"			       WHERE id_operacion = 1 AND estado = 1  \r\n" + 
			"			        AND id_estado_ticket = (SELECT COUNT(*) FROM ticket_fase2 WHERE id_ticket = t.id) \r\n" + 
			"			       ),0) horas,\r\n" + 
			"			       IFNULL((SELECT nombre FROM subdependencia WHERE id = t.sub_dependencia AND estado = 1),'') subDependencia, \r\n" + 
			"			       t.muelle muelle, \r\n" + 
			"						( \r\n" + 
			"						SELECT CASE  COUNT(id_ticket) WHEN 0 THEN 'NINGUNO' \r\n" + 
			"									      WHEN 1 THEN 'ARRIBO A PLANTA'  \r\n" + 
			"									      WHEN 2 THEN 'INGRESO A PLANTA'  \r\n" + 
			"									      WHEN 3 THEN 'INICIO OPERACION'  \r\n" + 
			"									      WHEN 4 THEN 'FINAL OPERACION' \r\n" + 
			"									      WHEN 5 THEN 'VERIFICACION' \r\n" + 
			"						       END \r\n" + 
			"						FROM ticket_fase2 WHERE id_ticket = t.id  \r\n" + 
			"						) estado, \r\n" + 
			"						( \r\n" + 
			"						SELECT CASE  COUNT(id_ticket) WHEN 0 THEN '' \r\n" + 
			"									      WHEN 1 THEN IFNULL((SELECT fecha_estado FROM ticket_fase2 WHERE id_ticket = t.id AND estado_ticket = 1),'') \r\n" + 
			"									      WHEN 2 THEN IFNULL((SELECT fecha_estado FROM ticket_fase2 WHERE id_ticket = t.id AND estado_ticket = 2),'')  \r\n" + 
			"									      WHEN 3 THEN IFNULL((SELECT fecha_estado FROM ticket_fase2 WHERE id_ticket = t.id AND estado_ticket = 3),'')  \r\n" + 
			"									      WHEN 4 THEN IFNULL((SELECT fecha_estado FROM ticket_fase2 WHERE id_ticket = t.id AND estado_ticket = 4),'') \r\n" + 
			"									      WHEN 5 THEN IFNULL((SELECT fecha_estado FROM ticket_fase2 WHERE id_ticket = t.id AND estado_ticket = 5),'') \r\n" + 
			"						         \r\n" + 
			"						       END \r\n" + 
			"						FROM ticket_fase2 WHERE id_ticket = t.id \r\n" + 
			"						) infoInicio \r\n" + 
			"						FROM tickets t INNER JOIN  tipo_transporte tt\r\n" + 
			"						ON t.id_tipo_transporte = tt.id \r\n" + 
			"						WHERE t.id_planta = #{idPlanta} and t.estado = 1 and t.cerrado = 0\r\n" + 
			"						ORDER BY t.fecha_creacion DESC")
	List<Bitacora> buscarBitacora(@PathParam("idPlanta") int idPlanta);
	
	@Select("SELECT id_estado_ticket idEstadoTicket,valor minutos FROM horas_operacion WHERE id_operacion = #{idOperacion} AND estado = 1")
	List<HoraOperacion> buscaHoraOperacion(@PathParam("idOperacion") int idOperacion);
	
	@Select("INSERT INTO registro_causales(id_ticket,id_Estado_ticket,id_causal_tiempo,minutos_retardo,estado) VALUES(#{idTicket},#{idEstadoTicket},#{idCausalTiempo},#{retardo},1)")
	void insertarCausalesDeTiempo(@PathParam("idTicket") int idTicket,@PathParam("idEstadoTicket") int idEstadoTicket,
							      @PathParam("idCausalTiempo") int idCausalTiempo,@PathParam("retardo") int retardo);
	
	@Select("SELECT  r.id_ticket idTicket,r.id_estado_ticket idEstadoTicket,\r\n" + 
			"	r.id_causal_tiempo idCausalTiempo,\r\n" + 
			"	r.minutos_retardo retardo,r.estado,c.causal nombreCausalTiempo\r\n" + 
			"FROM registro_causales r INNER JOIN causal_tiempos c\r\n" + 
			"ON r.id_causal_tiempo = c.id\r\n" + 
			"WHERE id_ticket = #{idTicket} \r\n" + 
			"AND id_estado_ticket = #{idEstadoTicket} \r\n" + 
			"AND id_causal_tiempo = #{idCausalTiempo}")
	RegistroCausales buscaRegistroCausal(@PathParam("idTicket") int idTicket,@PathParam("idEstadoTicket") int idEstadoTicket,
		                                 @PathParam("idCausalTiempo") int idCausalTiempo);
	
	@Select("SELECT  r.id_ticket idTicket,r.id_estado_ticket idEstadoTicket,\r\n" + 
			"	r.id_causal_tiempo idCausalTiempo,\r\n" + 
			"	r.minutos_retardo retardo,r.estado,c.causal nombreCausalTiempo\r\n" + 
			"FROM registro_causales r INNER JOIN causal_tiempos c\r\n" + 
			"ON r.id_causal_tiempo = c.id\r\n" + 
			"WHERE id_ticket = #{idTicket} \r\n" + 
			"AND id_estado_ticket = #{idEstadoTicket}")
	List<RegistroCausales> buscaRegistroCausal2(@PathParam("idTicket") int idTicket,@PathParam("idEstadoTicket") int idEstadoTicket);
	
	@Select("SELECT  r.id_ticket idTicket,r.id_estado_ticket idEstadoTicket,\r\n" + 
			"	r.id_causal_tiempo idCausalTiempo,\r\n" + 
			"	r.minutos_retardo retardo,r.estado,c.causal nombreCausalTiempo\r\n" + 
			"FROM registro_causales r INNER JOIN causal_tiempos c\r\n" + 
			"ON r.id_causal_tiempo = c.id\r\n" + 
			"WHERE id_ticket = #{idTicket} \r\n" + 
			"AND id_estado_ticket = #{idEstadoTicket}")
	List<RegistroCausales> buscarCausalesDeTiempoPorTicketEstado(@PathParam("idTicket") int idTicket,@PathParam("idEstadoTicket") int idEstadoTicket);
	
	@Select("SELECT r.id_ticket idTicket,r.id_estado_ticket idEstadoTicket,r.id_causal_tiempo idCausalTiempo,r.minutos_retardo retardo,r.estado,\r\n" + 
			"c.causal nombreCausalTiempo\r\n" + 
			"FROM registro_causales r INNER JOIN causal_tiempos c\r\n" + 
			"ON r.id_causal_tiempo = c.id")
	List<RegistroCausales> buscarTodaCausalesDeTiempo();
	

	@Select("UPDATE registro_causales SET minutos_retardo = #{tiempoMinuto} where id_ticket = #{idTicket} and id_estado_ticket = #{idEstadoTicket} and id_causal_tiempo = #{idCausalTiempo}")
	void editarTiempoCausalesDeTiempo(@PathParam("idTicket") int idTicket,@PathParam("idEstadoTicket") int idEstadoTicket,
            @PathParam("idCausalTiempo") int idCausalTiempo,@PathParam("tiempoMinuto") int tiempoMinuto);
	
	
}
