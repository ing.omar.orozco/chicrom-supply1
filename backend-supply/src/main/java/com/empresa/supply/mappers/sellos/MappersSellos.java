package com.empresa.supply.mappers.sellos;

import java.util.Date;
import java.util.List;

import javax.websocket.server.PathParam;

import org.apache.ibatis.annotations.Select;

import com.empresa.supply.modelo.sellos.Sellos;

public interface MappersSellos {
	
	@Select("INSERT INTO sellos(id,id_ticket,id_remision,estado,fecha_creacion) VALUES(#{id},#{idTicket},#{idRemision},#{estado},#{fechaCreacion})")
	void crearSellos(@PathParam("id") int id,@PathParam("idTicket") int idTicket,@PathParam("idRemision") int idRemision,@PathParam("estado") int estado,@PathParam("fechaCreacion") Date fechaCreacion);
	
	@Select("INSERT INTO sellos_detalle(id_sellos,nombre,operacion,sello_numero,foto) VALUES(#{idSellos},#{nombre},#{operacion},#{selloNumero},#{foto})")
	void crearDetalleSellos(@PathParam("idSellos") int idSellos,@PathParam("nombre") String nombre,@PathParam("operacion") int operacion,@PathParam("selloNumero") int selloNumero,@PathParam("foto") String foto);
	
	@Select("SELECT s.id id,s.id_ticket idTicket,s.id_remision idRemision,s.estado estado,d.nombre nombre,d.operacion operacion,d.sello_numero selloNumero,s.fecha_creacion fechaCreacion,d.foto foto \r\n" + 
			"FROM sellos s INNER JOIN sellos_detalle d\r\n" + 
			"ON s.id = d.id_sellos\r\n" + 
			"WHERE s.id = #{idSellos} AND estado = 1 ORDER BY d.sello_numero")
	List<Sellos> buscaListadoDeSellos(@PathParam("idSellos") int idSellos);
	
	@Select("UPDATE sellos SET estado = 2 WHERE id = #{idSellos}")
	void eliminarSelloPorId(@PathParam("idSellos") int idSellos);
	
	@Select("UPDATE sellos_detalle SET nombre = #{nombre}, operacion = #{operacion}, foto = #{foto} WHERE id_sellos  = #{idSellos} AND sello_numero = #{selloNumero}")
	void editarSello(@PathParam("idSellos") int idSellos,@PathParam("selloNumero") int selloNumero,@PathParam("nombre") String nombre,@PathParam("operacion") int operacion,@PathParam("foto") String foto);
	
	@Select("SELECT s.id id,s.id_ticket idTicket,s.id_remision idRemision,s.estado estado,d.nombre nombre,d.operacion operacion,d.sello_numero selloNumero,s.fecha_creacion fechaCreacion,d.foto foto \r\n" + 
			"FROM sellos s INNER JOIN sellos_detalle d\r\n" + 
			"ON s.id = d.id_sellos\r\n" + 
			"WHERE s.id = #{idSellos}\r\n" + 
			"AND d.sello_numero = #{selloNumero}")
	Sellos buscarSelloPorNumero(@PathParam("idSellos") int idSellos,@PathParam("selloNumero") int selloNumero);
		
	@Select("SELECT COUNT(*) FROM sellos WHERE id = #{idSellos}")
	int existeRemisionPorId(@PathParam("idSellos") int idSellos);
}
