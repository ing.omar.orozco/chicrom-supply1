package com.empresa.supply.mappers.administrador;

import java.util.List;

import javax.websocket.server.PathParam;

import org.apache.ibatis.annotations.Select;

import com.empresa.supply.modelo.administrador.Perfiles;


public interface MappersAdministrador {
	@Select("INSERT INTO perfiles(id,nombre,estado) VALUES(#{id},#{nombre},#{estado})")
	void crearPerfiles(@PathParam("id") int id,@PathParam("nombre") String nombre,@PathParam("estado") int estado);
	
	@Select("SELECT secuencia+1 FROM consecutivos WHERE CODIGO = #{codigo} AND estado = 1")
	int buscaConsecutivo(@PathParam("codigo") String codigo);
	
	@Select("UPDATE consecutivos SET secuencia = #{secuencia} WHERE CODIGO = #{codigo} AND estado = 1")
	void actualizaConsecutivo(@PathParam("codigo") String codigo,@PathParam("secuencia") int secuencia);
	
	@Select("SELECT id,nombre,estado FROM perfiles where id = #{id}")
	Perfiles buscarPerfil(@PathParam("id") int id);
	
	@Select("SELECT id,nombre,estado FROM perfiles where estado = 1")
	List<Perfiles> buscarPerfilesActivos();
	
	@Select("SELECT id,nombre,estado FROM perfiles")
	List<Perfiles> buscarPerfiles();
	
	@Select("UPDATE perfiles SET estado  = 2 WHERE id = #{id}")
	void inactivarPerfil(@PathParam("id") int id);
	
	@Select("UPDATE perfiles SET estado  = 1 WHERE id = #{id}")
	void activarPerfil(@PathParam("id") int id);
	
	@Select("UPDATE perfiles SET nombre = #{nombre} WHERE id = #{id}")
	void editarPerfil(@PathParam("id") int id,@PathParam("nombre") String nombre);
}
