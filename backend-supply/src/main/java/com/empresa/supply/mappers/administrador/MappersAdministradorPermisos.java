package com.empresa.supply.mappers.administrador;

import java.util.List;

import javax.websocket.server.PathParam;

import org.apache.ibatis.annotations.Select;

import com.empresa.supply.modelo.administrador.Permisos;


public interface MappersAdministradorPermisos {
	@Select("INSERT INTO permisos(id,url,estado) VALUES(#{id},#{url},#{estado})")
	void crearPermisos(@PathParam("id") int id,@PathParam("url") String url,@PathParam("estado") int estado);
	
	@Select("SELECT secuencia+1 FROM consecutivos WHERE CODIGO = #{codigo} AND estado = 1")
	int buscaConsecutivo(@PathParam("codigo") String codigo);
	
	@Select("UPDATE consecutivos SET secuencia = #{secuencia} WHERE CODIGO = #{codigo} AND estado = 1")
	void actualizaConsecutivo(@PathParam("codigo") String codigo,@PathParam("secuencia") int secuencia);
	
	@Select("SELECT id,url,estado FROM permisos where id = #{id}")
	Permisos buscarPermiso(@PathParam("id") int id);
	
	@Select("SELECT id,url,estado FROM permisos")
	List<Permisos> buscarPermisos();
	
	@Select("UPDATE permisos SET estado  = 2 WHERE id = #{id}")
	void inactivarPermiso(@PathParam("id") int id);
	
	@Select("UPDATE permisos SET estado  = 1 WHERE id = #{id}")
	void activarPermiso(@PathParam("id") int id);
	
	@Select("UPDATE permisos SET url = #{url} WHERE id = #{id}")
	void editarPermiso(@PathParam("id") int id,@PathParam("url") String url);
}
