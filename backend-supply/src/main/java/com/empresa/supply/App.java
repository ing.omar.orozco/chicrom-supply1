package com.empresa.supply;

import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.apache.ibatis.type.MappedTypes;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import com.empresa.supply.modelo.VOUser;
@MappedTypes(VOUser.class)
@MapperScan("com.empresa.supply.mappers")
@SpringBootApplication
@EnableAutoConfiguration

public class App extends SpringBootServletInitializer{
	public TimeZone zonaHorariaLocal;
    public static void main( String[] args ) {
    	SpringApplication.run(App.class, args);
    }
    
    @PostConstruct
    public void reset () {
    	zonaHorariaLocal = TimeZone.getTimeZone("America/Bogota"); //TimeZona.getTimeZone("America/Bogota");
    }
    
       
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(App.class);
    }
}