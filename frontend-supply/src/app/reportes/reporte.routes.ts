import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { MainComponent } from './componentes/main/main.component';
import { ReporteComponent } from './componentes/reportes/reporte.component';

export const routes: Routes = [

  {
    path: '',
    redirectTo: 'main',
    pathMatch: 'full',
  },
  {
    path: '',
    component: MainComponent,
    children: [
      { path: '', component: ReporteComponent },
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReporteRoutesModule { }

