import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { WebDataRocksPivot } from "../../../webdatarocks/webdatarocks.angular4";

@Component({
  selector: 'app-reporte',
  templateUrl: './reporte.component.html',
  styleUrls: ['./reporte.component.css']
})
export class ReporteComponent implements OnInit {

  @ViewChild("pivot1") child: WebDataRocksPivot;
  public pivotReport: any;
  dateForm: FormGroup;
  initialDate: string;
  endDate: string;
  submitted: boolean;
  url: string;

  constructor(private formBuilder: FormBuilder) {

    this.submitted = false;
    this.url = null;

  }

  ngOnInit() {
    // validacion del formulario
    this.dateForm = this.formBuilder.group({
      initialDate: ['', Validators.required],
      endDate: ['', Validators.required]
    });

    google.charts.load("current", { packages: ["corechart"] });
    google.charts.setOnLoadCallback(() => this.onGoogleChartsLoaded());
  }

  get f() { return this.dateForm.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.dateForm.invalid) {
      return;
    }

    this.url = "http://localhost:8080/supplyLogistics/servicesRest/supply/consulta/segmentoVolumen/" + this.initialDate + "/" + this.endDate

    this.pivotReport = {

      dataSource: {
        dataSourceType: 'json',
        filename: this.url
      },
      slice: {
        rows: [{ uniqueName: "Muelle" }],
        columns: [{ uniqueName: "Measures" }],
        measures: [{ uniqueName: "Price", aggregation: "sum" }]
      }
    };
  }

  onGoogleChartsLoaded() {
    this.googleChartsLoaded = true;
    if (this.pivotTableReportComplete) {
      this.createGoogleChart();
    }
  }

  onPivotReady(pivot: WebDataRocks.Pivot): void {
    console.log("[ready] WebDataRocksPivot", this.child);
  }

  onCustomizeCell(
    cell: WebDataRocks.CellBuilder,
    data: WebDataRocks.Cell
  ): void {
    //console.log("[customizeCell] WebDataRocksPivot");
    if (data.isClassicTotalRow) cell.addClass("fm-total-classic-r");
    if (data.isGrandTotalRow) cell.addClass("fm-grand-total-r");
    if (data.isGrandTotalColumn) cell.addClass("fm-grand-total-c");
  }

  pivotTableReportComplete: boolean = false;
  googleChartsLoaded: boolean = false;

  onReportComplete(): void {
    this.child.webDataRocks.off("reportcomplete");
    this.pivotTableReportComplete = true;
    this.createGoogleChart();
  }

  createGoogleChart() {
    if (this.googleChartsLoaded) {
      this.child.webDataRocks.googlecharts.getData(
        { type: "column" },
        data => this.drawChart(data),
        data => this.drawChart(data)
      );
    }
  }

  drawChart(_data: any) {
    var data = google.visualization.arrayToDataTable(_data.data);

    var options = {
      title: "SEGMENTO VOLUMEN",
      legend: { position: "top" },
      colors: ["#7570b3"],
      isStacked: true
    };

    var chart = new google.visualization.ColumnChart(
      document.getElementById("googlechart-container")
    );
    chart.draw(data, <google.visualization.ColumnChartOptions>options);
  }


}
