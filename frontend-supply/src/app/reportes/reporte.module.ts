import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './componentes/main/main.component';
import { ReporteComponent } from './componentes/reportes/reporte.component';
import { ReporteRoutesModule } from './reporte.routes';
import { WebDataRocksPivot } from "../././webdatarocks/webdatarocks.angular4";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GoogleChartsModule } from "angular-google-charts";

@NgModule({
  declarations: [
    MainComponent,
    ReporteComponent,
    WebDataRocksPivot
  ],
  imports: [
    CommonModule,
    ReporteRoutesModule,
    FormsModule,
    ReactiveFormsModule,
    GoogleChartsModule.forRoot()
  ]
})
export class ReporteModule { }
