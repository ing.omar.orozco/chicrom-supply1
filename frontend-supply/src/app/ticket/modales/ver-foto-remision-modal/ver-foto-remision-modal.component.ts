import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-ver-foto-remision-modal',
  templateUrl: './ver-foto-remision-modal.component.html',
  styleUrls: ['./ver-foto-remision-modal.component.css']
})
export class VerFotoRemisionModalComponent implements OnInit {
  @Input() public modalConfig: any;
  @Input() public foto: string;

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

}
