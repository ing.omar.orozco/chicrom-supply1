import {Component, Input, OnInit} from '@angular/core';
import {CausalTiempo} from '../../models/causalTiempo';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {BitacoraService} from '../../service/bitacora.service';
import {NotificacionesService} from '../../../core/services/notificaciones.service';
import {convertRuleOptions} from "tslint/lib/configuration";

@Component({
  selector: 'app-causal-de-tiempo-modal',
  templateUrl: './causal-de-tiempo-modal.component.html',
  styleUrls: ['./causal-de-tiempo-modal.component.css']
})
export class CausalDeTiempoModalComponent implements OnInit {
  @Input() public causales: CausalTiempo[];
  @Input() public causalesEdit: CausalTiempo[];
  @Input() public causalesAux: CausalTiempo;
  @Input() public causalesAuxArray: CausalTiempo[];
  @Input() public causalesTabla: any[];
  @Input() public idTicket: number;
  @Input() public idEstadoTicket: number;
  @Input() public totalTablaAux: number;
  @Input() public tieneCausales: string;
  loadTable: any[];
  // tabla
  constructor(public activeModal: NgbActiveModal,
              private serviceNotificaciones: NotificacionesService,
              private serviceBitacora: BitacoraService,
              private bitacoraServices: BitacoraService) {
  }

  ngOnInit() {
    this.totalTablaAux = 0;
    //  llenado del header tabla
    this.loadTable = [
      {  header: 'Nombre causal', class: 'w-50' },
      {  header: 'Retardo', class: 'w-25' }
    ];
  }
  validation(evento) {
    if (/^[0-9]*$/.test(evento.key)) {/*deja pasar el text*/} else {evento.preventDefault(); }
  }
  guardar() {
    this.causalesAuxArray = [];
    for (const i in this.causales) {
      if (this.causales[i].minutosRetardo !== null) {
        this.causalesAux = new CausalTiempo();
        this.causalesAux.idTicket = Number(this.idTicket);
        this.causalesAux.idEstadoTicket = this.idEstadoTicket;
        if (this.tieneCausales === 'N') {
          this.causalesAux.idCausalTiempo = this.causales[i].id;
          this.causalesAux.retardo = this.causales[i].minutosRetardo;
        } else {
          this.causalesAux.causal = this.causales[i].causal;
          this.causalesAux.id = this.causales[i].id;
          this.causalesAux.minutosRetardo = this.causales[i].minutosRetardo;
        }
        this.causalesAuxArray.push(this.causalesAux);
      }
    }
    if (this.tieneCausales === 'N') {
      this.crear();
    } else {
      this.editar();
    }
  }
  crear() {
    console.log('crear', this.causalesAuxArray)
    this.bitacoraServices.insertarTiempoCausales(this.causalesAuxArray).subscribe( response => {
      this.serviceNotificaciones.showAlertSucces('exitoso', 'Causales creado exitosamente');
      this.bitacoraServices.changeCausal.emit(true);
      this.tieneCausales = 'S';
      this.causalesEdit = [];
      this.causalesEdit = this.causalesAuxArray;
    });
  }
  editar() {
    console.log('editar', this.causalesAuxArray)
    this.bitacoraServices.editarTiempoCausales(this.causalesAuxArray).subscribe( response => {
      this.serviceNotificaciones.showAlertSucces('exitoso', 'Causales editado exitosamente');
      this.bitacoraServices.changeCausal.emit(true);
      this.tieneCausales = 'S';
      this.causalesEdit = [];
      this.causalesEdit = this.causalesAuxArray;
    });
  }
  textRetardo() {
    this.totalTablaAux = 0;
    for (const i in this.causales) {
      if (this.causales[i].minutosRetardo) {
        this.totalTablaAux = this.totalTablaAux + this.causales[i].minutosRetardo;
      }
    }
  }
}
