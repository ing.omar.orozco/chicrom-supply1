import {Component, Input, OnInit,} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Ticket} from '../../models/ticket';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {TicketService} from '../../service/ticket.service';
import { NotificacionesService } from 'src/app/core/services/notificaciones.service';
import { Operaciones } from '../../models/operaciones';
import {SessionStorageService} from 'angular-web-storage';
import {Usuario} from '../../../core/models/usuario';
import {TicketModalFase2Component} from "../ticket-modal-fase2/ticket-modal-fase2.component";


@Component({
  selector: 'app-ticket-modal',
  templateUrl: './ticket-modal.component.html',
  styleUrls: ['./ticket-modal.component.css']
})
export class TicketModalComponent implements OnInit {
  ticketForm: FormGroup;
  submitted = false;
  ticket: Ticket;
  operaciones: Operaciones[];
  @Input() public modalConfig: any;
  @Input() public modalData = new Ticket();
  @Input() public dataOperaciones: Operaciones[];
  @Input() public dataDependencias: any[];
  @Input() public dataDependenciasDestino: any[];
  @Input() public dataTransportista: any[];
  @Input() public dataAuxiliares: any[];
  @Input() public dataTipoTransporte: any[];
  @Input() public dataSubDependencia: any[];
  usuario: Usuario[];

  constructor(public activeModal: NgbActiveModal,
              private formBuilder: FormBuilder,
              private modalService: NgbModal,
              private sessionStorage: SessionStorageService,
              private serviceNotificaciones: NotificacionesService,
              private serviceTickect: TicketService) { }

  ngOnInit() {
    // validacion del formulario
    this.ticketForm = this.formBuilder.group({
      placa: ['', Validators.required ],
      idOperacion: ['', Validators.required ],
      idDependencia: ['', Validators.required ],
      idDependenciaDestino: ['', Validators.required ],
      idTransportista: ['', Validators.required ],
      idTipoTransporte: ['', Validators.required ],
      subDependencia: ['', Validators.required ],
      muelle: ['', Validators.required ]
    });


  }
  get f() {return  this.ticketForm.controls; }

  onSubmit() {
    this.usuario = this.sessionStorage.get('usuario');
    this.submitted = true;
    if (this.ticketForm.invalid) {
      return;
    }
    // metodo crear y editar despues de que el metodo validacion paso
    if (this.modalConfig !== 'Editar') {

      this.serviceNotificaciones.showConfirmacion('Confirmación', '¿Seguro de crear este nuevo ticket?')
      .then((result) => {
        if (result.value) {
          this.ticket = new Ticket();
          this.ticket.placa = this.modalData.placa;
          this.ticket.idOperacion = Number(this.modalData.idOperacion);
          this.ticket.idDependencia = Number(this.modalData.idDependencia);
          this.ticket.idDependenciaDestino = Number(this.modalData.idDependenciaDestino);
          this.ticket.idTransportista = Number(this.modalData.idTransportista);
          this.ticket.idTipoTransporte = Number(this.modalData.idTipoTransporte);
          this.ticket.subDependencia = Number(this.modalData.subDependencia);
          this.ticket.muelle = Number(this.modalData.muelle);
          this.ticket.idUsuario = this.usuario[0].idUsuario
          this.ticket.idAuxiliares = 1;
          this.ticket.transportista = Number(this.modalData.idTransportista);

          console.log(this.ticket);
          this.serviceTickect.create(this.ticket).subscribe( response => {
          console.log(response);
          // necesito que el servicio devuelva la data para enviarlo a la tabla y no llamar otro servicio
          // this.serviceTickect.emitirCreate(response);
          this.serviceTickect.createAux.emit(true);
          this.activeModal.close('Close click');
          this.serviceNotificaciones.showAlertSucces('exitoso', 'ticket  creado exitosamente');

          const modalRef = this.modalService.open(TicketModalFase2Component, {size: 'lg' });
          this.serviceTickect.buscaTicketsFase2Interfaz(response.id).subscribe( data => {
          if (data !== null) {
          modalRef.componentInstance.modalDataArray = data;
          }
          });
          modalRef.componentInstance.id = response.id;
          modalRef.componentInstance.tipoOperacion = response.tipoOperacion;
          modalRef.componentInstance.idOperacion = response.idOperacion;
          modalRef.componentInstance.tieneSellos = response.tieneSellos;
          modalRef.componentInstance.modalConfig = 'Fase2';
          });
        }
      });
    } else {
      this.serviceNotificaciones.showConfirmacion('Confirmación', '¿Seguro de editar este Ticket?')
      .then((result) => {
        if (result.value) {
          this.ticket = this.ticketForm.value;
          this.ticket.id = this.modalData.id;
          this.serviceTickect.edit(this.ticket).subscribe( response => {
            // this.serviceTickect.update.emit(response);
          this.serviceTickect.updateAux.emit(true);
          this.activeModal.close('Close click');
         });
        }});
    }
  }
  validation(evento) {
      if (/^[0-9]*$/.test(evento.key)) {/*deja pasar el text*/} else {evento.preventDefault(); }
  }
}
