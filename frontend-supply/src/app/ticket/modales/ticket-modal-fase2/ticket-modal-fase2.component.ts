import { Component, OnInit , Input} from '@angular/core';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {TicketFase2} from '../../models/ticketFase2';
import {TicketService} from '../../service/ticket.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { NotificacionesService } from 'src/app/core/services/notificaciones.service';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { MessageService } from 'primeng/primeng';
import {RemisionModalComponent} from '../remision-modal/remision-modal.component';
import {RemisionService} from '../../service/remision.service';
import {Auxiliares} from "../../models/auxiliares";
import {SellosService} from "../../service/sellos.service";
import {Usuario} from "../../../core/models/usuario";
import {SessionStorageService} from "angular-web-storage";

@Component({
  selector: 'app-ticket-modal-fase2',
  templateUrl: './ticket-modal-fase2.component.html',
  styleUrls: ['./ticket-modal-fase2.component.css'],
  providers: [MessageService]
})
export class TicketModalFase2Component implements OnInit {
  @Input() public idOperacion: any;
  @Input() public modalConfig: any;
  @Input() public remisiones: string;
  @Input() public modalDataArray: TicketFase2[];
  @Input() public auxiliaresList: Auxiliares[];
  @Input() public id: number;
  @Input() public tieneSellos: string;
  @Input() public tipoOperacion: string;

  AuxiliaresSelected: Auxiliares[];
  /* table */
  displayDialog: boolean;
  displayDialogFalse: boolean;
  selectTicketFase2 = new TicketFase2();
  cols: any[];
  loadTable: any[];
  /* formulario */
  ticketForm: FormGroup;
  submitted = false;
  accion: string;
  /*imagen */
  imageChangedEvent: any = '';
  croppedImage: string;
  imagenEdit: boolean;
  auxiliresEdit: boolean;
  arrayAux: any[];
  usuarioList: Usuario[];
  constructor(public activeModal: NgbActiveModal,
              private modalService: NgbModal,
              private formBuilder: FormBuilder,
              private messageService: MessageService,
              private serviceSellos: SellosService,
              private sessionStorage: SessionStorageService,
              private serviceNotificaciones: NotificacionesService,
              private serviceRemision: RemisionService,
              private serviceTicket: TicketService) {
      this.displayDialogFalse = true;
      this.imagenEdit = false;
      this.auxiliresEdit = false;
      this.AuxiliaresSelected = [];
     }

  ngOnInit() {
    this.cols = [
      { header: 'Fecha'},
      { header: 'Estado'},
      { header: 'Accion'},
      { header: 'Nota'}

    ];
    this.serviceSellos.changeSellos.subscribe( data => {
      if (data === true) {
        this.salirAlGuardar();
      }
    })

    //  llenado del header tabla
    this.loadTable = [
      {  header: 'Nombre auxiliar', class: 'w-50' }
    ];
    this.ticketForm = this.formBuilder.group({
      nota: ['', Validators.required ]
     });

  }
  get f() {return  this.ticketForm.controls; }

  onSubmit() {
    /*if  (this.croppedImage === null && this.selectTicketFase2.foto === '') {
     return this.messageService.add({severity:'warn', summary: 'Advertencia', detail:'falta subir la imagen'});
    }*/
    this.submitted = true;
    if (this.ticketForm.invalid) {
      return;
    }
    if  (this.accion === 'A') {
      this.guardar();
    } else {
      this.editar();
    }
  }
  onRowSelect(event) {
    console.log(event.data)
    if (event.data.estadoTicket === 4 &&  event.data.tipoOperacion === 'D' && event.data.tieneSello === 'N') {
      this.selectTicketFase2 = new TicketFase2();
      this.messageService.add({severity: 'warn', summary: 'Advertencia', detail: 'No se puede colocar este estado ir a remisión.'});
    } else {
      if (event.data.estado === 'A' || event.data.estado === 'E' ) {
        this.selectTicketFase2 = new TicketFase2();
        this.selectTicketFase2 = event.data;
        this.displayDialog = true;
        this.displayDialogFalse = false;
        this.croppedImage = null ;
        this.imagenEdit = false;
        // Accion
        this.accion = event.data.estado;
        console.log(1)

      } else {
        this.selectTicketFase2 = new TicketFase2();
        this.messageService.add({severity: 'warn', summary: 'Advertencia', detail: 'No se puede editar esta fase.'});
      }
    }
    if (event.data.estadoTicket === 3) {
      this.auxiliresEdit = true;
      this.auxiliaresGetAll();
      this.AuxiliaresSelected = event.data.listaAuxiliares;
    } else {
      this.auxiliresEdit = false;
    }
  }
  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
    this.imagenEdit = true;
  }
  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
  }
  imageLoaded() {
      // show cropper
  }
  loadImageFailed() {
      // show message
  }
  guardar() {
    console.log(this.selectTicketFase2.estadoTicket)
    console.log(this.tipoOperacion)
    this.serviceNotificaciones.showConfirmacion('Confirmación', '¿Seguro de crear este nuevo ticket?')
    .then((result) => {
      if (result.value) {
        this.selectTicketFase2.idTicket = this.id;
        this.selectTicketFase2.estado = 'A';
        this.selectTicketFase2.estadoAuxiliar = 1;
        this.selectTicketFase2.listaAuxiliares = this.AuxiliaresSelected;
        console.log(this.selectTicketFase2)
        this.serviceTicket.createFase2(this.selectTicketFase2).subscribe( data => {
        this.salirAlGuardar();
        if (this.selectTicketFase2.estadoTicket === 5 ||
          this.selectTicketFase2.estadoTicket === 3 && this.tipoOperacion === 'D'  ) {
          this.remisiones = '1';
          this.serviceTicket.remision.emit(true);
          this.verificacion();
        }
        });
      }
    });
  }
  editar() {
    this.serviceNotificaciones.showConfirmacion('Confirmación', '¿Seguro de editar  este nuevo ticket?')
      .then((result) => {
        if (result.value) {
          const array = [];
          console.log(this.AuxiliaresSelected)
          for (const i in this.AuxiliaresSelected) {
            array.push({id : this.AuxiliaresSelected[i].id});
          }
          this.selectTicketFase2.listaAuxiliares = array;
          const objetoEditar = {
            nota: this.selectTicketFase2.nota,
            idTicket: this.id,
            foto: this.selectTicketFase2.foto,
            estadoTicket: this.selectTicketFase2.estadoTicket,
            listaAuxiliares: this.selectTicketFase2.listaAuxiliares };
          console.log(objetoEditar);
          this.serviceTicket.ediatrFase2(objetoEditar).subscribe( data => {
            this.salirAlGuardar();
            if (this.selectTicketFase2.estadoTicket === 5 ||
              this.selectTicketFase2.estadoTicket === 3 && this.tipoOperacion === 'D'  ) {
              this.remisiones = '1';
              this.serviceTicket.remision.emit(true);
              this.verificacion();
            }


          });
        }});
  }

  salir() {
    this.displayDialog = false;
    this.displayDialogFalse = true;
    this.selectTicketFase2 = new TicketFase2();
    this.serviceTicket.buscaTicketsFase2Interfaz(this.id).subscribe( dataArray => {
      this.modalDataArray = [];
      this.modalDataArray = dataArray;
    });
  }
  salirAlGuardar() {
    this.serviceTicket.buscaTicketsFase2Interfaz(this.id).subscribe( dataArray => {
      this.modalDataArray = [];
      this.modalDataArray = dataArray;
      this.submitted = false;
      this.imagenEdit = false;
      this.displayDialog = false;
      this.displayDialogFalse = true;
      this.selectTicketFase2 = new TicketFase2();
      this.auxiliresEdit = false;
    });
  }
  verificacion() {
    console.log(this.idOperacion)
    if (this.remisiones === '1') {
      const modalRef = this.modalService.open(RemisionModalComponent, {size: 'xl' as 'lg'});
      this.serviceRemision.buscarRemisionPorIdTicket(this.id).subscribe( data => {
        modalRef.componentInstance.modalDataArray = data;
      });
      modalRef.componentInstance.id = this.id;
      modalRef.componentInstance.idOperacion = this.idOperacion;
      modalRef.componentInstance.modalConfig = 'Remision';
    } else {
      this.messageService.add({severity: 'warn', summary: 'Advertencia', detail:
          'No puede continuar con la siguiente etapa de remisiones porque no estan completados los estados.'});
    }
  }
  auxiliaresGetAll() {
    this.usuarioList = this.sessionStorage.get('usuario');
    // this.usuarioList[0].idPlanta
    this.serviceTicket.getAllAuxiliaresIdPlanta( this.usuarioList[0].idPlanta ).subscribe( response => {
      this.auxiliaresList = response;
    });
  }

}
