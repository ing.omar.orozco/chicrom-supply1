import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {NgbActiveModal, NgbModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-reloj-modal',
  templateUrl: './reloj-modal.component.html',
  styleUrls: ['./reloj-modal.component.css']
})
export class RelojModalComponent implements OnInit, OnDestroy  {
  @Input() public timer: number;
  interval: any;
  constructor(public activeModal: NgbActiveModal,  private modalService: NgbModal) {

  }

  ngOnInit() {
    this.startCountdown();
  }
  startCountdown() {
    if (this.timer > 0) {
      this.interval = setInterval(() => {
        console.log(this.timer);
        this.timer--;
        if (this.timer < 0 ) {
          // The code here will run when
          // the timer has reached zero.

          clearInterval(this.interval);
          console.log('Ding!');
        }
      }, 1000);
    }
  }
  ngOnDestroy() {
    clearInterval(this.interval);
  }
}
