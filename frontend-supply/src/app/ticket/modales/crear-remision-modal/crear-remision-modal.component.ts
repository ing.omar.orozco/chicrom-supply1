import { Router } from '@angular/router';
import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Remision} from '../../models/remision';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ImageCroppedEvent} from 'ngx-image-cropper';
import {FileUpload, Message} from 'primeng/primeng';
import {RemisionService} from '../../service/remision.service';
import {NotificacionesService} from '../../../core/services/notificaciones.service';
import {TicketService} from '../../service/ticket.service';

@Component({
  selector: 'app-crear-remision-modal',
  templateUrl: './crear-remision-modal.component.html',
  styleUrls: ['./crear-remision-modal.component.css']
})
export class CrearRemisionModalComponent implements OnInit {
  @Input() public modalConfig: any;
  @Input() public id: number;
  @Input() public remision = new Remision();
  /* formulario */
  remisionForm: FormGroup;
  submitted = false;
  /*imagen */
  imageChangedEvent: any = '';
  croppedImage: string;
  @ViewChild('Foto')  foto: FileUpload;
  msgs: Message[] = [];

  constructor(public activeModal: NgbActiveModal,
              private formBuilder: FormBuilder,
              private modalService: NgbModal,
              private router: Router,
              private serviceNotificaciones: NotificacionesService,
              private serviceTicket: TicketService,
              private serviceRemision: RemisionService) { }

  ngOnInit() {
    this.remisionForm = this.formBuilder.group({
      numeroRemision: ['', Validators.required ]
    });

  }
  get f() {return  this.remisionForm.controls; }
  onSubmit() {
    this.submitted = true;
    this.msgs = [];
    if (this.remisionForm.invalid) {
      return;
    }
    if (this.modalConfig === 'Crear') {
      this.crear();
    } else {
      this.edit();
    }
  }
  crear() {
    // if (this.croppedImage === undefined) {
    //   this.msgs.push({severity: 'error',
    //     detail: 'La foto es requerida.' });
    //   return;
    // }
    this.serviceNotificaciones.showConfirmacion('Confirmación', '¿Seguro de crear esta remision?')
      .then((result) => {
        if (result !== null) {
          this.remision = new Remision();
          this.remision.numeroRemision = this.f['numeroRemision'.toString()].value;
          this.remision.estado = 1;
          this.remision.idTicket = this.id;
          this.remision.foto = this.croppedImage;
          this.serviceRemision.crearRemisiones(this.remision).subscribe( response => {
            this.serviceRemision.changeRemision.emit(response);
            this.serviceTicket.remision.emit(true);
            // this.router.navigate(['/ticket']);
            this.activeModal.close('Close click');
          });
        }
      });
  }
  edit() {
    if (this.croppedImage !== undefined) {
      this.remision.foto = this.croppedImage;
    }
    this.serviceNotificaciones.showConfirmacion('Confirmación', '¿Seguro de editar esta remision?')
      .then((result) => {
        this.remision.numeroRemision = this.f['numeroRemision'.toString()].value;
        this.serviceRemision.editarRemisionPorId(this.remision).subscribe( response => {
          this.serviceRemision.changeRemision.emit(response);
          this.serviceTicket.remision.emit(true);
          this.activeModal.close('Close click');
        });
      });
  }
  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }
  imageCropped(event: ImageCroppedEvent) {

    this.croppedImage = event.base64;
  }
  imageLoaded() {
    // show cropper
  }
  loadImageFailed() {
    // show message
  }

}
