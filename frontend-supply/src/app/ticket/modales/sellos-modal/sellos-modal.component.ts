import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Sellos} from '../../models/sellos';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Message, MessageService} from 'primeng/api';
import {SellosService} from '../../service/sellos.service';
import {FileUpload} from 'primeng/primeng';
import {ImageCroppedEvent} from 'ngx-image-cropper';
import {VerFotoRemisionModalComponent} from '../ver-foto-remision-modal/ver-foto-remision-modal.component';
import {NotificacionesService} from '../../../core/services/notificaciones.service';

@Component({
  selector: 'app-sellos-modal',
  templateUrl: './sellos-modal.component.html',
  styleUrls: ['./sellos-modal.component.css'],
  providers: [MessageService]
})
export class SellosModalComponent implements OnInit {
  @Input() public modalConfig: any;
  @Input() public idRemision: number;
  @Input() public idTicket: number;
  @Input() public idOperacion: number;
  @Input() public modalDataArrayAux: any;
  @Input() public dataOperaciones: any;
  /* table */
  displayDialog: boolean;
  displayDialogFalse: boolean;
  cols: any[];
  @Input() public modalDataArrayTable: Sellos[];
  selectSello = new Sellos();
  /* formulario */
  sellosForm: FormGroup;
  submitted = false;
  /*imagen */
  imageChangedEvent: any = '';
  croppedImage: string;
  @ViewChild('Foto')  foto: FileUpload;
  msgs: Message[] = [];
  // operacion crear
  operacion: number;
  constructor(public activeModal: NgbActiveModal,
              private formBuilder: FormBuilder,
              private messageService: MessageService,
              private serviceNotificaciones: NotificacionesService,
              private serviceSellos: SellosService,
              private modalService: NgbModal) {
    this.displayDialogFalse = true;
  }

  ngOnInit() {
    this.cols = [
      { header: 'Numero' , class: 'w-25'},
      { header: 'Nombre' , class: 'w-25'},
      { header: 'Foto ' , class: 'w-25'}
    ];
    this.sellosForm = this.formBuilder.group({
      nombre: ['', Validators.required ],
      operacion: ['' ]
    });
    this.f['operacion'.toString()].disable();
  }
  get f() {return  this.sellosForm.controls; }
  onSubmit() {
    this.submitted = true;
    if (this.sellosForm.invalid) {
      return;
    }
    if (this.croppedImage !== undefined) {
      this.selectSello.foto = this.croppedImage;
    }
    this.selectSello.operacion = this.idOperacion;
    this.serviceSellos.editarSelloPorId(this.selectSello).subscribe(response => {
      this.salir();
    });
  }
  crearSellos() {
    if (this.modalDataArrayTable.length === 0) {
      this.modalDataArrayAux = [];
      for (let i = 1; 8 >= i; i++) {
        const json = {
          idTicket : this.idTicket,
          idRemision : this.idRemision,
          fechaCreacion : null,
          estado : 1,
          nombre : '',
          operacion : this.idOperacion,
          selloNumero : i,
          foto: ''
        };
        this.modalDataArrayAux.push(json);
      }
      this.serviceSellos.crearListaDeSellos(this.modalDataArrayAux).subscribe( response => {
        this.modalDataArrayTable = response;
        this.serviceSellos.changeSellos.emit(true);
      });
    } else {
      this.messageService.add({severity: 'warn', summary: 'Advertencia', detail: 'Ya estan creados los sellos.'});
    }
  }
  onRowSelect(event) {
      this.selectSello = new Sellos();
      this.selectSello = event.data;
      this.displayDialog = true;
      this.displayDialogFalse = false;
      console.log(this.selectSello)
  }
  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }
  imageCropped(event: ImageCroppedEvent) {

    this.croppedImage = event.base64;
  }
  imageLoaded() {
    // show cropper
  }
  loadImageFailed() {
    // show message
  }
  salir() {
    this.displayDialog = false;
    this.displayDialogFalse = true;
    this.imageChangedEvent = null;
    this.selectSello = new Sellos();
    this.serviceSellos.buscarSellosPorId(this.idRemision).subscribe( dataArray => {
      this.modalDataArrayTable = [];
      this.modalDataArrayTable = dataArray;
    });
  }
  verFoto(modo: string, foto: string) {
    const modalRef = this.modalService.open(VerFotoRemisionModalComponent, {size: 'lg', centered: true});
    modalRef.componentInstance.modalConfig = modo;
    modalRef.componentInstance.foto = foto;
  }
}
