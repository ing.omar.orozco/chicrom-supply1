
import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {MainComponent} from './componentes/main/main.component';
import {NuevoTicketComponent} from './componentes/nuevo-ticket/nuevo-ticket.component';
import {BitacoraComponent} from './componentes/bitacora/bitacora.component';

export const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {path: 'ticket', component: NuevoTicketComponent},
      {path: 'bitacora', component: BitacoraComponent}
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TicketRoutesModule { }

