import {EventEmitter, Injectable, Output} from '@angular/core';
import {Ticket} from '../models/ticket';
import {TicketFase2} from '../models/ticketFase2';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import { Operaciones } from '../models/operaciones';
import { TicketNombre } from '../models/ticketNombre';
import {Remision} from '../models/remision';
import {SessionStorageService} from 'angular-web-storage';
import {Auxiliares} from '../models/auxiliares';


@Injectable({
  providedIn: 'root'
})
export class TicketService {
  @Output() changeCreate: EventEmitter<Ticket> = new EventEmitter();
  update = new EventEmitter<Ticket>();
  idPlanta: number;

  @Output() createAux: EventEmitter<any> = new EventEmitter();
  @Output() updateAux: EventEmitter<any> = new EventEmitter();
  @Output() remision: EventEmitter<boolean> = new EventEmitter();

  constructor(private http: HttpClient,
              public sessionStorage: SessionStorageService) {

  }

  getAll(idPlanta: number): Observable<TicketNombre[]> {
    return this.http.get<TicketNombre[]>(environment.API_URL + '/servicesREST/JR/findIdTicketsAll/' + idPlanta);
  }

  getAll2(): Observable<Ticket[]> {
    return this.http.get<Ticket[]>(environment.API_URL + '/servicesREST/JR/findIdTicketsAll');
  }
  getId(id: string): Observable<Ticket> {
    return this.http.get<Ticket>(environment.API_URL + '/servicesREST/JR/findIdTickets/' + id);
  }
  create(body: Ticket): Observable<any> {
    return this.http.post<any>(environment.API_URL + '/servicesREST/JR/crearTickets', body);
  }
  edit(body: Ticket): Observable<any> {
    return this.http.post<any>(environment.API_URL + '/servicesREST/JR/editIdTickets', body);
  }
  delete(id: number): Observable<any> {
    return this.http.get<any>(environment.API_URL + '/servicesREST/JR/eliminaIdTickets/' + id);
  }
  cerrar(id: number): Observable<any> {
    return this.http.get<any>(environment.API_URL + '/servicesREST/JR/cerrarTickets/' + id);
  }

  buscaConsecutivo(codigo: string): Observable<any> {
      return this.http.get<any>(environment.API_URL + '/servicesREST/JR/buscaConsecutivo/' + codigo);
  }

  emitirCreate(data: Ticket) {
    this.changeCreate.emit(data);
  }
  getAllOperaciones(): Observable<any> {
    const idPlanta = this.sessionStorage.get('usuario');
    return this.http.get<any>(environment.API_URL + '/servicesREST/JR/buscarOperaciones/' + idPlanta[0].idPlanta);
  }

  getAllDependencias(): Observable<any> {
    return this.http.get<any>(environment.API_URL + '/servicesREST/JR/buscarDependencia');
  }

  getAllTransportista(): Observable<any> {
    return this.http.get<any>(environment.API_URL + '/servicesREST/JR/buscarTrasnportista');
  }

  getAllAuxiliaresIdPlanta(id: number): Observable<Auxiliares[]> {
    return this.http.get<Auxiliares[]>(environment.API_URL + '/servicesREST/JR/buscarAuxiliares/' + id);
  }
  buscarSubdependencias(): Observable<any> {
    return this.http.get<any>(environment.API_URL + '/servicesREST/JR/buscarSubdependencias ');
  }

  getAllTipoTransporte(): Observable<any> {
    return this.http.get<any>(environment.API_URL + '/servicesREST/JR/buscarTipoTransporte');
  }

  createFase2(body: TicketFase2): Observable<TicketFase2> {
    return this.http.post<TicketFase2>(environment.API_URL + '/servicesREST/JR/crearTicketsFase2', body);
  }

  ediatrFase2(body: TicketFase2): Observable<TicketFase2> {
    return this.http.post<TicketFase2>(environment.API_URL + '/servicesREST/JR/editarTicketsFase2', body);
  }

  buscaTicketsFase2Interfaz(id: any): Observable<TicketFase2[]> {
    return this.http.get<TicketFase2[]>(environment.API_URL + '/servicesREST/JR/buscaTicketsFase2Interfaz/' + id);
  }


}
