import {EventEmitter, Injectable, Output} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {Sellos} from '../models/sellos';

@Injectable({
  providedIn: 'root'
})
export class SellosService {
  @Output() changeSellos: EventEmitter<boolean> = new EventEmitter();
  constructor(private http: HttpClient) { }

  buscarSellosPorId(id: any): Observable<Sellos[]> {
    return this.http.get<Sellos[]>(environment.API_URL + '/servicesRest/supply/sellos/buscarSelloPorId/' + id );
  }
  crearListaDeSellos(body: any): Observable<Sellos[]> {
    return this.http.post<Sellos[]>(environment.API_URL + '/servicesRest/supply/sellos/crearListaDeSellos' , body);
  }
  editarSelloPorId (body: any): Observable<Sellos> {
    return this.http.post<Sellos>(environment.API_URL + '/servicesRest/supply/sellos/editarSelloPorId ' , body);
  }
}
