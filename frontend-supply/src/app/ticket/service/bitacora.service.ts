import {EventEmitter, Injectable, Output} from '@angular/core';
import {Observable} from 'rxjs';
import {Remision} from '../models/remision';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {CausalTiempo} from '../models/causalTiempo';

@Injectable({
  providedIn: 'root'
})
export class BitacoraService {

  @Output() changeCausal: EventEmitter<boolean> = new EventEmitter();

  constructor(private http: HttpClient) { }
  llenarTablaBitacora(idPalnta: number): Observable<any> {
    return this.http.get<any>(environment.API_URL + '/servicesRest/supply/bitacora/bitacora/' + idPalnta);
  }
  causalesDeTiempos(idTicket, idTicketEstado): Observable<CausalTiempo[]> {
    return this.http.get<CausalTiempo[]>(
      environment.API_URL + '/servicesRest/supply/causalTiempos/buscaCausalTiempo/' + idTicket + '/' +  idTicketEstado );
  }
  editarTiempoCausales(body: any): Observable<any> {
    return this.http.post<any>(environment.API_URL + '/servicesRest/supply/causalTiempos/editarTiempoCausales   ' , body);
  }
  insertarTiempoCausales(body: any): Observable<any> {
    return this.http.post<any>(environment.API_URL + '/servicesRest/supply/causalTiempos/insertarCausalesDeTiempo    ' , body);
  }
  buscarCausalesDeTiempoPorTicket(idTicket, idTicketEstado): Observable<any> {
    return this.http.get<any>
    (environment.API_URL + '/servicesRest/supply/bitacora/buscarCausalesDeTiempoPorTicketEstado/' + idTicket + '/' + idTicketEstado);
  }
}
