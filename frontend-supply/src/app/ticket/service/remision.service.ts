import {EventEmitter, Injectable, Output} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Remision} from '../models/remision';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RemisionService {

  @Output() changeRemision: EventEmitter<Remision> = new EventEmitter();
  @Output() changeRemisionDelet: EventEmitter<boolean> = new EventEmitter();

  constructor(private http: HttpClient) { }

  buscarRemisionPorId(id: any): Observable<Remision> {
    return this.http.get<Remision>(environment.API_URL + '/servicesRest/supply/remisiones/buscarRemisionPorIdRemision/' + id );
  }
  buscarRemisionPorIdTicket(id: any): Observable<Remision[]> {
    return this.http.get<Remision[]>(environment.API_URL + '/servicesRest/supply/remisiones/buscarRemisionPorIdTicket/' + id );
  }
  crearRemisiones(body: Remision): Observable<Remision> {
    return this.http.post<Remision>(environment.API_URL + '/servicesRest/supply/remisiones/crearRemisiones', body );
  }
  editarRemisionPorId(body: Remision): Observable<Remision> {
    return this.http.post<Remision>(environment.API_URL + '/servicesRest/supply/remisiones/editarRemisionPorId', body );
  }
  eliminarRemisionPorId(id: number): Observable<Remision> {
    return this.http.get<Remision>(environment.API_URL + '/servicesRest/supply/remisiones/eliminarRemisionPorId/' + id );
  }
}
