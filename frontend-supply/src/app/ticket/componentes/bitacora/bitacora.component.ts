import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {BitacoraService} from '../../service/bitacora.service';
import {NavigationEnd, Router} from "@angular/router";
import {Bitacora} from "../../models/bitacora";
import {MessageService} from "primeng/api";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {RelojModalComponent} from "../../modales/reloj-modal/reloj-modal.component";
import {CausalDeTiempoModalComponent} from "../../modales/causal-de-tiempo-modal/causal-de-tiempo-modal.component";
import {CausalTiempo} from "../../models/causalTiempo";
import {Usuario} from '../../../core/models/usuario';
import {SessionStorageService} from 'angular-web-storage';

@Component({
  selector: 'app-bitacora',
  templateUrl: './bitacora.component.html',
  styleUrls: ['./bitacora.component.css'],
  providers: [MessageService]
})
export class BitacoraComponent implements OnInit, OnDestroy  {
  title: string;
  bitacoArray: Bitacora[];
  causales: CausalTiempo[];
  loadTable: any[];
  notificacionList: any[];
  timer: any;
  notificacion: number;
  total: number;
  actualizacion: boolean;
  usuarioList: Usuario[];
  @ViewChild('audioOption') audioPlayerRef: ElementRef;
  constructor(private serviceBitacora: BitacoraService,
              private modalService: NgbModal,
              private sessionStorage: SessionStorageService,
              private router: Router) {
    this.title = 'Bitacora';
  }

  ngOnInit() {
    //  llenado del header tabla
    this.loadTable = [
      {  header: 'Fecha operacion', class: 'w-50' },
      {  header: 'Dependencia', class: 'w-25' },
      {  header: 'Muelle', class: 'w-25' },
      {  header: 'Numero Ticket', class: 'w-25' },
      {  header: 'Placa', class: 'w-25' },
      {  header: 'Vehiculo', class: 'w-25' },
      {  header: 'Estado', class: 'w-25' },
      {  header: 'InfoInicio', class: 'w-50' },
      {  header: 'T_Restante', class: 'w-25' },
      {  header: 'Defcom', class: 'w-25' },
    ];
    this.llenarTable();
    this.timeout();
    this.serviceBitacora.changeCausal.subscribe( response => {
      if (response === true) {
        this.llenarTable();
      }
    });
  }
  ngOnDestroy() {
    clearTimeout(   this.timer );
  }
  timeout() {
    this.timer = setTimeout(() => { this.llenarTable(); this.timeout(); }, 60000);
  }
  llenarTable() {
    this.usuarioList = this.sessionStorage.get('usuario');
    this.serviceBitacora.llenarTablaBitacora(this.usuarioList[0].idPlanta ).subscribe( response => {
      this.bitacoArray = response;
      console.log(response)
      this.notificacion = 0;
      this.notificacionList = [];
      for (const i in this.bitacoArray) {
        if (this.bitacoArray[i].color === 'R' && this.bitacoArray[i].tieneCausales === 'N') {
          this.notificacion = this.notificacion + 1;
          this.actualizacion = true;
          this.notificacionList.push(this.bitacoArray[i].idTicket);
        //  this.onAudioPlay();
        }
      }
    });
  }
  openReloj(timer) {
    const modalRef = this.modalService.open(RelojModalComponent, {size: 'lg'});
    if (timer > 0) {
      modalRef.componentInstance.timer = timer * 60;
    }
  }

  onAudioPlay() {
   this.audioPlayerRef.nativeElement.play();
  }
  openCausalDeTiempo(idTicket, idEstadoTicket, tieneCausales) {
    const modalRef = this.modalService.open(CausalDeTiempoModalComponent, {size: 'lg'});
    this.serviceBitacora.causalesDeTiempos(idTicket, idEstadoTicket).subscribe( response => {
      for (const i in response) {
        if (!response[i].minutosRetardo) {
          response[i].minutosRetardo = null;
        }
      }
      console.log(response)
      modalRef.componentInstance.causales = response;
      modalRef.componentInstance.idTicket = idTicket;
      modalRef.componentInstance.idEstadoTicket = idEstadoTicket;
      this.total = 0;
      for (const i of response) {
        this.total = this.total + i.minutosRetardo;
      }
      modalRef.componentInstance.totalTablaAux = this.total;
      modalRef.componentInstance.tieneCausales = tieneCausales;
    });

  }
}
