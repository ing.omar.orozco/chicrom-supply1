import { Component, OnInit } from '@angular/core';
import {TicketService} from '../../service/ticket.service';
import {TicketNombre} from '../../models/ticketNombre';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {TicketModalComponent} from '../../modales/ticket-modal/ticket-modal.component';
import {TicketModalFase2Component} from '../../modales/ticket-modal-fase2/ticket-modal-fase2.component';
import { NotificacionesService } from 'src/app/core/services/notificaciones.service';
import {RemisionModalComponent} from '../../modales/remision-modal/remision-modal.component';
import {RemisionService} from '../../service/remision.service';
import {Muelle} from '../../models/muelle';
import {Usuario} from '../../../core/models/usuario';
import {SessionStorageService} from 'angular-web-storage';

@Component({
  selector: 'app-nuevo-ticket',
  templateUrl: './nuevo-ticket.component.html',
  styleUrls: ['./nuevo-ticket.component.css']
})
export class NuevoTicketComponent implements OnInit {
  title: string;
  /*table*/
  loadTable: any[];
  ticketArray: TicketNombre[];
  usuarioList: Usuario[];

  constructor(
    private modalService: NgbModal,
    private serviceNotificacion: NotificacionesService,
    private serviceRemision: RemisionService,
    private sessionStorage: SessionStorageService,
    private serviceTicket: TicketService) {
    this.title = 'Lista de Ticketes';
  }

  ngOnInit() {
    this.llenarTabla();
    // llenado de la tabla cuando de hace un post
    this.serviceTicket.changeCreate.subscribe( data => {
      this.ticketArray.push(data);
    });

    // AUX PARA ACTUALIZAR LA TABLA
    this.serviceTicket.createAux.subscribe( data => {
      this.llenarTabla();
    });
    this.serviceTicket.updateAux.subscribe( data  => {
      this.llenarTabla();
    });
    this.serviceTicket.remision.subscribe( response  => {
      if (response === true) {
        this.llenarTabla();
      }
    });
       // AUX PARA ACTUALIZAR LA TABLA
    //  llenado del header tabla
    this.loadTable = [
      { field: 'id', header: '#', class: 'w-25' },
      { field: 'origen', header: 'Origen', class: 'w-50' },
      { field: 'operacion', header: 'Operación', class: 'w-75' },
      { field: 'destino', header: 'Destino', class: 'w-50' },
      { field: 'origen', header: 'Planta', class: 'w-50' },
      { field: 'subDependencia', header: 'Dependencia', class: 'w-50  ' },
      { field: 'muelle', header: 'Muelle', class: 'w-25' },
      { field: 'transportista', header: 'Transportista', class: 'w-50' },
      { field: 'placa', header: 'Placa' , class: 'w-25'},
      { field: 'tipoTransporte', header: 'Tipo Transporte' , class: 'w-50'}
    ];

  }
  //  abir el modal de los dos modos editar y crear
  openModal(modo: string, id: any) {
    const modalRef = this.modalService.open(TicketModalComponent, {size: 'xl' as 'lg'});
    modalRef.componentInstance.modalConfig = modo;
    if (modo === 'Editar') {
      this.serviceTicket.getId(id).subscribe(dataId => {
        modalRef.componentInstance.modalData = dataId;
        console.log(dataId)
      });
    }
    // busca servicio de operaciones
    this.serviceTicket.getAllOperaciones().subscribe( dataOperaciones => {
        if (dataOperaciones !== null) {
          modalRef.componentInstance.dataOperaciones = dataOperaciones;
        }
    });

    this.serviceTicket.getAllDependencias().subscribe( dataDependencias => {
      if (dataDependencias !== null) {
         modalRef.componentInstance.dataDependencias = dataDependencias;
         modalRef.componentInstance.dataDependenciasDestino = dataDependencias;
      }
    });

    this.serviceTicket.getAllTransportista().subscribe( dataTransportista => {
      if (dataTransportista !== null) {
        modalRef.componentInstance.dataTransportista = dataTransportista;
      }
  });

  /* this.serviceTicket.getAllAuxiliares().subscribe( dataAuxiliares => {
      if (dataAuxiliares !== null) {
        modalRef.componentInstance.dataAuxiliares = dataAuxiliares;
      }
  });*/
    this.serviceTicket.getAllTipoTransporte().subscribe( dataTipoTransporte => {
      if (dataTipoTransporte !== null) {
        modalRef.componentInstance.dataTipoTransporte = dataTipoTransporte;
      }
  });
    this.serviceTicket.buscarSubdependencias().subscribe( dataSubDependencia => {
      if (dataSubDependencia !== null) {
        console.log(dataSubDependencia)
        modalRef.componentInstance.dataSubDependencia = dataSubDependencia;
      }
    });
     }

  openModalFase2(modo: string, id: any, remisiones, idOperacion, tieneSellos, tipoOperacion) {
    const modalRef = this.modalService.open(TicketModalFase2Component, {size: 'xl' as 'lg'});
    this.serviceTicket.buscaTicketsFase2Interfaz(id).subscribe( data => {
      if (data !== null) {
        modalRef.componentInstance.modalDataArray = data;
        console.log(data)
      }
    });
    modalRef.componentInstance.id = id;
    modalRef.componentInstance.modalConfig = modo;
    modalRef.componentInstance.remisiones = remisiones;
    modalRef.componentInstance.idOperacion = idOperacion;
    modalRef.componentInstance.tieneSellos = tieneSellos;
    modalRef.componentInstance.tipoOperacion = tipoOperacion;
    /* this.usuarioList = this.sessionStorage.get('usuario');
    // this.usuarioList[0].idPlanta
    this.serviceTicket.getAllAuxiliaresIdPlanta( this.usuarioList[0].idPlanta ).subscribe( response => {
      console.log(response);
      modalRef.componentInstance.auxiliaresList = response;
    });*/
  }
  openModalRemision(modo: string, id: any, idOperacion) {
    const modalRef = this.modalService.open(RemisionModalComponent, {size: 'xl' as 'lg'});
    this.serviceRemision.buscarRemisionPorIdTicket(id).subscribe( data => {
      if (data !== null) {
        modalRef.componentInstance.modalDataArray = data;
      }
    });
    modalRef.componentInstance.id = id;
    modalRef.componentInstance.modalConfig = modo;
    modalRef.componentInstance.idOperacion = idOperacion;
  }
  //  eliminar ticket
  eliminar(codigo: any) {
    this.serviceNotificacion.showConfirmacion('Confirmacion', '¿Seguro de eliminar este ticket?')
    .then((result) => {
      if (result.value) {
        this.serviceTicket.delete(codigo).subscribe( dataEliminada => {
          this.llenarTabla();
          this.serviceNotificacion.showAlertSucces('Exitoso', 'Ticket eliminado de forma exitosa');
        });
      }
    });
  }
  llenarTabla() {
     //  Llenado de la tabla por el servicio
    this.usuarioList = this.sessionStorage.get('usuario');
    this.serviceTicket.getAll(this.usuarioList[0].idPlanta ).subscribe(data => {
      if (data !== null) {
        this.ticketArray = data;
      }
    });
  }
  cerrar(codigo) {
    this.serviceNotificacion.showConfirmacion('Confirmacion', '¿Seguro de cerrar este ticket?')
      .then((result) => {
        if (result.value) {
          this.serviceTicket.cerrar(codigo).subscribe( dataEliminada => {
            this.llenarTabla();
            this.serviceNotificacion.showAlertSucces('Exitoso', 'Ticket cerrado de forma exitosa');
          });
        }
      });
  }
}
