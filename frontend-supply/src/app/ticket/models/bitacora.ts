export class Bitacora {
  fechaOperacion: any;
  subDependencia: string;
  muelle: number;
  numeroTicket: string;
  placa: string;
  color: string;
  estado: string;
  horas: number;
  idEstadoTicket: number;
  idTicket: number;
  infoInicio: any;
  minutosRestante: any;
  vehiculo: string;
  tieneCausales: string;
}
