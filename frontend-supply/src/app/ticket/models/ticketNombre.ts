export class TicketNombre {
  id: number;
  placa: string;
  operacion: string;
  origen: string;
  destino: string;
  transportista: string;
  auxiliares: string;
  tipoTransporte: string;
  remisiones: string;

  constructor() {
    this.id = null;
    this.placa = '';
    this.operacion = null;
    this.origen = null;
    this.destino = null;
    this.transportista = null;
    this.auxiliares = null;
    this.tipoTransporte = null;
    this.remisiones = null;
  }


}
