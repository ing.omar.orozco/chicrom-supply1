export class CausalTiempo {
  id: number;
  idTicket?: number;
  idEstadoTicket?: number;
  idCausalTiempo?: number;
  causal?: string;
  retardo?: number;
  minutosRetardo?: number;
}
