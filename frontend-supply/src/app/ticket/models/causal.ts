export class Causal {
  idTicket: number;
  idEstadoTicket: number;
  idCausalTiempo: number;
  retardo: number;


  constructor() {
    this.idTicket = 0;
    this.idEstadoTicket = 0;
    this.idCausalTiempo = 0;
    this.retardo = 0;
  }
}
