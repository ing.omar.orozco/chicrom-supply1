export class Remision {
   numeroRemision: string;
   idTicket: number;
   foto: string;
   estado: number;
   tipo?: string;
  fechaCreacion: string;

  constructor() {
    this.numeroRemision = null;
    this.foto = '';
    this.idTicket = 0;
    this.tipo = '';
    this.estado = 0;
    this.fechaCreacion = '';
  }
}
