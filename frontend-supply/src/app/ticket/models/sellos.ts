export class Sellos {
   id: number;
   idTicket: number;
   idRemision: number;
   estado: number;
   fechaCreacion: string;
   nombre: string;
   operacion: number;
   selloNumero: number;
  foto: string;

  constructor() {
    this.id = null;
    this.idTicket = null;
    this.idRemision = null;
    this.estado = null;
    this.fechaCreacion = '';
    this.nombre = '';
    this.foto = '';
    this.operacion = null;
    this.selloNumero = null;
  }
}
