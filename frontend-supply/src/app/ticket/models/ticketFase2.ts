
export class TicketFase2 {
  fechaEstado?: any;
  estadoTicket: number;
  nota: string;
  nombre?: string;
  id?: number;
  estado?: string;
  foto: string;
  idTicket: number;
  estadoAuxiliar?: number;
  listaAuxiliares: any[];
  tieneSello?: any;
  tipoOperacion?: any;

  constructor() {
    this.fechaEstado = '';
    this.estadoTicket = 0;
    this.nota = '';
    this.nombre = '';
    this.id = 0;
    this.estado = '';
    this.idTicket = 0;
    this.estadoAuxiliar = 0;
    this.listaAuxiliares = null;
  }


}
