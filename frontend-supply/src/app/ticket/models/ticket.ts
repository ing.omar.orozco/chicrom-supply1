export class Ticket {
  id: number;
  placa: string;
  idOperacion: number;
  idDependencia: number;
  idDependenciaDestino: number;
  idTransportista: number;
  idTipoTransporte: number;
  subDependencia: number;
  muelle: number;
  idUsuario: number;
  idAuxiliares: number;
  transportista: number;
  nombreSubdependencia: string;

  constructor() {
    this.placa = '';
    this.idOperacion = null;
    this.idDependencia = null;
    this.idDependenciaDestino = null;
    this.idTransportista = null;
    this.idTipoTransporte = null;
    this.subDependencia = null;
    this.idUsuario = null;
    this.idAuxiliares = null;
    this.transportista = null;
    this.muelle = null;
    this.nombreSubdependencia = null;
  }


}
