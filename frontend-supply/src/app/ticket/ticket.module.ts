import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './componentes/main/main.component';
import {TicketRoutesModule} from './ticket.routes';
import { NuevoTicketComponent } from './componentes/nuevo-ticket/nuevo-ticket.component';
import {NgbModalModule} from '@ng-bootstrap/ng-bootstrap';
import {TableModule} from 'primeng/table';
import {ToastModule} from 'primeng/toast';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {
  TooltipModule,
  DialogModule,
  MessageModule,
  MessagesModule,
  SplitButtonModule,
  AccordionModule
} from 'primeng/primeng';
import { TicketModalComponent } from './modales/ticket-modal/ticket-modal.component';
import { TicketModalFase2Component } from './modales/ticket-modal-fase2/ticket-modal-fase2.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { RemisionModalComponent } from './modales/remision-modal/remision-modal.component';
import { CrearRemisionModalComponent } from './modales/crear-remision-modal/crear-remision-modal.component';
import { VerFotoRemisionModalComponent } from './modales/ver-foto-remision-modal/ver-foto-remision-modal.component';
import { SellosModalComponent } from './modales/sellos-modal/sellos-modal.component';
import { BitacoraComponent } from './componentes/bitacora/bitacora.component';
import { RelojModalComponent } from './modales/reloj-modal/reloj-modal.component';
import { CausalDeTiempoModalComponent } from './modales/causal-de-tiempo-modal/causal-de-tiempo-modal.component';

@NgModule({
  declarations: [MainComponent,
    NuevoTicketComponent,
    TicketModalComponent,
    TicketModalFase2Component,
    RemisionModalComponent,
    CrearRemisionModalComponent,
    VerFotoRemisionModalComponent,
    SellosModalComponent,
    BitacoraComponent,
    RelojModalComponent,
    CausalDeTiempoModalComponent
    ],
  imports: [
    CommonModule,
    NgbModalModule,
    TableModule,
    MessagesModule,
    MessageModule,
    DialogModule,
    TooltipModule,
    ToastModule,
    ReactiveFormsModule,
    TicketRoutesModule,
    ImageCropperModule,
    FormsModule,
    AccordionModule,
    ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'})
  ],
  entryComponents: [
    TicketModalComponent,
    TicketModalFase2Component,
    RemisionModalComponent,
    CrearRemisionModalComponent,
    VerFotoRemisionModalComponent,
    SellosModalComponent,
    RelojModalComponent,
    CausalDeTiempoModalComponent
  ]
})
export class TicketModule { }
