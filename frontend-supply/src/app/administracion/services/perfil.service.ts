import {EventEmitter, Injectable, Output} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {Perfil} from '../models/perfil';

@Injectable({
  providedIn: 'root'
})
export class PerfilService {

  @Output() changePerfil: EventEmitter<boolean> = new EventEmitter();

  constructor(private http: HttpClient) { }

  buscarPerfiles(): Observable<Perfil[]> {
    return this.http.get<Perfil[]>(environment.API_URL + '/servicesRest/supply/administrador/buscarPerfiles' );
  }
  buscarPerfilesActivos(): Observable<Perfil[]> {
    return this.http.get<Perfil[]>(environment.API_URL + '/servicesRest/supply/administrador/buscarPerfilesActivos' );
  }
  create(body: Perfil): Observable<any> {
    return this.http.post<any>(environment.API_URL + '/servicesRest/supply/administrador/crearPerfiles', body);
  }
  edit(id: number, text: string): Observable<any> {
    return this.http.get<any>(environment.API_URL + '/servicesRest/supply/administrador/editarPerfil/' + id + '/' + text);
  }
  activar(id: number): Observable<any> {
    return this.http.get<any>(environment.API_URL + '/servicesRest/supply/administrador/activarPerfil/' + id);
  }
  inctivar(id: number): Observable<any> {
    return this.http.get<any>(environment.API_URL + '/servicesRest/supply/administrador/inactivarPerfil/' + id);
  }

}
