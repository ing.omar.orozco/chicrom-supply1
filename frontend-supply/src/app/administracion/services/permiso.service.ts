import {EventEmitter, Injectable, Output} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Perfil} from '../models/perfil';
import {environment} from '../../../environments/environment';
import {Permiso} from '../models/permiso';

@Injectable({
  providedIn: 'root'
})
export class PermisoService {

  @Output() changePermiso: EventEmitter<boolean> = new EventEmitter();

  constructor(private http: HttpClient) { }

  buscarPermisos(): Observable<Permiso[]> {
    return this.http.get<Permiso[]>(environment.API_URL + '/servicesRest/supply/administradorPermisos/buscarPermisos' );
  }

  create(body: Permiso): Observable<any> {
    return this.http.post<any>(environment.API_URL + '/servicesRest/supply/administradorPermisos/crearPermisos', body);
  }
  edit(id: number, text: string): Observable<any> {
    return this.http.get<any>(environment.API_URL + '/servicesRest/supply/administradorPermisos/editarPermisos/' + id + '/' + text);
  }
  activar(id: number): Observable<any> {
    return this.http.get<any>(environment.API_URL + '/servicesRest/supply/administradorPermisos/activarPermisos/' + id);
  }
  inctivar(id: number): Observable<any> {
    return this.http.get<any>(environment.API_URL + '/servicesRest/supply/administradorPermisos/inactivarPermisos/' + id);
  }

}
