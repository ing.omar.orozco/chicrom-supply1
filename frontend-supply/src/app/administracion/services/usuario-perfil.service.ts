import {EventEmitter, Injectable, Output} from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {ArrayPermisoPerfiles} from '../models/arrayPermisoPerfiles';
import {HttpClient} from '@angular/common/http';
import {UsuarioPerfil} from '../models/usuarioPerfil';

@Injectable({
  providedIn: 'root'
})
export class UsuarioPerfilService {

  @Output() changeUsuarioPerfil: EventEmitter<boolean> = new EventEmitter();

  constructor(private http: HttpClient) { }

  buscarUsuarioPerfiles(): Observable<UsuarioPerfil[]> {
    return this.http.get<UsuarioPerfil[]>(environment.API_URL + '/servicesREST/adminUsuario/obtenerTodosLosUsuarioPerfil');
  }
  create(body: UsuarioPerfil): Observable<any> {
    return this.http.post<any>(environment.API_URL + '/servicesREST/adminUsuario/usuarioPerfil', body);
  }
  inctivar(idUsuario: number, idPerfil: number): Observable<any> {
    return this.http.get<any>(environment.API_URL + '/servicesREST/adminUsuario/inactivarUsuarioPerfil/' + idUsuario + '/' + idPerfil);
  }
}
