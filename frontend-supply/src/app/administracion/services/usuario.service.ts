import {EventEmitter, Injectable, Output} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {Usuario} from '../models/usuario';
import {ContratoUsuario} from '../models/contratoUsuario';
import {CargoUsuarios} from '../models/cargoUsuarios';
import {CiudadUsuarios} from '../models/ciudadUsuarios';
import {Planta} from '../models/planta';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  @Output() changeUsuario: EventEmitter<boolean> = new EventEmitter();

  constructor(private http: HttpClient) { }

  buscarUsuarios(): Observable<Usuario[]> {
    return this.http.get<Usuario[]>(environment.API_URL + '/servicesREST/adminUsuario/buscarTodosLosUsuarios' );
  }
  buscarTodosLosUsuariosActivos(): Observable<Usuario[]> {
    return this.http.get<Usuario[]>(environment.API_URL + '/servicesREST/adminUsuario/buscarTodosLosUsuariosActivos' );
  }
  create(body: Usuario): Observable<any> {
    return this.http.post<any>(environment.API_URL + '/servicesREST/adminUsuario/crearUsuario', body);
  }
  edit(body: Usuario): Observable<any> {
    return this.http.post<any>(environment.API_URL + '/servicesREST/adminUsuario/editarUsuario', body);
  }
  eliminar(id: number): Observable<any> {
    return this.http.get<any>(environment.API_URL + '/servicesREST/adminUsuario/eliminaUsuario/' + id);
  }
  activar(id: number): Observable<any> {
    return this.http.get<any>(environment.API_URL + '/servicesREST/adminUsuario/activaUsuario/' + id);
  }
  inctivar(id: number): Observable<any> {
    return this.http.get<any>(environment.API_URL + '/servicesREST/adminUsuario/inactivaUsuario/' + id);
  }
  buscarContratoUsuarios(): Observable<ContratoUsuario[]> {
    return this.http.get<ContratoUsuario[]>(environment.API_URL + '/servicesREST/adminUsuario/buscarContratoUsuarios' );
  }
  buscarCargoUsuarios(): Observable<CargoUsuarios[]> {
    return this.http.get<CargoUsuarios[]>(environment.API_URL + '/servicesREST/adminUsuario/buscarCargoUsuarios ' );
  }
  buscarCiudadUsuarios(): Observable<CiudadUsuarios[]> {
    return this.http.get<CiudadUsuarios[]>(environment.API_URL + '/servicesREST/adminUsuario/buscarCiudadUsuarios  ' );
  }
  buscarPlanta(): Observable<Planta[]> {
    return this.http.get<Planta[]>(environment.API_URL + '/servicesREST/adminUsuario/buscarPlantas' );
  }
  buscarTipoDoc(): Observable<Planta[]> {
    return this.http.get<Planta[]>(environment.API_URL + '/servicesREST/adminUsuario/obtenerTipoDocumentos' );
  }
}
