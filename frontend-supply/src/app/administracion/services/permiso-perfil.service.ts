import {EventEmitter, Injectable, Output} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {PermisosPerfiles} from '../models/permisosPerfiles';
import {ArrayPermisoPerfiles} from '../models/arrayPermisoPerfiles';

@Injectable({
  providedIn: 'root'
})
export class PermisoPerfilService {

  @Output() changePermisoPerfil: EventEmitter<boolean> = new EventEmitter();

  constructor(private http: HttpClient) { }

  buscarPermisosPerfiles(): Observable<PermisosPerfiles[]> {
    return this.http.get<PermisosPerfiles[]>(environment.API_URL + '/servicesRest/supply/administradorPermisoPerfil/buscarPermisoPerfiles' );
  }
  create(body: ArrayPermisoPerfiles[]): Observable<any> {
    return this.http.post<any>(environment.API_URL + '/servicesRest/supply/administradorPermisoPerfil/crearPermisoPerfil', body);
  }
  edit(id: number, idPermiso: number, idPerfil: number): Observable<any> {
    return this.http.get<any>(environment.API_URL + '/servicesRest/supply/administradorPermisoPerfil/editarPermisoPerfil/' + id + '/' + idPermiso + '/' + idPerfil);
  }
  activar(id: number): Observable<any> {
    return this.http.get<any>(environment.API_URL + '/servicesRest/supply/administradorPermisoPerfil/activarPermisoPerfil/' + id);
  }
  inctivar(idPermiso: number, idPerfil: number): Observable<any> {
    return this.http.get<any>(environment.API_URL + '/servicesRest/supply/administradorPermisoPerfil/inactivarPermisoPerfil/' + idPermiso + '/' + idPerfil);
  }

}
