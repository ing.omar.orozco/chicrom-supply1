
import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {MainComponent} from './componentes/main/main.component';
import {AuthenGuard} from '../core/services/authen.guard';
import {PerfilComponent} from './componentes/perfil/perfil.component';
import {PermisoComponent} from './componentes/permiso/permiso.component';
import {UsuarioComponent} from './componentes/usuario/usuario.component';
import {PermisoPerfilComponent} from './componentes/permiso-perfil/permiso-perfil.component';
import {UsuarioPerfilComponent} from './componentes/usuario-perfil/usuario-perfil.component';

export const routes: Routes = [

  {
    path: '',
    component: MainComponent,
    canActivate: [AuthenGuard],
    children: [
      {path: 'usuario', component: UsuarioComponent},
      {path: 'perfil', component: PerfilComponent},
      {path: 'permiso', component: PermisoComponent},
      {path: 'perfilPermiso', component: PermisoPerfilComponent},
      {path: 'usuarioPerfil', component: UsuarioPerfilComponent },
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdministracionRoutesModule { }

