import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {NotificacionesService} from '../../../core/services/notificaciones.service';
import {Permiso} from '../../models/permiso';
import {PermisoService} from '../../services/permiso.service';

@Component({
  selector: 'app-permiso-modal',
  templateUrl: './permiso-modal.component.html',
  styleUrls: ['./permiso-modal.component.css']
})
export class PermisoModalComponent implements OnInit {
  permisoForm: FormGroup;
  submitted = false;
  permiso: Permiso;
  estadoSelect: any[];

  @Input() public modalConfig: any;
  @Input() public modalData = new Permiso();

  constructor(
    public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
    private serviceNotificaciones: NotificacionesService,
    private permisoService: PermisoService,
  ) { }

  ngOnInit() {
    this.estadoSelect = [
      {nombre: 'Activo', id : 1},
      {nombre: 'Inactivo', id : 2}
    ];
    // validacion del formulario
    this.permisoForm = this.formBuilder.group({
      url: ['', Validators.required ],
      estado: ['' ]
    });
    if (this.modalConfig === 'Editar') {
      this.f['estado'.toString()].disable();
    }
  }

  get f() {return  this.permisoForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.permisoForm.invalid) {
      return;
    }
    if (this.modalConfig === 'Crear') {
      this.crear();
    } else {
      this.editar();
    }
  }
  crear() {
    this.serviceNotificaciones.showConfirmacion('Confirmación', '¿Seguro de crear este nuevo Permiso?')
      .then((result) => {
        if (result.value) {
          console.log(this.modalData)
          this.permisoService.create(this.modalData).subscribe( response => {
            this.permisoService.changePermiso.emit(true);
            this.activeModal.close('Close click');
            this.serviceNotificaciones.showAlertSucces('exitoso', 'Permiso  creado exitosamente');
          });
        }});
  }
  editar() {
    this.serviceNotificaciones.showConfirmacion('Confirmación', '¿Seguro de editar este  permiso?')
      .then((result) => {
        if (result.value) {
          console.log(this.modalData)
          this.permisoService.edit(this.modalData.id, this.modalData.url).subscribe( response => {
            this.permisoService.changePermiso.emit(true);
            this.activeModal.close('Close click');
            this.serviceNotificaciones.showAlertSucces('exitoso', 'Permiso  editado exitosamente');
          });
        }});
  }
}
