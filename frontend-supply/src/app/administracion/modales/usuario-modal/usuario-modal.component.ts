import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {NotificacionesService} from '../../../core/services/notificaciones.service';
import {Usuario} from '../../models/usuario';
import {ContratoUsuario} from '../../models/contratoUsuario';
import {CargoUsuarios} from '../../models/cargoUsuarios';
import {UsuarioService} from '../../services/usuario.service';
import {CiudadUsuarios} from '../../models/ciudadUsuarios';
import {Planta} from '../../models/planta';
import {TipoDoc} from '../../models/tipoDoc';

@Component({
  selector: 'app-usuario-modal',
  templateUrl: './usuario-modal.component.html',
  styleUrls: ['./usuario-modal.component.css']
})
export class UsuarioModalComponent implements OnInit {
  usuarioForm: FormGroup;
  submitted = false;
  usuario: Usuario;
  estadoSelect: any[];

  @Input() public modalConfig: any;
  @Input() public modalData = new Usuario();
  @Input() public contratoUsuarioList: ContratoUsuario[];
  @Input() public cargoUsuarioList: CargoUsuarios[];
  @Input() public ciudadUsuarioList: CiudadUsuarios[];
  @Input() public plantaList: Planta[];
  @Input() public tipoDocList: TipoDoc[];

  constructor(
    public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
    private serviceNotificaciones: NotificacionesService,
    private usuarioService: UsuarioService,
  ) { }

  ngOnInit() {
    this.estadoSelect = [
      {nombre: 'Activo', id : 1},
      {nombre: 'Inactivo', id : 2}
    ];
    // validacion del formulario
    this.usuarioForm = this.formBuilder.group({
      primerApellido: ['', Validators.required ],
      segundoApellido: ['', Validators.required ],
      primerNombre: ['', Validators.required ],
      segundoNombre: ['', Validators.required ],
      tipoDocumento: ['', Validators.required ],
      documento: ['', Validators.required ],
      idContratoUsuario: ['', Validators.required ],
      idCargoOcupacion: ['', Validators.required ],
      idCiudadUsuario: ['', Validators.required ],
      usuario: ['', Validators.required ],
      clave: ['', Validators.required ],
      idPlanta: ['', Validators.required ],
      estado: ['', Validators.required ]
    });
    if (this.modalConfig === 'Editar') {
      this.f['estado'.toString()].disable();
    }
  }

  get f() {return  this.usuarioForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.usuarioForm.invalid) {
      return;
    }
    if (this.modalConfig === 'Crear') {
      this.crear();
    } else {
      this.editar();
    }
  }
  crear() {
    this.serviceNotificaciones.showConfirmacion('Confirmación', '¿Seguro de crear este nuevo perfil?')
      .then((result) => {
        if (result.value) {
          this.modalData.idContratoUsuario = Number(this.modalData.idContratoUsuario);
          this.modalData.idCargoOcupacion = Number(this.modalData.idCargoOcupacion);
          this.modalData.idCiudadUsuario = Number(this.modalData.idCiudadUsuario);
          this.modalData.idPlanta = Number(this.modalData.idPlanta);
          this.modalData.estado = Number(this.modalData.estado);
          console.log(this.modalData)
          this.usuarioService.create(this.modalData).subscribe( response => {
            this.usuarioService.changeUsuario.emit(true);
            this.activeModal.close('Close click');
            this.serviceNotificaciones.showAlertSucces('exitoso', 'perfil  creado exitosamente');
          });
        }});
  }
  editar() {
    this.serviceNotificaciones.showConfirmacion('Confirmación', '¿Seguro de editar este  peril?')
      .then((result) => {
        if (result.value) {
          console.log(this.modalData)
          this.usuarioService.edit(this.modalData).subscribe( response => {
            this.usuarioService.changeUsuario.emit(true);
            this.activeModal.close('Close click');
            this.serviceNotificaciones.showAlertSucces('exitoso', 'perfil  editado exitosamente');
          });
        }});
  }
}
