import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Perfil} from '../../models/perfil';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {NotificacionesService} from '../../../core/services/notificaciones.service';
import {PerfilService} from '../../services/perfil.service';

@Component({
  selector: 'app-perfil-modal',
  templateUrl: './perfil-modal.component.html',
  styleUrls: ['./perfil-modal.component.css']
})
export class PerfilModalComponent implements OnInit {
  perfilForm: FormGroup;
  submitted = false;
  perfil: Perfil;
  estadoSelect: any[];

  @Input() public modalConfig: any;
  @Input() public modalData = new Perfil();

  constructor(
    public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
    private serviceNotificaciones: NotificacionesService,
    private perfilService: PerfilService,
  ) { }

  ngOnInit() {
    this.estadoSelect = [
      {nombre: 'Activo', id : 1},
      {nombre: 'Inactivo', id : 2}
    ];
    // validacion del formulario
    this.perfilForm = this.formBuilder.group({
      nombre: ['', Validators.required ],
      estado: ['' ]
    });
    if (this.modalConfig === 'Editar') {
      this.f['estado'.toString()].disable();
    }
  }

  get f() {return  this.perfilForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.perfilForm.invalid) {
      return;
    }
    if (this.modalConfig === 'Crear') {
      this.crear();
    } else {
      this.editar();
    }
  }
  crear() {
    this.serviceNotificaciones.showConfirmacion('Confirmación', '¿Seguro de crear este nuevo perfil?')
      .then((result) => {
        if (result.value) {
          console.log(this.modalData)
          this.perfilService.create(this.modalData).subscribe( response => {
            this.perfilService.changePerfil.emit(true);
            this.activeModal.close('Close click');
            this.serviceNotificaciones.showAlertSucces('exitoso', 'perfil  creado exitosamente');
          });
        }});
  }
  editar() {
    this.serviceNotificaciones.showConfirmacion('Confirmación', '¿Seguro de editar este  peril?')
      .then((result) => {
        if (result.value) {
          console.log(this.modalData)
          this.perfilService.edit(this.modalData.id, this.modalData.nombre).subscribe( response => {
            this.perfilService.changePerfil.emit(true);
            this.activeModal.close('Close click');
            this.serviceNotificaciones.showAlertSucces('exitoso', 'perfil  editado exitosamente');
          });
        }});
  }
}
