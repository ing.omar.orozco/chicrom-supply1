import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UsuarioPerfil} from '../../models/usuarioPerfil';
import {Usuario} from '../../models/usuario';
import {Perfil} from '../../models/perfil';
import {ArrayPermisoPerfiles} from '../../models/arrayPermisoPerfiles';
import {NotificacionesService} from '../../../core/services/notificaciones.service';
import {UsuarioPerfilService} from '../../services/usuario-perfil.service';
import {Permiso} from '../../models/permiso';

@Component({
  selector: 'app-usuario-perfil-modal',
  templateUrl: './usuario-perfil-modal.component.html',
  styleUrls: ['./usuario-perfil-modal.component.css']
})
export class UsuarioPerfilModalComponent implements OnInit {
  usuarioPerfilForm: FormGroup;
  submitted = false;

  selectedPerfiles: Perfil[];

  @Input() public modalConfig: any;
  @Input() public modalData = new UsuarioPerfil();
  @Input() public listaUsuario: Usuario[];

  @Input() public usuaiosPerfil: Perfil[];
  @Input() public listaPerfiles: Perfil[];


  constructor(
    private usuarioPerfilService: UsuarioPerfilService,
    public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
    private serviceNotificaciones: NotificacionesService,
  ) { }

  ngOnInit() {
    // validacion del formulario
    this.usuarioPerfilForm = this.formBuilder.group({
      idUsuario: ['', Validators.required ]
    });
  }


  get f() {return  this.usuarioPerfilForm.controls; }
  onSubmit() {
    this.submitted = true;
    if (this.usuarioPerfilForm.invalid) {
      return;
    }
    this.crear();
  }

  crear() {
    this.modalData.listaPerfiles = [];
    for (const i in this.selectedPerfiles) {
      const listaPerfiles = {
        id: this.selectedPerfiles[i].id
      };
      this.modalData.listaPerfiles.push(listaPerfiles);
    }
    this.modalData.idUsuario = Number(this.f['idUsuario'.toString()].value)
    console.log(this.modalData);
    if (this.modalData.listaPerfiles.length > 0) {
      this.serviceNotificaciones.showConfirmacion('Confirmación', '¿Seguro de crear este nuevo usuario pefil?')
        .then((result) => {
          if (result.value) {
            this.usuarioPerfilService.create(this.modalData).subscribe( response => {
              this.usuarioPerfilService.changeUsuarioPerfil.emit(true);
              this.activeModal.close('Close click');
              this.serviceNotificaciones.showAlertSucces('exitoso', 'Usuario pefil creado exitosamente');
            });
          }});
    }
  }
  inactivarPerfil(idPerfil: number) {
    this.serviceNotificaciones.showConfirmacion('Confirmación', '¿Seguro de inactivar este  perfil de este usuario?')
      .then((result) => {
        if (result.value) {
          this.usuarioPerfilService.inctivar(this.modalData.idUsuario, idPerfil).subscribe( response => {
            this.usuarioPerfilService.changeUsuarioPerfil.emit(true);
            this.activeModal.close('Close click');
            this.serviceNotificaciones.showAlertSucces('exitoso', 'usuario perfil  inactivado exitosamente');
          });
        }});
  }

}
