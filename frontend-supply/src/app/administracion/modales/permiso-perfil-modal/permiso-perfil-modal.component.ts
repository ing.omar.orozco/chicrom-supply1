import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PermisosPerfiles} from '../../models/permisosPerfiles';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {NotificacionesService} from '../../../core/services/notificaciones.service';
import {PermisoPerfilService} from '../../services/permiso-perfil.service';
import {Perfil} from '../../models/perfil';
import {Permiso} from '../../models/permiso';
import {ArrayPermisoPerfiles} from '../../models/arrayPermisoPerfiles';

@Component({
  selector: 'app-permiso-perfil-modal',
  templateUrl: './permiso-perfil-modal.component.html',
  styleUrls: ['./permiso-perfil-modal.component.css']
})
export class PermisoPerfilModalComponent implements OnInit {
  permisoPerfilForm: FormGroup;
  submitted = false;
  permiso: PermisosPerfiles;

  selectedPermisos: Permiso[];

  @Input() public modalConfig: any;
  @Input() public modalData = new PermisosPerfiles();
  @Input() public listaPermisos: Permiso[];
  @Input() public permisosPerfil: Permiso[];
  @Input() public listaPerfiles: Perfil[];

  @Input() public permisoPerfil: ArrayPermisoPerfiles[];

  constructor(
    public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
    private serviceNotificaciones: NotificacionesService,
    private permisoPerfilesService: PermisoPerfilService,
  ) { }

  ngOnInit() {

    // validacion del formulario
    this.permisoPerfilForm = this.formBuilder.group({
      idPerfil: ['', Validators.required ]
    });
    if (this.modalConfig === 'Editar') {
      this.f['idPerfil'.toString()].disable();
    }
  }

  get f() {return  this.permisoPerfilForm.controls; }
  onSubmit() {
    this.submitted = true;
    if (this.permisoPerfilForm.invalid) {
      return;
    }
    this.crear();

  }

  crear() {
    this.permisoPerfil = [];
    for (const i in this.selectedPermisos) {
      const permisoPef = {
        idPerfil: Number(this.f['idPerfil'.toString()].value),
        idPermiso: this.selectedPermisos[i].id
      };
      this.permisoPerfil.push(permisoPef);
    }
    console.log(this.permisoPerfil.length)
    if (this.permisoPerfil.length > 0) {
      this.serviceNotificaciones.showConfirmacion('Confirmación', '¿Seguro de crear este nuevo Permiso pefil?')
        .then((result) => {
          if (result.value) {
            this.permisoPerfilesService.create(this.permisoPerfil).subscribe( response => {
              this.permisoPerfilesService.changePermisoPerfil.emit(true);
              this.activeModal.close('Close click');
              this.serviceNotificaciones.showAlertSucces('exitoso', 'Permiso pefil creado exitosamente');
            });
          }});
    }
    }
  inactivarPerfil(idPermiso: number) {
    this.serviceNotificaciones.showConfirmacion('Confirmación', '¿Seguro de inactivar este permiso perfil?')
      .then((result) => {
        if (result.value) {
          this.permisoPerfilesService.inctivar(idPermiso, this.modalData.idPerfil).subscribe( response => {
            this.permisoPerfilesService.changePermisoPerfil.emit(true);
            this.activeModal.close('Close click');
            this.serviceNotificaciones.showAlertSucces('exitoso', 'permiso perfil  inactivado exitosamente');
          });
        }});
  }
}
