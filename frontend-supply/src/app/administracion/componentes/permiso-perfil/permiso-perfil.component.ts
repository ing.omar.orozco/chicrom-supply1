import { Component, OnInit } from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {NotificacionesService} from '../../../core/services/notificaciones.service';
import {PermisoPerfilService} from '../../services/permiso-perfil.service';
import {PermisosPerfiles} from '../../models/permisosPerfiles';
import {PermisoPerfilModalComponent} from '../../modales/permiso-perfil-modal/permiso-perfil-modal.component';
import {PermisoService} from '../../services/permiso.service';
import {PerfilService} from '../../services/perfil.service';

@Component({
  selector: 'app-permiso-perfil',
  templateUrl: './permiso-perfil.component.html',
  styleUrls: ['./permiso-perfil.component.css']
})
export class PermisoPerfilComponent implements OnInit {
  title: string;
  /*table*/
  loadTable: any[];
  permisoPerfilArray: PermisosPerfiles[];

  constructor(private permisoPerfilService: PermisoPerfilService,
              private permisoService: PermisoService,
              private perfilService: PerfilService,
              private modalService: NgbModal,
              private serviceNotificaciones: NotificacionesService) {
    this.title = 'Lista de perfil permiso ';
  }

  ngOnInit() {
    //  llenado del header tabla
    this.loadTable = [
      {field: 'nombrePerfil', header: 'Nombre perfil', class: 'w-50'}
    ];
    this.llenarTabla();
    this.permisoPerfilService.changePermisoPerfil.subscribe( data => {
      if (data === true) {
        this.llenarTabla();
      }
    });
  }

//  abir el modal de los dos modos editar y crear
  openModal(modo: string, perfil: any) {
    const modalRef = this.modalService.open(PermisoPerfilModalComponent, {size: 'xl' as 'lg'});
    modalRef.componentInstance.modalConfig = modo;
    this.permisoService.buscarPermisos().subscribe( response => {
      const permiso = response;
      if (modo === 'Crear') {
        modalRef.componentInstance.listaPermisos = permiso;
      } else {
        for (const i of perfil.listaPermisos) {
         /* result = permiso.filter(text => text.id !== i.id);*/
          const x = permiso.findIndex(value => value.id === i.id );
          permiso.splice(x, 1);
        }
        modalRef.componentInstance.listaPermisos = permiso;
      }

    });
    this.perfilService.buscarPerfilesActivos().subscribe( response => {
      modalRef.componentInstance.listaPerfiles = response;
    });
    if (modo === 'Editar') {
      modalRef.componentInstance.modalData = perfil;
      modalRef.componentInstance.permisosPerfil = perfil.listaPermisos;
    }
  }

  llenarTabla() {
    //  Llenado de la tabla por el servicio
    this.permisoPerfilService.buscarPermisosPerfiles().subscribe(data => {
      if (data !== null) {
        this.permisoPerfilArray = data;
        console.log(this.permisoPerfilArray)
      }
    });
  }
  activarPerfil(id: number) {
    this.serviceNotificaciones.showConfirmacion('Confirmación', '¿Seguro de activar este permiso perfil?')
      .then((result) => {
        if (result.value) {
          this.permisoPerfilService.activar(id).subscribe( response => {
            this.permisoPerfilService.changePermisoPerfil.emit(true);
            this.serviceNotificaciones.showAlertSucces('exitoso', 'permiso perfil  activado exitosamente');
          });
        }});
  }


}
