import { Component, OnInit } from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {NotificacionesService} from '../../../core/services/notificaciones.service';
import {PermisoService} from '../../services/permiso.service';
import {Permiso} from '../../models/permiso';
import {PermisoModalComponent} from '../../modales/permiso-modal/permiso-modal.component';

@Component({
  selector: 'app-permiso',
  templateUrl: './permiso.component.html',
  styleUrls: ['./permiso.component.css']
})
export class PermisoComponent implements OnInit {
  title: string;
  /*table*/
  loadTable: any[];
  permisoArray: Permiso[];

  constructor(private permisoService: PermisoService,
              private modalService: NgbModal,
              private serviceNotificaciones: NotificacionesService) {
    this.title = 'Lista de permisos';
  }

  ngOnInit() {
    //  llenado del header tabla
    this.loadTable = [
      {field: 'url', header: 'Url', class: 'w-50'},
      {field: 'estado', header: 'Estado', class: 'w-25'}
    ];
    this.llenarTabla();
    this.permisoService.changePermiso.subscribe( data => {
      if (data === true) {
        this.llenarTabla();
      }
    });
  }

//  abir el modal de los dos modos editar y crear
  openModal(modo: string, perfil: any) {
    const modalRef = this.modalService.open(PermisoModalComponent, {size: 'xl' as 'lg'});
    modalRef.componentInstance.modalConfig = modo;
    if (modo === 'Editar') {
      modalRef.componentInstance.modalData = perfil;
    }
  }

  llenarTabla() {
    //  Llenado de la tabla por el servicio
    this.permisoService.buscarPermisos().subscribe(data => {
      if (data !== null) {
        this.permisoArray = data;
      }
    });
  }
  activarPerfil(id: number) {
    this.serviceNotificaciones.showConfirmacion('Confirmación', '¿Seguro de activar este permiso?')
      .then((result) => {
        if (result.value) {
          this.permisoService.activar(id).subscribe( response => {
            this.permisoService.changePermiso.emit(true);
            this.serviceNotificaciones.showAlertSucces('exitoso', 'permiso  activado exitosamente');
          });
        }});
  }
  inactivarPerfil(id: number) {
    this.serviceNotificaciones.showConfirmacion('Confirmación', '¿Seguro de inactivar este permiso?')
      .then((result) => {
        if (result.value) {
          this.permisoService.inctivar(id).subscribe( response => {
            this.permisoService.changePermiso.emit(true);
            this.serviceNotificaciones.showAlertSucces('exitoso', 'permiso  inactivado exitosamente');
          });
        }});
  }

}
