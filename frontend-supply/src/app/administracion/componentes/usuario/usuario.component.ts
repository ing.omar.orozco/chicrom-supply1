import { Component, OnInit } from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {NotificacionesService} from '../../../core/services/notificaciones.service';
import {UsuarioService} from '../../services/usuario.service';
import {UsuarioModalComponent} from '../../modales/usuario-modal/usuario-modal.component';
import {Usuario} from '../../models/usuario';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {
  title: string;
  /*table*/
  loadTable: any[];
  usuarioArray: Usuario[];

  constructor(private usuarioService: UsuarioService,
              private modalService: NgbModal,
              private serviceNotificaciones: NotificacionesService) {
    this.title = 'Lista de usuarios';
  }

  ngOnInit() {
    //  llenado del header tabla
    this.loadTable = [
      {field: 'id', header: 'Id', class: 'w-25'},
      {field: 'tipoDocumento', header: 'Tipo Doc', class: 'w-25'},
      {field: 'documento', header: 'Documento', class: 'w-50'},
      {field: 'primerApellido', header: '1er Apellido', class: 'w-50'},
      {field: 'segundoApellido', header: '2do Apellido', class: 'w-50'},
      {field: 'primerNombre', header: '1er Nombre', class: 'w-50'},
      {field: 'segundoNombre', header: '2do Nombre', class: 'w-50'},
      {field: 'idContratoUsuario', header: 'Contrato', class: 'w-50'},
      {field: 'idCargoOcupacion', header: 'CargoOcupacion', class: 'w-50'},
      {field: 'idCiudadUsuario', header: 'Ciudad', class: 'w-50'},
      {field: 'usuario', header: 'Usuario', class: 'w-50'},
      {field: 'estado', header: 'Estado', class: 'w-25'},
      {field: 'idPlanta', header: 'Planta', class: 'w-50'}
    ];
    this.llenarTabla();
    this.usuarioService.changeUsuario.subscribe( data => {
      if (data === true) {
        this.llenarTabla();
      }
    });
  }

//  abir el modal de los dos modos editar y crear
  openModal(modo: string, perfil: any) {
    const modalRef = this.modalService.open(UsuarioModalComponent, {size: 'xl' as 'lg'});
    modalRef.componentInstance.modalConfig = modo;
    this.usuarioService.buscarContratoUsuarios().subscribe( response => {
      modalRef.componentInstance.contratoUsuarioList = response;
    });
    this.usuarioService.buscarCargoUsuarios().subscribe( response => {
      modalRef.componentInstance.cargoUsuarioList = response;
    });
    this.usuarioService.buscarCiudadUsuarios().subscribe( response => {
      modalRef.componentInstance.ciudadUsuarioList = response;
    });
    this.usuarioService.buscarPlanta().subscribe( response => {
      modalRef.componentInstance.plantaList = response;
    });
    this.usuarioService.buscarTipoDoc().subscribe( response => {
      modalRef.componentInstance.tipoDocList = response;
    });
    if (modo === 'Editar') {
      modalRef.componentInstance.modalData = perfil;
    }
  }

  llenarTabla() {
    //  Llenado de la tabla por el servicio
    this.usuarioService.buscarUsuarios().subscribe(data => {
      if (data !== null) {
        this.usuarioArray = data;
        console.log(this.usuarioArray)
      }
    });
  }
  activarUsuario(id: number) {
    this.serviceNotificaciones.showConfirmacion('Confirmación', '¿Seguro de activar este usuario?')
      .then((result) => {
        if (result.value) {
          this.usuarioService.activar(id).subscribe( response => {
            this.usuarioService.changeUsuario.emit(true);
            this.serviceNotificaciones.showAlertSucces('exitoso', 'usuario  activado exitosamente');
          });
        }});
  }
  inactivarUsuario(id: number) {
    this.serviceNotificaciones.showConfirmacion('Confirmación', '¿Seguro de inactivar este usuario?')
      .then((result) => {
        if (result.value) {
          this.usuarioService.inctivar(id).subscribe( response => {
            this.usuarioService.changeUsuario.emit(true);
            this.serviceNotificaciones.showAlertSucces('exitoso', 'usuario  inactivado exitosamente');
          });
        }});
  }
  eliminarUsuario(id: number) {
    this.serviceNotificaciones.showConfirmacion('Confirmación', '¿Seguro de eliminar este usuario?')
      .then((result) => {
        if (result.value) {
          this.usuarioService.eliminar(id).subscribe( response => {
            this.usuarioService.changeUsuario.emit(true);
            this.serviceNotificaciones.showAlertSucces('exitoso', 'usuario  eliminar exitosamente');
          });
        }});
  }

}
