import { Component, OnInit } from '@angular/core';
import {Perfil} from '../../models/perfil';
import {PerfilService} from '../../services/perfil.service';
import {NotificacionesService} from '../../../core/services/notificaciones.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {PerfilModalComponent} from '../../modales/perfil-modal/perfil-modal.component';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {
  title: string;
  /*table*/
  loadTable: any[];
  perfilArray: Perfil[];

  constructor(private perfilService: PerfilService,
              private modalService: NgbModal,
              private serviceNotificaciones: NotificacionesService,) {
    this.title = 'Lista de perfiles';
  }

  ngOnInit() {
    //  llenado del header tabla
    this.loadTable = [
      {field: 'nombre', header: 'Nombre', class: 'w-50'},
      {field: 'estado', header: 'Estado', class: 'w-25'}
    ];
    this.llenarTabla();
    this.perfilService.changePerfil.subscribe( data => {
      if (data === true) {
        this.llenarTabla();
      }
    });
  }

//  abir el modal de los dos modos editar y crear
  openModal(modo: string, perfil: any) {
    const modalRef = this.modalService.open(PerfilModalComponent, {size: 'xl' as 'lg'});
    modalRef.componentInstance.modalConfig = modo;
    if (modo === 'Editar') {
      modalRef.componentInstance.modalData = perfil;
    }
  }

  llenarTabla() {
    //  Llenado de la tabla por el servicio
    this.perfilService.buscarPerfiles().subscribe(data => {
      if (data !== null) {
        this.perfilArray = data;
      }
    });
  }
  activarPerfil(id: number) {
    this.serviceNotificaciones.showConfirmacion('Confirmación', '¿Seguro de activar este peril?')
      .then((result) => {
        if (result.value) {
          this.perfilService.activar(id).subscribe( response => {
            this.perfilService.changePerfil.emit(true);
            this.serviceNotificaciones.showAlertSucces('exitoso', 'perfil  activado exitosamente');
          });
        }});
  }
  inactivarPerfil(id: number) {
    this.serviceNotificaciones.showConfirmacion('Confirmación', '¿Seguro de inactivar este peril?')
      .then((result) => {
        if (result.value) {
          this.perfilService.inctivar(id).subscribe( response => {
            this.perfilService.changePerfil.emit(true);
            this.serviceNotificaciones.showAlertSucces('exitoso', 'perfil  inactivado exitosamente');
          });
        }});
  }

}
