import { Component, OnInit } from '@angular/core';
import {PerfilService} from '../../services/perfil.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {NotificacionesService} from '../../../core/services/notificaciones.service';
import {UsuarioPerfil} from '../../models/usuarioPerfil';
import {UsuarioPerfilService} from '../../services/usuario-perfil.service';
import {UsuarioPerfilModalComponent} from '../../modales/usuario-perfil-modal/usuario-perfil-modal.component';
import {UsuarioService} from '../../services/usuario.service';

@Component({
  selector: 'app-usuario-perfil',
  templateUrl: './usuario-perfil.component.html',
  styleUrls: ['./usuario-perfil.component.css']
})
export class UsuarioPerfilComponent implements OnInit {
  title: string;
  /*table*/
  loadTable: any[];
  usuarioPerfilArray: UsuarioPerfil[];

  constructor(private usuarioPerfilService: UsuarioPerfilService,
              private perfilService: PerfilService,
              private usuarioService: UsuarioService,
              private modalService: NgbModal,
              private serviceNotificaciones: NotificacionesService) {
    this.title = 'Lista de usuarios perfiles';
  }

  ngOnInit() {
    //  llenado del header tabla
    this.loadTable = [
      {field: 'nombreUsuario', header: 'Nombre Usuario', class: 'w-50'}
    ];
    this.llenarTabla();
    this.usuarioPerfilService.changeUsuarioPerfil.subscribe( data => {
      if (data === true) {
        this.llenarTabla();
      }
    });
  }

//  abir el modal de los dos modos editar y crear
  openModal(modo: string, obje: any) {
    console.log(obje)
    const modalRef = this.modalService.open(UsuarioPerfilModalComponent, {size: 'xl' as 'lg'});
    modalRef.componentInstance.modalConfig = modo;
    this.usuarioService.buscarTodosLosUsuariosActivos().subscribe(response => {
      modalRef.componentInstance.listaUsuario = response;
    });
    this.perfilService.buscarPerfiles().subscribe( response => {
      const perfiles = response;
      console.log(perfiles)
      if (modo === 'Crear') {
        modalRef.componentInstance.listaPerfiles = perfiles;
      } else {
        for (const i of obje.listaPerfiles) {
          /* result = permiso.filter(text => text.id !== i.id);*/
          const x = perfiles.findIndex(value => value.id === i.id );
          perfiles.splice(x, 1);
        }
        modalRef.componentInstance.listaPerfiles = perfiles;
      }
    });
    if (modo === 'Editar') {
      modalRef.componentInstance.modalData = obje;
      modalRef.componentInstance.usuaiosPerfil = obje.listaPerfiles;
    }
  }

  llenarTabla() {
    //  Llenado de la tabla por el servicio
    this.usuarioPerfilService.buscarUsuarioPerfiles().subscribe(data => {
      if (data !== null) {
        this.usuarioPerfilArray = data;
        console.log(this.usuarioPerfilArray)
      }
    });
  }

}
