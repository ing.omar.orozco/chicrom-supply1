import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AdministracionRoutesModule} from './administracion.routes';
import { MainComponent } from './componentes/main/main.component';
import { UsuarioComponent } from './componentes/usuario/usuario.component';
import { PerfilComponent } from './componentes/perfil/perfil.component';
import { PermisoComponent } from './componentes/permiso/permiso.component';
import {ReactiveFormsModule} from '@angular/forms';
import {AccordionModule} from 'primeng/primeng';
import {NgbModalModule} from '@ng-bootstrap/ng-bootstrap';
import {TableModule} from 'primeng/table';
import { PerfilModalComponent } from './modales/perfil-modal/perfil-modal.component';
import { PermisoModalComponent } from './modales/permiso-modal/permiso-modal.component';
import { UsuarioModalComponent } from './modales/usuario-modal/usuario-modal.component';
import { PermisoPerfilComponent } from './componentes/permiso-perfil/permiso-perfil.component';
import { PermisoPerfilModalComponent } from './modales/permiso-perfil-modal/permiso-perfil-modal.component';
import { UsuarioPerfilComponent } from './componentes/usuario-perfil/usuario-perfil.component';
import { UsuarioPerfilModalComponent } from './modales/usuario-perfil-modal/usuario-perfil-modal.component';

@NgModule({
  declarations: [
    MainComponent,
    UsuarioComponent,
    PerfilComponent
    , PermisoComponent,
    PerfilModalComponent,
    PermisoModalComponent,
    UsuarioModalComponent,
    PermisoPerfilComponent,
    PermisoPerfilModalComponent,
    UsuarioPerfilComponent,
    UsuarioPerfilModalComponent],
  imports: [
    CommonModule,
    NgbModalModule,
    TableModule,
    AdministracionRoutesModule,
    AccordionModule,
    ReactiveFormsModule,
    ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'})
  ],
  entryComponents: [
    PerfilModalComponent,
    PermisoModalComponent,
    UsuarioModalComponent,
    PermisoPerfilModalComponent,
    UsuarioPerfilModalComponent
  ]
})
export class AdministracionModule { }
