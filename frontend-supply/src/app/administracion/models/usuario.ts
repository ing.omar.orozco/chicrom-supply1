export class Usuario {
  id?: number;
  tipoDocumento: string;
  documento: number;
  primerApellido: string;
  segundoApellido: string;
  primerNombre: string;
  segundoNombre: string;
  idContratoUsuario: number;
  idCargoOcupacion: number;
  idCiudadUsuario: number;
  usuario: string;
  clave: string;
  estado: number;
  idPlanta: number;

  nombreCargo?: string;
  nombreCiudad?: string;
  nombreContrato?: string;
  nombrePlanta?: string;
  nombretipoDocumento?: string;
}
