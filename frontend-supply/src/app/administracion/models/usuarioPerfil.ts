import {Perfil} from './perfil';

export class UsuarioPerfil {
  idUsuario: number;
  listaPerfiles: any[];
  nombreUsuario?: string;
}
