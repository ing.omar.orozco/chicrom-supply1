export class PermisosPerfiles {
  idPermiso: number;
  idPerfil: number;
  nombrePerfil?: string;
  nombrePermiso?: string;
  id?: number;
  estado?: string;
}
