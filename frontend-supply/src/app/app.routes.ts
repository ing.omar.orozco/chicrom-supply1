
import { Routes } from '@angular/router';
import {AppComponent} from './app.component';

export const AppRoutes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: '',
    component: AppComponent,
    children: [
      {
        path: 'core',
        loadChildren: './core/core.module#CoreModule'
      },
      {
        path: 'login',
        loadChildren: './login/login.module#LoginModule'
      }

    ]
  }
];

