import { Component, OnInit } from '@angular/core';
import { UserIdleService } from 'angular-user-idle';
import { SessionStorageService } from 'angular-web-storage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor(
    private userIdle: UserIdleService,
    private  sessionStorage: SessionStorageService,
    private router: Router) { }
  title = 'supplyLogistics';
  ngOnInit(): void {
    // this.userIdle.startWatching();
    // console.log('si');

    // // Start watching when user idle is starting.
    // this.userIdle.onTimerStart().subscribe(count => console.log(count));

    // // Start watch when time is up.
    // this.userIdle.onTimeout().subscribe(() => this.cerrarSesion());

  }

  cerrarSesion() {
    this.sessionStorage.clear();
    this.router.navigate(['/']);
  }
}
