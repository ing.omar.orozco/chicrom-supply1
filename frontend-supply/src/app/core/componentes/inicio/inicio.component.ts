import { NotificacionesService } from 'src/app/core/services/notificaciones.service';
import { Component, OnInit } from '@angular/core';
import {Usuario} from '../../models/usuario';
import {LocalStorageService, SessionStorageService} from 'angular-web-storage';
import {ActivatedRoute, NavigationEnd, Route, Router} from '@angular/router';
import {filter} from 'rxjs/operators';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {
  usuario: Usuario;
  classBoolean: boolean;
  constructor(private  sessionStorage: SessionStorageService,
              private router: Router,private serviceLogin: NotificacionesService) {
    this.classBoolean = false;
    router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe((event: NavigationEnd) => {
      const url = event.url;
      if (url !== '/supply/supply#/core') {
        this.classBoolean = true;
      } else {
        this.classBoolean = false;
      }
      if (url !== '/core') {
        this.classBoolean = true;
      } else {
        this.classBoolean = false;
      }
    });
  }

  ngOnInit() {
    this.usuario = this.sessionStorage.get('usuario');
    console.log(this.usuario);
  }
  classBooleanActive() {
    this.classBoolean = true;
  }
  cerrarSesion() {
    this.usuario.forEach(element => {

      this.serviceLogin.closeLogin(element.idUsuario).subscribe(response =>{
          console.log(element.idUsuario);
          console.log('sesion cerrada');
      });
    });
    // console.log(this.usuario[0].idUsuario);
    // this.serviceLogin.closeLogin(id=1).subscribe(response =>{
    //   console.log('sesion cerrada');
    // });
    this.sessionStorage.clear();
    this.router.navigate(['/']);
  }
}
