import { Injectable } from '@angular/core';
import {HttpHandler, HttpRequest, HttpEvent, HttpResponse, HttpErrorResponse} from '@angular/common/http';
import {Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';
import {Observable, throwError} from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import {LocalStorageService, SessionStorageService} from 'angular-web-storage';

@Injectable()
export class HttpInterceptor implements HttpInterceptor {
  constructor(private spinner: NgxSpinnerService) {}

  /*https://scotch.io/@vigneshsithirai/angular-6-7-http-client-interceptor-with-error-handling*/

  intercept( request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.spinner.show();
    return next.handle(request).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          this.spinner.hide();
        }
        return event;
      }), catchError((error: HttpErrorResponse) => {

        this.spinner.hide();
        return throwError(error);
      }));
  }



}
