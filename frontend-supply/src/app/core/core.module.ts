import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './componentes/main/main.component';
import { InicioComponent } from './componentes/inicio/inicio.component';
import {CoreRoutesModule} from './core.routes';
import {NgxSpinnerModule} from 'ngx-spinner';

@NgModule({
  declarations: [MainComponent, InicioComponent],
  imports: [
    CommonModule,
    CoreRoutesModule,
    NgxSpinnerModule
  ]
})
export class CoreModule { }
