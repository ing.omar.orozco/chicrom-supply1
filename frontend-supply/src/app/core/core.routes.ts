
import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {AuthenGuard} from './services/authen.guard';
import {MainComponent} from './componentes/main/main.component';

export const routes: Routes = [

  {
    path: '',
    component: MainComponent,
    canActivate: [AuthenGuard],
    children: [
      {
        path: '',
        loadChildren: '../ticket/ticket.module#TicketModule'
      },
      {
        path: 'administracion',
        loadChildren: '../administracion/administracion.module#AdministracionModule'
      },
      {
        path: 'consulta',
        loadChildren: '../consultas/consultas.module#ConsultasModule'
      },
      {
        path: 'reporte',
        loadChildren: '../reportes/reporte.module#ReporteModule'
      }
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoreRoutesModule { }

