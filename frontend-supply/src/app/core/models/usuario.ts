export class Usuario {
  forEach(arg0: (element: any) => void) {
    throw new Error("Method not implemented.");
  }
  clave: string;
  nombreCargo: string;
  numeroContrato: string;
  perfil: string;
  primerNombre: string;
  segundoNombre: string;
  primerApellido: string;
  segundoApellido: string;
  usuario: string;
  url: string;
  idPlanta?: any;
  idUsuario?: any;

  constructor() {
    this.clave = null;
    this.nombreCargo = '';
    this.numeroContrato = '';
    this.perfil = '';
    this.primerNombre = '';
    this.segundoNombre = '';
    this.primerApellido = '';
    this.segundoApellido = '';
    this.usuario = '';
    this.url = '';
}
}
