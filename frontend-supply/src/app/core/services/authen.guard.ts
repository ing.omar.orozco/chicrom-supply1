import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {LocalStorageService, SessionStorageService} from 'angular-web-storage';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class AuthenGuard implements  CanActivate {

  constructor(public sessionStorage: SessionStorageService,
              private  router: Router) {
  }
  canActivate(
    next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if ((this.sessionStorage.get('usuario') !== null) ) {
    return true;
     } else {
      if (environment.production === false) {
        this.router.navigate(['/core']);
      } else {
        this.router.navigate(['/supply/supply#/core']);
      }
      return false;
    }

  }
}
