import { Injectable } from '@angular/core';
import swal from 'sweetalert2';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class NotificacionesService {

  constructor(private http: HttpClient) { }

  showConfirmacion(titulo: string, texto: string): any {
    const style = '<p><h5 style="color: black">' + texto + '</h5></p>';
    let titulo1 ;
    let titulo2 ;
    if (titulo === 'Sellos') {
       titulo1 = 'Cargue';
       titulo2 = 'Descargue';
    } else {
       titulo1 = 'Si';
       titulo2 = 'No';
    }
    return swal.fire({
      title: titulo,
      html: style,
      type: 'question',
      confirmButtonColor: '#CB601F',
      cancelButtonColor: '#5A5A5C',
      showCancelButton: true,
      confirmButtonText: titulo1,
      cancelButtonText: titulo2
    });
  }

  showAlertSucces(titulo: string, texto: string){
    const style = '<p><h5 style="color: black">' + texto + '</h5></p>';
    swal.fire({
      title: titulo,
      html: style,
      type: 'success'
    });
  }
  showAlertError(titulo: string, texto: string){
    const style = '<p><h5 style="color: black">' + texto + '</h5></p>';
    swal.fire({
      title: titulo,
      html: style,
      type: 'error'
    });
  }

  closeLogin(id: number): Observable<any> {
    return this.http.get<any>(environment.API_URL + '/servicesREST/JR/closelogin/' + id);
  }

}

