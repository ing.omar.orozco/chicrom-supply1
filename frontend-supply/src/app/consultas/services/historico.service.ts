import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';
@Injectable({
  providedIn: 'root'
})
export class HistoricoService {


  constructor(private http: HttpClient) { }

  consultaCerrado(idPalnta: number, fechaInicio: any, fechaFin: any): Observable<any> {
    return this.http.get<any>(environment.API_URL + '/servicesRest/supply/consulta/cerrados/' + idPalnta
    + '/' + fechaInicio + '/' + fechaFin);
  }
  // -- nuevo codigo //
  graficos1(idPalnta: number, fechaInicio: any, fechaFin: any): Observable<any> {
    return this.http.get<any>(environment.API_URL + '/servicesRest/supply/graficos/grafico1/' + idPalnta
    + '/' + fechaInicio + '/' + fechaFin);
  }
  graficos2(idPalnta: number, fechaInicio: any, fechaFin: any): Observable<any> {
    return this.http.get<any>(environment.API_URL + '/servicesRest/supply/graficos/grafico2/' + idPalnta
    + '/' + fechaInicio + '/' + fechaFin);
  }
  graficos3(idPalnta: number, fechaInicio: any, fechaFin: any): Observable<any> {
    return this.http.get<any>(environment.API_URL + '/servicesRest/supply/graficos/grafico3/' + idPalnta
    + '/' + fechaInicio + '/' + fechaFin);
  }

  graficos4(idPalnta: number, fechaInicio: any, fechaFin: any): Observable<any> {
    return this.http.get<any>(environment.API_URL + '/servicesRest/supply/graficos/grafico4/' + idPalnta
    + '/' + fechaInicio + '/' + fechaFin);
  }

  graficos5_c(idPalnta: number, fechaInicio: any, fechaFin: any): Observable<any> {
    return this.http.get<any>(environment.API_URL + '/servicesRest/supply/graficos/grafico5_c/' + idPalnta
    + '/' + fechaInicio + '/' + fechaFin);
  }

  graficos5_d(idPalnta: number, fechaInicio: any, fechaFin: any): Observable<any> {
    return this.http.get<any>(environment.API_URL + '/servicesRest/supply/graficos/grafico5_d/' + idPalnta
    + '/' + fechaInicio + '/' + fechaFin);
  }

  graficos6(idPalnta: number, fechaInicio: any, fechaFin: any): Observable<any> {
    return this.http.get<any>(environment.API_URL + '/servicesRest/supply/graficos/grafico6/' + idPalnta
    + '/' + fechaInicio + '/' + fechaFin);
  }

  graficos13(idPalnta: number, fechaInicio: any, fechaFin: any): Observable<any> {
    return this.http.get<any>(environment.API_URL + '/servicesRest/supply/graficos/grafico13/' + idPalnta
    + '/' + fechaInicio + '/' + fechaFin);
  }

  grafico13_inicio(idPalnta: number, fechaInicio: any, fechaFin: any): Observable<any> {
    return this.http.get<any>(environment.API_URL + '/servicesRest/supply/graficos/grafico13_inicio/' + idPalnta
    + '/' + fechaInicio + '/' + fechaFin);
  }

  grafico13_operacion(idPalnta: number, fechaInicio: any, fechaFin: any): Observable<any> {
    return this.http.get<any>(environment.API_URL + '/servicesRest/supply/graficos/grafico13_operacion/' + idPalnta
    + '/' + fechaInicio + '/' + fechaFin);
  }

  graficos14(idPalnta: number, fechaInicio: any, fechaFin: any): Observable<any> {
    return this.http.get<any>(environment.API_URL + '/servicesRest/supply/graficos/grafico14/' + idPalnta
    + '/' + fechaInicio + '/' + fechaFin);
  }

  grafico14_inicio(idPalnta: number, fechaInicio: any, fechaFin: any): Observable<any> {
    return this.http.get<any>(environment.API_URL + '/servicesRest/supply/graficos/grafico14_inicio/' + idPalnta
    + '/' + fechaInicio + '/' + fechaFin);
  }

  grafico14_operacion(idPalnta: number, fechaInicio: any, fechaFin: any): Observable<any> {
    return this.http.get<any>(environment.API_URL + '/servicesRest/supply/graficos/grafico14_operacion/' + idPalnta
    + '/' + fechaInicio + '/' + fechaFin);
  }

  graficos15(idPalnta: number, fechaInicio: any, fechaFin: any): Observable<any> {
    return this.http.get<any>(environment.API_URL + '/servicesRest/supply/graficos/grafico15/' + idPalnta
    + '/' + fechaInicio + '/' + fechaFin);
  }

  // fin nuevo codigo //
  public exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }
  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {type: EXCEL_TYPE});
    const fecha =  new  Date().toLocaleDateString();
    FileSaver.saveAs(data, fileName + fecha + EXCEL_EXTENSION);
  }
}
