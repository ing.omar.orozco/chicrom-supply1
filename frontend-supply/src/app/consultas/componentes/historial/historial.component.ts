import { element } from 'protractor';
import { Component, OnInit } from '@angular/core';
import {HistoricoService} from '../../services/historico.service';
import {Usuario} from '../../../core/models/usuario';
import {SessionStorageService} from 'angular-web-storage';
import {Historico} from '../../models/historico';
import {HistoricoExcel} from '../../models/historicoExcel';

import * as $ from 'jquery';

@Component({
  selector: 'app-historial',
  templateUrl: './historial.component.html',
  styleUrls: ['./historial.component.css']
})
export class HistorialComponent implements OnInit {


  constructor(private serviceHistorico: HistoricoService,
              private sessionStorage: SessionStorageService) { }
  usuarioList: Usuario[];
  consultaHistoricoArray: Historico[];
  GraficoHistoricoArray: [];
  consultaHistoricoExcel: HistoricoExcel[];
  excelObje: HistoricoExcel;
  rangeDates: Date[];
  // tslint:disable-next-line: ban-types
  tiposgraph: Number;
  tiposgraficos = Array<TiposGraficos>();

  googleChartsLoaded = false;

  ngOnInit() {
    this.usuarioList = this.sessionStorage.get('usuario');
    this.tiposgraficos = Array<TiposGraficos>();
    this.tiposgraficos.push(new TiposGraficos(-1, 'Seleccione un tipo de gráfico'));
    this.tiposgraficos.push(new TiposGraficos(1, '1 - Total volumen por mes por dependencia'));
    this.tiposgraficos.push(new TiposGraficos(2, '2 - Volumen por tipo de operación'));
    this.tiposgraficos.push(new TiposGraficos(3, '3 - Volumenes vs Jornadas'));
    this.tiposgraficos.push(new TiposGraficos(4, '4 - Volumenes vs Tiempo de operacion por muelle'));
    this.tiposgraficos.push(new TiposGraficos(5, '5 - Volumenes vs Promedio Tiempo de operacion por muelle'));
    this.tiposgraficos.push(new TiposGraficos(6, '6 - Productividad diaria por muelle'));
    this.tiposgraficos.push(new TiposGraficos(7, '7 - HR inicio de operación real vs esperada'));
    this.tiposgraficos.push(new TiposGraficos(8, '8 - Distribución de jornadas de operación'));
    this.tiposgraficos.push(new TiposGraficos(9, '9 - Comportamiento tiempo de atención total'));
    this.tiposgraficos.push(new TiposGraficos(10, '10 - Tiempo de espera para el inicio de operación'));
    this.tiposgraficos.push(new TiposGraficos(11, '11 - Tiempo de operación'));
    this.tiposgraficos.push(new TiposGraficos(12, '12 - Tiempo de operación por muelle'));
    this.tiposgraficos.push(new TiposGraficos(13, '13 - Análisis de tiempos perdidos'));
    this.tiposgraficos.push(new TiposGraficos(14, '14 - Análisis de tiempos perdidos por muelles'));
    this.tiposgraficos.push(new TiposGraficos(15, '15 - Volumenes vs Horas perdidas totales'));
    this.tiposgraficos.push(new TiposGraficos(16, '16 - Kpls Gestión operador logístico'));










    google.charts.load('current', { packages: ['corechart','table'] });
    google.charts.setOnLoadCallback(() => this.onGoogleChartsLoaded());
  }

  generarGraficos() {
    const fechaInicio = new Date( this.rangeDates[0]).toISOString().slice(0, 10);
    const fechaFin = new Date( this.rangeDates[1]).toISOString().slice(0, 10);
    const tiposgraphs = this.tiposgraph;
    switch (tiposgraphs.toString()) {
      case '1':
        console.log('si');
        this.grafico1(fechaInicio, fechaFin);
        break;
      case '2':
        this.grafico2(fechaInicio, fechaFin);
        break;
      case '3':
        this.grafico3(fechaInicio, fechaFin);
        break;
      case '4':
        this.grafico4(fechaInicio, fechaFin);
        break;
      case '5':
        this.grafico5(fechaInicio, fechaFin);
        break;
      case '6':
        this.grafico6(fechaInicio, fechaFin);
        break;
      case '7':
        this.grafico7(fechaInicio, fechaFin);
        break;
      case '8':
        this.grafico8(fechaInicio, fechaFin);
        break;
      case '9':
        this.grafico9(fechaInicio, fechaFin);
        break;
      case '10':
        this.grafico10(fechaInicio, fechaFin);
        break;
      case '11':
        this.grafico11(fechaInicio, fechaFin);
        break;
      case '12':
        this.grafico12(fechaInicio, fechaFin);
        break;
      case '13':
        this.grafico13(fechaInicio, fechaFin);
        break;
      case '14':
        this.grafico14(fechaInicio, fechaFin);
        break;
      case '15':
        this.grafico15(fechaInicio, fechaFin);
        break;
      case '16':
        // this.grafico16(fechaInicio, fechaFin);
        break;
      case '17':
        // this.grafico17(fechaInicio, fechaFin);
        break;
      case '18':
        // this.grafico18(fechaInicio, fechaFin);
        break;
      case '19':
        // this.grafico19(fechaInicio, fechaFin);
        break;
      default:
        break;
    }
  }

  onGoogleChartsLoaded() {
    this.googleChartsLoaded = true;
  }
  grafico16(fechaInicio, fechaFin) {

  }
  grafico15(fechaInicio, fechaFin) {
    this.serviceHistorico.graficos15(this.usuarioList[0].idPlanta, fechaInicio, fechaFin)
    .subscribe( respose => {
      const datosgraficos = Array();
      const cabecera = Array();
      let bandera = 0;
      let elemento = "";
      let fecha = "";
      let cantidad = 0;
      let tiempo = 0.0;
      // agregando cabecera
      cabecera.push('Fecha');
      cabecera.push('Viajes');
      cabecera.push('Tiempo perdido por viaje');
      cabecera.push('Tiempo perdido total');


      // agregando cabecera en matrix de google chart
      datosgraficos.push(cabecera);

      respose.forEach(element => {
        let temporal = Array();
        temporal.push(element.fecha);
        temporal.push(element.viajes);
        let tpv = element.minutos / element.viajes;
        let h = tpv / 60;
        let m = tpv % 60;
        temporal.push([(h | 0),(m | 0),0]);
        let tpt = element.minutos;
        let ht = tpt / 60;
        let mt = tpt % 60;
        temporal.push([(ht | 0),(mt | 0),0]);
        datosgraficos.push(temporal);
      });
      //https://jsfiddle.net/7yuxxw3q/1/
      const data = google.visualization.arrayToDataTable(datosgraficos);

      // opciones del grafico
      let arraySeries = Array();
      arraySeries.push({
        type: 'bars',
        targetAxisIndex: 0,
        annotations: {
          stem: {
            color: '#000000',
          },
          textStyle: {
            color: '#000000',
          }
        }
      });
      arraySeries.push({
        type: 'linea',
        targetAxisIndex: 1,
        color: '#cb6027',
        annotations: {
          stem: {
            color: '#cb6027',
          },
          textStyle: {
            color: '#cb6027',
          }
        }
      });
      arraySeries.push({
        type: 'linea',
        targetAxisIndex: 1,
        color: '#3fc314',
        annotations: {
          stem: {
            color: '#3fc314',
          },
          textStyle: {
            color: '#3fc314',
          }
        }
      });

      const  options = {
        title: 'Volumenes vs Horas perdidas totales',
        height: $(window).height()*0.75,
        vAxes: [{
            minValue: 0,
            maxValue: [23,59,59]
        }, {
            minValue: [0,0,0],
            maxValue: [23,59,59]
        }],
        hAxis: {
            title: ''
        },
        seriesType: 'linea',
        series: arraySeries,
        axes: {
            y: {
                0: {
                    label: 'leftyaxis'
                },
                1: {
                    side: 'right',
                    label: 'rightyaxis'
                }
            }
    }
    };

      const view = new google.visualization.DataView(data);
      let legenda = Array();
      legenda.push(0);
      legenda.push(1);
      legenda.push({
        calc: 'stringify',
        sourceColumn: 1,
        type: 'string',
        role: 'annotation'});
      legenda.push(2);
      legenda.push({
        calc: 'stringify',
        sourceColumn: 2,
        type: 'string',
        role: 'annotation'});
      legenda.push(3);


      view.setColumns(legenda);

      const chart = new google.visualization.ComboChart(
        document.getElementById('googlechart-container')
      );

      chart.draw(view, options);

    });
  }
  grafico14(fechaInicio, fechaFin) {
    document.getElementById('googlechart-container').innerHTML = '<h3>Muelles Inicio por muelles</h3><div id="googlechart-inicio"></div>' +
    '<br><br><h3>Muelles Operaciones por muelles</h3><div id="googlechart-operacion"></div> '+
    '<br><br><h3>Muelles Totales por muelles</h3><div id="googlechart-total"></div>';
    this.serviceHistorico.graficos14(this.usuarioList[0].idPlanta, fechaInicio, fechaFin)
      .subscribe( respose => {
        console.log(respose);
        let data = new google.visualization.DataTable();
        data.addColumn('string', 'Muelles');
        data.addColumn('string', 'Horas Perdidas');
        data.addColumn('number', 'Horas Perdidas %');
        let cien = 0;
        respose.forEach(element => {
          cien =cien + element.minutos;
        });

        let datos = Array()
        respose.forEach(element => {
          let porcen = (element.minutos * 100)/cien;
          let fila = Array();
          fila.push(element.causal);
          let h = (element.minutos/60 | 0);
          let m = (element.minutos%60 | 0);
          let t = h + ":" + m;
          fila.push(t);
          fila.push(porcen);
          datos.push(fila);
        });

        let fila = Array();
        let h = (cien/60 | 0);
        let m = (cien%60 | 0);
        let t = h + ":" + m;
        fila.push('Total General');
        fila.push(t);
        fila.push(100);
        datos.push(fila);
        data.addRows(datos);

        let table = new google.visualization.Table(document.getElementById('googlechart-total'));

        table.draw(data, {width:'50%', height: '100%'});
      });
    this.serviceHistorico.grafico14_inicio(this.usuarioList[0].idPlanta, fechaInicio, fechaFin)
    .subscribe( respose => {
      console.log(respose);
      let data = new google.visualization.DataTable();
      data.addColumn('string', 'Muelles');
      data.addColumn('string', 'Horas Perdidas');
      data.addColumn('number', 'Horas Perdidas %');
      let cien = 0;
      respose.forEach(element => {
        cien =cien + element.minutos;
      });

      let datos = Array()
      respose.forEach(element => {
        let porcen = (element.minutos * 100)/cien;
        let fila = Array();
        fila.push(element.causal);
        let h = (element.minutos/60 | 0);
        let m = (element.minutos%60 | 0);
        let t = h + ":" + m;
        fila.push(t);
        fila.push(porcen);
        datos.push(fila);
      });
      let fila = Array();
      let h = (cien/60 | 0);
      let m = (cien%60 | 0);
      let t = h + ":" + m;
      fila.push('Total General');
      fila.push(t);
      fila.push(100);
      datos.push(fila);
      data.addRows(datos);

      let table = new google.visualization.Table(document.getElementById('googlechart-inicio'));

      table.draw(data, {width:'50%',height: '100%'});
    });
    this.serviceHistorico.grafico14_operacion(this.usuarioList[0].idPlanta, fechaInicio, fechaFin)
      .subscribe( respose => {
        console.log(respose);
        let data = new google.visualization.DataTable();
        data.addColumn('string', 'Muelles');
        data.addColumn('string', 'Horas Perdidas');
        data.addColumn('number', 'Horas Perdidas %');
        let cien = 0;
        respose.forEach(element => {
          cien =cien + element.minutos;
        });

        let datos = Array()
        respose.forEach(element => {
          let porcen = (element.minutos * 100)/cien;
          let fila = Array();
          fila.push(element.causal);
          let h = (element.minutos/60 | 0);
          let m = (element.minutos%60 | 0);
          let t = h + ":" + m;
          fila.push(t);
          fila.push(porcen);
          datos.push(fila);
        });
        let fila = Array();
        let h = (cien/60 | 0);
        let m = (cien%60 | 0);
        let t = h + ":" + m;
        fila.push('Total General');
        fila.push(t);
        fila.push(100);
        datos.push(fila);
        data.addRows(datos);

        const options = {
          animation:{
            duration: 1000,
            easing: 'out',
            startup: true
          },
          width:'50%', height: '100%'
        };

        let table = new google.visualization.Table(document.getElementById('googlechart-operacion'));

        table.draw(data, options );
      });
  }
  grafico13(fechaInicio, fechaFin) {
    document.getElementById('googlechart-container').innerHTML = '<h3>Muelles Inicio</h3><div id="googlechart-inicio"></div>' +
    '<br><br><h3>Muelles Operaciones</h3><div id="googlechart-operacion"></div> '+
    '<br><br><h3>Muelles Totales</h3><div id="googlechart-total"></div>';
    this.serviceHistorico.graficos13(this.usuarioList[0].idPlanta, fechaInicio, fechaFin)
      .subscribe( respose => {
        console.log(respose);
        let data = new google.visualization.DataTable();
        data.addColumn('string', 'Causales');
        data.addColumn('string', 'Horas Perdidas');
        data.addColumn('number', 'Horas Perdidas %');
        let cien = 0;
        respose.forEach(element => {
          cien =cien + element.minutos;
        });

        let datos = Array()
        respose.forEach(element => {
          let porcen = (element.minutos * 100)/cien;
          let fila = Array();
          fila.push(element.causal);
          let h = (element.minutos/60 | 0);
          let m = (element.minutos%60 | 0);
          let t = h + ":" + m;
          fila.push(t);
          fila.push(porcen);
          datos.push(fila);
        });
        let fila = Array();
        let h = (cien/60 | 0);
        let m = (cien%60 | 0);
        let t = h + ":" + m;
        fila.push('Total General');
        fila.push(t);
        fila.push(100);
        datos.push(fila);
        data.addRows(datos);

        let table = new google.visualization.Table(document.getElementById('googlechart-total'));

        table.draw(data, {width:'50%', height: '100%'});
      });
    this.serviceHistorico.grafico13_inicio(this.usuarioList[0].idPlanta, fechaInicio, fechaFin)
    .subscribe( respose => {
      console.log(respose);
      let data = new google.visualization.DataTable();
      data.addColumn('string', 'Causales');
      data.addColumn('string', 'Horas Perdidas');
      data.addColumn('number', 'Horas Perdidas %');
      let cien = 0;
      respose.forEach(element => {
        cien =cien + element.minutos;
      });

      let datos = Array()
      respose.forEach(element => {
        let porcen = (element.minutos * 100)/cien;
        let fila = Array();
        fila.push(element.causal);
        let h = (element.minutos/60 | 0);
        let m = (element.minutos%60 | 0);
        let t = h + ":" + m;
        fila.push(t);
        fila.push(porcen);
        datos.push(fila);
      });
      let fila = Array();
      let h = (cien/60 | 0);
      let m = (cien%60 | 0);
      let t = h + ":" + m;
      fila.push('Total General');
      fila.push(t);
      fila.push(100);
      datos.push(fila);
      data.addRows(datos);

      let table = new google.visualization.Table(document.getElementById('googlechart-inicio'));

      table.draw(data, {width:'50%',height: '100%'});
    });
    this.serviceHistorico.grafico13_operacion(this.usuarioList[0].idPlanta, fechaInicio, fechaFin)
      .subscribe( respose => {
        console.log(respose);
        let data = new google.visualization.DataTable();
        data.addColumn('string', 'Causales');
        data.addColumn('string', 'Horas Perdidas');
        data.addColumn('number', 'Horas Perdidas %');
        let cien = 0;
        respose.forEach(element => {
          cien =cien + element.minutos;
        });

        let datos = Array()
        respose.forEach(element => {
          let porcen = (element.minutos * 100)/cien;
          let fila = Array();
          fila.push(element.causal);
          let h = (element.minutos/60 | 0);
          let m = (element.minutos%60 | 0);
          let t = h + ":" + m;
          fila.push(t);
          fila.push(porcen);
          datos.push(fila);
        });
        let fila = Array();
        let h = (cien/60 | 0);
        let m = (cien%60 | 0);
        let t = h + ":" + m;
        fila.push('Total General');
        fila.push(t);
        fila.push(100);
        datos.push(fila);
        data.addRows(datos);

        const options = {
          animation:{
            duration: 1000,
            easing: 'out',
            startup: true
          },
          width:'50%', height: '100%'
        };

        let table = new google.visualization.Table(document.getElementById('googlechart-operacion'));

        table.draw(data, options );
      });
  }
  grafico12(fechaInicio, fechaFin) {
    this.serviceHistorico.graficos6(this.usuarioList[0].idPlanta, fechaInicio, fechaFin)
      .subscribe( respose => {
        let muelles  = Array();
        respose.forEach(element => {
          muelles.push(element.muelle);
        });

        let newArr = Array();
        muelles.forEach((item, index) => {
          if (newArr.findIndex(i => i == item) === -1) {
          newArr.push(item);
        }
        });
        muelles = newArr;
        let capas = '';
        muelles.forEach(element => {
          capas = capas + '<div id="googlechart-' + element + '"></div>';
        });
        document.getElementById('googlechart-container').innerHTML = capas;
        // iniciando graficos por muelles
        // array por muelles
        muelles.forEach(element3 => {
          let graficosmuelles = Array();
          let i = 0;
          respose.forEach(element1 => {
            if(element3 === element1.muelle){
              let dato_tmp = Array();
              dato_tmp.push(element1.fechaCreacion2);
              dato_tmp.push(element1.fechaCreacion);
              dato_tmp.push(element1.tiempoOperacion);
              dato_tmp.push(element1.horaVerificacion);
              dato_tmp.push(element1.tiempoAtencionSinAfectacion);

              graficosmuelles.push(dato_tmp);
            }
          });
          console.log(element3);
          // ordenando array de carga
          i = 0;
          let bandera = 0;
          let elemento = "";
          let fecha = "";
          let cantidad = 0;
          let tiempo = 0.0;
          let tiempo2 = 0.0;
          let datosgraficos = Array();
          let cabecera = Array();
          // agregando cabecera
          cabecera.push('Fecha');
          cabecera.push('Promedio de tiempo de operación');
          cabecera.push('Promedio de tiempo de atención sin afectación');
          datosgraficos.push(cabecera);

          let maximo = 0;
          let tiempo_inicio = 0;
          let tiempo_fin = 0;
          graficosmuelles.forEach(element => {
            console.log(element);
            i = i + 1;
            if (bandera === 0){
              elemento = element[0];
              bandera = 1;
              fecha = element[0];
              tiempo_inicio = element[3];
            }
            if (element[0] === elemento) {
              // tiempo = tiempo + element.tiempoOperacion;
              tiempo2 = tiempo2 + element[4];
              cantidad = cantidad + 1;
              tiempo = tiempo + element[2];
              tiempo_fin = element[1];
              if (i === graficosmuelles.length) {
                const temp_datos =  Array();
                temp_datos.push(fecha.toString());
                temp_datos.push(this.convertidorDecimalTiempo(tiempo/cantidad));
                temp_datos.push(this.convertidorDecimalTiempo(tiempo2/cantidad));
                datosgraficos.push(temp_datos);
                if (cantidad > maximo) {
                  maximo = cantidad;
                }
              }
            } else {
              if (!(fecha === '') && !(cantidad === 0)) {
                const temp_datos =  Array();
                temp_datos.push(fecha.toString());
                temp_datos.push(this.convertidorDecimalTiempo(tiempo/cantidad));
                temp_datos.push(this.convertidorDecimalTiempo(tiempo2/cantidad));
                datosgraficos.push(temp_datos);
                if (cantidad > maximo) {
                  maximo = cantidad;
                }
              }
              bandera = 0;
              cantidad = 0;
              tiempo = 0;
              fecha = '';
              tiempo2 = 0;
            }
          });
          // covertimos el array para ser usado por google chart
          console.log(datosgraficos);
          let data = google.visualization.arrayToDataTable(datosgraficos);
          const view = new google.visualization.DataView(data);
          const legenda = Array();
          legenda.push(0);
          legenda.push(1);
          legenda.push({
            calc: 'stringify',
            sourceColumn: 1,
            type: 'string',
            role: 'annotation'});
          legenda.push(2);
          legenda.push({
            calc: 'stringify',
            sourceColumn: 2,
            type: 'string',
            role: 'annotation'});

          view.setColumns(legenda);
          let arraySeries = Array();
          arraySeries.push({
            annotations: {
              stem: {
                color: '#000000',
              },
              textStyle: {
                color: '#000000',
              }
            }
          });
          arraySeries.push({
            annotations: {
              stem: {
                color: '#000000',
              },
              textStyle: {
                color: '#000000',
              }
            }
          });

          const options = {
            animation:{
              duration: 1000,
              easing: 'out',
              startup: true
            },
            vAxes: [{
              minValue: [0,0,0],
              maxValue: [23,59,59]
            }],
            hAxis: {
              title: 'Muelle: ' + element3
            },
            height: $(window).height()*0.75,
            // width: $(window).width(),
            title: 'Tiempo de espera para inicio de operación',
            legend: { position: 'top' },
            // isStacked: true,
            alignment: 'start',
            series: arraySeries

          };

          const chart = new google.visualization.LineChart(
            document.getElementById('googlechart-' + element3)
          );
          chart.draw(view, options as google.visualization.ColumnChartOptions);
        });

      });
  }
  grafico11(fechaInicio, fechaFin) {
    document.getElementById('googlechart-container').innerHTML = '<div id="googlechart-carga"></div> <div id="googlechart-descarga"></div>';
    this.serviceHistorico.graficos5_c(this.usuarioList[0].idPlanta, fechaInicio, fechaFin)
      .subscribe( respose => {
      const datosgraficos = Array();
      let cabecera = Array();

      // agregando cabecera
      cabecera.push('Fecha');
      cabecera.push('Promedio de tiempo de operación');
      cabecera.push('Promedio de tiempo de atención sin afectación');
      cabecera.push({ role: 'annotation' });
      datosgraficos.push(cabecera);
      // variables acumuladores
      let i = 0;
      let maximo = 0;
      let bandera = 0;
      let elemento = "";
      let fecha = "";
      let cantidad = 0;
      let tiempo = 0.0;
      let tiempo2 = 0.0;

      respose.forEach(element => {
        i = i + 1;
        if (bandera === 0){
          elemento = element.fechaCreacion2;
          bandera = 1;
          fecha = element.fechaCreacion2;
        }

        if (element.fechaCreacion2 === elemento) {
          // console.log(element.tiepoTotalPlanta);
          tiempo = tiempo + element.tiempoOperacion;
          tiempo2 = tiempo2 + element.tiempoAtencionSinAfectacion;
          cantidad = cantidad + 1;
          if (i === respose.length) {
            const temp_datos =  Array();
            temp_datos.push(fecha);
            temp_datos.push(this.convertidorDecimalTiempo(tiempo/cantidad));
            temp_datos.push(this.convertidorDecimalTiempo(tiempo2/cantidad));
            temp_datos.push('');
            // temp_datos.push(cien);
            if (cantidad > maximo) {
              maximo = cantidad;
            }
            datosgraficos.push(temp_datos);
          }

        } else {
          if (!(fecha === '')) {
            const temp_datos =  Array();
            temp_datos.push(fecha);
            temp_datos.push(this.convertidorDecimalTiempo(tiempo/cantidad));
            temp_datos.push(this.convertidorDecimalTiempo(tiempo2/cantidad));
            temp_datos.push('');
            if (cantidad > maximo) {
              maximo = cantidad;
            }
            datosgraficos.push(temp_datos);
          }
          bandera = 0;
          cantidad = 0;
          tiempo = 0;
          tiempo2 = 0;
          fecha = '';
        }
      });

      const data = google.visualization.arrayToDataTable(datosgraficos);

      const view = new google.visualization.DataView(data);
      const legenda = Array();
      legenda.push(0);
      legenda.push(1);
      legenda.push({
        calc: 'stringify',
        sourceColumn: 1,
        type: 'string',
        role: 'annotation'});
      legenda.push(2);
      legenda.push({
        calc: 'stringify',
        sourceColumn: 2,
        type: 'string',
        role: 'annotation'});
      legenda.push(3);
      legenda.push({
        calc: 'stringify',
        sourceColumn: 3,
        type: 'string',
        role: 'annotation'});

      view.setColumns(legenda);
      let arraySeries = Array();
      arraySeries.push({
        annotations: {
          stem: {
            color: '#000000',
          },
          textStyle: {
            color: '#000000',
          }
        }
      });
      arraySeries.push({
        annotations: {
          stem: {
            color: '#000000',
          },
          textStyle: {
            color: '#000000',
          }
        }
      });
      arraySeries.push({
        annotations: {
          stem: {
            color: '#000000',
          },
          textStyle: {
            color: '#000000',
          }
        }
      });
      const options = {
        animation:{
          duration: 1000,
          easing: 'out',
          startup: true
        },
        vAxes: [{
          minValue: [0,0,0],
          maxValue: [23,59,59]
        }],
        hAxis: {
          title: 'Carga'
        },
        height: $(window).height()*0.75,
        // width: $(window).width(),
        title: 'Tiempo de operación',
        legend: { position: 'top' },
        // isStacked: true,
        alignment: 'start',
        series: arraySeries

      };

      let chart = new google.visualization.LineChart(document.getElementById('googlechart-carga'));

      chart.draw(view, options as google.visualization.ColumnChartOptions);

    });
    this.serviceHistorico.graficos5_d(this.usuarioList[0].idPlanta, fechaInicio, fechaFin)
      .subscribe( respose => {
      const datosgraficos = Array();
      let cabecera = Array();

      // agregando cabecera
      cabecera.push('Fecha');
      cabecera.push('Promedio de tiempo de operación');
      cabecera.push('Promedio de tiempo de atención sin afectación');
      cabecera.push({ role: 'annotation' });
      datosgraficos.push(cabecera);
      // variables acumuladores
      let i = 0;
      let maximo = 0;
      let bandera = 0;
      let elemento = "";
      let fecha = "";
      let cantidad = 0;
      let tiempo = 0.0;
      let tiempo2 = 0.0;

      respose.forEach(element => {
        i = i + 1;
        if (bandera === 0){
          elemento = element.fechaCreacion2;
          bandera = 1;
          fecha = element.fechaCreacion2;
        }

        if (element.fechaCreacion2 === elemento) {
          // console.log(element.tiepoTotalPlanta);
          tiempo = tiempo + element.tiempoOperacion;
          tiempo2 = tiempo2 + element.tiempoAtencionSinAfectacion;
          cantidad = cantidad + 1;
          if (i === respose.length) {
            const temp_datos =  Array();
            temp_datos.push(fecha);
            temp_datos.push(this.convertidorDecimalTiempo(tiempo/cantidad));
            temp_datos.push(this.convertidorDecimalTiempo(tiempo2/cantidad));
            temp_datos.push('');
            // temp_datos.push(cien);
            if (cantidad > maximo) {
              maximo = cantidad;
            }
            datosgraficos.push(temp_datos);
          }

        } else {
          if (!(fecha === '')) {
            const temp_datos =  Array();
            temp_datos.push(fecha);
            temp_datos.push(this.convertidorDecimalTiempo(tiempo/cantidad));
            temp_datos.push(this.convertidorDecimalTiempo(tiempo2/cantidad));
            temp_datos.push('');
            if (cantidad > maximo) {
              maximo = cantidad;
            }
            datosgraficos.push(temp_datos);
          }
          bandera = 0;
          cantidad = 0;
          tiempo = 0;
          tiempo2 = 0;
          fecha = '';
        }
      });

      const data = google.visualization.arrayToDataTable(datosgraficos);

      const view = new google.visualization.DataView(data);
      const legenda = Array();
      legenda.push(0);
      legenda.push(1);
      legenda.push({
        calc: 'stringify',
        sourceColumn: 1,
        type: 'string',
        role: 'annotation'});
      legenda.push(2);
      legenda.push({
        calc: 'stringify',
        sourceColumn: 2,
        type: 'string',
        role: 'annotation'});
      legenda.push(3);
      legenda.push({
        calc: 'stringify',
        sourceColumn: 3,
        type: 'string',
        role: 'annotation'});

      view.setColumns(legenda);
      let arraySeries = Array();
      arraySeries.push({
        annotations: {
          stem: {
            color: '#000000',
          },
          textStyle: {
            color: '#000000',
          }
        }
      });
      arraySeries.push({
        annotations: {
          stem: {
            color: '#000000',
          },
          textStyle: {
            color: '#000000',
          }
        }
      });
      arraySeries.push({
        annotations: {
          stem: {
            color: '#000000',
          },
          textStyle: {
            color: '#000000',
          }
        }
      });
      const options = {
        animation:{
          duration: 1000,
          easing: 'out',
          startup: true
        },
        vAxes: [{
          minValue: [0,0,0],
          maxValue: [23,59,59]
        }],
        hAxis: {
          title: 'Descarga'
        },
        height: $(window).height()*0.75,
        // width: $(window).width(),
        title: 'Tiempo de operación',
        legend: { position: 'top' },
        // isStacked: true,
        alignment: 'start',
        series: arraySeries

      };

      let chart = new google.visualization.LineChart(document.getElementById('googlechart-descarga'));

      chart.draw(view, options as google.visualization.ColumnChartOptions);

    });
  }


  grafico10(fechaInicio, fechaFin) {
    this.serviceHistorico.graficos3(this.usuarioList[0].idPlanta, fechaInicio, fechaFin)
      .subscribe( respose => {
      const datosgraficos = Array();
      let cabecera = Array();

      // agregando cabecera
      cabecera.push('Fecha');
      cabecera.push('Total');
      cabecera.push('Lineal (total)');
      cabecera.push({ role: 'annotation' });
      datosgraficos.push(cabecera);
      // variables acumuladores
      let pm = 0;
      let am = 0;
      let nocturno = 0;
      let i = 0;
      let tiempo_inicio = 0;
      let tiempo_fin = 0;
      let maximo = 0;
      let bandera = 0;
      let elemento = "";
      let fecha = "";
      let cantidad = 0;
      let tiempo = 0.0;
      respose.forEach(element => {
        i = i + 1;
        if (bandera === 0){
          elemento = element.fechaCreacion2;
          bandera = 1;
          fecha = element.fechaCreacion2;
        }

        if (element.fechaCreacion2 === elemento) {
          // console.log(element.tiepoTotalPlanta);
          tiempo = tiempo + element.tiempoEsperaInicioOperacion;
          cantidad = cantidad + 1;
          if (i === respose.length) {
            const temp_datos =  Array();
            temp_datos.push(fecha);
            temp_datos.push(this.convertidorDecimalTiempo(tiempo/cantidad));
            temp_datos.push([0,40,0]);
            temp_datos.push('');
            // temp_datos.push(cien);
            if (cantidad > maximo) {
              maximo = cantidad;
            }
            datosgraficos.push(temp_datos);
          }

        } else {
          if (!(fecha === '')) {
            const temp_datos =  Array();
            temp_datos.push(fecha);
            temp_datos.push(this.convertidorDecimalTiempo(tiempo/cantidad));
            temp_datos.push([0,40,0]);
            temp_datos.push('');
            if (cantidad > maximo) {
              maximo = cantidad;
            }
            datosgraficos.push(temp_datos);
          }
          bandera = 0;
          cantidad = 0;
          tiempo = 0;
          fecha = '';
          am = 0;
          pm = 0;
          nocturno = 0;
        }
      });

      const data = google.visualization.arrayToDataTable(datosgraficos);

      const view = new google.visualization.DataView(data);
      const legenda = Array();
      legenda.push(0);
      legenda.push(1);
      legenda.push({
        calc: 'stringify',
        sourceColumn: 1,
        type: 'string',
        role: 'annotation'});
      legenda.push(2);
      legenda.push({
        calc: 'stringify',
        sourceColumn: 2,
        type: 'string',
        role: 'annotation'});
      legenda.push(3);
      legenda.push({
        calc: 'stringify',
        sourceColumn: 3,
        type: 'string',
        role: 'annotation'});

      view.setColumns(legenda);
      let arraySeries = Array();
      arraySeries.push({
        annotations: {
          stem: {
            color: '#000000',
          },
          textStyle: {
            color: '#000000',
          }
        }
      });
      arraySeries.push({
        annotations: {
          stem: {
            color: 'transparent',
          },
          textStyle: {
            color: 'transparent',
          }
        }
      });
      arraySeries.push({
        annotations: {
          stem: {
            color: 'transparent',
          },
          textStyle: {
            color: 'transparent',
          }
        }
      });



      const options = {
        animation:{
          duration: 1000,
          easing: 'out',
          startup: true
        },
        vAxes: [{
          title:'Tiempo (hh:mm)',
          minValue: 0,
          maxValue: [23,59,59]
        }],
        hAxis: {
          title: 'Dias'
      },
        height: $(window).height()*0.75,
        // width: $(window).width(),
        title: 'Tiempo de espera para inicio de operación',
        legend: { position: 'top' },
        // isStacked: true,
        alignment: 'start',
        series: arraySeries

      };

      let chart = new google.visualization.LineChart(document.getElementById('googlechart-container'));

      chart.draw(view, options as google.visualization.ColumnChartOptions);

    });
  }

  grafico9(fechaInicio, fechaFin) {
    const months = ['error','ene', 'feb', 'mar', 'abr', 'may', 'jun', 'jul', 'ago', 'sep', 'oct', 'nov', 'dec'];
    this.serviceHistorico.graficos3(this.usuarioList[0].idPlanta, fechaInicio, fechaFin)
      .subscribe( respose => {
      const datosgraficos = Array();
      let cabecera = Array();

      // agregando cabecera
      cabecera.push('Periodo');
      cabecera.push('Promedio de tiempo de espera para inicio de operación');
      cabecera.push('Promedio de tiempo de operacion');
      cabecera.push('Promedio de tiempo total en planta (No incluye espera para el ingreso a planta)');
      cabecera.push({ role: 'annotation' });
      datosgraficos.push(cabecera);
      // variables acumuladores
      let pm = 0;
      let am = 0;
      let nocturno = 0;
      let i = 0;
      let tiempo_inicio = 0;
      let tiempo_fin = 0;
      let maximo = 0;
      let bandera = 0;
      let elemento = "";
      let fecha = "";
      let cantidad = 0;
      let tiempo = 0.0;
      let promedio1 = 0;
      let promedio2 = 0;
      let promedio3 = 0;
      respose.forEach(element => {
        i = i + 1;
        let p = months[element.mesCreacion] + '-' + element.anioCreacion.substr(-2);

        if (bandera === 0){
          elemento = p;
          bandera = 1;
          fecha = element.fechaCreacion2;
        }
        if (p === elemento) {
          promedio1 = promedio1 + element.tiempoEsperaInicioOperacion;
          promedio2 = promedio2 + element.tiempoOperacion;
          promedio3 = promedio3 + element.tiempoAtencionSinAfectacion;
          cantidad = cantidad + 1;
          if (i === respose.length) {
            const temp_datos =  Array();
            temp_datos.push(p);
            temp_datos.push(this.convertidorDecimalTiempo(promedio1/cantidad));
            temp_datos.push(this.convertidorDecimalTiempo(promedio2/cantidad));
            temp_datos.push(this.convertidorDecimalTiempo(promedio3/cantidad));
            temp_datos.push('');
            // temp_datos.push(cien);
            if (cantidad > maximo) {
              maximo = cantidad;
            }
            datosgraficos.push(temp_datos);
          }

        } else {
          if (!(fecha === '')) {
            const temp_datos =  Array();
            temp_datos.push(p);
            temp_datos.push(this.convertidorDecimalTiempo(promedio1/cantidad));
            temp_datos.push(this.convertidorDecimalTiempo(promedio2/cantidad));
            temp_datos.push(this.convertidorDecimalTiempo(promedio3/cantidad));
            temp_datos.push('');
            if (cantidad > maximo) {
              maximo = cantidad;
            }
            datosgraficos.push(temp_datos);
          }
          bandera = 0;
          cantidad = 0;
          tiempo = 0;
          fecha = '';
          am = 0;
          pm = 0;
          nocturno = 0;
          promedio1 = 0;
          promedio2 = 0;
          promedio3 = 0;

        }
      });

      const data = google.visualization.arrayToDataTable(datosgraficos);

      // const data = google.visualization.arrayToDataTable(datosgraficos);
      // console.log(data);
      const view = new google.visualization.DataView(data);
      const legenda = Array();
      legenda.push(0);
      legenda.push(1);
      legenda.push({
        calc: 'stringify',
        sourceColumn: 1,
        type: 'string',
        role: 'annotation'});
      legenda.push(2);
      legenda.push({
        calc: 'stringify',
        sourceColumn: 2,
        type: 'string',
        role: 'annotation'});
      legenda.push(3);
      legenda.push({
        calc: 'stringify',
        sourceColumn: 3,
        type: 'string',
        role: 'annotation'});

      view.setColumns(legenda);
      let arraySeries = Array();
      arraySeries.push({
        annotations: {
          stem: {
            color: 'transparent',
          },
          textStyle: {
            color: 'transparent',
          }
        }
      });
      arraySeries.push({
        annotations: {
          stem: {
            color: 'transparent',
          },
          textStyle: {
            color: 'transparent',
          }
        }
      });
      arraySeries.push({
        annotations: {
          stem: {
            color: 'transparent',
          },
          textStyle: {
            color: 'transparent',
          }
        }
      });



      const options = {
        animation:{
          duration: 1000,
          easing: 'out',
          startup: true
        },
        vAxes: [{
          minValue: 0,
          maxValue: [23,59,59]
        }],
        height: $(window).height()*0.75,
        // width: $(window).width(),
        title: 'Comportamiento tiempo de atención total',
        legend: { position: 'top' },
        // isStacked: true,
        alignment: 'start',
        series: arraySeries

      };

      const chart = new google.visualization.ColumnChart(
        document.getElementById('googlechart-container')
      );

      chart.draw(view, options as google.visualization.ColumnChartOptions);


    });
  }

  grafico8(fechaInicio, fechaFin) {
    this.serviceHistorico.graficos3(this.usuarioList[0].idPlanta, fechaInicio, fechaFin)
      .subscribe( respose => {
      const datosgraficos = Array();
      let cabecera = Array();

      // agregando cabecera
      cabecera.push('Fecha');
      cabecera.push('AM');
      cabecera.push('PM');
      cabecera.push('Nocturno');
      cabecera.push({ role: 'annotation' });
      datosgraficos.push(cabecera);
      // variables acumuladores
      let pm = 0;
      let am = 0;
      let nocturno = 0;
      let i = 0;
      let tiempo_inicio = 0;
      let tiempo_fin = 0;
      let maximo = 0;
      let bandera = 0;
      let elemento = "";
      let fecha = "";
      let cantidad = 0;
      let tiempo = 0.0;
      respose.forEach(element => {
        i = i + 1;
        if (bandera === 0){
          elemento = element.fechaCreacion2;
          bandera = 1;
          fecha = element.fechaCreacion2;
        }

        if (element.fechaCreacion2 === elemento) {

          let hora_array = element.horaCreacion2.split(':');
          console.log(hora_array);
          console.log(Number(hora_array[0]));

          if((Number(hora_array[0]) > 6) &&  (Number(hora_array[0]) <= 11)){
            am = am + 1;
          }
          if((Number(hora_array[0]) > 12) &&  (Number(hora_array[0]) <= 18)){
            pm = pm + 1;
          }
          if((Number(hora_array[0]) >18 ) &&  (Number(hora_array[0]) <= 23)){
            nocturno = nocturno + 1;
          }
          // console.log(element.tiepoTotalPlanta);
          if (i === respose.length) {
            let cien = (am + pm + nocturno);

            const temp_datos =  Array();
            temp_datos.push(fecha);
            temp_datos.push(((am*100)/cien));
            temp_datos.push(((pm*100)/cien));
            temp_datos.push(((nocturno*100)/cien));
            temp_datos.push('');
            // temp_datos.push(cien);
            if (cantidad > maximo) {
              maximo = cantidad;
            }
            datosgraficos.push(temp_datos);
          }

        } else {
          if (!(fecha === '')) {
            let cien = (am + pm + nocturno);
            const temp_datos =  Array();
            temp_datos.push(fecha);
            // tslint:disable-next-line: no-bitwise
            temp_datos.push(((am*100)/cien)| 0);
            // tslint:disable-next-line: no-bitwise
            temp_datos.push(((pm*100)/cien)| 0);
            // tslint:disable-next-line: no-bitwise
            temp_datos.push(((nocturno*100)/cien)| 0);
            temp_datos.push('');
            if (cantidad > maximo) {
              maximo = cantidad;
            }
            datosgraficos.push(temp_datos);
          }
          bandera = 0;
          cantidad = 0;
          tiempo = 0;
          fecha = '';
          am = 0;
          pm = 0;
          nocturno = 0;
        }
      });

      const data = google.visualization.arrayToDataTable(datosgraficos);

      // const data = google.visualization.arrayToDataTable(datosgraficos);
      // console.log(data);
      const view = new google.visualization.DataView(data);
      const legenda = Array();
      legenda.push(0);
      legenda.push(1);
      legenda.push({
        calc: 'stringify',
        sourceColumn: 1,
        type: 'string',
        role: 'annotation'});
      legenda.push(2);
      legenda.push({
        calc: 'stringify',
        sourceColumn: 2,
        type: 'string',
        role: 'annotation'});
      legenda.push(3);
      legenda.push({
        calc: 'stringify',
        sourceColumn: 3,
        type: 'string',
        role: 'annotation'});

      view.setColumns(legenda);
      let arraySeries = Array();
      arraySeries.push({
        annotations: {
          stem: {
            color: 'transparent',
          },
          textStyle: {
            color: 'transparent',
          }
        }
      });
      arraySeries.push({
        annotations: {
          stem: {
            color: 'transparent',
          },
          textStyle: {
            color: 'transparent',
          }
        }
      });
      arraySeries.push({
        annotations: {
          stem: {
            color: 'transparent',
          },
          textStyle: {
            color: 'transparent',
          }
        }
      });



      const options = {
        animation:{
          duration: 1000,
          easing: 'out',
          startup: true
        },
        vAxes: [{
          title: 'Porcentaje',
          minValue: 0,
          maxValue: (maximo + 20)
        }],
        hAxis: {
            title: 'Dias'
        },
        height: $(window).height()*0.75,
        // width: $(window).width(),
        title: 'Distribución de jornadas de operación',
        legend: { position: 'top' },
        isStacked: true,
        alignment: 'start',
        series: arraySeries

      };

      const chart = new google.visualization.ColumnChart(
        document.getElementById('googlechart-container')
      );

      chart.draw(view, options as google.visualization.ColumnChartOptions);


    });
  }

  grafico7(fechaInicio, fechaFin) {
    this.serviceHistorico.graficos3(this.usuarioList[0].idPlanta, fechaInicio, fechaFin)
    .subscribe( respose => {
      const datosgraficos = Array();
      const cabecera = Array();
      let bandera = 0;
      let elemento = "";
      let fecha = "";
      let cantidad = 0;
      let tiempo = 0.0;
      // agregando cabecera
      cabecera.push('Fecha');
      cabecera.push('Horas perdidas al inicio');
      cabecera.push('HR inicio');
      cabecera.push('Hora de Inicio Esperada');


      // agregando cabecera en matrix de google chart
      datosgraficos.push(cabecera);
      let i = 0;
      let tiempo_inicio = 0;
      let tiempo_fin = 0;
      let maximo = 0;
      let hora_inicio = [];
      let hora_array = [];
      let ei = '';
      respose.forEach(element => {
        i = i + 1;

        if (bandera === 0){
          elemento = element.fechaCreacion2;
          bandera = 1;
          tiempo_inicio = element.fechaCreacion;
          fecha = element.fechaCreacion2;
          ei = element.fechaCreacion2 + ' 06:00:00.0';
          let hora_array = element.horaCreacion2.split(':');

          hora_inicio.push(Number(hora_array[0]));
          hora_inicio.push(Number(hora_array[1]));
          hora_inicio.push(Number(hora_array[2]));
        }
        if (element.fechaCreacion2 === elemento) {
          tiempo_fin = element.horaVerificacion
          cantidad = cantidad + 1;
          tiempo = tiempo + element.tiepoTotalPlanta;
          // console.log(element.tiepoTotalPlanta);
          if (i === respose.length) {
            const temp_datos =  Array();
            temp_datos.push(fecha);
            console.log(hora_inicio);
            temp_datos.push(this.diferenciaEntreDiasEnDias(new Date(ei),new Date(tiempo_inicio)));
            temp_datos.push(hora_inicio);
            temp_datos.push([6,0,0]);
            // console.log(this.diferenciaEntreDiasEnDias(new Date(tiempo_inicio),new Date(tiempo_fin)));
            if (cantidad > maximo) {
              maximo = cantidad;
            }
            datosgraficos.push(temp_datos);
          }

        } else {
          if (!(fecha === '')) {
            const temp_datos =  Array();
            temp_datos.push(fecha);
            console.log(hora_inicio);
            temp_datos.push(this.diferenciaEntreDiasEnDias(new Date(ei),new Date(tiempo_inicio)));
            temp_datos.push(hora_inicio);
            temp_datos.push([6,0,0]);
            // console.log(this.diferenciaEntreDiasEnDias(new Date(tiempo_inicio),new Date(tiempo_fin)));
            if (cantidad > maximo) {
              maximo = cantidad;
            }
            datosgraficos.push(temp_datos);
          }
          bandera = 0;
          cantidad = 0;
          tiempo = 0;
          fecha = '';
          ei = '';
          hora_inicio = [];
          // hora_array = [];
        }
      });
      //https://jsfiddle.net/7yuxxw3q/1/
      const data = google.visualization.arrayToDataTable(datosgraficos);

      // opciones del grafico
      let arraySeries = Array();
      arraySeries.push({
        type: 'bars',
        targetAxisIndex: 0,
        annotations: {
          stem: {
            color: '#000000',
          },
          textStyle: {
            color: '#000000',
          }
        }
      });
      arraySeries.push({
        type: 'area',
        targetAxisIndex: 1,
        color: '#cb6027',
        annotations: {
          stem: {
            color: '#cb6027',
          },
          textStyle: {
            color: '#cb6027',
          }
        }
      });
      arraySeries.push({
        type: 'linea',
        targetAxisIndex: 1,
        color: '#3fc314',
        annotations: {
          stem: {
            color: '#3fc314',
          },
          textStyle: {
            color: '#3fc314',
          }
        }
      });

      const  options = {
        title: 'HR inicio de operación real vs esperada',
        height: $(window).height()*0.75,
        vAxes: [{
          title: 'Tiempo (hh:mm)',
            minValue: 0,
            maxValue: [23,59,59]
        }, {
          title: 'Tiempo (hh:mm)',
            minValue: [0,0,0],
            maxValue: [23,59,59]
        }],
        hAxis: {
            title: 'Dias'
        },
        seriesType: 'linea',
        series: arraySeries,
        axes: {
            y: {
                0: {
                    label: 'leftyaxis'
                },
                1: {
                    side: 'right',
                    label: 'rightyaxis'
                }
            }
    }
    };

      const view = new google.visualization.DataView(data);
      let legenda = Array();
      legenda.push(0);
      legenda.push(1);
      legenda.push({
        calc: 'stringify',
        sourceColumn: 1,
        type: 'string',
        role: 'annotation'});
      legenda.push(2);
      legenda.push({
        calc: 'stringify',
        sourceColumn: 2,
        type: 'string',
        role: 'annotation'});
      legenda.push(3);


      view.setColumns(legenda);

      const chart = new google.visualization.ComboChart(
        document.getElementById('googlechart-container')
      );

      chart.draw(view, options);

    });
  }

  grafico6(fechaInicio, fechaFin) {
    this.serviceHistorico.graficos6(this.usuarioList[0].idPlanta, fechaInicio, fechaFin)
      .subscribe( respose => {
        let muelles  = Array();
        respose.forEach(element => {
          muelles.push(element.muelle);
        });

        let newArr = Array();
        muelles.forEach((item, index) => {
          if (newArr.findIndex(i => i == item) === -1) {
          newArr.push(item);
        }
        });
        muelles = newArr;
        let capas = '';
        muelles.forEach(element => {
          capas = capas + '<div id="googlechart-' + element + '"></div>';
        });
        document.getElementById('googlechart-container').innerHTML = capas;
        // iniciando graficos por muelles
        // array por muelles
        muelles.forEach(element3 => {
          let graficosmuelles = Array();
          let i = 0;
          respose.forEach(element1 => {
            if(element3 === element1.muelle){
              let dato_tmp = Array();
              dato_tmp.push(element1.fechaCreacion2);
              dato_tmp.push(element1.fechaCreacion);
              dato_tmp.push(element1.tiempoOperacion);
              dato_tmp.push(element1.horaVerificacion);
              graficosmuelles.push(dato_tmp);
            }
          });
          console.log(element3);
          // ordenando array de carga
          i = 0;
          let bandera = 0;
          let elemento = "";
          let fecha = "";
          let cantidad = 0;
          let tiempo = 0.0;
          let datosgraficos = Array();
          let cabecera = Array();
          // agregando cabecera
          cabecera.push('Fecha');
          cabecera.push('Total operaciones');
          cabecera.push('Productividad horas');
          datosgraficos.push(cabecera);

          let maximo = 0;
          let tiempo_inicio = 0;
          let tiempo_fin = 0;
          graficosmuelles.forEach(element => {
            i = i + 1;
            if (bandera === 0){
              elemento = element[0];
              bandera = 1;
              fecha = element[0];
              tiempo_inicio = element[3];
            }
            if (element[0] === elemento) {
              cantidad = cantidad + 1;
              tiempo = tiempo + element[2];
              tiempo_fin = element[1];
              if (i === graficosmuelles.length) {
                const temp_datos =  Array();
                temp_datos.push(fecha.toString());
                temp_datos.push(cantidad);
                // tslint:disable-next-line: max-line-length
                let productividad = tiempo * 24;
                productividad = cantidad / productividad;
                temp_datos.push(productividad);
                datosgraficos.push(temp_datos);
                if (cantidad > maximo) {
                  maximo = cantidad;
                }
              }
            } else {
              if (!(fecha === '') && !(cantidad === 0)) {
                const temp_datos =  Array();
                temp_datos.push(fecha.toString());
                temp_datos.push(cantidad);
                // tslint:disable-next-line: max-line-length
                let productividad = tiempo * 24;
                productividad = cantidad / productividad;
                temp_datos.push(productividad);
                // tslint:disable-next-line: max-line-length
                // temp_datos.push(this.diferenciaEntreDiasEnDiasHorasTotalesPromedioDiario(new Date(tiempo_inicio),new Date(tiempo_fin),cantidad));
                datosgraficos.push(temp_datos);
                if (cantidad > maximo) {
                  maximo = cantidad;
                }
              }
              bandera = 0;
              cantidad = 0;
              tiempo = 0;
              fecha = '';
            }
          });
          // covertimos el array para ser usado por google chart
          console.log(datosgraficos);
          let data = google.visualization.arrayToDataTable(datosgraficos);
          const arraySeries = Array();
          arraySeries.push({
            targetAxisIndex: 0,
            annotations: {
              stem: {
                color: '#000000',
              },
              textStyle: {
                color: '#000000',
              }
            }
          });
          arraySeries.push({
            type: 'line',
            targetAxisIndex: 1,
            color: '#cb6027',
            annotations: {
              stem: {
                color: '#cb6027',
              },
              textStyle: {
                color: '#cb6027',
              }
            }
          });
          // opciones del grafico
          const  options = {
            title: 'Productividad diaria por muelle',
            height: $(window).height()*0.75,
              vAxes: [{
                title:'Cantidad de operaciones',
                minValue: 0,
                maxValue: maximo + 20
            }, {
              title: 'Operaciones/hora',
                minValue: [0,0,0],
            }],
              hAxis: {
                  title: 'Muelle: '+element3
              },
              seriesType: 'bars',
              series: arraySeries,
              axes: {
                  y: {
                      0: {
                          label: 'leftyaxis'
                      },
                      1: {
                          side: 'right',
                          label: 'rightyaxis'
                      }}}};
          const view = new google.visualization.DataView(data);
          const legenda = Array();
          legenda.push(0);
          legenda.push(1);
          legenda.push({
            calc: 'stringify',
            sourceColumn: 1,
            type: 'string',
            role: 'annotation'});
          legenda.push(2);
          legenda.push({
            calc: 'stringify',
            sourceColumn: 2,
            type: 'string',
            role: 'annotation'});
          view.setColumns(legenda);
          const chart = new google.visualization.ComboChart(
            document.getElementById('googlechart-' + element3)
          );
          chart.draw(view, options);
        });

      });
  }

  grafico5(fechaInicio, fechaFin){
    document.getElementById('googlechart-container').innerHTML = '<div id="googlechart-carga"></div> <div id="googlechart-descarga"></div>';

    this.serviceHistorico.graficos5_c(this.usuarioList[0].idPlanta, fechaInicio, fechaFin)
      .subscribe( respose => {
        const datosgraficos = Array();
        const cabecera = Array();
        let bandera = 0;
        let elemento = '';
        let muelle = '';
        let cantidad = 0;

        // agregando cabecera
        cabecera.push('Muelle');
        cabecera.push('Total operaciones');
        cabecera.push('Promedio Tiempo de operaciones');
        datosgraficos.push(cabecera);

        let maximo = 0;
        let i = 0;
        let tiempo_inicio = 0;
        let tiempo_fin = 0;
        respose.forEach(element => {
          i = i + 1;
          if (bandera === 0){
            elemento = element.muelle;
            bandera = 1;
            muelle = element.muelle;
            tiempo_inicio = element.fechaCreacion;
          }
          if (element.muelle === elemento) {
            cantidad = cantidad + 1;
            tiempo_fin = element.horaVerificacion;
            if (i === respose.length) {
              const temp_datos =  Array();
              temp_datos.push(muelle.toString());
              temp_datos.push(cantidad);
              temp_datos.push(this.diferenciaEntreDiasEnDiasHorasTotalesPromedio(new Date(tiempo_inicio),new Date(tiempo_fin),cantidad));
              datosgraficos.push(temp_datos);
              if(cantidad > maximo){
                maximo = cantidad;
              }
            }
          } else {
            if (!(muelle === '') && !(cantidad === 0)) {
              const temp_datos =  Array();
              temp_datos.push(muelle.toString());
              temp_datos.push(cantidad);
              temp_datos.push(this.diferenciaEntreDiasEnDiasHorasTotalesPromedio(new Date(tiempo_inicio),new Date(tiempo_fin),cantidad));
              datosgraficos.push(temp_datos);
              if (cantidad > maximo) {
                maximo = cantidad;
              }
            }
            bandera = 0;
            cantidad = 0;
            muelle = '';
          }
        });

        // covertimos el array para ser usado por google chart
        const data = google.visualization.arrayToDataTable(datosgraficos);
        const arraySeries = Array();
        arraySeries.push({
          targetAxisIndex: 0,
          annotations: {
            stem: {
              color: '#000000',
            },
            textStyle: {
              color: '#000000',
            }
          }
        });
        arraySeries.push({
          type: 'line',
          targetAxisIndex: 1,
          color: '#cb6027',
          annotations: {
            stem: {
              color: '#cb6027',
            },
            textStyle: {
              color: '#cb6027',
            }
          }
        });

        const  options = {
          title: 'Volumenes vs Promedio Tiempo de operacion por muelle',
          height: $(window).height()*0.75,
          vAxes: [{
            title: 'Cantidad de operaciones',
            minValue: 0,
            maxValue: maximo + 20
        }, {
            title: 'Tiempo (hh:mm)',
            minValue: [0,0,0],
        }],
          hAxis: {
              title: '# Muelle'
          },
          seriesType: 'bars',
          series: arraySeries,
          axes: {
              y: {
                  0: {
                      label: 'leftyaxis'
                  },
                  1: {
                      side: 'right',
                      label: 'rightyaxis'
                  }}}};

        const view = new google.visualization.DataView(data);
        const legenda = Array();
        legenda.push(0);
        legenda.push(1);
        legenda.push({
          calc: 'stringify',
          sourceColumn: 1,
          type: 'string',
          role: 'annotation'});
        legenda.push(2);
        legenda.push({
          calc: 'stringify',
          sourceColumn: 2,
          type: 'string',
          role: 'annotation'});
        view.setColumns(legenda);

        // tslint:disable-next-line: max-line-length
        const chart = new google.visualization.ComboChart(
        document.getElementById('googlechart-carga')
      );
        chart.draw(view, options);
      });

    this.serviceHistorico.graficos5_d(this.usuarioList[0].idPlanta, fechaInicio, fechaFin)
    .subscribe( respose => {
      const datosgraficos = Array();
      const cabecera = Array();
      let bandera = 0;
      let elemento = '';
      let muelle = '';
      let cantidad = 0;

      // agregando cabecera
      cabecera.push('Muelle');
      cabecera.push('Total operaciones');
      cabecera.push('Promedio Tiempo de operaciones');
      datosgraficos.push(cabecera);

      let maximo = 0;
      let i = 0;
      let tiempo_inicio = 0;
      let tiempo_fin = 0;
      respose.forEach(element => {
        i = i + 1;
        if (bandera === 0){
          elemento = element.muelle;
          bandera = 1;
          muelle = element.muelle;
          tiempo_inicio = element.fechaCreacion;
        }
        if (element.muelle === elemento) {
          cantidad = cantidad + 1;
          tiempo_fin = element.horaVerificacion;
          if (i === respose.length) {
            const temp_datos =  Array();
            temp_datos.push(muelle.toString());
            temp_datos.push(cantidad);
            temp_datos.push(this.diferenciaEntreDiasEnDiasHorasTotalesPromedio(new Date(tiempo_inicio),new Date(tiempo_fin),cantidad));
            datosgraficos.push(temp_datos);
            if(cantidad > maximo){
              maximo = cantidad;
            }
          }
        } else {
          if (!(muelle === '') && !(cantidad === 0)) {
            const temp_datos =  Array();
            temp_datos.push(muelle.toString());
            temp_datos.push(cantidad);
            temp_datos.push(this.diferenciaEntreDiasEnDiasHorasTotalesPromedio(new Date(tiempo_inicio),new Date(tiempo_fin),cantidad));
            datosgraficos.push(temp_datos);
            if (cantidad > maximo) {
              maximo = cantidad;
            }
          }
          bandera = 0;
          cantidad = 0;
          muelle = '';
        }
      });

      // covertimos el array para ser usado por google chart
      const data = google.visualization.arrayToDataTable(datosgraficos);
      const arraySeries = Array();
      arraySeries.push({
        targetAxisIndex: 0,
        annotations: {
          stem: {
            color: '#000000',
          },
          textStyle: {
            color: '#000000',
          }
        }
      });
      arraySeries.push({
        type: 'line',
        targetAxisIndex: 1,
        color: '#cb6027',
        annotations: {
          stem: {
            color: '#cb6027',
          },
          textStyle: {
            color: '#cb6027',
          }
        }
      });

      const  options = {
        title: 'Volumenes vs Promedio Tiempo de operacion por muelle',
        height: $(window).height()*0.75,
        vAxes: [{
          title: 'Cantidad de operaciones',
          minValue: 0,
          maxValue: maximo + 20
      }, {
          title: 'Tiempo (hh:mm)',
          minValue: [0,0,0],
      }],
        hAxis: {
            title: '# muelles'
        },
        seriesType: 'bars',
        series: arraySeries,
        axes: {
            y: {
                0: {
                    label: 'leftyaxis'
                },
                1: {
                    side: 'right',
                    label: 'rightyaxis'
                }}}};

      const view = new google.visualization.DataView(data);
      const legenda = Array();
      legenda.push(0);
      legenda.push(1);
      legenda.push({
        calc: 'stringify',
        sourceColumn: 1,
        type: 'string',
        role: 'annotation'});
      legenda.push(2);
      legenda.push({
        calc: 'stringify',
        sourceColumn: 2,
        type: 'string',
        role: 'annotation'});
      view.setColumns(legenda);

      // tslint:disable-next-line: max-line-length
      const chart = new google.visualization.ComboChart(
      document.getElementById('googlechart-descarga')
    );
      chart.draw(view, options);
    });
  }

  grafico4(fechaInicio, fechaFin){
    this.serviceHistorico.graficos4(this.usuarioList[0].idPlanta, fechaInicio, fechaFin)
      .subscribe( respose => {
        const datosgraficos = Array();
        const cabecera = Array();
        let bandera = 0;
        let elemento = "";
        let fecha = "";
        let cantidad = 0;
        let tiempo = 0.0;
        // agregando cabecera
        cabecera.push('Muelle');
        cabecera.push('Total operaciones');
        cabecera.push('Total Tiempo de operaciones');

        // agregando cabecera en matrix de google chart
        datosgraficos.push(cabecera);
        let maximo = 0;
        let i = 0;
        let tiempo_inicio = 0;
        let tiempo_fin = 0;
        respose.forEach(element => {
          i = i + 1;
          if (bandera === 0){
            elemento = element.muelle;
            bandera = 1;
            fecha = element.muelle;
            tiempo_inicio = element.fechaCreacion;

          }
          if (element.muelle === elemento) {
            cantidad = cantidad + 1;
            tiempo = tiempo + element.tiepoTotalPlanta;
            tiempo_fin = element.horaVerificacion;
            if (i === respose.length) {
              const temp_datos =  Array();
              temp_datos.push(fecha.toString());
              temp_datos.push(cantidad);
              temp_datos.push(this.diferenciaEntreDiasEnDiasHorasTotales(new Date(tiempo_inicio),new Date(tiempo_fin)));
              datosgraficos.push(temp_datos);
              if(cantidad > maximo){
                maximo = cantidad;
              }
            }
          } else {
            if (!(fecha === '') && !(cantidad === 0)) {
              const temp_datos =  Array();
              temp_datos.push(fecha.toString());
              temp_datos.push(cantidad);
              temp_datos.push(this.diferenciaEntreDiasEnDiasHorasTotales(new Date(tiempo_inicio),new Date(tiempo_fin)));
              datosgraficos.push(temp_datos);
              if(cantidad > maximo){
                maximo = cantidad;
              }
            }
            bandera = 0;
            cantidad = 0;
            tiempo = 0;
            fecha = '';
          }
        });
        // covertimos el array para ser usado por google chart
        const data = google.visualization.arrayToDataTable(datosgraficos);

        const arraySeries = Array();
        arraySeries.push({
          targetAxisIndex: 0,
          annotations: {
            stem: {
              color: '#000000',
            },
            textStyle: {
              color: '#000000',
            }
          }
        });
        arraySeries.push({
          type: 'line',
          targetAxisIndex: 1,
          color: '#cb6027',
          annotations: {
            stem: {
              color: '#cb6027',
            },
            textStyle: {
              color: '#cb6027',
            }
          }
        });
        // opciones del grafico
        const  options = {
          title: 'Volumenes vs Tiempo de operacion por muelle',
          height: $(window).height()*0.75,
          vAxes: [{
            title: 'Cantidad de operaciones',
            minValue: 0,
            maxValue: maximo + 20
        }, {
            title: 'Tiempo (hh:mm)',
            minValue: [0,0,0],
        }],
          hAxis: {
              title: '# muelles'
          },
          seriesType: 'bars',
          series: arraySeries,
          axes: {
              y: {
                  0: {
                      label: 'leftyaxis'
                  },
                  1: {
                      side: 'right',
                      label: 'rightyaxis'
                  }}}};

        const view = new google.visualization.DataView(data);
        const legenda = Array();
        legenda.push(0);
        legenda.push(1);
        legenda.push({
          calc: 'stringify',
          sourceColumn: 1,
          type: 'string',
          role: 'annotation'});
        legenda.push(2);
        legenda.push({
          calc: 'stringify',
          sourceColumn: 2,
          type: 'string',
          role: 'annotation'});
        view.setColumns(legenda);
        const chart = new google.visualization.ComboChart(
          document.getElementById('googlechart-container')
        );
        chart.draw(view, options);
      });
  }

  grafico3(fechaInicio, fechaFin){
    this.serviceHistorico.graficos3(this.usuarioList[0].idPlanta, fechaInicio, fechaFin)
      .subscribe( respose => {
        const datosgraficos = Array();
        const cabecera = Array();
        console.log(respose);
        let bandera = 0;
        let elemento = "";
        let fecha = "";
        let cantidad = 0;
        let tiempo = 0.0;
        // agregando cabecera
        cabecera.push('Fecha');
        cabecera.push('Total Viajes');
        cabecera.push('Duración Jornada');

        // agregando cabecera en matrix de google chart
        datosgraficos.push(cabecera);
        let i = 0;
        let tiempo_inicio = 0;
        let tiempo_fin = 0;
        let maximo = 0;
        respose.forEach(element => {
          i = i + 1;
          if (bandera === 0){
            elemento = element.fechaCreacion2;
            bandera = 1;
            tiempo_inicio = element.fechaCreacion;
            fecha = element.fechaCreacion2;
          }
          console.log(element.fechaCreacion2);
          if (element.fechaCreacion2 === elemento) {
            console.log(elemento);
            tiempo_fin = element.horaVerificacion
            cantidad = cantidad + 1;
            tiempo = tiempo + element.tiepoTotalPlanta;
            // console.log(element.tiepoTotalPlanta);
            if (i === respose.length) {
              const temp_datos =  Array();
              temp_datos.push(fecha);
              temp_datos.push(cantidad);
              temp_datos.push(this.diferenciaEntreDiasEnDias(new Date(tiempo_inicio),new Date(tiempo_fin)));
              console.log('si');
              // console.log(this.diferenciaEntreDiasEnDias(new Date(tiempo_inicio),new Date(tiempo_fin)));
              if (cantidad > maximo) {
                maximo = cantidad;
              }
              datosgraficos.push(temp_datos);
            }

          } else {
            if (!(fecha === '')) {
              console.log('no');
              const temp_datos =  Array();
              temp_datos.push(fecha);
              temp_datos.push(cantidad);
              temp_datos.push(this.diferenciaEntreDiasEnDias(new Date(tiempo_inicio),new Date(tiempo_fin)));
              // console.log(this.diferenciaEntreDiasEnDias(new Date(tiempo_inicio),new Date(tiempo_fin)));
              if (cantidad > maximo) {
                maximo = cantidad;
              }
              datosgraficos.push(temp_datos);
            }
            bandera = 0;
            cantidad = 0;
            tiempo = 0;
            fecha = '';
          }
        });
        //https://jsfiddle.net/7yuxxw3q/1/
        const data = google.visualization.arrayToDataTable(datosgraficos);

        // opciones del grafico
        let arraySeries = Array();
        arraySeries.push({
          targetAxisIndex: 0,
          annotations: {
            stem: {
              color: '#000000',
            },
            textStyle: {
              color: '#000000',
            }
          }
        });
        arraySeries.push({
          type: 'line',
          targetAxisIndex: 1,
          color: '#cb6027',
          annotations: {
            stem: {
              color: '#cb6027',
            },
            textStyle: {
              color: '#cb6027',
            }
          }
        });

        const  options = {
          title: 'Volumenes vs Jornadas',
          height: $(window).height()*0.75,
          vAxes: [{
              title: 'Cantidad de operaciones',
              minValue: 0,
              maxValue: maximo + 20
          }, {
              title: 'Tiempo (hh:mm)',
              minValue: [0,0,0],
              maxValue: [23,59,59]
          }],
          hAxis: {
              title: ''
          },
          seriesType: 'bars',
          series: arraySeries,
          axes: {
              y: {
                  0: {
                      label: 'leftyaxis'
                  },
                  1: {
                      side: 'right',
                      label: 'rightyaxis'
                  }
              }
      }
      };

        const view = new google.visualization.DataView(data);
        let legenda = Array();
        legenda.push(0);
        legenda.push(1);
        legenda.push({
          calc: 'stringify',
          sourceColumn: 1,
          type: 'string',
          role: 'annotation'});
        legenda.push(2);
        legenda.push({
          calc: 'stringify',
          sourceColumn: 2,
          type: 'string',
          role: 'annotation'});


        view.setColumns(legenda);

        const chart = new google.visualization.ComboChart(
          document.getElementById('googlechart-container')
        );

        chart.draw(view, options);

      });
  }

  grafico2(fechaInicio, fechaFin) {
    this.serviceHistorico.graficos2(this.usuarioList[0].idPlanta, fechaInicio, fechaFin)
      .subscribe( respose => {
        const datosgraficos = Array();
        const cabecera = Array();
        function getRandomColor() {
          let letters = '0123456789ABCDEF'.split('');
          let color = '#';
          for (let i = 0; i < 6; i++ ) {
              color += letters[Math.floor(Math.random() * 16)];
          }
          return 'color:' + color.toString();
      }

        // agregando cabecera
        cabecera.push('Operación');
        cabecera.push('Volumen');
        cabecera.push('{ role: "style" }');
        cabecera.push('{ role: \'annotation\' }');
        // agregando cabecera en matrix de google chart
        datosgraficos.push(cabecera);

        // agregando cuerpo
        for (const i of respose) {
          let cuerpo = Array();
          cuerpo.push(i.operaciones);
          cuerpo.push(i.cantidad);
          cuerpo.push(getRandomColor().toString());
          cuerpo.push(i.cantidad);
          datosgraficos.push(cuerpo);
        }
        // console.log(cuerpo);


        // covertimos el array para ser usado por google chart
        const data = google.visualization.arrayToDataTable(datosgraficos);
        // opciones del grafico

        // let arraySeries = Array();
        // let i = 0;
        // for (const d of respose) {
        //   arraySeries.push({i});
        //   arraySeries.push({
        //     color:getRandomColor()
        //   });
        //   i = i + 1;
        // }

        let options = {
          // width: 1001,
          height: $(window).height()*0.75,
          title: 'Volumen por tipo de operación',
          vAxes: [{
            title: 'Cantidad de Operaciones',
          }],
          hAxis : {
            textStyle : {
                fontSize: 7 // or the number you want
            }

        },
          is3D:true
        };


        const view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" }]);

        const chart = new google.visualization.ColumnChart(
          document.getElementById('googlechart-container')
        );

        chart.draw(view, options as google.visualization.ColumnChartOptions);
      });
  }

  grafico1(fechaInicio, fechaFin) {
    const months = ['error','ene', 'feb', 'mar', 'abr', 'may', 'jun', 'jul', 'ago', 'sep', 'oct', 'nov', 'dec'];
    this.serviceHistorico.graficos1(this.usuarioList[0].idPlanta, fechaInicio, fechaFin)
      .subscribe( respose => {
      const datosgraficos = Array();
      let cabecera = Array();
      let dependencias = Array();
      let periodo = Array();

      // agregando cabecera
      cabecera.push('Mes');
      for (const i of respose) {
        cabecera.push(i.dependencia);
        dependencias.push(i.dependencia);
      }
      cabecera.push({ role: 'annotation' });

      // Eliminando duplicados de la cabecera
      let newArr = Array();
      cabecera.forEach((item, index) => {
        if (newArr.findIndex(i => i == item) === -1) {
        newArr.push(item);
      }
      });
      cabecera = newArr;
      // Eliminando duplicados de las dependencias
      newArr = Array();
      dependencias.forEach((item, index) => {
        if (newArr.findIndex(i => i == item) === -1) {
        newArr.push(item);
      }
      });
      dependencias = newArr;

      // agregando cabecera en matrix de google chart
      datosgraficos.push(cabecera);
      // agregando periodos
      for (const i of respose) {
        // var p = i.mes + '-' + i.anio;
        let p = months[i.mes] + '-' + i.anio.substr(-2);
        periodo.push(p);
      }
      // eliminando duplicados de los periodos
      const newArr2 = Array();
      periodo.forEach((item, index) => {
        if (newArr2.findIndex(i => i == item) === -1) {
          newArr2.push(item);
      }
      });
      periodo = newArr2;
      // Generando cuerpo de la matrix de google chart
      for (const j of periodo) {
         // tslint:disable-next-line: no-shadowed-variable
         let contador = 0;
         const cuerpo = Array();
         cuerpo.push(j);
         for (const d of dependencias) {
            let cantidad = 0;
            for (const i of respose) {
              let p = months[i.mes] + '-' + i.anio.substr(-2);
              // console.log(p);
              // console.log(j);
              // console.log('---');
              // console.log(d);
              // console.log(i.dependencia);
              if ((p === j) && (d === i.dependencia)) {
                cantidad = i.cantidad;
              }
            }
            cuerpo.push(cantidad);
            contador = contador + 1;
         }
         cuerpo.push('');
         datosgraficos.push(cuerpo);
      }
      // console.log(datosgraficos);


      // for (const i of this.consultaHistoricoArray) {
      //   // datosgraficos.push(['Nov-19', 10, 24, 20, 32, 18, 5, '']);
      // }

      const data = google.visualization.arrayToDataTable(datosgraficos);

      // const data = google.visualization.arrayToDataTable(datosgraficos);
      // console.log(data);
      const view = new google.visualization.DataView(data);
      const legenda = Array();
      legenda.push(0);
      let contador = 1;
      let maximo = 0;
      console.log(dependencias.length);
      let numero = dependencias.length;
      for (const d of dependencias) {
        legenda.push(contador);
        legenda.push({
          calc: 'stringify',
          sourceColumn: contador,
          type: 'string',
          role: 'annotation'});
        contador = contador + 1;
      }
      legenda.push({
        calc: function(dt,row) {return 0; },
        label: 'Total',
        type: 'number'});
      legenda.push({
        // calc: function (dt, row) {
        //   let total = 0;
        //   let i = 1;
        //   for (const d of dependencias) {
        //     total = total + dt.getValue(row, i);
        //     i = i + 1;
        //   }
        //   if(total > maximo){
        //     maximo = total;
        //   }
        //   return total;
        // },
        calc: function (dt,row) {
          let total = 0;
          let i = 1;
          console.log('pagona2');

          console.log(dt);
          for (const d of dependencias) {
            total = total + dt.getValue(row, i);
            i = i + 1;
          }
          console.log(total);
          if(total > maximo){
            maximo = total;
          }
          return 'total='+total.toString();
        },
        type: 'string',
        role: 'annotation'
      });
      view.setColumns(legenda);
      let arraySeries = Array();
      for (const d of dependencias) {
        arraySeries.push({
          annotations: {
            stem: {
              color: 'transparent',
            },
            textStyle: {
              color: '#000000',
            }
          }
        });
      }
      arraySeries.push({
        annotations: {
          stem: {
            color: '#000000',
          },
          textStyle: {
            color: '#4d89f9',
            fontSize: 20,
            fontName: 'Arial',
            bold: true,
            italic: true
          }
        },
        enableInteractivity: false,
        tooltip: 'none',
        visibleInLegend: true
      });

      console.log(arraySeries);

      const options = {
        animation:{
          duration: 1000,
          easing: 'out',
          startup: true
        },
        vAxes: [{
          minValue: 0,
          maxValue: (maximo + 20),
          title: 'Cantidad de Operaciones',
        }],
        height: $(window).height()*0.75,
        // width: $(window).width(),
        title: 'Total volumen por mes por dependencia',
        legend: { position: 'top' },
        isStacked: true,
        alignment: 'start',
        series: arraySeries
        // series: {
        //   6: {
        //     annotations: {
        //       stem: {
        //         color: "transparent",
        //       },
        //       textStyle: {
        //         color: "#212524",
        //       }
        //     },
        //     enableInteractivity: false,
        //     tooltip: "none",
        //     visibleInLegend: false
        //   }
        // }
      };

      const chart = new google.visualization.ColumnChart(
        document.getElementById('googlechart-container')
      );

      google.visualization.events.addListener(chart, 'animationfinish', function () {
        $('#googlechart-container text[fill="#4d89f9"]').each(function (index, annotation) {
          if (!isNaN(parseFloat(annotation.textContent))) {
            var xCoord = parseFloat($(annotation).attr('y'));
            $(annotation).attr('y', xCoord - 18);
          }
        });
      });

      chart.draw(view, options as google.visualization.ColumnChartOptions);
    });
  }



diferenciaEntreDiasEnDias(a, b)
{
  console.log(a);
  console.log(b);
  const time = Math.abs(a -  b) / 1000;
  let hours = Math.floor( time / 3600 );
  let minutes = Math.floor( (time % 3600) / 60 );
  let seconds = time % 60;
  const minutess = minutes < 10 ? '0' + minutes : minutes;
  const secondss = seconds < 10 ? '0' + seconds : seconds;
  console.log(hours + ':' + minutess + ':' + secondss);

  // let MILISENGUNDOS_POR_DIA = 1000 * 60 * 60 * 24;
  // var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
  // var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());
  const timeArray = Array();
  if (hours > 23) {
    hours = 23;
  }
  timeArray.push(Number(hours));
  timeArray.push(Number(minutess));
  timeArray.push(Number(secondss));
  console.log(timeArray);
  // return Math.floor((utc2 - utc1) / MILISENGUNDOS_POR_DIA);
  return timeArray;
}

diferenciaEntreDiasEnDiasHorasTotales(a, b)
{
  console.log(a);
  console.log(b);
  const time = Math.abs(a -  b) / 1000;
  let hours = Math.floor( time / 3600 );
  let minutes = Math.floor( (time % 3600) / 60 );
  let seconds = time % 60;
  const minutess = minutes < 10 ? '0' + minutes : minutes;
  const secondss = seconds < 10 ? '0' + seconds : seconds;
  console.log(hours + ':' + minutess + ':' + secondss);

  // let MILISENGUNDOS_POR_DIA = 1000 * 60 * 60 * 24;
  // var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
  // var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());
  const timeArray = Array();
  timeArray.push(Number(hours));
  timeArray.push(Number(minutess));
  timeArray.push(Number(secondss));
  console.log(timeArray);
  // return Math.floor((utc2 - utc1) / MILISENGUNDOS_POR_DIA);
  return timeArray;
}

diferenciaEntreDiasEnDiasHorasTotalesPromedio(a, b, p)
{
  console.log(a);
  console.log(b);
  let time = Math.abs(a -  b) / 1000;
  time = time / p;
  let hours = Math.floor( time / 3600 );
  let minutes = Math.floor( (time % 3600) / 60 );
  let seconds = time % 60;
  const minutess = minutes < 10 ? '0' + minutes : minutes;
  const secondss = seconds < 10 ? '0' + seconds : seconds;
  console.log(hours + ':' + minutess + ':' + secondss);
  const timeArray = Array();
  timeArray.push(Number(hours));
  timeArray.push(Number(minutess));
  timeArray.push(Number(secondss));
  console.log(timeArray);
  return timeArray;
}

diferenciaEntreDiasEnDiasHorasTotalesPromedioDiario(a, b, p)
{
  console.log(a);
  console.log(b);
  let time = Math.abs(a -  b) / 1000;
  time = time / p;
  let hours = Math.floor( time / 3600 );
  let minutes = Math.floor( (time % 3600) / 60 );
  let seconds = time % 60;
  const minutess = minutes < 10 ? '0' + minutes : minutes;
  const secondss = seconds < 10 ? '0' + seconds : seconds;
  console.log(hours + ':' + minutess + ':' + secondss);
  const timeArray = Array();
  if (hours > 23) {
    hours = 23;
  }
  timeArray.push(Number(hours));
  timeArray.push(Number(minutess));
  timeArray.push(Number(secondss));
  console.log(timeArray);
  return timeArray;
}

convertidorDecimalTiempo(minutos){
  console.log(minutos);

  let hours = minutos/60;
  console.log(hours | 0);
  if((hours | 0) > 23 ){
    hours = 23;
  }

  let minutess = Number(minutos%60);
  console.log(minutess | 0);

  const timeArray = Array();
  timeArray.push(Number(hours  | 0));
  timeArray.push(Number(minutess  | 0));
  timeArray.push(Number(0));
  console.log(timeArray);
  return timeArray;
}



}

export class TiposGraficos {
    constructor(id: number, name: string) {
        this.id = id;
        this.name = name;
    }

    id: number;
    name: string;
}

