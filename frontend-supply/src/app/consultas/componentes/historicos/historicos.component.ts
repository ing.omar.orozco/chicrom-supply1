import { RemisionService } from './../../../ticket/service/remision.service';
import { RemisionModalComponent } from './../../modales/remision-modal/remision-modal.component';
import { Component, OnInit } from '@angular/core';
import {HistoricoService} from '../../services/historico.service';
import {Usuario} from '../../../core/models/usuario';
import {SessionStorageService} from 'angular-web-storage';
import {Historico} from '../../models/historico';
import {HistoricoExcel} from '../../models/historicoExcel';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-historicos',
  templateUrl: './historicos.component.html',
  styleUrls: ['./historicos.component.css']
})
export class HistoricosComponent implements OnInit {

  usuarioList: Usuario[];
  consultaHistoricoArray: Historico[];
  consultaHistoricoExcel: HistoricoExcel[];
  excelObje: HistoricoExcel;
  rangeDates: Date[];

  constructor(private modalService: NgbModal, private serviceRemision: RemisionService, private serviceHistorico: HistoricoService,
              private sessionStorage: SessionStorageService) { }

  ngOnInit() {
    this.usuarioList = this.sessionStorage.get('usuario');

  }
  buscarHistorico() {
    const fechaInicio = new Date( this.rangeDates[0]).toISOString().slice(0, 10);
    const fechaFin = new Date( this.rangeDates[1]).toISOString().slice(0, 10);
    this.serviceHistorico.consultaCerrado(this.usuarioList[0].idPlanta, fechaInicio, fechaFin)
      .subscribe( respose => {
      console.log(respose);
      this.consultaHistoricoArray = respose;
    });
  }

  openModalRemision(modo: string, id: any, idOperacion) {
    const modalRef = this.modalService.open(RemisionModalComponent, {size: 'xl' as 'lg'});
    this.serviceRemision.buscarRemisionPorIdTicket(id).subscribe( data => {
      if (data !== null) {
        modalRef.componentInstance.modalDataArray = data;
      }
    });
    modalRef.componentInstance.id = id;
    modalRef.componentInstance.modalConfig = modo;
    modalRef.componentInstance.idOperacion = idOperacion;
  }

  // export excel
  exportAsXLSX(): void {
    this.consultaHistoricoExcel = [];
    for (const i of this.consultaHistoricoArray) {
      this.excelObje = new HistoricoExcel();
      this.excelObje.TIKETE = i.id;
      this.excelObje.Planta = i.nombrePlanta;
      this.excelObje.Numero_de_muelle = i.muelle;
      this.excelObje.Fecha = i.fechaCreacion;
      this.excelObje.Placa = i.placa;
      this.excelObje.Tipo_de_Vehiculo = i.tipoTransporte;
      this.excelObje.Trasportador = i.nombreTransportista;
      this.excelObje.Tipo_operacion = i.nombreOperacion;
      this.excelObje.Tarifa = i.tarifa;
      this.excelObje.Nro_de_documento = i.numeroRemision;
      this.excelObje.Hora_Arribo_Planta = i.horaArriboPlanta;
      this.excelObje.Hora_Ingreso_Planta = i.horaIngresoPlanta;
      this.excelObje.Hora_Inicio_Operacion = i.horaInicioOperacion;
      this.excelObje.Hora_Fin_Operacion = i.horaFinOperacion;
      this.excelObje.Hora_Verificacion = i.horaVerificacion;
      this.excelObje.Observaciones = i.observaciones;
      this.excelObje.Tiempo_de_espera_para_ingreso_a_planta = i.tiempoEsperaIngresoPlanta;
      this.excelObje.Tiempo_de_espera_para_inicio_de_operacion = i.tiempoEsperaInicioOperacion;
      this.excelObje.Tiempo_de_operacion = i.tiempoOperacion;
      this.excelObje.Tiempo_espera_para_verfificacion = i.tiempoEsperaVerfificacion;
      this.excelObje.Tiempo_total_en_planta_No_incluye_espera_para_el_ingreso_a_planta = i.tiepoTotalPlanta;
      this.excelObje.Tiempo_total_atraso_en_operacion = i.tiempoTotalAtrasoOperacion;
      this.excelObje.Tiempo_de_atencion_sin_afectacion = i.tiempoAtencionSinAfectacion;
      this.excelObje.Cedula1 = i.cedula1;
      this.excelObje.Cedula2 = i.cedula2;
      this.excelObje.Cedula3 = i.cedula3;
      this.excelObje.Cedula4 = i.cedula4;
      this.excelObje.Cedula5 = i.cedula5;
      this.excelObje.Cedula6 = i.cedula6;
      this.excelObje.Cedula7 = i.cedula7;
      this.excelObje.Cedula8 = i.cedula8;
      this.excelObje.Cedula9 = i.cedula9;
      this.excelObje.Cedula10 = i.cedula10  ;



      this.consultaHistoricoExcel.push(this.excelObje);
    }
    this.serviceHistorico.exportAsExcelFile(this.consultaHistoricoExcel, 'Historico_');
  }
}
