
import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {HistoricosComponent} from './componentes/historicos/historicos.component';
import {MainComponent} from './componentes/main/main.component';
import { HistorialComponent } from './componentes/historial/historial.component';

export const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {path: 'historico', component: HistoricosComponent },
      {path: 'graficos', component: HistorialComponent }
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConsultasRoutesModule { }

