import { ImageCropperModule } from 'ngx-image-cropper';
import { ToastModule } from 'primeng/toast';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { VerFotoRemisionModalComponent } from './modales/ver-foto-remision-modal/ver-foto-remision-modal.component';
import { CrearRemisionModalComponent } from './modales/crear-remision-modal/crear-remision-modal.component';
import { RemisionModalComponent } from './modales/remision-modal/remision-modal.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './componentes/main/main.component';
import {ConsultasRoutesModule} from './consultas.routes';
import { HistoricosComponent } from './componentes/historicos/historicos.component';
import {TableModule} from 'primeng/table';
import { CalendarModule, MessagesModule, MessageModule, DialogModule, TooltipModule, AccordionModule } from 'primeng/primeng';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HistorialComponent } from './componentes/historial/historial.component';
import { GoogleChartsModule } from 'angular-google-charts';

@NgModule({
  declarations: [MainComponent, HistoricosComponent, HistorialComponent, RemisionModalComponent,
    CrearRemisionModalComponent,
    VerFotoRemisionModalComponent],
  imports: [
    CommonModule,
    NgbModalModule,
    ConsultasRoutesModule,
    TableModule,
    MessagesModule,
    MessageModule,
    DialogModule,
    TooltipModule,
    ToastModule,
    ReactiveFormsModule,
    FormsModule,
    CalendarModule,
    ImageCropperModule,
    FormsModule,
    AccordionModule,
    ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'}),
    GoogleChartsModule.forRoot()
  ],
  entryComponents: [
    RemisionModalComponent,
    CrearRemisionModalComponent,
    VerFotoRemisionModalComponent
  ]
})
export class ConsultasModule { }
