import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerFotoRemisionModalComponent } from './ver-foto-remision-modal.component';

describe('VerFotoRemisionModalComponent', () => {
  let component: VerFotoRemisionModalComponent;
  let fixture: ComponentFixture<VerFotoRemisionModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerFotoRemisionModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerFotoRemisionModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
