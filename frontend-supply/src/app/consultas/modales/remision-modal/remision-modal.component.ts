import { SellosModalComponent } from './../../../ticket/modales/sellos-modal/sellos-modal.component';
import {Component, Input, OnInit} from '@angular/core';
import {Remision} from './../../../ticket/models/remision';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {CrearRemisionModalComponent} from '../crear-remision-modal/crear-remision-modal.component';
import {NotificacionesService} from '../../../core/services/notificaciones.service';
import {RemisionService} from './../../../ticket/service/remision.service';
import {VerFotoRemisionModalComponent} from '../ver-foto-remision-modal/ver-foto-remision-modal.component';
import {SellosService} from './../../../ticket/service/sellos.service';
import {MessageService} from 'primeng/api';
import {TicketService} from './../../../ticket/service/ticket.service';

@Component({
  selector: 'app-remision-modal',
  templateUrl: './remision-modal.component.html',
  styleUrls: ['./remision-modal.component.css'],
  providers: [MessageService]
})
export class RemisionModalComponent implements OnInit {
  @Input() public modalConfig: any;
  @Input() public idOperacion: any;
  @Input() public id: number;
  @Input() public modalDataArray: Remision[];
  /* table */
  cols: any[];

  constructor(public activeModal: NgbActiveModal,
              private serviceNotificaciones: NotificacionesService,
              private serviceRemision: RemisionService,
              private serviceTicket: TicketService,
              private messageService: MessageService,
              private serviceSellos: SellosService,
              private modalService: NgbModal) { }

  ngOnInit() {
    this.cols = [
      { header: 'Numero' , class: 'w-25'},
      { header: 'Clase' , class: 'w-25'},
      { header: 'Foto' , class: 'w-25'},
      { header: 'Fecha' , class: 'w-50'}
    ];
    this.serviceRemision.changeRemisionDelet.subscribe( response => {
      if (response === true) {
        this.serviceRemision.buscarRemisionPorIdTicket(this.id).subscribe( responseArrayTale => {
          this.modalDataArray = [];
          this.modalDataArray = responseArrayTale;
        });
      }
    });

    this.serviceRemision.changeRemision.subscribe( response => {
      this.serviceRemision.buscarRemisionPorIdTicket(response.idTicket).subscribe( responseArrayTale => {
        this.modalDataArray = [];
        this.modalDataArray = responseArrayTale;
      });
    });
  }
  openModal(modo: string, id: any) {
    const modalRef = this.modalService.open(CrearRemisionModalComponent, {size: 'lg', centered: true});
    modalRef.componentInstance.modalConfig = modo;
    modalRef.componentInstance.id = id;
    if (modo === 'Editar') {
      this.serviceRemision.buscarRemisionPorId(id).subscribe( response => {
        if (response !== null) {
          modalRef.componentInstance.remision = response;
        }
      });
    }
  }
  openModalSello(modo: string, id: number) {
    console.log(this.idOperacion)
    const modalRef = this.modalService.open(SellosModalComponent, {size: 'xl' as 'lg'});
    modalRef.componentInstance.idTicket = this.id;
    modalRef.componentInstance.idRemision = id;
    modalRef.componentInstance.idOperacion = this.idOperacion;
    modalRef.componentInstance.modalConfig = modo;
    this.serviceSellos.buscarSellosPorId(id).subscribe( response => {
      if (response !== null) {
        console.log(response);
        modalRef.componentInstance.modalDataArrayTable = response;
      }
    });
    // busca servicio de operaciones
    this.serviceTicket.getAllOperaciones().subscribe( dataOperaciones => {
      if (dataOperaciones !== null) {
        console.log(dataOperaciones)
        modalRef.componentInstance.dataOperaciones = dataOperaciones;
      }
    });
  }
  verFoto(modo: string, foto: string) {
    const modalRef = this.modalService.open(VerFotoRemisionModalComponent, {size: 'lg', centered: true});
    modalRef.componentInstance.modalConfig = modo;
    modalRef.componentInstance.foto = foto;
  }
  eliminar(id, tipo) {
    if (tipo === 'P') {
      let validacionEliminar = true;
      for (const i in this.modalDataArray) {
        if (this.modalDataArray[i].tipo === 'S') {
          validacionEliminar = false;
        }
      }
      if (validacionEliminar === true) {
        this.eliminarRemision(id);
      } else {
        this.messageService.add({severity: 'warn', summary: 'Advertencia', detail:
          'No se puede eliminar esta remisión.'});
      }
    } else {
      this.eliminarRemision(id);
    }
  }
  eliminarRemision(id) {
    this.serviceNotificaciones.showConfirmacion('Confirmación', '¿Seguro de eliminar esta remision?')
      .then((result) => {
        if (result.value) {
          this.serviceRemision.eliminarRemisionPorId(id).subscribe( response => {
            this.serviceRemision.changeRemisionDelet.emit(true);
          });
        }
      });
  }

}
