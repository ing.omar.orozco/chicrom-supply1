import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {PreloadAllModules, RouterModule} from '@angular/router';
import {AppRoutes} from './app.routes';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import {NgxSpinnerModule} from 'ngx-spinner';
import {HttpInterceptor} from './core/interceptors/http-interceptor';
import { UserIdleModule } from 'angular-user-idle';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgxSpinnerModule,
    UserIdleModule.forRoot({idle: 1800, timeout: 10, ping: 5}),
    RouterModule,
      RouterModule.forRoot(AppRoutes, { useHash: true }),
      FormsModule
  ],
 // providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}]
  providers: [HttpInterceptor, { provide: HTTP_INTERCEPTORS, useClass: HttpInterceptor, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
