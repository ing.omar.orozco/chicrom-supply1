import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {Login} from "../model/login";
import {environment} from "../../../environments/environment";
import { headersToString } from 'selenium-webdriver/http';

@Injectable({
  providedIn: 'root'
})
export class AutenticationService {

  constructor(private http: HttpClient) { }

  validarLogin(usuarioLogin): Observable<Login> {
    return this.http.get<Login>(environment.API_URL + '/Autenticacion/' + usuarioLogin);
  }

  autenticaLogin(usuarioLogin: Body): Observable<Login> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.http.post<Login>(environment.API_URL + '/servicesREST/JR/validateUser', usuarioLogin , {headers: headers});
  }
  get(): Observable<any>{
    return this.http.get<any>(environment.API_URL + '/servicesREST/JR//findId/1');
  }

  closeLogin(id: number): Observable<any> {
    return this.http.get<any>(environment.API_URL + '/servicesREST/JR/closelogin/' + id);
  }

}
