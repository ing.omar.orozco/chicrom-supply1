// import { Usuario } from './../../administracion/models/usuario';
import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AutenticationService} from '../services/autentication.service';
import { Login } from '../model/login';
import { NotificacionesService } from 'src/app/core/services/notificaciones.service';
import {LocalStorageService, SessionStorageService} from 'angular-web-storage';
import { NotIdle } from 'idlejs/dist';
// import { BnNgIdleService } from 'bn-ng-idle';
import { UserIdleService } from 'angular-user-idle';
import { Usuario } from 'src/app/core/models/usuario';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;
  login: Login;
  usuario: Usuario;

  constructor( private  router: Router,
               private formBuilder: FormBuilder,
               private serviceNotificacion: NotificacionesService,
               private serviceLogin: AutenticationService,
               public sessionStorage: SessionStorageService,
               private userIdle: UserIdleService) { }

  ngOnInit() {


    if ((this.sessionStorage.get('usuario') !== null) ) {
      this.router.navigate(['/core']);
    }
    this.loginForm = this.formBuilder.group({
        usuario: ['', Validators.required],
        clave: ['', Validators.required]
      }
    );
  }
  get f() {return  this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    this.serviceLogin.autenticaLogin(this.loginForm.value).subscribe( response => {
      if (response != null) {
        console.log(response);
        this.userIdle.startWatching();
        console.log('si');

        // Start watching when user idle is starting.
        this.userIdle.onTimerStart().subscribe(count => console.log(count));

        // Start watch when time is up.
        this.userIdle.onTimeout().subscribe(() => this.cerrarSesion());
        this.sessionStorage.set('usuario', response);
        this.router.navigate(['/core']);

       } else {
        this.serviceNotificacion.showAlertError('Error' , 'credenciales incorrectas. Por favor vuelva a loguearse...');
      }
    });
  }

  cerrarSesion() {
    this.usuario = this.sessionStorage.get('usuario');
    const opcion = confirm('Su session esta proxima a cerrarse, desea continuar?');
    if (opcion === true) {
      this.restart();
    } else {
      this.stop();
      this.usuario.forEach(element => {
        this.serviceLogin.closeLogin(element.idUsuario).subscribe(response =>{
            console.log(element.idUsuario);
            console.log('sesion cerrada');
        });
      });
      this.sessionStorage.clear();
      this.router.navigate(['/']);
    }

  }

  stop() {
    this.userIdle.stopTimer();
  }

  stopWatching() {
    this.userIdle.stopWatching();
  }

  startWatching() {
    this.userIdle.startWatching();
  }

  restart() {
    this.userIdle.resetTimer();
  }
}
