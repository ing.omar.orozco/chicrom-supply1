import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main/main.component';
import {LoginRoutesModule} from './login.routes';
import { LoginComponent } from './login/login.component';
import {ReactiveFormsModule} from '@angular/forms';
import { AngularWebStorageModule } from 'angular-web-storage';
@NgModule({
  declarations: [MainComponent, LoginComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    LoginRoutesModule,
    AngularWebStorageModule
  ]
})
export class LoginModule { }
