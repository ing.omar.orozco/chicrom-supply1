// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  //
  // API_URL: 'http://35.225.181.3:8081/supplyLogistics'
  API_URL: window.location.protocol + '//' + '127.0.0.1:8081'
  // API_URL: window.location.protocol + '//' + '127.0.0.1:8080/supplyLogistics'
  // API_URL: 'http://181.206.60.107:8080/supplyLogistics'
  // API_URL: 'http://35.236.234.139/supplyLogistics'
  // API_URL: 'http://35.196.128.11/supplyLogistics'

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 * http://127.0.0.1:8080/supplyLogistics/
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
